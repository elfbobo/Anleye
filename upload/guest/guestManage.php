<?php
 /**
  * 个人管理房源
  *
  * @copyright Copyright (c) 2007 - 2010 Yanwee.com (www.anleye.com)
  * @author 阿一 ayi@yanwee.com
  * @package package
  * @version $Id$
  */
require('path.inc.php');
$houseSell = new HouseSell($query);
$houseRent = new HouseRent($query);
if($page->action=='search'){
	if(empty($_GET['mobile'])){
		$page->urlto('guestManage.php','请输入手机号码');
		}
	//获取出售列表
	$dataList = $houseSell->getList('','*','3',' and owner_phone = '.$_GET['mobile'],'');
	$page->tpl->assign('dataList',$dataList);
	//获取出租列表
	$dataList1 = $houseRent->getList('','*','3',' and owner_phone = '.$_GET['mobile'],'');
	$page->tpl->assign('dataList1',$dataList1);
	
	}
$page->name = 'guestManage';
$page->title = $page->titlec.' 个人出租出售房源管理';
$page->show();
?>