<?php
 /**
  * 添加/编辑房源页面
  *
  * @copyright Copyright (c) 2007 - 2010 Yanwee.com (www.anleye.com)
  * @author 阿一 ayi@yanwee.com
  * @package package
  * @version $Id$
  */
 
require('path.inc.php');

if($page->guest==2){
	$page->urlto('../index.php','网站未开通游客发布功能');
	}
//#######此处应该判断用户是否登录#############

$houseSell = new HouseSell($query);
if($page->action == 'save'){
   //通过手机号判断发布房源数量
   $phone = $houseSell->getCount(3,' and owner_phone='.$_POST['owner_phone']);
   
   if(empty($_POST['id'])){  //如果为编辑房源则跳过条数判断
	  if($phone>=3){
	   $page->urlto('houseSale.php','您的手机号码发布次数超过3条');
	   }
	 }
   //保存
	try{
		if($_POST['id']){
			$_POST['is_checked'] = 0;
			$house_id = $houseSell->save($_POST);
			$houseImg = $houseSell->getImgNum($house_id);
					if($houseImg>=3){
						//户型图超过3条为多图房源
						$houseSell->update($house_id,is_more_pic,1);
					}
					if($houseImg<3){
						//户型图少于3条取消多图房源
						$houseSell->update($house_id,is_more_pic,0);
					}
		    $page->urlto('../sale/d-'.$house_id.'.html','编辑成功,重新进入审核');
			}else{
				$house_id = $houseSell->save($_POST);
				$houseImg = $houseSell->getImgNum($house_id);
					if($houseImg>=3){
						//户型图超过3条为多图房源
						$houseSell->update($house_id,is_more_pic,1);
					}
	           	$page->urlto('../sale/d-'.$house_id.'.html','发布成功等待管理员审核');
				}
	}catch ( Exception $e){
		$page->back('保存信息失败');
		//$page->back($e->getMessage());
	}
	exit;
}else{
	//包括增加表单页面，编辑表单，没有action 也是默认这个页面
	$page->name = 'houseSale';
	$page->addJs('FormValid.js');
	$page->addJs('FV_onBlur.js');
	//增加小区的thickBox
	$page->addcss("thickbox.css");
	$page->addjs("thickbox.js");
	//autocomplete
	$page->addjs($cfg['path']['js']."Autocompleter/lib/jquery.bgiframe.min.js");
	$page->addjs($cfg['path']['js']."Autocompleter/lib/ajaxQueue.js");
	$page->addcss($cfg['path']['js']."Autocompleter/jquery.autocomplete.css");
	$page->addjs($cfg['path']['js']."Autocompleter/jquery.autocomplete.js");
	
	//房源类型
	$house_type_option = Dd::getArray('house_type');
	$page->tpl->assign('house_type_option', $house_type_option);
	//装修情况
	$house_fitment_option = Dd::getArray('house_fitment');
	$page->tpl->assign('house_fitment_option', $house_fitment_option);
	//房屋产权
	$belong_option = Dd::getArray('belong');
	$page->tpl->assign('belong_option', $belong_option);
	//房源特色
	$dd = new Dd($query);
	$house_feature_option = $dd->getArrayGrouped('house_feature');
	$page->tpl->assign('house_feature_option', $house_feature_option);
	$house_feature_group = array(1=>"小区室内",2=>'地段周边',3=>'其它特色');
	$page->tpl->assign('house_feature_group', $house_feature_group);
	//区域，增加小区使用
	$cityarea_option = Dd::getArray('cityarea');
	$page->tpl->assign('cityarea_option', $cityarea_option);
	//小区物业类型
	$borough_type_option = Dd::getArray('borough_type');
	$page->tpl->assign('borough_type_option', $borough_type_option);
	$picture_num = 0;
	//房龄
	for($i = 1980; $i <= date('Y');$i++){
		$house_age_option[] = $i;
	}
	$page->tpl->assign('house_age_option', $house_age_option);
	//配套
/*	$house_installation_option = Dd::getArray('house_installation');
	$page->tpl->assign('house_installation_option', $house_installation_option);*/
	//朝向
	$house_toward_option = Dd::getArray('house_toward');
	$page->tpl->assign('house_toward_option', $house_toward_option);
	
	$picture_num = 0;
	$dataInfo['house_feature'] = array();
		//编辑取数据
	if($_GET['id']){
		$id = intval($_GET['id']);
		$owner_notes = $houseSell->getInfo($id,'owner_notes');
		
		if($_POST['deletePwd'] != $owner_notes){
		$page->urlto('../sale/d-'.$id.'.html','密码错误');
		}
		
		$dataInfo = $houseSell->getInfo($id,'*',1);
		//print_rr($dataInfo);
		$dataInfo['house_feature'] = explode(',',$dataInfo['house_feature']);
		array_remove_empty($dataInfo['house_feature'],true);
		//$dataInfo['house_installation'] = explode(',',$dataInfo['house_installation']);
		$dataInfo['house_pic'] = $houseSell->getImgList($id);
		$picture_num = count($dataInfo['house_pic']);
	}
	$page->tpl->assign('dataInfo', $dataInfo);
	$page->tpl->assign('to_url', $_SERVER['HTTP_REFERER']);
	$page->tpl->assign('picture_num', $picture_num);
}
$page->show();
?>