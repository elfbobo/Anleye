<?php
/**
 * 上传文件
 * to : uploadBoroughThumb|borough|picture
 * to : 函数名|目录分类|图片类型
 * 
 */
require('path.inc.php');
$to = $_GET["to"];
$action = $_GET['action'];
if($action==""){
	$action = "form";
}
if($action=="doupload"){
	echo '<html>';
	echo '<head>';
	echo '<title>上传成功</title>';
	echo "<meta http-equiv=\"content-type\" content=\"text/html; charset=gb2312\">";
	echo '</head>';
	
	$store_info = explode('|',$to);
	$js_func = $store_info[0];

	/*  判断特殊字符 */
	if($store_info[1]){
		if(!ereg("^[A-Za-z]+$",$store_info[1])){
			exit;
		}
	}
	if($store_info[2]){
		if(!ereg("^[A-Za-z]+$",$store_info[2])){
			exit;
		}
	}

	$upload_conf = require($cfg['path']['conf'].'upload.cfg.php');
	
	$this_config = (array)$upload_conf[$store_info[1]][$store_info[2]];
	if(empty($this_config)){
		exit;
	}
	$upload = new UploadFile();//实例化上传对象
	//设置可以上传文件的类型
	$upload->setAllowFileType($this_config['allowType']);
	foreach ($_FILES as $a_file){
		if($a_file['error']!=UPLOAD_ERR_NO_FILE) {
			try{
				$fileName = $upload->upload($a_file,$cfg['path']['root'].'upfile/'.$this_config['originalPath'], 1);
				$f_path['url'] = $this_config['originalPath'].$fileName;
				$f_path['name'] = $a_file['name'];
				$attach_file[] = $f_path;
				if(in_array(strtolower(FileSystem::fileExt($f_path['name'])),array('gif','jpeg','jpg','png')) && !$this_config['noResize']){
					//先缩略到指定大小
					$image = new Image($cfg['path']['root'].'upfile/'.$this_config['originalPath'].$fileName);
					$image->resizeImage($this_config['width'],$this_config['height'],$this_config['resizeType']);
					$image->save();
					//加水印
					if($this_config['watermark']){
						$image = new Image($cfg['path']['root'].'upfile/'.$this_config['originalPath'].$fileName);
						$image->waterMark($this_config['watermarkPic'],$this_config['watermarkPos']);
						$image->save();
					}
					//如果需要再生成缩略图
					if($this_config['thumb']){
						$image = new Image($cfg['path']['root'].'upfile/'.$this_config['originalPath'].$fileName);
						$image->resizeImage($this_config['thumbWidth'],$this_config['thumbHeight'],$this_config['thumbResizeType']);
						if($this_config['originalPath']==$this_config['thumbDir']){
							//防止存储目录相同时覆盖原有的图片，不存储缩略图直接设置 thumb 属性为空
							$image->save(2,$cfg['path']['root'].'upfile/'.$this_config['thumbDir'],'_thumb');
							$thumb_path = $this_config['thumbDir'].FileSystem::getBasicName($fileName, false).'_thumb'.FileSystem::fileExt($fileName, true);
						}else{
							$image->save(1,$cfg['path']['root'].'upfile/'.$this_config['thumbDir']);
							$thumb_path = $this_config['thumbDir'].$fileName;
						}
					}
				}
				//回传参数
				echo "<script>
					var parentForm;
					if(window.opener){
						parentForm = window.opener;
					}else{
						parentForm = window.parent;
					}
					parentForm.".$js_func."('".$f_path['url']."','".$f_path['name']."','".$thumb_path."');
				</script>";
				
			}catch(Exception $e){
				$page->back( $e->getMessage());
			}
		}else{
			echo "<script>
					alert('请先浏览文件后点击上传');
					history.back();
			</script>";
			exit;
		}
		echo "<script>
			/*if(window.opener){
				window.close();
			}else{
				history.back();
			}*/
			history.back();
		</script>";
	}
	echo '</body>';
	echo '</html>';
}elseif($action=="form"){
	echo '<html>';
	echo '<head>';
	echo '<title>上传文件</title>';
	echo "<meta http-equiv=\"content-type\" content=\"text/html; charset=gb2312\">";
	echo '</head>';
	echo "<body leftmargin=\"0\" topmargin=\"0\">";
	echo "<table cellpadding=\"2\" cellspacing=\"1\" border=\"0\" height=\"100%\" align=\"left\">";
	echo "<form action='upload.php?action=doupload&to=".$to."' method='post' enctype='multipart/form-data'>";
	echo "<tr ><td  valign='middle'>";
	echo "<input type='file' name='uploadfile'>";
	echo "<input name='submit' type='submit' value='上传'>";
	echo "</td></tr>";
	echo "</form>";
	echo "</table";
	echo "</body>";
	echo '</html>';
}
?>