<?php
require('path.inc.php');



$page->addCss('sale.css');

$houseWanted = new HouseWanted($query);

if($page->action =="save"){
	//回复
	$_POST['wanted_type'] = 2;
	$statistics = new Statistics($query);
	$_POST['house_no'] = $statistics->getNum('houseWanted_no')+1;
	$statistics->add('houseWanted_no');
	try{
		$id = $houseWanted->save($_POST);
		if($_GET['consign']){
			$page->urlto('requireExpert.php?id='.$id);
		}else{
			$page->urlto('requireDone.php?id='.$id);
		}
	}catch (Exception $e){
		$page->back('出错了');
	}
}else{
	$page->name = 'requireForm'; //页面名字,和文件名相同
	
	$page->addJs('FormValid.js');
	//区域字典
	$cityarea_option = Dd::getArray('cityarea');
	$page->tpl->assign('cityarea_option', $cityarea_option);
	
	$house_price_option = array(
		'0-600'=>'600元以下',
		'600-800'=>'600-800元',
		'800-1000'=>'800-1000元',
		'1000-1200'=>'1000-1200元',
		'1200-1500'=>'1200-1500元',
		'1500-2000'=>'1500-2000元',
		'2000-3000'=>'2000-3000元',
		'3000-4000'=>'3000-4000元',
		'4000-5000'=>'4000-5000元',
		'5000-0'=>'5000元以上'
	);
	$page->tpl->assign('house_price_option', $house_price_option);
	
	//详细信息
	$id = $_GET['id']?intval($_GET['id']):0;
	if($id){
		$dataInfo = $houseWanted->getInfo($id,'*');
		if(!$dataInfo){
			$page->urlto('requireList.php','信息不存在或已删除');
		}
		$dataInfo['requirement_short'] = substrs($dataInfo['requirement'],40);
		$page->tpl->assign('dataInfo', $dataInfo);
	}
	$page->show();
}
?>