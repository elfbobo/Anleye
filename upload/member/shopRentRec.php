<?php
require('path.inc.php');


$member_id = $member->getAuthInfo('id');
$houseSell = new HouseSell($query);
$houseRent = new HouseRent($query);

$where = ' and broker_id = '.$member_id;
$where .=" and status = 1";
$where .=" and is_promote = 1";
$houseNum = $houseRent->getCount(0,$where);
$page->tpl->assign('houseNum',$houseNum);//总共多少条
$allowNum = 4;
$houseLeft= $allowNum -$houseNum;
$page->tpl->assign('houseLeft',$houseLeft);

if($page->action == 'promote'){
	$id = intval($_GET['id']);
	if($houseLeft <=0){
		$page->back("橱窗已满");
	}
	$houseRent->promote($id,1);
	$page->urlto('shopRentRec.php');
}elseif ($page->action =='cancel'){
	$id = intval($_GET['id']);
	$houseRent->promote($id,2);
	$page->urlto('shopRentRec.php');
}else{
	$page->name = 'shopRentRec';
	
	$where = ' and broker_id = '.$member_id;
	//这里显示状态为1（正在出售）的房源
	$where .=" and status = 1";
	$q = $_GET['q']=='输入房源编号或小区名称'?"":trim($_GET['q']);
	if($q){
		$borough = new Borough($query);
		$search_bid = $borough->getAll('id',' borough_name like \'%'.$q.'%\'');
		if($search_bid){
			$search_bid = implode(',',$search_bid);
			$where .= " and (borough_name like '%".$q."%' or house_no like '%".$q."%' or borough_id in (".$search_bid."))";
		}else{
			$where .= " and (borough_name like '%".$q."%' or house_no like '%".$q."%')";	
		}
	}
	$page->tpl->assign('q', $q);
	if($_GET['is_promote']){
		$is_promote = intval($_GET['is_promote']);
		$where .= " and is_promote = ".$is_promote;	
	}
	
	require($cfg['path']['lib'] . 'classes/Pages.class.php');
	$pages = new Pages($houseRent->getCount(0,$where),10,'pages_g.tpl');
	$pageLimit = $pages->getLimit();
	$dataList = $houseRent->getList($pageLimit,'*',0,$where,' order by created desc ');
	foreach ($dataList as $key => $value){
		//echo date("Y-m-d" ,$value['created']);
		$dataList[$key]['day_left'] = intval(($value['created'] - $cfg['time'] )/86400+90); 
	}
	
	$page->tpl->assign('to_url', $_SERVER['REQUEST_URI']);
	$page->tpl->assign('dataList', $dataList);
	$page->tpl->assign('pagePanel', $pages->showCtrlPanel_m(5));//分页条
}

$page->show();
?>