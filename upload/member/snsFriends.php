<?php
require('path.inc.php');


$broker_friend = new BrokerFriend($query);
if ($page->action =='add'){
	//添加好友
	$friend_id = intval($_GET['friend']);
	if(!$friend_id){
		$page->back('参数错误');
	}
	$broker_id = $member->getAuthInfo('id');
	if($friend_id == $broker_id){
		$page->back('不能添加自己为好友');
	}
	$memberInfo = $member->getInfo($broker_id,'*',true);
	if( $memberInfo['idcard']=='' || $memberInfo['mobile']==''){
		$page->back('你没有完善你的资料或者没有通过实名认证，无法添加好友！');
	}
	$fieldset = array(
		'friend_id'=>$friend_id,
		'broker_id'=>$broker_id,
	);
	try{
		if($broker_friend->isAdded($fieldset)){
			$page->urlto('snsFriends.php','你已提交申请，请等待好友验证通过');
		}else{
			$broker_friend->save($fieldset);
		}
		$page->urlto('snsFriends.php','添加好友成功，请等待好友验证通过');
	}catch (Exception $e){
		$page->back("删除失败，请重试");
	}
}elseif ($page->action =='delete'){
	//删除
	$ids = intval($_GET['id']);
	try{
		$broker_friend->delete($ids);
		$page->urlto('snsFriends.php','删除成功');
	}catch (Exception $e){
		$page->back("删除失败，请重试");
	}
}else{
	$page->name = 'snsFriends';

	$cityarea_option = Dd::getArray('cityarea');
	$broker_type_option = Dd::getArray('broker_type');
	
	$member_id = $member->getAuthInfo('id');
	$where = 'status =1 and broker_id ='.$member_id;
	require($cfg['path']['lib'] . 'classes/Pages.class.php');
	$pages = new Pages($broker_friend->getCount($where),15,'pages_g.tpl');
	$pageLimit = $pages->getLimit();
	$dataList = $broker_friend->getList($pageLimit,'*',$where,' order by id desc ');
	foreach ($dataList as $key => $item){
		$friend_info= $member->getInfo($item['friend_id'],'*',true);
		$birth_m_t = MyDate::transform('timestamp',$friend_info['birthday']);
		$birth_m = date('m',$birth_m_t)*30+date('d',$birth_m_t);
		$this_m = date('m')*30+date('d');
		$next_m = (date('m')+1)*30+date('d');
		$friend_info['cityarea_name'] = $cityarea_option[$friend_info['cityarea_id']];
		$friend_info['broker_type'] = $broker_type_option[$friend_info['broker_type']];
		$friend_info['birth_remember'] =  $birth_m < $next_m && $birth_m > $this_m;
		$dataList[$key]=$friend_info;
		$dataList[$key]['id'] = $item['id'];
		$dataList[$key]['friend_id'] = $item['friend_id'];
	}
	
	$page->tpl->assign('to_url', $_SERVER['REQUEST_URI']);
	$page->tpl->assign('dataList', $dataList);
	$page->tpl->assign('pagePanel', $pages->showCtrlPanel_m(5));//分页条
}

$page->show();
?>