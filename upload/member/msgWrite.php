<?php
require('path.inc.php');


$member_id = $member->getAuthInfo('id');
$member_username = $member->getAuthInfo('username');

$innernote = new Innernote($query);

if($page->action == 'save'){
	if($_POST['msg_to'] ==""){
		$page->back("没有填写发送人地址");
	}
	try{
		$reciever = explode(',',$_POST['msg_to']);
		if(is_array($reciever)){
			foreach($reciever as $a_r){
				$a_r = trim($a_r);
				if($a_r == ""){
					continue;
				}
				$innernote->send($member_username,$a_r,$_POST['msg_title'],$_POST['msg_content'],$_POST['reply_to']);
			}
		}
		$page->urlto('msgSend.php','发送成功');
	}catch (Exception $e){
		$page->back("发送失败，请重试");
	}
	exit;
}else{
	$page->name = 'msgWrite';
	$reply_id = intval($_GET['reply_id']);
	$page->addJs('FormValid.js');
	$page->addJs('FV_onBlur.js');
	
	if($reply_id){
		$dataInfo = $innernote->getInfo($reply_id);
	}
	
	if($dataInfo){
		$dataInfo['reply_to'] = $dataInfo['id'];
		$dataInfo['msg_to'] = $dataInfo['msg_from'];
		$dataInfo['msg_title'] = "Re.".$dataInfo['msg_title'];
	}else{
		$dataInfo['msg_to'] = $_GET['msg_to'];
	}
	$page->tpl->assign('dataInfo', $dataInfo);
	
}
$page->show();
?>