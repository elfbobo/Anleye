<?php
require('path.inc.php');
$housesell = new HouseSell($query);
$return_to = $_SERVER['HTTP_REFERER'];
if($page->action == 'save'){
    $_POST['member_id'] = $member->getAuthInfo('id');  //获取会员ID
	//计算余额是否够用
	$price = intval($_POST['days'])*$page->sellPrice;
	$memberInfo = $member->getInfo($_POST['member_id'],'*',true);
	$_POST['to_time'] = $_POST['days']*86400+$cfg['time'];
	if(empty($_POST['days'])){
		$page->urlto($return_to,'请输入置顶天数');
		}
	if($memberInfo['money'] >= $price){
	try{
		//扣除费用
		$member->deletemoney($_POST['member_id'],$price);
		//更改出售房源状态
		$housesell->update($_POST['house_id'],'is_top',1);
		$housesell->housetopsave($_POST);
		$page->urlto($return_to,'置顶成功');
	}catch (Exception $e){
		$page->back($e->getMessage());
	}
	
	exit;
		
		}else{
		$page->urlto($return_to,'余额不足，请充值！');
		}
	

}else{
	$page->name = 'saleTop';
	$money =  $member->getAuthInfo('money');
	$house_id = intval($_GET['house_id']);
	$page->tpl->assign('house_id', $house_id);
	$page->tpl->assign('money', $money);
}

$page->show();
?>