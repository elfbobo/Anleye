<?php
require('path.inc.php');


$broker_friend = new BrokerFriend($query);
if($page->action == 'status'){
	$ids=intval($_GET['id']);
	$status = intval($_GET['status']);
	
	try{
		$broker_friend->changeStatus($ids,$status);
		if($status == 1){
			$broker_friend->confirm($ids);
			$page->urlto('snsInviteIn.php','成功添加好友');
		}else{
			$page->urlto('snsInviteIn.php','已拒绝添加为好友');
		}
		
	}catch (Exception $e){
		$page->back("操作失败，请重试");
	}
	exit;
}else{
	$page->name = 'snsInviteIn';

	$cityarea_option = Dd::getArray('cityarea');
	$broker_type_option = Dd::getArray('broker_type');
	
	$member_id = $member->getAuthInfo('id');
	$where = 'status =0 and friend_id ='.$member_id;
	require($cfg['path']['lib'] . 'classes/Pages.class.php');
	$pages = new Pages($broker_friend->getCount($where),15,'pages_g.tpl');
	$pageLimit = $pages->getLimit();
	$dataList = $broker_friend->getList($pageLimit,'*',$where,' order by id desc ');
	foreach ($dataList as $key => $item){
		$friend_info= $member->getInfo($item['broker_id'],'*',true);
		$birth_m_t = MyDate::transform('timestamp',$friend_info['birthday']);
		$birth_m = date('m',$birth_m_t)*30+date('d',$birth_m_t);
		$this_m = date('m')*30+date('d');
		$next_m = (date('m')+1)*30+date('d');
		$friend_info['cityarea_name'] = $cityarea_option[$friend_info['cityarea_id']];
		$friend_info['broker_type'] = $broker_type_option[$friend_info['broker_type']];
		$friend_info['birth_remember'] =  $birth_m < $next_m && $birth_m > $this_m;
		$dataList[$key]=$friend_info;
		$dataList[$key]['id'] = $item['id'];
		$dataList[$key]['broker_id'] = $item['broker_id'];
		$dataList[$key]['add_time'] = $item['add_time'];
	}
	
	$page->tpl->assign('to_url', $_SERVER['REQUEST_URI']);
	$page->tpl->assign('dataList', $dataList);
	$page->tpl->assign('pagePanel', $pages->showCtrlPanel_m(5));//分页条
}

$page->show();
?>