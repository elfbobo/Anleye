<?php
 /**
  * 会员升级
  * @copyright Copyright (c) 2007 - 2010 Yanwee.net (www.anleye.com)
  * @author 阿一 yandy@yanwee.com
  */ 

require('path.inc.php');
$member = new Member($query);
$return_to = $_SERVER['HTTP_REFERER'];
if($page->action == 'save'){
    $_POST['member_id'] = $member->getAuthInfo('id');  //获取会员ID

	if($_GET['type']==1){
		$price = $page->vip1Price;
		$_POST['to_time'] = 2592000+$cfg['time'];    //30天
		}
	if($_GET['type']==2){   
			$price = $page->vip2Price;
			$_POST['to_time'] = 7776000+$cfg['time'];   //90天
			}
			
	//计算余额是否够用
	$memberInfo = $member->getInfo($_POST['member_id'],'*',true);
	if($memberInfo['money'] >= $price){
	try{
		//扣除费用
		$member->deletemoney($_POST['member_id'],$price);
		
		//更改会员标识 并写入时间表
		$member->vipSave($_POST,$_GET['type']);
		
		$page->urlto($return_to,'升级成功');
	}catch (Exception $e){
		$page->back($e->getMessage());
	}
	
	exit;
		
		}else{
		$page->urlto($return_to,'余额不足，请充值！');
		}
	

}else{
	$page->name = 'vipShop';
	$money =  $member->getAuthInfo('money');
	$page->tpl->assign('money', $money);
	$vip =  $member->getAuthInfo('vip');
	$page->tpl->assign('vip', $vip);
}
$page->show();
?>