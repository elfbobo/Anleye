<?php
 /**
  * 会员页面
  */
 
require('path.inc.php');

$page->name = 'index';

//成交使用的thickBox加载
$page->addcss("thickbox.css");
$page->addjs("thickbox.js");

$nowHour = date('H');
if($nowHour > 17 || $nowHour < 6){
	$nowTime = "晚上";
}elseif($nowHour > 12){
	$nowTime = "下午";
}else{
	$nowTime = "早上";
}
$page->tpl->assign('nowTime',$nowTime);

$member_id = $member->getAuthInfo('id');
$memberInfo = $member->getInfo($member_id,'*',true);

$integral_array = require_once($cfg['path']['conf'].'integral.cfg.php');
$memberInfo['brokerRank'] = getNumByScore($memberInfo['scores'],$integral_array,'pic');

$memberInfo['active_str'] = explode('|',$memberInfo['active_str']);

$memberInfo['specialty'] = Dd::getCaption('specialty',$memberInfo['specialty']);

$page->tpl->assign('memberInfo',$memberInfo);

//会员到期时间
$vip_totime = $member->getVipTime($member_id,'to_time');
$page->tpl->assign('vip_totime', $vip_totime);





//count
$member_username = $member->getAuthInfo('username');

$where = " msg_to = '".$member_username."' and is_new = 1 and to_del = 0 ";
$innernote = new Innernote($query);
$messageCount = $innernote->getCount($where);
$page->tpl->assign('messageCount', $messageCount);


$broker_friend = new BrokerFriend($query);
$where = 'status =0 and friend_id ='.$member_id;
$firendInviteCount = $broker_friend->getCount($where);
$page->tpl->assign('firendInviteCount', $firendInviteCount);

$houseSell = new HouseSell($query);
$houseRent = new HouseRent($query);
$where = ' and broker_id = '.$member_id;
$where .=" and status = 1 and is_top = 0";
$saleCount = $houseSell->getCount(0,$where);
$page->tpl->assign('saleCount', $saleCount);

$where = ' and broker_id = '.$member_id;
$where .=" and is_top = 1 and status = 1";
$saleTopCount = $houseSell->getCount(0,$where);
$page->tpl->assign('saleTopCount', $saleTopCount);

$where = ' and broker_id = '.$member_id;
$where .=" and is_top = 1 and status = 1";
$rentTopCount = $houseRent->getCount(0,$where);
$page->tpl->assign('rentTopCount', $rentTopCount);

$where = ' and broker_id = '.$member_id;
$where .=" and status = 2";
$saleRecycleCount = $houseSell->getCount(0,$where);
$page->tpl->assign('saleRecycleCount', $saleRecycleCount);


$where = ' and broker_id = '.$member_id;
$where .=" and status = 1 and is_top = 0";
$rentCount = $houseRent->getCount(0,$where);
$page->tpl->assign('rentCount', $rentCount);

$where = ' and broker_id = '.$member_id;
$where .=" and status = 2";
$rentRecycleCount = $houseRent->getCount(0,$where);
$page->tpl->assign('rentRecycleCount', $rentRecycleCount);

$page->show();
?>