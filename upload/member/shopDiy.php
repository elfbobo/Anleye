<?php
require('path.inc.php');

if($page->action == 'save'){
	try{
		if($_POST['banner']){
			$member->updateBroker($member_id,'banner',$_POST['banner']);
			}
		if($_POST['background']){
			$member->updateBroker($member_id,'background',$_POST['background']);
			}
		$page->urlto('shopDiy.php','上传成功');
	}catch (Exception $e){
		$page->back("上传失败");
	}
	exit;
}else{
	$page->name = 'shopDiy';
	$page->addJs('FormValid.js');

	$memberInfo = $member->getInfo($member_id,'*',true);
	$page->tpl->assign('memberInfo',$memberInfo);

}

$page->show();
?>