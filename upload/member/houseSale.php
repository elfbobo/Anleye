<?php
 /**
  * 添加/编辑房源页面
  *
  * @copyright Copyright (c) 2007 - 2008 Yanwee.net (www.anleye.com)
  * @author 阿一 yandy@yanwee.com
  * @package package
  * @version $Id$
  */
 
require('path.inc.php');

$member_id = $member->getAuthInfo('id');
$houseSell = new HouseSell($query);

//判断用户是不是通过认证等
$memberInfo = $member->getInfo($member_id,'*',true);
$page->tpl->assign('memberInfo',$memberInfo);
if($memberInfo['mobile']==""){
	$page->urlto("brokerProfile.php","请先完善你的资料，以便于购房者能够联系你");
}
if($page->memberOpen==2){
	if($memberInfo['status']=="1"){
	  $page->urlto("index.php","您的账户尚未开通！");
      }
	}
if($page->action == 'save'){
	//保存
	$to_url = $_POST['to_url'];
	$_POST['broker_id'] = $member_id;
	$_POST['creater'] = $member->getAuthInfo('username');
	
	//将房源归属到经济公司
	$_POST['company_id'] = $member->getAuthInfo('company_id');
	try{
		$house_id = $houseSell->save($_POST);
		if(strpos('manageSale.php',$to_url)==0){
			$to_url = "manageSale.php";
		}
		
			$integral = new Integral($query);
			//通过，增加积分
				if($_POST['broker_id']){
					//发布一条增加5分
					$integral->add($_POST['broker_id'],7);
					$houseImg = $houseSell->getImgNum($house_id);
					if($houseImg>=3){
						//户型图超过3条为多图房源
						$houseSell->update($house_id,is_more_pic,1);
						$integral->add($_POST['broker_id'],11);
					}
					if($houseImg<3){
						//户型图少于3条取消多图房源
						$houseSell->update($house_id,is_more_pic,0);
					}
					if($_POST['drawing_id'] || $_POST['house_drawing'] ){
						$integral->add($_POST['broker_id'],12);
					}
				}
			$page->urlto($to_url,'信息保存成功');

			
	}catch ( Exception $e){
	     $page->back('保存信息失败');
		//$page->back($e->getMessage());
	}
	exit;
}else{
	//包括增加表单页面，编辑表单，没有action 也是默认这个页面
	$page->name = 'houseSale';
	$page->addJs('FormValid.js');
	$page->addJs('FV_onBlur.js');
	//增加小区的thickBox
	$page->addcss("thickbox.css");
	$page->addjs("thickbox.js");
	//autocomplete
	$page->addjs($cfg['path']['js']."Autocompleter/lib/jquery.bgiframe.min.js");
	$page->addjs($cfg['path']['js']."Autocompleter/lib/ajaxQueue.js");
	$page->addcss($cfg['path']['js']."Autocompleter/jquery.autocomplete.css");
	$page->addjs($cfg['path']['js']."Autocompleter/jquery.autocomplete.js");
	
	//新增加的，判断是否超过了 数量
	if(!$_GET['id']){
		//取用户的分数
		$scores = $member->getAuthInfo('scores');
		//取积分配置情况
		$scores_conf = require_once($cfg['path']['conf'].'integral.cfg.php');
		$sellnum = getNumByScore($scores,$scores_conf,'sell_num');
		//取用户已发房源
		$where = ' and broker_id = '.$member_id;
		$where .=" and status =1 ";
		$sellCount = $houseSell->getCount(1,$where);
		
		//比较房源数量
		if($member->getAuthInfo('vip')==1){
			if($page->vip1SaleNum <= $sellCount ){
			$page->back('你正在出售的房源已经超过了'.$page->vip1SaleNum.'条，请把无效的房源删除后再发布新房源！');
		    }
		 }elseif($member->getAuthInfo('vip')==2){
			if($page->vip2SaleNum <= $sellCount ){
			$page->back('你正在出售的房源已经超过了'.$page->vip2SaleNum.'条，请把无效的房源删除后再发布新房源！');
		    }
		}else{
			if($sellnum+$member->getAuthInfo('addsale') <= $sellCount ){
			$page->back('你正在出售的房源已经超过了'.$sellnum.'条，请把无效的房源下架后再发布新房源！');
	     	}
       }
		
	}
	//房源类型
	$house_type_option = Dd::getArray('house_type');
	$page->tpl->assign('house_type_option', $house_type_option);
	//装修情况
	$house_fitment_option = Dd::getArray('house_fitment');
	$page->tpl->assign('house_fitment_option', $house_fitment_option);
	//房屋产权
	$belong_option = Dd::getArray('belong');
	$page->tpl->assign('belong_option', $belong_option);
	//房源特色
	$dd = new Dd($query);
	$house_feature_option = $dd->getArrayGrouped('house_feature');
	$page->tpl->assign('house_feature_option', $house_feature_option);
	$house_feature_group = array(1=>"小区室内",2=>'地段周边',3=>'其它特色');
	$page->tpl->assign('house_feature_group', $house_feature_group);
	//区域，增加小区使用
	$cityarea_option = Dd::getArray('cityarea');
	$page->tpl->assign('cityarea_option', $cityarea_option);
	//小区物业类型
	$borough_type_option = Dd::getArray('borough_type');
	$page->tpl->assign('borough_type_option', $borough_type_option);
	$picture_num = 0;
	//房龄
	for($i = 1980; $i <= date('Y');$i++){
		$house_age_option[] = $i;
	}
	$page->tpl->assign('house_age_option', $house_age_option);
	//配套
/*	$house_installation_option = Dd::getArray('house_installation');
	$page->tpl->assign('house_installation_option', $house_installation_option);*/
	//朝向
	$house_toward_option = Dd::getArray('house_toward');
	$page->tpl->assign('house_toward_option', $house_toward_option);
	
	$picture_num = 0;
	$dataInfo['house_feature'] = array();
	//编辑取数据
	if($_GET['id']){
		$id = intval($_GET['id']);
		$dataInfo = $houseSell->getInfo($id,'*',1);
		//print_rr($dataInfo);
		$dataInfo['house_feature'] = explode(',',$dataInfo['house_feature']);
		array_remove_empty($dataInfo['house_feature'],true);
		//$dataInfo['house_installation'] = explode(',',$dataInfo['house_installation']);
		$dataInfo['house_pic'] = $houseSell->getImgList($id);
		$picture_num = count($dataInfo['house_pic']);
	}
	$page->tpl->assign('dataInfo', $dataInfo);
	$page->tpl->assign('to_url', $_SERVER['HTTP_REFERER']);
	$page->tpl->assign('picture_num', $picture_num);
}
$page->show();
?>