<!--{include file="admin/header.tpl"}-->
<!--列表-->
<table cellpadding="0" cellspacing="1" class="table_info">
  <caption>小区管理</caption>
  <tr>
    <td><a href="index.php"><font class="red">小区列表</font></a> | <a href="index.php?action=add">添加小区</a> | <a href="index.php?action=search&nofull=1" title="没有填写地址、缩略图">信息不全的小区</a></td>
  </tr>
</table>

<form name="search" method="get" action="index.php?action=search">
<input type="hidden" name="action" value="search">
<table cellpadding="0" cellspacing="1" class="table_form">
	<caption>信息查询</caption>
	<tr>
		<td class="align_c">
			<select name='cityarea'>
				<option value="">所在区域</option>
				<!--{foreach from=$areaLists key=key item=item}-->
				<!--{if $cityarea==$key}-->
				<option value="<!--{$key}-->" selected><!--{$item}--></option>
				<!--{else}-->
				<option value="<!--{$key}-->"><!--{$item}--></option>
				<!--{/if}-->
				<!--{/foreach}-->
			</select>
			<input type="text" name="q" onblur="if(this.value ==''||this.value == '请输入小区名称,小区地址'){this.value = '请输入小区名称,小区地址';this.style.color = '#999999';}" onfocus="if(this.value == '请输入小区名称,小区地址'){this.value = '';this.style.color = '#333333';}" value="<!--{if $q}--><!--{$q}--><!--{else}-->请输入小区名称,小区地址<!--{/if}-->" size="35" />&nbsp;
			<input type="submit" name="dosubmit" value=" 查询 " />
		</td>
	</tr>
</table>
</form>

<form name="myform" method="post" action="">
<table cellpadding="2" cellspacing="1" class="table_list">
	<caption>小区信息管理</caption>
	<tr>
		<th width="30">选中</th>
		<th>小区名称</th>
		<th>小区别名</th>
		<th width="60">所在区域</th>
		<th>所在板块</th>
		<th>小区地址</th>
		<th>添加人</th>
		<th width="100">管理操作</th>
	</tr>
<tbody>
<!--{foreach item=item from=$boroughList }-->
  <tr>
  	<td class="align_c" ><input type="checkbox" name="ids[]" value="<!--{$item.id}-->" title="<!--{$item.borough_name}-->"></td>
	<td><a href="../../community/general.php?id=<!--{$item.id}-->" target="_blank"><!--{$item.borough_name}--> </a> &nbsp;</td>
	<td class="align_c"><!--{$item.borough_alias}--> </td>
	<td class="align_c"><!--{$item.cityarea_id}--> </td>
	<td class="align_c"><!--{$item.borough_section}--></td>
	<td class="align_c"><!--{$item.borough_address}--></td>
	<td class="align_c"><!--{$item.creater}--></td>
	<td class="align_c">
		<a href="?action=edit&id=<!--{$item.id}-->">修改</a>
	</td>
  </tr>
<!--{/foreach}-->
</tbody>
</table>
<div class="button_box">
	<span style="width:60px"><a href="#" onclick="javascript:$('input[type=checkbox]').attr('checked', true);return false;">全选</a>/<a href="#" onclick="javascript:$('input[type=checkbox]').attr('checked', false)">取消</a></span>
	<input type="button" name="delete" value=" 删除 " onclick="if(confirm('删除小区需要把其中的房源移动到其他的小区，你确认删除选中的条目么？')){myform.action='index.php?action=delete';myform.submit();}">
</div>
<!--{$pagePanel}-->

</form>
<form name="combineForm" id="combineForm" action="index.php?action=combine" method="POST" >
<input type="hidden" name="fromaction" value="<!--{$smarty.request.action}-->">
<input type="hidden" name="q" value="<!--{$smarty.request.q}-->">
<input type="hidden" name="cityarea" value="<!--{$smarty.request.cityarea}-->">
<input type="hidden" name="pageno" value="<!--{$smarty.get.pageno}-->">
<table cellpadding="2" cellspacing="1" class="table_form">
	<caption>合并小区</caption>
	<tr>
		<th width="30%"><input type="button" name="selectIdFrom" onclick="selectFromId();" value="点击按钮后选择需要合并的小区"></th>
		<td><span id="fromIdSelected"></span></td>
	</tr>
	<tr>
		<th><input type="button" name="selectIdTo"  onclick="selectToId();" value="点击按钮后选择合并到哪个小区"></th>
		<td><span id="toIdSelected"></span></td>
	</tr>
	<tr>
		<th></th>
		<td><input type="button" name="combine"  value="合并" onclick="combineForm.submit();"></td>
	</tr>
</table>
</form>

<script language="javascript">

function selectFrom(event) {
	var newStr;
	var thisId = document.getElementById('From_id_'+this.value);
	var selectedId = document.getElementById('fromIdSelected');
	if(thisId){
		selectedId.removeChild(thisId);
	}else{
		newStr =  $('#fromIdSelected').html()+'<div id="From_id_'+this.value+'" class="upload_shower"><input type="hidden" name="fromId[]" value="'+this.value+'">'+this.title+'<div>';
		$('#fromIdSelected').html(newStr);
	}
}
function selectTo(event) {
	var newStr;
	var thisId = document.getElementById('To_id_'+this.value);
	var selectedId = document.getElementById('toIdSelected');
	if(thisId){
		selectedId.removeChild(thisId);
	}else{
		selectedId.innerHTML =  '<div id="To_id_'+this.value+'" class="upload_shower"><input type="hidden" name="toId[]" value="'+this.value+'">'+this.title+'<div>';
	}
}
function selectFromId(){
	$('input[type=checkbox]').attr('checked', false);
	$("input[type='checkbox']").unbind( "click" );
	$("input[type='checkbox']").bind("click", selectFrom);
}

function selectToId(){
	$('input[type=checkbox]').attr('checked', false);
	$("input[type='checkbox']").unbind( "click" );
	$("input[type='checkbox']").bind("click", selectTo);
}
</script>
<!--{include file="admin/footer.tpl"}-->
