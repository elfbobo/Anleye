<!--{include file="admin/header.tpl"}-->
<script language="javascript">
$(document).ready(function() {
	$('.editable').editable('evaluate.php?action=save',{
		placeholder:'点击添加评估价',
		submit : '确定',
		cancel : '取消',
		tooltip : '点击编辑信息',
		width  : '98%',
		cssclass : "input",
		height : 18
	});
});

</script>
<!--列表-->
<table cellpadding="0" cellspacing="1" class="table_info">
  <caption>按时间划分</caption>
  <tr>
    <td>
	     <a href="evaluate.php?cityarea=<!--{$smarty.get.cityarea}-->"><!--{if $smarty.get.time==0 }--><font class="red">全部</font><!--{else}-->全部<!--{/if}--></a> | 
	     <a href="evaluate.php?time=1&cityarea=<!--{$smarty.get.cityarea}-->"><!--{if  $smarty.get.time==1}--><font class="red">未评估</font><!--{else}-->未评估<!--{/if}--></a> | 
		 <a href="evaluate.php?time=2&cityarea=<!--{$smarty.get.cityarea}-->"><!--{if $smarty.get.time==2}--><font class="red">一个月以上未更新</font><!--{else}-->一个月以上未更新<!--{/if}--></a> | 
	     <a href="evaluate.php?time=3&cityarea=<!--{$smarty.get.cityarea}-->"><!--{if $smarty.get.time==3}--><font class="red">三个月以上未更新</font><!--{else}-->三个月以上未更新<!--{/if}--></a> 
    </td>
  </tr>
</table>

<table cellpadding="0" cellspacing="1" class="table_info">
  <caption>按区域分类</caption>
  <tr>
    <td>
    	<a href="evaluate.php?time=<!--{$smarty.get.time}-->"><!--{if $smarty.get.cityarea == 0}--><font class="red">全部</font><!--{else}-->全部<!--{/if}--></a> | 
    	<!--{foreach from=$areaLists item=item key=key}-->
    	<!--{if $key == $smarty.get.cityarea}-->
    	<a href="evaluate.php?cityarea=<!--{$key}-->&time=<!--{$smarty.get.time}-->"><font class="red"><!--{$item}--></font></a> | 
    	<!--{else}-->
    	<a href="evaluate.php?cityarea=<!--{$key}-->&time=<!--{$smarty.get.time}-->"><!--{$item}--></a> | 
    	<!--{/if}-->
    	<!--{/foreach}-->
    </td>
  </tr>
</table>

<form name="search" method="get" action="evaluate.php">
<input type="hidden" name="action" value="search">
<table cellpadding="0" cellspacing="1" class="table_form">
	<caption>信息查询</caption>
	<tr>
		<td class="align_c">
			<select name='cityarea'>
				<option value="">所在区域</option>
				<!--{foreach from=$areaLists key=key item=item}-->
				<!--{if $smarty.get.cityarea==$key}-->
				<option value="<!--{$key}-->" selected><!--{$item}--></option>
				<!--{else}-->
				<option value="<!--{$key}-->"><!--{$item}--></option>
				<!--{/if}-->
				<!--{/foreach}-->
			</select>
			<input type="text" name="q" onblur="if(this.value ==''||this.value == '请输入小区名称,小区地址'){this.value = '请输入小区名称,小区地址';this.style.color = '#999999';}" onfocus="if(this.value == '请输入小区名称,小区地址'){this.value = '';this.style.color = '#333333';}" value="<!--{if $q}--><!--{$q}--><!--{else}-->请输入小区名称,小区地址<!--{/if}-->" size="35" />&nbsp;
			<input type="submit" name="dosubmit" value=" 查询 " />
		</td>
	</tr>
</table>
</form>

<form name="myform" method="post" action="">
<table cellpadding="2" cellspacing="1" class="table_list">
	<caption>小区评估标准价管理</caption>
	<tr>
		<th>小区名称</th>
		<th width="60">所在区域</th>
		<th>小区地址</th>
		<th>评估价(元/平方)</th>
		<th>最后更新时间</th>
	</tr>
<tbody>
<!--{foreach item=item from=$boroughList }-->
  <tr>
	<td><a href="../../community/general.php?id=<!--{$item.id}-->" target="_blank"><!--{$item.borough_name}--> </a> &nbsp;</td>
	<td class="align_c"><!--{$item.cityarea_id}--> </td>
	<td class="align_c"><!--{$item.borough_address}--></td>
	<td class="align_c"><span class="editable" id="borough_evaluate|<!--{$item.id}-->"><!--{$item.borough_evaluate}--></span></td>
	<td class="align_c"><!--{$item.last_update}--></td>
  </tr>
<!--{/foreach}-->
</tbody>
</table>
<!--{$pagePanel}-->

</form>
<!--{include file="admin/footer.tpl"}-->
