<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=gb2312" />
<title><!--{$cfg.page.title}--></title>
<!--{$cssFiles}-->
<!--{$jsFiles}-->
<script language="javascript">
function fnOnMenuClick(menu)
{
    $("#menus > ul > li").removeClass("current");
    $(menu).parent().addClass("current");
}

</script>
</head>
<body>
  <div id="sidebar">
    <div id="menus">
      <ul>
	<!--{foreach name=submenu from=$menus key=key item=item}-->
        <li <!--{if $smarty.foreach.submenu.index == 0 }--> class="current" <!--{/if}--> >
          <a href="<!--{$item.url}-->" onclick="fnOnMenuClick(this);" target="mainFrame"><!--{$item.caption}--></a>
	    </li>
	<!--{/foreach}-->
      </ul>
    </div>
    <div id="copyright">
	  <p>Powered by <a href="htp://anleye.yanwee.com" target="_blank">anleye.com</a></p>
	  <p>&copy; 2012, <a href="http://www.yanwee.com/" target="_blank">Yanwee Inc.</a></p>
	</div>
  </div>
</body>
</html>