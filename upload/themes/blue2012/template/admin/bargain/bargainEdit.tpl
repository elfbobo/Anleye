<!--{include file="admin/header.tpl"}-->
<form id="editForm" name="editForm" method="post" action="index.php?action=save" onsubmit="return validator(this)">
	<input name="id" type="hidden" value="<!--{$dataInfo.id}-->">
    <table class="table_list" cellpadding="2" cellspacing="1">
    	<caption>添加规则</caption>
    	<tr>
        	<th width="60">区域</th>
        	<th>成交套数</th>
        	<th>成交面积</th>
        	<th>成交金额</th>
        </tr>
        <!--{foreach from=$bargain_ciytarea key=key item=item}-->
        <tr>
        	<td><!--{$item}-->：</th>
        	<td><input name="bargain_num[<!--{$key}-->]" type="text" onchange="bargain_sum('bargain_num')" class="bargain_num" size="25" valid="required" errmsg="成交套数必填" value="<!--{$cityareaList.$key.bargain_num}-->"  /></td>
        	<td><input name="bargain_area[<!--{$key}-->]" type="text" onchange="bargain_sum('bargain_area')" class="bargain_area" size="25" valid="required" errmsg="成交面积必填" value="<!--{$cityareaList.$key.bargain_area}-->"  /></td>
        	<td><input name="bargain_price[<!--{$key}-->]" type="text" onchange="bargain_sum('bargain_price')" class="bargain_price" size="25" valid="required" errmsg="成交金额必填" value="<!--{$cityareaList.$key.bargain_price}-->"  /></td>
        </tr>
        <!--{/foreach}-->
        <tr>
        	<td>全市：</th>
        	<td><input name="bargain_num_total" type="text" id="bargain_num_total" size="25" value="<!--{$dataInfo.bargain_num}-->"  /></td>
        	<td><input name="bargain_area_total" type="text" id="bargain_area_total" size="25" value="<!--{$dataInfo.bargain_area}-->"  /></td>
        	<td><input name="bargain_price_total" type="text" id="bargain_price_total" size="25" value="<!--{$dataInfo.bargain_price}-->"  /></td>
        </tr>
        <tr>
        	<td>时间：</td>
        	<td><input type="text" name="add_time" value="<!--{$lastDay}-->" onclick="WdatePicker({skin:'whyGreen'})"></td>
        	<td colspan="2"><input name="Submit" type="button" value=" 提交 " onclick="javascript:if (validator(document.editForm)) document.editForm.submit();" ></td>
        </tr>
    </table>
</form>

<script language="javascript">
function bargain_sum(s_type){
	var selectValue = 0;
	var theVal;
	var intTheVal;
	inputs_ele = $("."+s_type);
	jQuery.each(inputs_ele,function(i,val){
		theVal = jQuery(val).val();
		intTheVal = parseInt(theVal);
		if(intTheVal){
			selectValue = selectValue+intTheVal;
		}
		alert(intTheVal);
	});
	alert(selectValue);
}
</script>
<script src="<!--{$cfg.path.js}-->My97DatePicker/WdatePicker.js" language="javascript"></script>
<!--{include file="admin/footer.tpl"}-->