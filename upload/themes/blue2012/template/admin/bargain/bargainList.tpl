<!--{include file="admin/header.tpl"}-->
<!--列表-->
<table cellpadding="0" cellspacing="1" class="table_info">
  <caption>成交交易管理</caption>
  <tr>
    <td><a href="index.php">成交交易列表</a> | <a href="index.php?action=add">成交交易添加</a></td>
  </tr>
</table>
<form name="myform" method="post" action="">
<table cellpadding="2" cellspacing="1" class="table_list">
	<caption>广告位置列表</caption>
	<tr>
		<th width="30">选中</th>
		<th>区域</th>
		<th>时间</th>
		<th>成交套数(套)</th>
		<th>成交面积(M2)</th>
		<th>成交总金额(元)</th>
		<th>均价(元/M2)</th>
		<th width="20%">操作</th>
	</tr>
<tbody>
<!--{foreach item=item from=$dataList }-->
  <tr>
  	<td class="align_c" ><input type="checkbox" name="ids[]" value="<!--{$item.id}-->" title="<!--{$item.id}-->"></td>
	<td class="align_c"><a href="index.php?action=edit&id=<!--{$item.id}-->"><!--{$item.cityarea_name}--></a></td>
	<td class="align_c"><!--{$item.add_time|date_format:"Y-m-d"}--></td>
	<td class="align_c"><!--{$item.bargain_num}--></td>
	<td class="align_c"><!--{$item.bargain_area}--></td>
	<td class="align_c"><!--{$item.bargain_price}--></td>
	<td class="align_c"><!--{$item.bargain_avgprice}--></td>
  	<td class="align_c">
		<a href="?action=edit&id=<!--{$item.id}-->">修改</a>
	</td>
  </tr>
<!--{/foreach}-->
</tbody>
</table>
<div class="button_box">
	<span style="width:60px"><a href="#" onclick="javascript:$('input[type=checkbox]').attr('checked', true);return false;">全选</a>/<a href="#" onclick="javascript:$('input[type=checkbox]').attr('checked', false)">取消</a></span>
	<input type="button" name="delete" value=" 删除 " onclick="if(confirm('你确认删除选中的条目么？')){myform.action='index.php?action=delete';myform.submit();}">
</div>
<!--{$pagePanel}-->

</form>
<!--{include file="admin/footer.tpl"}-->
