<!--{include file="admin/header.tpl"}-->
<!--列表-->
<table cellpadding="0" cellspacing="1" class="table_info">
  <caption>群发站内信管理</caption>
  <tr>
    <td><a href="index.php">发件箱</a> | <a href="index.php?action=add">发送站内信</a></td>
  </tr>
</table>
<form name="myform" method="post" action="">
<table cellpadding="2" cellspacing="1" class="table_list">
	<caption>信息列表</caption>
	<tr>
		<th width="30">选中</th>
		<th>接收人</th>
		<th>标题</th>
		<th>内容</th>
		<th>申请时间</th>
	</tr>
<tbody>
<!--{foreach item=item from=$dataList }-->
  <tr>
  	<td class="align_c" ><input type="checkbox" name="ids[]" value="<!--{$item.id}-->" title="<!--{$item.username}-->"></td>
	<td class="align_c"><!--{$item.msg_to}--></td>
	<td class="align_c"><!--{$item.msg_title}--></td>
	<td class="align_c"><!--{$item.msg_content}--></td>
	<td class="align_c"><!--{$item.add_time|date_format:"%Y-%m-%d %T"}--></td>
  </tr>
<!--{/foreach}-->
</tbody>
</table>
<div class="button_box">
	<span style="width:60px"><a href="#" onclick="javascript:$('input[type=checkbox]').attr('checked', true);return false;">全选</a>/<a href="#" onclick="javascript:$('input[type=checkbox]').attr('checked', false)">取消</a></span>
	<input type="button" name="fromDel" value=" 删除 " onclick="if(confirm('你确认删除选中的条目么？')){myform.action='index.php?action=fromDel';myform.submit();}">
<!--	<input type="button" name="delete" value=" 物理删除 " onclick="if(confirm('物理删除不当删除后台显示，也会删除接收方的站内信，你确认物理删除选中的条目么？')){myform.action='index.php?action=delete';myform.submit();}">-->
</div>
<!--{$pagePanel}-->

</form>
<!--{include file="admin/footer.tpl"}-->
