<!--{include file="admin/header.tpl"}-->
<!--列表-->

<table cellpadding="0" cellspacing="1" class="table_info">
  <caption>广告条目管理</caption>
  <tr>
    <td>
     	<a href="index.php?action=add">广告条目添加</a>
    </td>
  </tr>
</table>
<form name="myform" method="post" action="">
<table cellpadding="2" cellspacing="1" class="table_list">
	<input type="hidden" name="place_id" value="<!--{$place_id}-->">
	<caption>广告条目列表</caption>
	<tr>
		<th width="30">选中</th>
		<th>广告名称</th>
		<th>类型</th>
		<th>图片联接</th>
		<th>链接地址</th>
        <th>调用地址</th>
		<th>状态</th>
		<th>管理操作</th>
	</tr>
<tbody>
<!--{foreach item=item from=$dataList }-->
  <tr>
  	<td class="align_c" ><input type="checkbox" name="ids[]" value="<!--{$item.id}-->" title="<!--{$item.username}-->"></td>
	<td class="align_c"><!--{$item.ads_name}--></td>
	<td class="align_c"><!--{if $item.ad_type==1}-->图片<!--{elseif $item.ad_type==2}-->FLASH<!--{elseif $item.ad_type==3}-->文字<!--{elseif $item.ad_type==4}-->代码<!--{/if}--></td>
	<td class="align_c">
		<!--{if $item.ad_type =='1'}-->
			<img src="<!--{$cfg.url}-->upfile/<!--{$item.image_url}-->" width="200" height="60" >
		<!--{elseif $item.ad_type =='2'}-->
			<embed type="application/x-shockwave-flash" src="<!--{$cfg.url}-->upfile/<!--{$item.flash_url}-->" bgcolor="#ffffff" wmode="opaque" quality="autohigh" ></embed>
		<!--{elseif $item.ad_type =='3'}-->
			<!--{$item.text}-->
		<!--{elseif $item.ad_type =='4'}-->
			<script type="text/javascript"><!--{$item.code}--></script>
		<!--{/if}-->
	</td>
	<td class="align_c"><!--{$item.link_url}--></td>
    <td class="align_c">&lt;script src=&quot;<!--{$cfg.url}-->js/adjs.php?id=<!--{$item.id}-->&quot; type=&quot;text/javascript&quot;&gt;&lt;/script&gt;</td>
	<td class="align_c"><!--{if $item.status == 0}-->有效<!--{elseif  $item.status == 1}--><font color="Red">锁定</font><!--{/if}--></td>
  	<td class="align_c">
		<a href="index.php?action=edit&id=<!--{$item.id}-->">修改</a>
	</td>
  </tr>
<!--{/foreach}-->
</tbody>
</table>
<div class="button_box">
	<span style="width:60px"><a href="#" onclick="javascript:$('input[type=checkbox]').attr('checked', true);return false;">全选</a>/<a href="#" onclick="javascript:$('input[type=checkbox]').attr('checked', false)">取消</a></span>
	<input type="button" name="delete" value=" 删除 " onclick="if(confirm('你确认删除选中的条目么？')){myform.action='index.php?action=delete';myform.submit();}">
	<input type="button" name="lock" value=" 锁定 " onclick="if(confirm('你确认锁定选中的条目么？')){myform.action='index.php?action=lock';myform.submit();}">
	<input type="button" name="unlock" value=" 解锁 " onclick="if(confirm('你确认解锁选中的条目么？')){myform.action='index.php?action=unlock';myform.submit();}">
</div>
<!--{$pagePanel}-->

</form>
<!--{include file="admin/footer.tpl"}-->
