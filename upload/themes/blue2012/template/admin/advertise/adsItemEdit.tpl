<!--{include file="admin/header.tpl"}-->
<script language="javascript">
function switchContent(switchType,switchId){
	$('.'+switchType+'_content').css('display','none');
	$('#'+switchType+'_content_'+switchId).css('display','');
}
</script>
<form id="editForm" name="editForm" method="post" action="index.php?action=save" onsubmit="return validator(this)">
	<input type="hidden" name="id" value="<!--{$dataInfo.id}-->">
	<table class="table_form" cellpadding="2" cellspacing="1">
    	<caption>添加广告条目</caption>
    	<tr>
        	<th width="17%">名称：</th>
        	<td><input name="ads_name" type="text" id="ads_name" size="25" valid="required" errmsg="名称必填" value="<!--{$dataInfo.ads_name}-->"  /></td>
        </tr>
    	<tr>
        	<th width="17%">类型：</th>
        	<td><select name="ad_type" onchange="switchContent('ad_type',this.value);">
        		<option value="1" <!--{if $dataInfo.ad_type=='1'}-->selected<!--{/if}-->>图片</option>
        		<option value="2" <!--{if $dataInfo.ad_type=='2'}-->selected<!--{/if}-->>Flash</option>
        		<option value="3" <!--{if $dataInfo.ad_type=='3'}-->selected<!--{/if}-->>文字</option>
        		<option value="4" <!--{if $dataInfo.ad_type=='4`'}-->selected<!--{/if}-->>代码</option>
        	</select></td>
        </tr>
        <tr>
        	<th width="17%">内容：<br/>所有广告图片不进行缩略，不加水印，请把图片处理好再上传</th>
        	<td>
		        <div class="ad_type_content" id="ad_type_content_1" <!--{if $dataInfo.ad_type!='1' && $dataInfo.ad_type!="" }-->style="display:none"<!--{/if}-->>
		        	
                    <input type="hidden" name="image_url" id="image_url" value="<!--{$dataInfo.image_url}-->" >
		        	<input type="hidden" name="image_thumb" id="image_thumb" value="<!--{$dataInfo.image_thumb}-->">
		        	
                    <div id="image_dis"><!--{if $dataInfo.image_url}--><img src="<!--{$cfg.url}-->upfile/<!--{ $dataInfo.image_url}-->" width="250" height="60" /><!--{/if}--></div>
		        	图片Alt标记内容:<input type="text" name="alt" value="<!--{$dataInfo.alt}-->" size="25">
                    
		        	<iframe name="uploadImage" width="100%" height="35" scrolling="No" frameborder="no"  src="<!--{$cfg.url}-->upload.php?to=uploadImage|ads|pic" align="left"></iframe>
                    
		        </div>
                
		        <div class="ad_type_content" id="ad_type_content_2" <!--{if $dataInfo.ad_type!='2'}-->style="display:none"<!--{/if}-->>
		        	<input type="hidden" name="flash_url" value="<!--{$dataInfo.flash_url}-->">
		        	<div id="flash_dis"><!--{if $dataInfo.flash_url}--><embed type="application/x-shockwave-flash" src="<!--{$cfg.url}-->upfile/<!--{$dataInfo.flash_url}-->" bgcolor="#ffffff" wmode="opaque" quality="autohigh" width="354" height="68"></embed><!--{/if}--></div>
		        	<iframe name="uploadFlash" width="100%" height="35" scrolling="No" frameborder="no"  src="<!--{$cfg.url}-->upload.php?to=uploadFlash|ads|flash" align="left"></iframe>
		        </div>
		        <div class="ad_type_content" id="ad_type_content_3" <!--{if $dataInfo.ad_type!='3'}-->style="display:none"<!--{/if}-->>
		        	<input type="text" name="text" value="<!--{$dataInfo.text}-->">
		        </div>
		        <div class="ad_type_content" id="ad_type_content_4" <!--{if $dataInfo.ad_type!='4'}-->style="display:none"<!--{/if}-->>
		        	<textarea name="code" rows="5"><!--{$dataInfo.code}--></textarea>
		        </div>
        	</td>
        </tr>
         <tr>
        	<th width="17%">链接地址：</th>
        	<td>
        		<input type="text" name="link_url"  style="padding:6px; width:98%; line-height:17px;" value="<!--{$dataInfo.link_url}-->" id="link_url" size="50" valid="required" errmsg="链接地址必填"/>
        	</td>
        </tr>
         <tr>
        	<th width="17%">说明：</th>
        	<td><textarea name="introduce" rows="5" style="padding:6px; width:98%; line-height:17px;"/><!--{$dataInfo.introduce}--></textarea></td>
        </tr>
        
          <tr>
        	<th width="17%">宽</th>
        	<td><input type="text" valid="required|isNumber" errmsg="高度必填" name="width" value="<!--{$dataInfo.width}-->" size="10">px  高<input valid="required|isNumber" errmsg="宽度必填" type="text" name="height" value="<!--{$dataInfo.height}-->" size="10">px</td>
        </tr>
        
        <tr>
        	<td></td>
        	<td><input name="Submit" type="button" value=" 提交 " onclick="javascript:if (validator(document.editForm)) document.editForm.submit();" ></td>
        </tr>
    </table>
</form>
<script language="javascript">
function uploadImage( furl,fname,thumbUrl ){
	document.getElementById('image_url').value = furl;
	document.getElementById('image_thumb').value = thumbUrl;
	document.getElementById('image_dis').innerHTML = '<img src="<!--{$cfg.url}-->upfile/'+furl+'" width="250" height="60">';
}
function uploadFlash( furl,fname,thumbUrl ){
	document.getElementById('flash_url').value = furl;
	document.getElementById('flash_dis').innerHTML = '<embed type="application/x-shockwave-flash" src="<!--{$cfg.url}-->upfile/'+furl+'" bgcolor="#ffffff" wmode="opaque" quality="autohigh" width="354" height="68"></embed>';
}

</script>
<script src="<!--{$cfg.path.js}-->My97DatePicker/WdatePicker.js" language="javascript"></script>
<!--{include file="admin/footer.tpl"}-->