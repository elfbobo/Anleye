<!--{include file="header.tpl"}-->
<div class="wrap">
<h2>用户组管理</h2>
<form name="addItem" method="post" action="group.php?action=save" onsubmit="return validator(this)">
<input type="hidden" name="group_id" value="<!--{$info.group_id}-->" />
名称：<input name="group_name" value="<!--{$info.group_name}-->" valid="required" noValue="0" errmsg="值不能为空!" />
<input type="submit" value="保存" class="button" />
</form>
<form name="form1" method="post" action="group.php?action=delete">
<table class="widefat">
<thead>
  <tr>
    <th scope="col" width="20"><input type="checkbox" onClick="checkAll(this.form)"></th>
    <th scope="col">ID</th>
    <th scope="col">名称</th>
    <th scope="col">操作</th>
  </tr>
</thead>
<tbody>
<!--{foreach item=item from=$groupList}-->
  <tr class='alternate'>
    <td><input type="checkbox" name="dds[]" value="<!--{$item.group_id}-->"></td>
    <td><!--{$item.group_id}--></td>
    <td> <!--{$item.group_name}--></td>
    <td><a href="?group_id=<!--{$item.group_id}-->">编辑</a></td>
  </tr>
<!--{/foreach}-->
</tbody>
</table>
<div align="right" style="padding-top: 15px;"><input class="button" type="submit" value="删除选中" onclick="return confirm('真的要删除吗?')"></div>
</form>
</div>
<script type="text/javascript">
ifcheck = true;
function checkAll(form) {
	for (var i=0;i<form.elements.length;i++) {
		var e = form.elements[i];
		e.checked = ifcheck;
	}
	ifcheck = ifcheck == true ? false : true;
}
</script>
<!--{include file="footer.tpl"}-->
