<!--{include file="admin/header.tpl"}-->
<!--列表-->

<table cellpadding="0" cellspacing="1" class="table_info">
  <caption>信息筛选</caption>

  <td>
<a href="index.php?action=intention&id=<!--{$id}-->"><!--{if $smarty.get.linktype==''}--><font class="red">全部意向</font><!--{else}-->全部意向<!--{/if}--></a> | 
<a href="index.php?action=intention&id=<!--{$id}-->&linktype=1"><!--{if $smarty.get.linktype==1}--><font class="red">参加团购</font><!--{else}-->参加团购<!--{/if}--></a> |
<a href="index.php?action=intention&id=<!--{$id}-->&linktype=2"><!--{if $smarty.get.linktype==2}--><font class="red">邮寄楼书发我</font><!--{else}-->邮寄楼书发我<!--{/if}--></a> |
<a href="index.php?action=intention&id=<!--{$id}-->&linktype=3"><!--{if $smarty.get.linktype==3}--><font class="red">登记现场看房</font><!--{else}-->登记现场看房<!--{/if}--></a> |
<a href="index.php?action=intention&id=<!--{$id}-->&linktype=4"><!--{if $smarty.get.linktype==4}--><font class="red">通过电子邮件联系</font><!--{else}-->通过电子邮件联系<!--{/if}--></a> |
<a href="index.php?action=intention&id=<!--{$id}-->&linktype=5"><!--{if $smarty.get.linktype==5}--><font class="red">通过电话联系</font><!--{else}-->通过电话联系<!--{/if}--></a>
  </td>

</table>



<form name="myform" method="post" action="">
<table cellpadding="2" cellspacing="1" class="table_list">
	<caption><!--{$boroughName}--> 购买意向管理</caption>
	<tr>
		<th width="3%">选中</th>
	  <th width="5%">姓名</th>
  <th width="10%">给开发商留言</th>
		<th width="5%">性别</th>
	  <th width="10%">手机</th>
  <th width="10%">QQ</th>
       	  <th width="10%">电子邮件</th>
  <th width="10%">家庭电话</th>
       	  <th width="10%">邮政编码</th>
	  <th width="7%">通信地址</th>
      <th width="25%">意向</th>
	</tr>
<tbody>
<!--{foreach item=item from=$intentionList }-->
  <tr>
  	<td class="align_c" ><input type="checkbox" name="ids[]" value="<!--{$item.id}-->"></td>
	<td><!--{$item.nickname}--></td>
	<td class="align_c"><!--{$item.content}--> </td>
	<td class="align_c"><!--{if $item.gender == 0}-->男<!--{else}-->女<!--{/if}--></td>
	<td class="align_c"><!--{$item.mobile}--></td>
	<td class="align_c"><!--{$item.qq}--></td>
    	<td class="align_c"><!--{$item.email}--></td>
        	<td class="align_c"><!--{$item.tel}--></td>
            	<td class="align_c"><!--{$item.postcode}--></td>
                	<td class="align_c"><!--{$item.address}--></td>
                    <td class="align_c"><!--{$item.link_type}--></td>
	</tr>
<!--{/foreach}-->
</tbody>
</table>
<div class="button_box">
	<span style="width:60px"><a href="#" onclick="javascript:$('input[type=checkbox]').attr('checked', true);return false;">全选</a>/<a href="#" onclick="javascript:$('input[type=checkbox]').attr('checked', false)">取消</a></span>
	<input type="button" name="delete" value=" 删除 " onclick="if(confirm('你确认删除选中的条目么？')){myform.action='index.php?action=intentionDelete';myform.submit();}">
</div>
<!--{$pagePanel}-->

</form>
<!--{include file="admin/footer.tpl"}-->
