<!--{include file="admin/header.tpl"}-->
<script language="javascript">
function switchContent(switchType,switchId){
	$('.'+switchType+'_content').css('display','none');
	$('#'+switchType+'_content_'+switchId).css('display','');
}
</script>
<form id="editForm" name="editForm" method="post" action="?action=save" onsubmit="return validator(this)">
	<input type="hidden" name="id" value="<!--{$dataInfo.id}-->">
	<input type="hidden" name="back_url" value="<!--{$back_url}-->">
    <table class="table_form" cellpadding="2" cellspacing="1">
    	<caption>添加外部链接</caption>
    	<tr>
        	<th width="17%">外部链接名称：<br/>后台管理区别使用</th>
        	<td><input name="link_name" type="text" id="link_name" size="25" valid="required" errmsg="名称必填" value="<!--{$dataInfo.link_name}-->"  /></td>
        </tr>
    	<tr>
        	<th width="17%">选择链接分类：</th>
        	<td><select name="link_class" valid="required" errmsg="选择链接分类">
        	<option value="">请选择链接分类</option>
        	<!--{foreach from=$link_class key=key item=item}-->
				<!--{if $dataInfo.link_class == $key}-->
					<option value="<!--{$key}-->" selected><!--{$item}--></option>
				<!--{else}-->
					<option value="<!--{$key}-->" ><!--{$item}--></option>
				<!--{/if}-->
			<!--{/foreach}-->
        	</select></td>
        </tr>
        <tr>
        	<th width="17%">选择链接类型：</th>
        	<td><select name="link_type" onchange="switchContent('link_type',this.value);"><br>
        		<option value="2" <!--{if $dataInfo.link_type==2}-->selected<!--{/if}-->>图片</option>
        		<option value="1" <!--{if $dataInfo.link_type==1}-->selected<!--{/if}-->>文字</option>
        	</select></td>
        </tr>
        
        <tr>
        	<th width="17%">内容：<br/>显示在页面链接上的文字或者是图片</th>
        	<td>
		        <div class="link_type_content" id="link_type_content_2" <!--{if $dataInfo.link_type!=2 && $dataInfo.link_type!="" }-->style="display:none"<!--{/if}-->>
		        	<input type="hidden" name="link_logo" id="link_logo" value="<!--{$dataInfo.link_logo}-->" >
		        	<div id="image_dis"><!--{if $dataInfo.link_logo}--><img src="<!--{$cfg.url}-->upfile/<!--{ $dataInfo.link_logo}-->" width="88" height="31" /><!--{/if}--></div>
		        	<iframe name="uploadImage" width="100%" height="35" scrolling="No" frameborder="no"  src="<!--{$cfg.url}-->upload.php?to=uploadImage|link|logo" align="left"></iframe>
		        </div>
		        <div class="link_type_content" id="link_type_content_1" <!--{if $dataInfo.link_type!=1}-->style="display:none"<!--{/if}-->>
		        	<input type="text" name="link_text" id="link_text" value="<!--{$dataInfo.link_text}-->" >
		        </div>
        	</td>
        </tr>
        <tr>
        	<th width="17%">链接地址：</th>
        	<td>
        		<input type="text" name="link_url"  style="padding:6px; width:98%; line-height:17px;" value="<!--{$dataInfo.link_url}-->" id="link_url" size="50" valid="required" errmsg="链接地址必填"/>
        	</td>
        </tr>
        <tr>
        	<td></td>
        	<td><input name="Submit" type="button" value=" 提交 " onclick="javascript:if (validator(document.editForm)) document.editForm.submit();" ></td>
        </tr>
    </table>
</form>
<script language="javascript">
function uploadImage( furl,fname,thumbUrl ){
	document.getElementById('link_logo').value = furl;
	document.getElementById('image_dis').innerHTML = '<img src="<!--{$cfg.url}-->upfile/'+furl+'" width="88" height="31">';
}

</script>
<!--{include file="admin/footer.tpl"}-->