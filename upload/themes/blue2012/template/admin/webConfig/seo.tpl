<!--{include file="admin/header.tpl"}-->
<form name="editForm" method="post" action="seo.php?action=save">
<table cellpadding="2" cellspacing="1" class="table_form">
	<caption>网站关键词描述设置</caption>
	<tr>
		<td>网站关键词就是一个网站给首页设定的以便用户通过搜索引擎能搜到本网站的词汇，网站关键词代表了网站的市场定位。网站的关键词至关重要，如果选择的关键词不当，对网站来说就是灾难性的后果。<br />
更多详细资料访问<a href="http://baike.baidu.com/view/3690455.htm" target="_blank">百度百科</a></td>
	</tr>
  </table>			
<table cellpadding="2" cellspacing="1" class="table_form">
	<caption>首页</caption>
	<tr>
		<th width="13%"><strong>关键词</strong></th>
		<td colspan="3">
  <input name="webConfig[index_keyword]" type="text"  size="50" value="<!--{$webConfig.index_keyword}-->" /></td>
	</tr>
	<tr>
		<th width="13%"><strong>描述</strong></th>
		<td colspan="3">
  <input name="webConfig[index_description]" type="text"  size="80" value="<!--{$webConfig.index_description}-->" /></td>
	</tr>
</table>
<table cellpadding="2" cellspacing="1" class="table_form">
	<caption>二手房</caption>
	<tr>
		<th width="13%"><strong>关键词</strong></th>
		<td colspan="3">
  <input name="webConfig[sale_keyword]" type="text"  size="50" value="<!--{$webConfig.sale_keyword}-->" /></td>
	</tr>
	<tr>
		<th width="13%"><strong>描述</strong></th>
		<td colspan="3">
  <input name="webConfig[sale_description]" type="text"  size="80" value="<!--{$webConfig.sale_description}-->" /></td>
	</tr>
</table>
<table cellpadding="2" cellspacing="1" class="table_form">
	<caption>租房</caption>
	<tr>
		<th width="13%"><strong>关键词</strong></th>
		<td colspan="3">
  <input name="webConfig[rent_keyword]" type="text"  size="50" value="<!--{$webConfig.rent_keyword}-->" /></td>
	</tr>
	<tr>
		<th width="13%"><strong>描述</strong></th>
		<td colspan="3">
  <input name="webConfig[rent_description]" type="text"  size="80" value="<!--{$webConfig.rent_description}-->" /></td>
	</tr>
</table>
<table cellpadding="2" cellspacing="1" class="table_form">
	<caption>经纪人</caption>
	<tr>
		<th width="13%"><strong>关键词</strong></th>
		<td colspan="3">
  <input name="webConfig[broker_keyword]" type="text"  size="50" value="<!--{$webConfig.broker_keyword}-->" /></td>
	</tr>
	<tr>
		<th width="13%"><strong>描述</strong></th>
		<td colspan="3">
  <input name="webConfig[broker_description]" type="text"  size="80" value="<!--{$webConfig.broker_description}-->" /></td>
	</tr>
</table>
<table cellpadding="2" cellspacing="1" class="table_form">
	<caption>小区</caption>
	<tr>
		<th width="13%"><strong>关键词</strong></th>
		<td colspan="3">
  <input name="webConfig[community_keyword]" type="text"  size="50" value="<!--{$webConfig.community_keyword}-->" /></td>
	</tr>
	<tr>
		<th width="13%"><strong>描述</strong></th>
		<td colspan="3">
  <input name="webConfig[community_description]" type="text"  size="80" value="<!--{$webConfig.community_description}-->" /></td>
	</tr>
</table>
<table cellpadding="2" cellspacing="1" class="table_form">
	<caption>新楼盘</caption>
	<tr>
		<th width="13%"><strong>关键词</strong></th>
		<td colspan="3">
  <input name="webConfig[newhouse_keyword]" type="text"  size="50" value="<!--{$webConfig.newhouse_keyword}-->" /></td>
	</tr>
	<tr>
		<th width="13%"><strong>描述</strong></th>
		<td colspan="3">
  <input name="webConfig[newhouse_description]" type="text"  size="80" value="<!--{$webConfig.newhouse_description}-->" /></td>
	</tr>
</table>

<br/>
<table cellpadding="2" cellspacing="1" class="table_form">
	<tr>
		<td height="50" class="align_c"><input type="Submit" value="保存配置"></td>
	</tr>
</table>
</form>


<script src="<!--{$cfg.path.js}-->My97DatePicker/WdatePicker.js" language="javascript"></script>
<!--{include file="admin/footer.tpl"}-->
