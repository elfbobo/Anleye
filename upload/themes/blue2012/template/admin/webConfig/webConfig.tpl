<!--{include file="admin/header.tpl"}-->
<form name="editForm" method="post" action="webConfig.php?action=save">
<table cellpadding="2" cellspacing="1" class="table_form">
	<caption>基本信息</caption>
	<tr>
		<th width="13%"><strong>网站标题</strong></th>
		<td colspan="3">
  <input name="webConfig[base_title]" type="text"  size="30" value="<!--{$webConfig.base_title}-->" /> </td> 
	</tr>
  <tr>
		<th width="13%"><strong>网站名称</strong></th>
		<td colspan="3">
  <input name="webConfig[base_titlec]" type="text"  size="30" value="<!--{$webConfig.base_titlec}-->" /> <font color="#999999">要与网站标题一致</font></td>
	</tr>
	<tr>
		<th width="13%"><strong>所在城市</strong></th>
		<td colspan="3">
  <input name="webConfig[base_city]" type="text"  size="30" value="<!--{$webConfig.base_city}-->" /> <font color="#999999">您当前所在的城市</font></td>
	</tr>
	<tr>
		<th width="13%"><strong>地图城市</strong></th>
		<td colspan="3">
  <input name="webConfig[base_mapcity]" type="text"  size="30" value="<!--{$webConfig.base_mapcity}-->" /> <font color="#999999">地图归属地区 （例如：哈尔滨市 一定要加“市”）</font></td>
	</tr>
    <tr>
		<th width="13%"><strong>城市地图lat坐标</strong></th>
		<td colspan="3">
  <input name="webConfig[lat]" type="text"  size="30" value="<!--{$webConfig.lat}-->" /> <a href="http://www.yanwee.com/forum.php?mod=viewthread&tid=30793" target="_blank">如何设置lat与lnt坐标？</td>
	</tr>

 <tr>
		<th width="13%"><strong>城市地图lnt坐标</strong></th>
		<td colspan="3">
  <input name="webConfig[lnt]" type="text"  size="30" value="<!--{$webConfig.lnt}-->" /></td>
	</tr>
    	<tr>
		<th width="13%"><strong>城市切换地址</strong></th>
		<td colspan="3">
        
<input name="webConfig[switch_url]" type="text"  size="30" value="<!--{$webConfig.switch_url}-->" /> <font color="#999999">输入切换城市地址既开启切换城市，如果为空是关闭状态</font></td>
	</tr>
    
    	<tr>
		<th width="13%"><strong>切换默认城市地址</strong></th>
		<td colspan="3">
        
<input name="webConfig[citySwitch_site]" type="text"  size="30" value="<!--{$webConfig.citySwitch_site}-->" /> <font color="#999999">如果访客所在城市没有开通，则默认跳转到该站，留空则访问切换页面</font></td>
	</tr>
    <tr>
		<th width="13%"><strong>是否房源自动过期</strong></th>
		<td colspan="3">
        <input name="webConfig[expired_switch]" type="radio" id="1" value="1" <!--{if $webConfig.expired_switch == 1 }--> checked="checked" <!--{/if}-->/> 是
  <input type="radio" name="webConfig[expired_switch]" id="1" value="2" <!--{if $webConfig.expired_switch == 2 }--> checked="checked" <!--{/if}-->/> 否
  
 <font color="#999999">如果开启 超过7天未刷新的房源将下架处理，发布超过90天的房源自动删除</font></td>
	</tr>
    
	<tr>
		<th width="13%"><strong>实名认证</strong></th>
		<td colspan="3">
        <input name="webConfig[base_is_shop]" type="radio" id="1" value="1" <!--{if $webConfig.base_is_shop == 1 }--> checked="checked" <!--{/if}-->/> 是
  <input type="radio" name="webConfig[base_is_shop]" id="1" value="2" <!--{if $webConfig.base_is_shop == 2 }--> checked="checked" <!--{/if}-->/> 否
  
 <font color="#999999">经纪人是否需要实名认证才开通网店</font></td>
	</tr>
	<tr>
		<th width="13%"><strong>游客发布</strong></th>
		<td colspan="3">
        <input name="webConfig[base_is_guest]" type="radio" id="1" value="1" <!--{if $webConfig.base_is_guest == 1 }--> checked="checked" <!--{/if}-->/> 是
  <input type="radio" name="webConfig[base_is_guest]" id="1" value="2" <!--{if $webConfig.base_is_guest == 2 }--> checked="checked" <!--{/if}-->/> 否
  
<font color="#999999">是否开通游客发布功能</font></td>
	</tr>
	<tr>
		<th width="13%"><strong>经纪人免费注册</strong></th>
		<td colspan="3">
   <input name="webConfig[base_member_open]" type="radio" id="1" value="1" <!--{if $webConfig.base_member_open == 1 }--> checked="checked" <!--{/if}-->/> 是
  <input type="radio" name="webConfig[base_member_open]" id="1" value="2" <!--{if $webConfig.base_member_open == 2 }--> checked="checked" <!--{/if}-->/> 否
 
 <font color="#999999">是否开通免费经纪人注册</font></td>
	</tr>
    <tr>
		<th width="13%"><strong>是否开启ucenter</strong></th>
		<td colspan="3">
        
         <input name="webConfig[base_uc]" type="radio" id="1" value="1" <!--{if $webConfig.base_uc == 1 }--> checked="checked" <!--{/if}-->/> 是
  <input type="radio" name="webConfig[base_uc]" id="1" value="2" <!--{if $webConfig.base_uc == 2 }--> checked="checked" <!--{/if}-->/> 否
   <a href="http://www.yanwee.com/forum.php?mod=viewthread&tid=30792" target="_blank">如何整合ucenter？</a></td>
	</tr>
    
	<tr>
		<th width="13%"><strong>新闻频道</strong></th>
		<td colspan="3">
        <input name="webConfig[newsOpen]" type="radio" id="1" value="1" <!--{if $webConfig.newsOpen == 1 }--> checked="checked" <!--{/if}-->/> 是
  <input type="radio" name="webConfig[newsOpen]" id="1" value="2" <!--{if $webConfig.newsOpen == 2 }--> checked="checked" <!--{/if}-->/> 否
  
  
   <font color="#999999">是否开启新闻频道</font> <a href="http://www.yanwee.com/forum.php?mod=viewthread&tid=30791" target="_blank">如何安装新闻频道？</a></td>
	</tr>
    
    <tr>
		<th width="13%"><strong>新房频道</strong></th>
		<td colspan="3">
        <input name="webConfig[newhouseOpen]" type="radio" id="1" value="1" <!--{if $webConfig.newhouseOpen == 1 }--> checked="checked" <!--{/if}-->/> 是
       <input type="radio" name="webConfig[newhouseOpen]" id="1" value="2" <!--{if $webConfig.newhouseOpen == 2 }--> checked="checked" <!--{/if}-->/> 否
   <font color="#999999">是否开启新房频道</font> </td>
	</tr>
    
	<tr>
		<th width="13%"><strong>论坛频道</strong></th>
		<td colspan="3">
        
         <input name="webConfig[bbsOpen]" type="radio" id="1" value="1" <!--{if $webConfig.bbsOpen == 1 }--> checked="checked" <!--{/if}-->/> 是
  <input type="radio" name="webConfig[bbsOpen]" id="1" value="2" <!--{if $webConfig.bbsOpen == 2 }--> checked="checked" <!--{/if}-->/> 否
   <font color="#999999">是否开启新闻频道 <a href="http://www.yanwee.com/forum.php?mod=viewthread&tid=30790" target="_blank"> 如何安装整合论坛？</a></font></td>
	</tr>
	 <tr>
		<th width="13%"><strong>新浪微博ID</strong></th>
		<td colspan="3">
  <input name="webConfig[sinaapp]" type="text"  size="30" value="<!--{$webConfig.sinaapp}-->" /><font color="#999999"> 输入新浪微博ID</font></td>
	</tr>
	
	<tr>
		<th width="13%"><strong>统计代码</strong></th>
		<td colspan="3"><textarea name="webConfig[base_tongji]" cols="30" rows="5"><!--{$webConfig.base_tongji}--></textarea> <a href="http://new.cnzz.com/user/reg.php?b951a832370420ba4a334e07b526d84a">申请CNZZ统计代码</a></td>
	</tr>
	<tr>
		<th width="13%"><strong>备案信息</strong></th>
		<td colspan="3">
  <input name="webConfig[base_beian]" type="text"  size="30" value="<!--{$webConfig.base_beian}-->" /></td>
	</tr>
</table>			
<table cellpadding="2" cellspacing="1" class="table_form">
	<caption>联系方式</caption>
	<tr>
		<th width="13%"><strong>公司名称</strong></th>
		<td colspan="3">
  <input name="webConfig[contact_gongsi]" type="text"  size="30" value="<!--{$webConfig.contact_gongsi}-->" /></td>
	</tr>
	<tr>
		<th width="13%"><strong>公司地址</strong></th>
		<td colspan="3">
  <input name="webConfig[contact_dizhi]" type="text"  size="30" value="<!--{$webConfig.contact_dizhi}-->" /></td>
	</tr>
	<tr>
		<th width="13%"><strong>邮政编号</strong></th>
		<td colspan="3">
  <input name="webConfig[contact_youbian]" type="text"  size="30" value="<!--{$webConfig.contact_youbian}-->" /></td>
	</tr>
	<tr>
		<th width="13%"><strong>传真</strong></th>
		<td colspan="3">
  <input name="webConfig[contact_chuanzhen]" type="text"  size="30" value="<!--{$webConfig.contact_chuanzhen}-->" /></td>
	</tr>
	<tr>
		<th width="13%"><strong>联系电话</strong></th>
		<td colspan="3">
  <input name="webConfig[contact_dianhua]" type="text"  size="30" value="<!--{$webConfig.contact_dianhua}-->" /></td>
	</tr>
	<tr>
		<th width="13%"><strong>客服热线</strong></th>
		<td colspan="3">
  <input name="webConfig[contact_rexian]" type="text"  size="30" value="<!--{$webConfig.contact_rexian}-->" /></td>
	</tr>
	<tr>
		<th width="13%"><strong>QQ号码</strong></th>
		<td colspan="3">
  <input name="webConfig[contact_qq]" type="text"  size="30" value="<!--{$webConfig.contact_qq}-->" /></td>
	</tr>
	
</table>


<table cellpadding="2" cellspacing="1" class="table_form">
	<tr>
		<td height="50" class="align_c"><input type="Submit" value="保存配置"></td>
	</tr>
</table>
</form>


<script src="<!--{$cfg.path.js}-->My97DatePicker/WdatePicker.js" language="javascript"></script>
<!--{include file="admin/footer.tpl"}-->
