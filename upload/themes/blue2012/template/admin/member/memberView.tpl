<!--{include file="admin/header.tpl"}-->
    <table class="table_form" cellpadding="2" cellspacing="1">
    	<caption>用户基本信息信息</caption>
    	<tr>
        	<th width="17%"><strong>登陆用户名：</strong></th>
        	<td width="33%" ><!--{$dataInfo.username}--></td>
        	<th width="17%"><strong>Email：</strong></th>
        	<td width="33%"><!--{$dataInfo.email}-->&nbsp;<!--{if $dataInfo.user_type==2}--><input type="checkbox" name="email_publish" <!--{if $dataInfo.email_publish}-->checked<!--{/if}--> >公开<!--{/if}--></td>
        </tr>
		<tr>
        	<th width="17%"><strong>账户余额：</strong></th>
        	<td><!--{$dataInfo.money}-->元</td>
        	<th width="17%"><strong>登陆次数：</strong></th>
        	<td><!--{$dataInfo.logins}--></td>
        </tr>
        <tr>
        	<th width="17%"><strong>积分：</strong></th>
        	<td><!--{$dataInfo.scores}--></td>
        	<th width="17%"><strong>最后登陆时间：</strong></th>
        	<td><!--{$dataInfo.last_login|date_format:"%Y-%m-%d %T"}--></td>
        </tr>
        <tr>
        	<th width="17%"><strong>加急标签：</strong></th>
        	<td><!--{$dataInfo.vexation}--></td>
        	<th width="17%"><strong>注册时间：</strong></th>
        	<td><!--{$dataInfo.add_time|date_format:"%Y-%m-%d %T"}--></td>
        </tr>
        <tr>
        	<th width="17%"><strong>用户状态：</strong></th>
        	<td><!--{if $dataInfo.status==0}-->正常<!--{/if}--><!--{if $dataInfo.status==1}--><font color="red">未开通</font><!--{/if}--></td>
        	<th width="17%"><strong>用户套餐：</strong></th>
        	<td><!--{if $dataInfo.vip==1}-->体验套餐<!--{elseif $dataInfo.vip==2}-->标准套餐<!--{else}-->普通用户<!--{/if}--></td>
        </tr>
        
    </table>
    <!--{if $dataInfo.user_type == 1}-->
    <table class="table_form" cellpadding="2" cellspacing="1">
        <caption>经纪人详细信息</caption>
    	<tr>
        	<th width="17%"><strong>真实姓名：</strong></th>
        	<td width="33%"><!--{$dataInfo.realname}--></td>
        	<th width="17%"><strong>服务区域：</strong></th>
        	<td width="33%"><!--{$dataInfo.cityarea_id}--></td>
        </tr>
        <tr>
        	<th width="17%"><strong>移动电话：</strong></th>
        	<td><!--{$dataInfo.mobile}--></td>
        	<th width="17%"><strong>身份证号：</strong></th>
        	<td><!--{$dataInfo.idcard}--></td>
        </tr>
        <tr>
        	<th width="17%"><strong>身份证照片：</strong></th>
        	<td><!--{if $dataInfo.idcard_pic}--><img src="<!--{$cfg.url}-->upfile/<!--{$dataInfo.idcard_pic}-->"><!--{/if}--></td>
        	<th width="17%"><strong>执业资格证书：</strong></th>
        	<td><!--{if $dataInfo.aptitude}--><img src="<!--{$cfg.url}-->upfile/<!--{$dataInfo.aptitude}-->"><!--{/if}--></td>
        </tr>
        <tr>
        	<th width="17%"><strong>头像：</strong></th>
        	<td><!--{if $dataInfo.avatar}--><img src="<!--{$cfg.url}-->upfile/<!--{$dataInfo.avatar}-->"><!--{/if}--></td>
        	<th width="17%"><strong>公司：</strong></th>
        	<td><!--{$dataInfo.company}--></td>
        </tr>
        <tr>
        	<th width="17%"><strong>签名：</strong></th>
        	<td><!--{$dataInfo.signed}--></td>
        	<th width="17%"><strong>门店：</strong></th>
        	<td><!--{$dataInfo.outlet}--></td>
        </tr>
         <tr>
        	<th width="17%"><strong>门店地址：</strong></th>
        	<td><!--{$dataInfo.outlet_addr}--></td>
        	<th width="17%"><strong>邮政编码：</strong></th>
        	<td><!--{$dataInfo.post_code}--></td>
        </tr>
         <tr>
        	<th width="17%"><strong>公司电话：</strong></th>
        	<td><!--{$dataInfo.com_tell}--></td>
        	<th width="17%"><strong>公司传真：</strong></th>
        	<td><!--{$dataInfo.com_fax}--></td>
        </tr> 
        <tr>
        	<th width="17%"><strong>性别：</strong></th>
        	<td><!--{$dataInfo.gender}--></td>
        	<th width="17%"><strong>现住小区：</strong></th>
        	<td><!--{$dataInfo.borough_id}--></td>
        </tr>
        <tr>
        	<th width="17%"><strong>QQ：</strong></th>
        	<td><!--{$dataInfo.qq}--></td>
        	<th width="17%"><strong>MSN：</strong></th>
        	<td><!--{$dataInfo.MSN}--></td>
        </tr>
         <tr>
        	<th width="17%"><strong>自我介绍：</strong></th>
        	<td colspan="3"><!--{$dataInfo.introduce}--></td>
        </tr>
        
    </table>
    <!--{else}-->
    <table class="table_form" cellpadding="2" cellspacing="1">
        <caption>业主详细信息</caption>
    	<tr>
        	<th width="17%"><strong>真实姓名：</strong></th>
        	<td width="33%"><!--{$dataInfo.first_name}--><!--{$dataInfo.last_name}-->&nbsp;<input type="checkbox" name="name_publish" <!--{if $dataInfo.name_publish}-->checked<!--{/if}--> >公开</td>
        	<th width="17%"><strong>移动电话：</strong></th>
        	<td width="33%"><!--{$dataInfo.mobile}-->&nbsp;<input type="checkbox" name="mobile_publish" <!--{if $dataInfo.mobile_publish}-->checked<!--{/if}--> >公开</td>
        </tr>
        <tr>
        	<th width="17%"><strong>固定电话：</strong></th>
        	<td><!--{$dataInfo.tell}-->&nbsp;<input type="checkbox" name="tell_publish" <!--{if $dataInfo.tell_publish}-->checked<!--{/if}--> >公开</td>
        	<th width="17%"><strong>QQ：</strong></th>
        	<td><!--{$dataInfo.qq}-->&nbsp;<input type="checkbox" name="qq_publish" <!--{if $dataInfo.qq_publish}-->checked<!--{/if}--> >公开</td>
        </tr>
         <tr>
        	<th width="17%"><strong>MSN：</strong></th>
        	<td><!--{$dataInfo.msn}-->&nbsp;<input type="checkbox" name="msn_publish" <!--{if $dataInfo.msn_publish}-->checked<!--{/if}--> >公开</td>
        	<th width="17%"><strong>性别：</strong></th>
        	<td><!--{$dataInfo.gender}-->&nbsp;<input type="checkbox" name="gender_publish" <!--{if $dataInfo.gender_publish}-->checked<!--{/if}--> >公开</td>
        </tr>
        <tr>
        	<th width="17%"><strong>头像：</strong></th>
        	<td><!--{if $dataInfo.avatar}--><img src="<!--{$cfg.url}-->upfile/<!--{$dataInfo.avatar}-->"><!--{/if}--></td>
        	<th width="17%"><strong>签名：</strong></th>
        	<td><!--{$dataInfo.signed}--></td>
        </tr>
    </table>
    
    <!--{/if}-->
    <div class="button_box">
		<input type="button" name="return" value=" 返回 " onclick="javascript:history.go(-1);">
	</div>
<!--{include file="admin/footer.tpl"}-->