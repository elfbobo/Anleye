<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=gb2312" />
<title><!--{$cfg.page.title}--></title>
<!--{$jsFiles}-->
<!--{$cssFiles}-->
<body>
<script language="JavaScript">
if(self.parent.frames.length != 0) {
    self.parent.location=document.location;
}
</script>
<table class="logintb">
  <tr>
    <td class="login"><h1> <!--{$cfg.page.title}--></h1>
    
      <p>&nbsp;</p>
    </td>
    <td>
      <form id="loginform" name="login" method="post" action="" onsubmit="return validator(this)">
        <input name="action" type="hidden" value="login" />
        <p class="logintitle">用户名: </p>
        <p class="loginform">
          <input name="username" type="text" id="username" maxlength="16" valid="required" errmsg="用户名不能为空!"/>
        </p>
        <p class="logintitle">密　码:</p>
        <p class="loginform">
          <input name="passwd" type="password" id="passwd" maxlength="16" valid="required" errmsg="密码不能为空!"/>
        </p>
        <p class="logintitle">验证码:</p>
        <p class="loginform">
	      <input size="4" name="vaild" type="text" id="text" maxlength="4" valid="required" errmsg="验证码不能为空!"  /><img src="../valid.php" />
        </p>
        <p class="loginnofloat">
          <input name="submit" value="提交"  tabindex="3" type="submit" class="btn" />
        </p>
      </form>
    </td>
  </tr>
  <tr>
    <td colspan="2" class="footer"><div class="copyright">
        <p>Powered by <a href="http://anleye.yanwee.com" target="_blank">anleye.com</a></p>
        <p>&copy;2012 <a href="http://www.yanwee.com" target="_blank">Yanwee Inc.</a></p>
      </div></td>
  </tr>
</table>
<script type="text/javascript">
<!--{if $errorMsg}-->
alert("<!--{$errorMsg}-->");
<!--{/if}-->
document.getElementById('loginform').username.focus();
</script>
</body>
</html>