<!--{include file="admin/header.tpl"}-->
<style type="text/css">
<!--
.y {
	color: #F00;
}
.y {
	font-style: italic;
	font-size: 14px;
}
-->
</style>


<table width="100%"  border="0">
  <tr>
    <td width="50%" valign="top">
     <table class="table_list" >
	<caption>信息统计</caption>
	<tr>
		<th>二手房</th>
		<th>租房</th>
		<th>经纪人</th>
		<th>小区</th>
		<th>二手房均价</th>
        <th>出售置顶</th>
	</tr>
	<tr>
		<td class="align_c"><!--{$statistics.sellNum}-->套</td>
		<td class="align_c"><!--{$statistics.rentNum}-->套</td>
		<td class="align_c"><!--{$statistics.brokerNum}-->人</td>
		<td class="align_c"><!--{$statistics.boroughNum}-->个</td>
		<td class="align_c"><!--{$val.avgprice}-->元/平方</td>
        <td class="align_c"><!--{$Unaudited_housesell}-->套</td>
	</tr>
    <tr>
		<th>出租置顶</th>
		<th>新房</th>
		<th>中介公司</th>
        <th>搬家公司</th>
		<th>装修公司</th>
		<th></th>

	</tr>
	<tr>
		<td class="align_c"><!--{$Unaudited_houserent}-->套</td>
		<td class="align_c"><!--{$newhouse}-->套</td>
		<td class="align_c"><!--{$companyCount}-->家</td>
        <td class="align_c"><!--{$moveCompanyCount}-->家</td>
		<td class="align_c"><!--{$decorationCompanyCount}-->家</td>
		<td class="align_c"></td>
   
	</tr>
    
    </table>
    
      <table class="table_list" >
	<caption><font color="#FF0000">当前需要处理</font></caption>
	<tr>
		<th>审核小区</th>
		<th>身份审核</th>
		<th>资质审核</th>
        <th>头像审核</th>
<th>未开通经纪人</th>
	</tr>
	<tr>
		<td class="align_c"><a href="borough/check.php"><!--{$borough_num}-->个</a></td>
		<td class="align_c"><a href="member/identity.php?status=0"><!--{$broker_id}-->位</a></td>
		<td class="align_c"><a href="member/aptitude.php"><!--{$broker_api}-->位</a></td>
        <td class="align_c"><a href="member/avatar.php?status=0"><!--{$broker_avatar}-->位</a></td>
		<td class="align_c"><a href="member/index.php?status=1"><!--{$noOpen}-->位</a></td>
	</tr>
    
    	<tr>
		<th>个人出租审核</th>
		<th>个人出售审核</th>
		<th>求购审核</th>
		<th>求租审核</th>
        <th>未受理委托房源</th>
      


	</tr>
	<tr>
		<td class="align_c"><a href="house/rent.php?check=2"><!--{$guestRent}-->套</a></td>
		<td class="align_c"><a href="house/sell.php?check=2"><!--{$guestSale}-->套</a></td>
		 <td class="align_c"><a href="house/buy.php?status=0"><!--{$Unaudited_buy}-->套</a></td>
         <td class="align_c"><a href="house/hold.php?status=0"><!--{$Unaudited_hold}-->套</a></td>
        <td class="align_c"><a href="house/consign.php"><!--{$consignCount}-->套</a></td>

	</tr> 	<tr>
		<th>小区更新</th>
		<th>未受理加盟信息</th>
		<th></th>
		<th></th>
        <th></th>
      


	</tr>
	<tr>
		<td class="align_c"><a href="borough/updateCheck.php?status=0"><!--{$xiaoqu_num}-->个</a></td>
		<td class="align_c"><a href="city/union.php"><!--{$UunionCount}-->个</a></td>
		 <td class="align_c"></td>
         <td class="align_c"></td>
        <td class="align_c"></td>

	</tr>
    
    </table>
    <table class="table_list" >
	<caption>商业服务</caption>
	<tr>
		<th>版本信息</th>
		<th>授权</th>
		<th>在线服务</th>
		<th>论坛服务</th>

	</tr>
	<tr>
		<td class="align_c">当前：<!--{$cfg.page.version}--><br />
最新：<script type="text/javascript" src="http://www.yanwee.com/anleye/js/banben.html"></script></td>
		<td class="align_c"><a target="_blank" href="http://sq.yanwee.com">授权查询</a></td>
		<td class="align_c"><a target="_blank" href="http://wpa.qq.com/msgrd?v=3&uin=2621895237&site=qq&menu=yes"><img border="0" src="http://wpa.qq.com/pa?p=2:2621895237:41" alt="点击这里给我发消息" title="点击这里给我发消息"></a></td>
		<td class="align_c"><a href="http://www.yanwee.com/forum.php?mod=forumdisplay&fid=11" target="_blank" title="商业用户论坛支持，发布问题1小时内响应">论坛支持</a></td>
		
	</tr>
    <tr>
		<th colspan="4" align="left">版权所有</th>
		</tr>
	<tr>
		<td colspan="4" align="left" class="align_c"><a href="http://www.yanwee.com" target="_blank">哈尔滨言微网络技术开发有限公司</a> | <a href="http://anleye.yanwee.com" target="_blank">安乐业房产网站管理系统</a></td>
		</tr>
    
    
    
    </table>
    
    </td>
    <td width="50%" valign="top">
     <table class="table_list" >
	<caption>服务器信息</caption>
	<tr>
		<th>操作系统/PHP</th>
		<th>magic_quotes_gpc</th>
		<th>上传大小</th>
		<th>MYSQL版本</th>
	</tr>
	<tr>
		<td class="align_c"><!--{$cfg.page.xitong}-->/<!--{$cfg.page.banben}--></td>
		<td class="align_c"><!--{if $cfg.page.mag==1}-->on<!--{else}--><font color="#FF0000">off</font><!--{/if}--></td>
		<td class="align_c"><!--{$cfg.page.size}--></td>
		<td class="align_c"><!--{$cfg.page.mysqlbanben}--></td>
	</tr></table>
<br />

  <table width="100%" border="1" cellspacing="0" cellpadding="0">
  <tr>
    <td bgcolor="#F7F7F7"><strong class="y">补丁更新列表 </strong>      <script type="text/javascript" src="http://www.yanwee.com/api.php?mod=js&bid=2"></script></td>
  </tr>
</table>


  
    
    </td>
  </tr>
</table>

    
    


<!--{include file="admin/footer.tpl"}-->