<!--{include file="admin/header.tpl"}-->
<!--{if $cfg.page.action=='edit'}-->
<!--{if $dd_id=='1'}-->
<form name="addItem" method="post" action="dd.php?action=edit" onsubmit="return validator(this)">
<input type="hidden" name="di_id" value="<!--{$diInfo.di_id}-->" />
<input type="hidden" name="dd_id" value="<!--{$dd_id}-->" />
<table cellpadding="1" class="table_form">
	<caption>增加新字典值</caption>
	<tr>
		<td>
值：<input name="di_value" value="<!--{$diInfo.di_value}-->" valid="required|isNumber|isNo" noValue="0" errmsg="值不能为空!|值必须为数字|值不允许为0" />
   所属区域：
<select name="p_id">
<option value="0" style="color:red">一级区域</option>
<!--{foreach item=item from=$list1}-->
<option value="<!--{$item.di_id}-->" <!--{if $item.di_id==$diInfo.p_id}-->selected<!--{/if}--> ><!--{$item.di_caption}--></option>
<!--{/foreach}-->
</select>
名称:<input name="di_caption" value="<!--{$diInfo.di_caption}-->" valid="required" errmsg="名称不能为空!" /> <input type="submit" value="保存" />
		</td>
	</tr>
</table>
</form>
<!--{else}-->
<form name="addItem" method="post" action="dd.php?action=edit" onsubmit="return validator(this)">
<input type="hidden" name="di_id" value="<!--{$diInfo.di_id}-->" />
<input type="hidden" name="dd_id" value="<!--{$dd_id}-->" />
<table cellpadding="1" class="table_form">
	<caption>增加新字典值</caption>
	<tr>
		<td>
值：<input name="di_value" value="<!--{$diInfo.di_value}-->" valid="required|isNumber|isNo" noValue="0" errmsg="值不能为空!|值必须为数字|值不允许为0" />
名称:<input name="di_caption" value="<!--{$diInfo.di_caption}-->" valid="required" errmsg="名称不能为空!" /> <input type="submit" value="保存" />
		</td>
	</tr>
</table>
</form>
<!--{/if}-->


<form name="myform" method="post" action="dd.php?action=delete&dd_id=<!--{$dd_id}-->">
<table cellpadding="2" cellspacing="1" class="table_list">
 <caption>字典值列表</caption>
  <tr>
    <th width="30">选中</th>
    <th width="60">排序</th>
    <th width="200">值</th>
    <th>名称</th>
    <th width="60">分组</th>
    <th width="150">操作</th>
  </tr>
 <!--{if $dd_id=='1'}--> 

<!--{section name=class loop=$class}-->
  <tr style="background:#E7F5FE">
    <td class="align_c"><input type="checkbox" name="dds[]" value="<!--{$class[class].di_id}-->"></td>
    <td class="align_c"><input type="text" name="list_order[<!--{$class[class].di_id}-->]" value="<!--{$class[class].list_order}-->" size="3"></td>
    <td class="align_c"><b><!--{$class[class].di_value}--></b></td>
    <td> <b><!--{$class[class].di_caption}--></b></td>
    <td><input type="text" name="list_group[<!--{$class[class].di_id}-->]" value="<!--{$class[class].list_group}-->" size="3"></td>
    <td class="align_c"><a href="?action=edit&di_id=<!--{$class[class].di_id}-->&dd_id=<!--{$dd_id}-->">编辑</a></td>
  </tr>
    <!--{section name=subclass loop=$class[class].son}-->
  <tr>
    <td class="align_c"><input type="checkbox" name="dds[]" value="<!--{$class[class].son[subclass].di_id}-->"></td>
    <td class="align_c"><input type="text" name="list_order[<!--{$class[class].son[subclass].di_id}-->]" value="<!--{$class[class].son[subclass].list_order}-->" size="3"></td>
    <td class="align_c"><!--{$class[class].son[subclass].di_value}--></td>
    <td>&nbsp;-<!--{$class[class].son[subclass].di_caption}--></td>
    <td><input type="text" name="list_group[<!--{$class[class].son[subclass].di_id}-->]" value="<!--{$class[class].son[subclass].list_group}-->" size="3"></td>
    <td class="align_c"><a href="?action=edit&di_id=<!--{$class[class].son[subclass].di_id}-->&dd_id=<!--{$dd_id}-->">编辑</a></td>
  </tr>
    <!--{/section}-->
<!--{/section}-->

  
 <!--{else}-->
<!--{foreach item=item from=$list}-->
  <tr>
    <td class="align_c"><input type="checkbox" name="dds[]" value="<!--{$item.di_id}-->"></td>
    <td class="align_c"><input type="text" name="list_order[<!--{$item.di_id}-->]" value="<!--{$item.list_order}-->" size="3"></td>
    <td class="align_c"><!--{$item.di_value}--></td>
    <td> <!--{$item.di_caption}--></td>
    <td><input type="text" name="list_group[<!--{$item.di_id}-->]" value="<!--{$item.list_group}-->" size="3"></td>
    <td class="align_c"><a href="?action=edit&di_id=<!--{$item.di_id}-->&dd_id=<!--{$dd_id}-->">编辑</a></td>
  </tr>
<!--{/foreach}-->
 <!--{/if}-->
</table>
<div class="button_box">
	<span style="width:60px"><a href="#" onclick="javascript:$('input[type=checkbox]').attr('checked', true);return false;">全选</a>/<a href="#" onclick="javascript:$('input[type=checkbox]').attr('checked', false)">取消</a></span>
	<input type="button" name="delete" value=" 排序 " onclick="myform.action='?action=order&dd_id=<!--{$dd_id}-->';myform.submit();">
	<input type="button" name="delete" value=" 删除 " onclick="if(confirm('你确认删除选中的条目么？')){myform.submit();}">
</div>
</form>
<!--{else}-->
<form name="myform" method="post" action="dd.php?action=delete">
<table cellpadding="2" cellspacing="1" class="table_list">
<caption>字典列表</caption>
   <tr >
	 <th width="30">ID</th>
     <th>名称</th>
     <th width="150">操作</th>
   </tr>
   <!--{foreach item=item from=$list}-->
   <tr>     
     <td class="align_c"><!--{$item.dd_id}--></td>
     <td><!--{$item.dd_caption}--></td>
     <td class="align_c"><a href="?action=edit&dd_id=<!--{$item.dd_id}-->">编辑</a></td>
   </tr>
  <!--{/foreach}-->
</table>
 </form>
<!--{/if}-->
<!--{include file="admin/footer.tpl"}-->
