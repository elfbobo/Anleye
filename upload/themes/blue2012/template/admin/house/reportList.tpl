<!--{include file="admin/header.tpl"}-->
<!--列表-->
<table cellpadding="0" cellspacing="1" class="table_info">
  <caption>房源虚假举报管理</caption>
  <tr>
    <td>
     <a href="report.php"><!--{if !isset($smarty.get.status) }--><font class="red">全部</font><!--{else}-->全部<!--{/if}--></a> | 
     <a href="report.php?status=0"><!--{if isset($smarty.get.status) && $smarty.get.status==0}--><font class="red">未审核</font><!--{else}-->未审核<!--{/if}--></a> | 
     <a href="report.php?status=1"><!--{if $smarty.get.status==1}--><font class="red">已审核</font><!--{else}-->已审核<!--{/if}--></a> | 
     <a href="report.php?type=sell"><!--{if $smarty.get.type=='sell'}--><font class="red">出售</font><!--{else}-->出售<!--{/if}--></a> |
     <a href="report.php?type=rent"><!--{if $smarty.get.type=='rent'}--><font class="red">出租</font><!--{else}-->出租<!--{/if}--></a> 
    </td>
  </tr>
</table>
<form name="myform" method="post" action="">
<table cellpadding="2" cellspacing="1" class="table_list">
	<caption>信息列表</caption>
	<tr>
		<th width="30">选中</th>
		<th>房源类型</th>
		<th>房源信息</th>
		<th>举报人</th>
		<th>被举报人</th>
		<th>举报原因</th>
		<th>状态</th>
		<th>申请时间</th>
	</tr>
<tbody>
<!--{foreach item=item from=$dataList }-->
  <tr>
  	<td class="align_c" ><input type="checkbox" name="ids[]" value="<!--{$item.id}-->" title="<!--{$item.username}-->"></td>
	<td class="align_c"><!--{if $item.house_type=="sell"}-->出售<!--{/if}--><!--{if $item.house_type=="rent"}-->出租<!--{/if}--></td>
	<td class="align_l"><a target="_blank" href="<!--{if $item.house_type=="sell"}-->../../sale/detail.php?id=<!--{$item.house_id}--><!--{else}-->../../rent/detail.php?id=<!--{$item.house_id}--><!--{/if}-->">小区：<!--{$item.house.borough_name}--><br/><!--{$item.house.house_room}-->室<!--{$item.house.house_hall}-->厅<!--{$item.house.house_toilet}-->卫<!--{$item.house.house_veranda}-->阳</a> &nbsp; <!--{$item.house.house_totalarea}-->平 &nbsp;<!--{$item.house.house_price}--><!--{if $item.house_type=="sell"}-->万<!--{else}-->元<!--{/if}--></td>
	<td class="align_c">游客</td>
	<td class="align_c">经纪人：<a href="<!--{$cfg.url_shop}--><!--{$item.report_target.id}-->" target="_blank"><!--{$item.report_target.realname}-->[<!--{$item.report_target.username}-->]</td>
	<td class="align_l"><!--{$item.reason}--></td>
	<td class="align_c"><!--{if $item.status==0}-->未处理<!--{/if}--><!--{if $item.status==1}--><font color="red">已通过</font><!--{/if}--><!--{if $item.status==2}--><font color="red">已删除</font><!--{/if}--></td>
	<td class="align_c"><!--{$item.addtime|date_format:"%Y-%m-%d %T"}--></td>
  </tr>
<!--{/foreach}-->
</tbody>
</table>
<div class="button_box">
	<span style="width:60px"><a href="#" onclick="javascript:$('input[type=checkbox]').attr('checked', true);return false;">全选</a>/<a href="#" onclick="javascript:$('input[type=checkbox]').attr('checked', false)">取消</a></span>
	<input type="button" name="lock" value=" 审核通过 " onclick="if(confirm('你确认审核通过选中的条目么？')){myform.action='?action=status&dostatus=1';myform.submit();}">
	<input type="button" name="delete" value=" 删除 " onclick="if(confirm('你确认删除选中的条目么？')){myform.action='?action=status&dostatus=2';myform.submit();}">
</div>
<!--{$pagePanel}-->

</form>
<div id="pic_shower" style="position:absolute;color:white; width:640px; right:-30px; top:-10px; z-index:10000;background-color:#6680aa;display:none;"></div>
<script language="javascript">
function showPic(picUrl){
	//alert('aaaa');
	if (!e) var e = window.event;

	if(!document.all){
        mouse_x=e.pageX;
        mouse_y=e.pageY;
    }else{
        mouse_x=document.body.scrollLeft+event.clientX;
        mouse_y=document.body.scrollTop+event.clientY;
    }
    //便于跟踪调试
    $('#pic_shower').html('<img src="<!--{$cfg.url}-->upfile/'+picUrl+'">');
    $('#pic_shower').css('left',mouse_x+10).css('top',mouse_y+10).css('display','block');

}
function hidePic(){
	//alert('bbb');
	$('#pic_shower').css('display','none');
}
$(document).click(function(){hidePic();});
</script>
<!--{include file="admin/footer.tpl"}-->
