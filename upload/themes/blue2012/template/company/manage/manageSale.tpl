<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="X-UA-Compatible" content="IE=EmulateIE7" />
<meta http-equiv="Content-Type" content="text/html; charset=gb2312" />
<title><!--{$cfg.page.title}--> - 房源管理</title>
<!--{$cssFiles}-->
<!--{$jsFiles}-->

</head>
<body>
<!--{include file="inc/companyHeader.tpl"}-->
<div class="main">
	<div class="page">
<!--{include file="inc/company_left.tpl"}-->
		<div class="memberBox">
		
			<div class="manageSub">
				<ul class="manageSubNav">
					<li class="linkOn"><a href="#"><span>出售中</span></a></li>
					<li><a href="manageSaleRecycle.php"><span>回收站</span></a></li>
					
				</ul>
			</div>
<div class="manageBox">
				<div class="houseSearch">
					<form name="searchForm" action="" method="GET">
						<input type="text" class="input" name="q" size="30" value="<!--{if $q}--><!--{$q}--><!--{else}-->输入房源编号或小区名称<!--{/if}-->" onblur="if(this.value ==''||this.value == '输入房源编号或小区名称'){this.value = '输入房源编号或小区名称';}" onfocus="if(this.value == '输入房源编号或小区名称'){this.value = '';}" />&nbsp;<input type="button" value="搜 索" onclick="javascript:document.searchForm.submit();" />&nbsp;<a href="#notice">出售房源管理必读</a><span class="tip">您公司已发布<span class="f90"><!--{$houseNum}--></span>条出售房源</span>
					</form>
				</div>
				<div class="houseList">
					<table border="0" cellpadding="0" cellspacing="1">
						<thead class="tableTitle">
							<tr>
								<td colspan="7">房源基本信息</td>
								<td colspan="4">经纪人信息</td>
							</tr>
						</thead>
						<thead class="tableSubTitle">
							<tr>
								<td width="7%">房源编号</td>
								<td width="12%">小区名称</td>
								<td width="7%">户型</td>
								<td width="7%">面积</td>
								<td width="7%">点击</td>
								<td width="13%">发布时间</td>
								<td width="14%">售价</td>
								<td width="11%">经纪人</td>					
								<td width="22%">门店</td>
                                
							</tr>
						</thead>
						<tbody>
						<form name="myform" enctype="text/html" action="" method="POST">
							<input type="hidden" name="to_url" value="<!--{$to_url}-->">
							<!--{foreach name=dataList from=$dataList item=item key=key}-->
							<tr>
								<td><input type="checkbox" name="ids[]" value="<!--{$item.id}-->" />&nbsp;<a href="<!--{$cfg.url_sale}-->d-<!--{$item.id}-->.html" title="查看房源详细信息" target="_blank"><!--{$item.house_no}--></a></td>
								<td><!--{$item.borough_name}--></td>
								<td><!--{$item.house_room}-->-<!--{$item.house_hall}-->-<!--{$item.house_toilet}-->-<!--{$item.house_veranda}--></td>
								<td><!--{$item.house_totalarea}-->㎡</td>
								<td class="f90"><!--{$item.click_num}--></td>
								<td class="f90"><!--{$item.created|date_format:'%Y-%m-%d'}--></td>
								<td class="f90"><!--{if $item.house_price == 0}-->面议<!--{else}--><!--{$item.house_price}-->万<!--{/if}-->								  </td>
								<td><a target="_blank" href="<!--{$cfg.url}-->shop/<!--{$item.brokerInfo.id}-->"><!--{$item.brokerInfo.realname}--></a></td>		
        <td><!--{$item.brokerInfo.outlet_last}--></td>
						    
                                            </tr>
                                            <!--{/foreach}-->
                                            <tr>
                                                <td colspan="11" class="listOperation">
                                                    <label for="checkBoxAll"><input type="checkbox" id="checkBoxAll" name="checkBoxAll" value="1" onclick="checkAll(this);" />全选</label>
                                                    <input class="listOperationBtn" type="button" value="下架选中的房源" onclick="if(confirm('你确认下架选中的房源么？')){myform.action='?action=notSell';myform.submit();}" />
                                                </td>
                                            </tr>
                                    </form>
                                </tbody>
                            </table>
                            <!--{$pagePanel}-->
</div>
                    </div>
                    <div class="note">
                       <p><a name="notice" id="notice"></a>出售房源管理必读：</p>
				<ul>
					
					
					<li>点击房源编号链接浏览房源详细信息；</li>
					<li>下架房源将进入回收站，可以在回收站中彻底删除；</li>
				</ul>
			</div>
		</div>
	</div>
	
</div>
<script language="javascript">
function checkAll(formObj){
	if(formObj.checked){
		$('input[type=checkbox]').attr('checked', true);	
	}else{
		$('input[type=checkbox]').attr('checked', false);
	}
}
function showBox(item_id){
	TB_show('房源成交','saleBargain.php?house_id='+item_id+'&height=370&width=400&modal=true&rnd='+Math.random(),false);
}
function showBoxOwner(item_id){
	TB_show('业主信息','landlordSaleInfo.php?house_id='+item_id+'&height=150&width=400&modal=true&rnd='+Math.random(),false);
}
function showBoxtop(item_id){
	TB_show('房源置顶','saleTop.php?house_id='+item_id+'&height=200&width=400&modal=true&rnd='+Math.random(),false);
}
function showBoxnum(){
	TB_show('购买条数','saleNum.php?height=200&width=400&modal=true&rnd='+Math.random(),false);
}
</script>
<script src="<!--{$cfg.path.js}-->My97DatePicker/WdatePicker.js" language="javascript"></script>
</body>
</html>
