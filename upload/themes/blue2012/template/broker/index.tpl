<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<!--{include file="inc/head.tpl"}-->
<link href="<!--{$cfg.path.css}-->add.css" rel="stylesheet" type="text/css" />
<script type="text/javascript">
function changeSearch(formDom){
	location.href=formDom.value;
}

function quickSearch(domValue,domType){
	var inputDom = document.getElementById(domType);
	if(inputDom){
		inputDom.value=domValue;
	}
	var cityareaValue = document.getElementById('cityarea').value;
	var qValue = document.getElementById('S_q').value;
	var typeValue = document.getElementById('type').value;
	var avgprice = document.getElementById('avgprice').value;
	var boroughValue = document.getElementById('borough').value;
	var list_numValue = document.getElementById('list_num').value;
	var list_orderValue = document.getElementById('list_order').value;
	if(domType == 'cityarea') boroughValue = '';
	location.href='index.php?q='+qValue+'&borough='+boroughValue+'&cityarea='+cityareaValue+'&type='+typeValue+'&list_num='+list_numValue+'&list_order='+list_orderValue+'&avgprice='+avgprice;
	return false;
}
</script>
</head>

<body>
<!--{include file="inc/top.tpl"}-->

<!--{include file="inc/city_selecter_broker.tpl"}-->
<div style="height:20px; clear:both; overflow:hidden;"></div>
<div class="content">
 <div class="w746">
       <!--{include file="inc/broker_content_head.tpl"}-->
        <div class="agent_list">
        	<ul>
           <!--{foreach from=$dataList item=item key=key}-->
            <li class="cur">
            <div class="agent_img">
                <a href="<!--{$cfg.url_shop}--><!--{$item.id}-->" target="_blank" title="<!--{$item.realname}-->-<!--{$item.outlet}-->"><img src="<!--{if $item.avatar}--><!--{$cfg.url_upfile}--><!--{$item.avatar}--><!--{else}--><!--{$cfg.path.images}-->demoPhoto.jpg<!--{/if}-->" alt="" />
                </a><span><a href="<!--{$cfg.url_shop}--><!--{$item.id}-->" target="_blank">进入店铺</a></span>
            </div><!--end agent_img-->
            <div class="agent_txt">
                <div class="agent_name">
                <b><a href="<!--{$cfg.url_shop}--><!--{$item.id}-->" target="_blank"><!--{$item.realname}--></a></b>
               <!-- <div class="star"><p></p></div> --><div><img src="<!--{$cfg.path.images}-->rank/<!--{$item.brokerRank}-->.png"
/></div><!--end star-->
                </div><!--end agent_name-->
                <p>所属公司：<!--{if $item.company_name}--><a target="_blank" href="<!--{$cfg.url}-->cshop/<!--{$item.company_id}-->"><!--{$item.company_name}--> - <!--{$item.outlet_last}--></a><!--{else}--><!--{$item.outlet_first}--> - <!--{$item.outlet_last}--><!--{/if}--></p>
                <p>服务区域：<a href="<!--{$cfg.url_sale}-->index.php?cityarea=<!--{$item.cityarea_id}-->"><!--{$item.cityarea_name}--></a></p>
                <p>联系电话：<span><!--{$item.mobile}--></span></p>
                <p class="gray">最近登录：<!--{$item.last_login}-->前</p>
            </div><!--end agent_txt-->
            </li>
            <!--{/foreach}-->
            </ul>
            <div class="jjr-div">
		        <div class="contain">
            <!-- 小区列表分页 -->
            <!--{$pagePanel}-->
            <!-- 小区列表分页 结束 -->                </div>
		        <div class="result">共找到 <span style="color:#61a00d; font-weight:bold;"><!--{$row_count}--></span> 位符合要求的经纪人</div>
   	  </div><!--end page-->
        </div><!--end agent_list-->
    </div> 
<div class="w230">
    	<div class="agent_box">
        	<div class="agent_boxt">
            	<h2>推荐经纪人</h2>
            </div><!--end agent_boxt-->
            <div class="agent_boxc">
            	<div class="agent_rec">
            	<table width="100%" border="0" cellspacing="0" cellpadding="0">
                 <!--{foreach from=$arrRecommendBroker item=item key=key}-->
                  <tr>
                    <td align="left" width="40%"><a href="<!--{$cfg.url_shop}--><!--{$item.id}-->" class="agent_recl" target="_blank"><!--{$item.realname}--></a></td>
                    <td align="right"></td>
                    <td><img src="<!--{$cfg.path.images}-->rank/<!--{$item.brokerRank}-->.png"
/></td>
                  </tr>
                  <!--{/foreach}-->
                </table>
                </div><!--end agent_rec-->
            </div><!--end agent_boxc-->
            <div class="agent_boxb"></div><!--end agent_boxb-->
        </div><!--end agent_box-->
    </div>
</div>
<!--{include file="inc/foot.tpl"}-->
</body>
</html>