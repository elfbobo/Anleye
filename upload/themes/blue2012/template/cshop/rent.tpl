<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=gb2312" />
<title><!--{$cfg.page.title}--></title>
<meta name="keywords" content="<!--{$cfg.page.keyword}-->" />
<meta name="description" content="<!--{$cfg.page.description}-->" />
 
<!--{$cssFiles}-->
<!--{$jsFiles}-->
</head>
<body>

<div class="head">
	<div class="headMain">
    	<div class="title"><!--{$dataInfo.company_name}--></div>
        <div class="link">主页地址：<!--{$cfg.url}-->cshop/<!--{$dataInfo.id}-->&nbsp;&nbsp;<a href="#"  onclick="javascript:window.external.AddFavorite('<!--{$cfg.url}-->cshop/<!--{$dataInfo.id}-->',' <!--{$cfg.page.title}-->--<!--{$dataInfo.company_name}-->');return false;">收藏</a></div>
        <ul class="headNav">
        	<li><a href="<!--{$cfg.url_cshop}--><!--{$dataInfo.id}-->"><span>公司首页</span></a></li>
        	<li><a href="<!--{$cfg.url_cshop}-->sale.php?id=<!--{$dataInfo.id}-->"><span>二手房</span></a></li>
        	<li class="linkOn"><a href="<!--{$cfg.url_cshop}-->rent.php?id=<!--{$dataInfo.id}-->"><span>租房</span></a></li>
       	<li><a target="_blank" href="<!--{$cfg.url_broker}-->index.php?company_id=<!--{$dataInfo.id}-->"><span>团队精英</span></a></li>
        	<li><a href="<!--{$cfg.url_cshop}-->p-<!--{$dataInfo.id}-->.html"><span>关于我们</span></a></li>
		
        </ul>
    </div>
</div>

<div class="main">
	<div class="mainLeft">
        <div class="comMainBox">
        <!-- 二手房列表 -->
		<div class="dataListBox">
			<table class="tHead" cellpadding="0" cellspacing="1" border="0">
				<thead>
					<tr>
						<td width="13%">图片</td>
						<td width="32%">标题/小区名/户型</td>
						<td width="11%"><a href="?id=<!--{$dataInfo.id}-->&borough_id=<!--{$smarty.get.borough_id}-->&price=<!--{$smarty.get.price}-->&room=<!--{$smarty.get.room}-->&list_order=<!--{if $smarty.get.list_order=='house_totalarea desc'}-->house_totalarea asc<!--{else}-->house_totalarea desc<!--{/if}-->">面积<!--{if $smarty.get.list_order=='house_totalarea desc'}--><img src="<!--{$cfg.path.images}-->cshop/arrow_down.gif"><!--{elseif $smarty.get.list_order=='house_totalarea asc'}--><img src="<!--{$cfg.path.images}-->cshop/arrow_up.gif"><!--{/if}--></a></td>
						<td width="11%">楼层</td>
						<td width="15%"><a href="?id=<!--{$dataInfo.id}-->&borough_id=<!--{$smarty.get.borough_id}-->&price=<!--{$smarty.get.price}-->&room=<!--{$smarty.get.room}-->&list_order=<!--{if $smarty.get.list_order=='house_price desc'}-->house_price asc<!--{else}-->house_price desc<!--{/if}-->">租金/月<!--{if $smarty.get.list_order=='house_price desc'}--><img src="<!--{$cfg.path.images}-->cshop/arrow_down.gif"><!--{elseif $smarty.get.list_order=='house_price asc'}--><img src="<!--{$cfg.path.images}-->cshop/arrow_up.gif"><!--{/if}--></a></td>
						<td width="18%"><a href="?id=<!--{$dataInfo.id}-->&borough_id=<!--{$smarty.get.borough_id}-->&price=<!--{$smarty.get.price}-->&room=<!--{$smarty.get.room}-->&list_order=<!--{if $smarty.get.list_order=='created desc'}-->created asc<!--{else}-->created desc<!--{/if}-->">发布时间<!--{if $smarty.get.list_order=='created desc'}--><img src="<!--{$cfg.path.images}-->cshop/arrow_down.gif"><!--{elseif $smarty.get.list_order=='created asc'}--><img src="<!--{$cfg.path.images}-->cshop/arrow_up.gif"><!--{/if}--></a></td>
					</tr>
				</thead>
			</table>
			<table class="tBody" cellpadding="0" cellspacing="0" border="0">
				<tbody>
					<!--{foreach from=$dataList item=item key=key}-->
					<!-- 循环体 -->
					<tr onmouseover="this.className='mouseOver'" onmouseout="this.className=''">
						<td width="13%"><span class="thumbnail"><a href="<!--{$cfg.url_rent}-->d-<!--{$item.id}-->.html" title="<!--{$item.borough_name}-->" target="_blank"><img src="<!--{if $item.house_thumb}--><!--{$cfg.url}-->upfile/<!--{$item.house_thumb}--><!--{else}--><!--{$cfg.path.images}-->housePhotoDefault.gif<!--{/if}-->" width="80" height="60" /></a></span></td>
						<td class="tdAlignLeft" width="32%">
							<p><a class="color000" href="<!--{$cfg.url_rent}-->d-<!--{$item.id}-->.html" title="<!--{$item.borough_name}-->" target="_blank"><!--{$item.house_title}--></a></p>
							<p><span class="weightBold"><!--{$item.borough_name}--></span>&nbsp;<!--{$item.house_room}-->室<!--{$item.house_hall}-->厅<!--{$item.house_toilet}-->卫<!--{$item.house_veranda}-->阳</p>
							<p>
						
							</p>
						</td>
						<td width="11%"><span class="familyAlpha"><!--{$item.house_totalarea}-->㎡</span></td>
						<td width="11%"><span class="familyAlpha"><!--{$item.house_floor}-->F/<!--{$item.house_topfloor}-->F</span></td>
						<td width="15%">
						<p><span class="familyAlpha colorF60"><span class="size16px weightBold"><!--{$item.house_price}-->元</span></p>
						</td>
						<td class="tdAlignLeft" width="18%">
							<p><!--{$item.updated|date_format:'%Y-%m-%d %T'}--></p>
						</td>
					</tr>
					<!-- 循环体 结束 -->
					<!--{/foreach}-->
				</tbody>
			</table>
		</div>
		<!-- 二手房列表 结束 -->

		<!-- 二手房列表分页 -->
		<!--{$pagePanel}-->
		<!-- 二手房列表分页 结束 -->

        </div>
    </div>
    
    <div class="mainRight">
        <div class="comMainBox"><div class="boxBg">
        	<div class="boxHead">房源搜索</div>
            <div class="houseSeach">
				<form name="searchForm" action="rent.php" method="GET">
					<input type="hidden" name="id" value="<!--{$dataInfo.id}-->">
					<input class="inps" name="q" type="text" size="30" style="width:175px;" value="<!--{if $smarty.get.q}--><!--{$smarty.get.q}--><!--{else}-->输入小区名或房源编号<!--{/if}-->" onblur="if(this.value ==''||this.value == '输入小区名或房源编号'){this.value = '输入小区名或房源编号';this.style.color = '#999999';}" onfocus="if(this.value == '输入小区名或房源编号'){this.value = '';this.style.color = '#333333';}" style="color:#999;" />
					<input class="searchSmt" type="submit" value="搜 索" />
				</form>
			</div>
        </div></div>
        
        <div class="comMainBox"><div class="boxBg">
        	<div class="boxHead">房源分类</div>
            <div class="houseType">
                <div class="TypeItemBox" id="houseType2">
                	<p class="TypeItemTitle">按小区：</p>
                    <ul class="TypeItem">
                    	<!--{foreach from=$rentCountBorough key=key item=item}-->
                    	<li><a href="rent.php?id=<!--{$dataInfo.id}-->&borough_id=<!--{$item.borough_id}-->"><!--{$item.borough_name}-->(<!--{$item.house_num}-->)</a></li>
                    	<!--{/foreach}-->
                    </ul>
                	<p class="TypeItemTitle">按租金：</p>
                    <ul class="TypeItem">
                    	<!--{foreach from=$rentCountPrice key=key item=item}-->
                    	<li><a href="rent.php?id=<!--{$dataInfo.id}-->&price=<!--{$item.price}-->"><!--{$item.house_price}-->(<!--{$item.house_num}-->)</a></li>
                    	<!--{/foreach}-->
                    </ul>
                	<p class="TypeItemTitle">按户型：</p>
                    <ul class="TypeItem TypeItem1">
                    	<!--{foreach from=$rentCountRoom key=key item=item}-->
                    	<li><a href="rent.php?id=<!--{$dataInfo.id}-->&room=<!--{$item.room}-->"><!--{$item.house_room}-->(<!--{$item.house_num}-->)</a></li>
                    	<!--{/foreach}-->
                    </ul>
                </div>
            </div>
        </div></div>
    </div>
</div>
    

</body>
</html>
