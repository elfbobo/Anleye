<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="X-UA-Compatible" content="IE=EmulateIE7" />
<meta http-equiv="Content-Type" content="text/html; charset=gb2312" />
<title><!--{$cfg.page.title}--> - 谁来看过我</title>
<!--{$cssFiles}-->
<!--{$jsFiles}-->
</head>
<body>
<!--{include file="inc/memberHeader.tpl"}-->
<div class="main">
	<div class="page">
<!--{include file="inc/member_left.tpl"}-->
		<div class="memberBox">
			<div class="msgOperationBox">
				<ul class="msgOperation">
					<li><a href="snsFriends.php"><span>我的好友</span></a></li>
					<li class="linkOn"><a href="snsLinkIn.php"><span>谁来看过我</span></a></li>
					<li><a href="snsInviteIn.php"><span>好友邀请</span></a></li>
				</ul>
			</div>
			<div class="manageBox">
				<div class="friendsList">
					<table border="0" cellpadding="0" cellspacing="1">	
						<tbody class="friendsLinkIn">
							<tr>
								<!--{foreach name=list from=$dataList key=key item=item}-->
								<td class="alphaFontFamily">
									<span>
										<a href="<!--{$cfg.url_shop}--><!--{$item.friend_id}-->" title="<!--{$item.realname}-->" target="_blank"><!--{if $item.avatar}--><img class="demoImgBorder" src="<!--{$cfg.url}-->upfile/<!--{$item.avatar}-->" width="42" height="50" /><!--{else}--><img class="demoImgBorder" src="<!--{$cfg.path.images}-->demoPhoto.jpg" width="42" height="50" /><!--{/if}--></a>
									</span>
									<span>
										<p><a class="font14px" href="<!--{$cfg.url_shop}--><!--{$item.friend_id}-->" target="_blank"><!--{if $item.realname}--><!--{$item.realname}--><!--{else}--><!--{$item.username}--><!--{/if}--></a></p>
										<p><!--{$item.outlet}--></p>
										<p>来访：<!--{$item.add_time|date_format:'%Y-%m-%d %T'}--></p>
									</span>
								</td>
								<!--{if $smarty.foreach.list.iteration % 3 == 0}-->
								<tr>
								</tr>
								<!--{/if}-->
								<!--{/foreach}-->
							</tr>
						</tbody>
					</table>
					<!--{$pagePanel}-->
				</div>
			</div>
		</div>
	</div>
	
</div>
</body>
</html>