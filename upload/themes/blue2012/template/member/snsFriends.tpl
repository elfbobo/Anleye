<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="X-UA-Compatible" content="IE=EmulateIE7" />
<meta http-equiv="Content-Type" content="text/html; charset=gb2312" />
<title><!--{$cfg.page.title}--> - 查看好友</title>
<!--{$cssFiles}-->
<!--{$jsFiles}-->
</head>
<body>
<!--{include file="inc/memberHeader.tpl"}-->
<div class="main">
	<div class="page">
<!--{include file="inc/member_left.tpl"}-->
		<div class="memberBox">
			<div class="msgOperationBox">
				<ul class="msgOperation">
					<li class="linkOn"><a href="snsFriends.php"><span>我的好友</span></a></li>
					<li><a href="snsLinkIn.php"><span>谁来看过我</span></a></li>
					<li><a href="snsInviteIn.php"><span>好友邀请</span></a></li>
				</ul>
			</div>
			<div class="manageBox">
				<div class="friendsList">
					<!--{foreach name=list from=$dataList key=key item=item}-->
					<table border="0" cellpadding="0" cellspacing="0" onmouseover="document.getElementById('friendsListExt_<!--{$smarty.foreach.list.iteration}-->').style.display=''" onmouseout="document.getElementById('friendsListExt_<!--{$smarty.foreach.list.iteration}-->').style.display='none'">	
						<tbody>
							<tr>
								<td class="rowPhoto"><a href="<!--{$cfg.url_shop}--><!--{$item.friend_id}-->" title="<!--{$item.realname}-->" target="_blank"><!--{if $item.avatar}--><img class="demoImgBorder" src="<!--{$cfg.url}-->upfile/<!--{$item.avatar}-->" width="42" height="50" /><!--{else}--><img class="demoImgBorder" src="<!--{$cfg.path.images}-->demoPhoto.jpg" width="42" height="50" /><!--{/if}--></a></td>
								<td class="rowName" colspan="2">
									<p class="p1"><span><a class="font14px fontBold" href="<!--{$cfg.url_shop}--><!--{$item.friend_id}-->" target="_blank"><!--{$item.realname}--></a>&nbsp;<!--{$item.broker_type}--></span><!--{if $item.birth_remember}--><span><img src="<!--{$cfg.path.images}-->snsBirth.gif" align="absmiddle" /> 生日提醒：<a class="underline" href="msgWrite.php?msg_to=<!--{$item.username}-->" title="发送生日祝福"><!--{$item.realname}--> 快过生日了</a></span><!--{/if}--></p>
									<p class="p2"><span class="alphaFontFamily">手机：<!--{$item.mobile}--></span><span class="alphaFontFamily">出售：<a class="underline" href="<!--{$cfg.url_shop}-->sale.php?id=<!--{$item.friend_id}-->" target="_blank"><!--{$item.sell_num}-->套</a></span></span><span class="alphaFontFamily">出租：<a class="underline" href="<!--{$cfg.url_shop}-->rent.php?id=<!--{$item.friend_id}-->" target="_blank"><!--{$item.rent_num}-->套</a></span></p>
								</td>
								<td><img src="<!--{$cfg.path.images}-->msgUnread.gif" /> <a href="msgWrite.php?msg_to=<!--{$item.username}-->">发送消息</a></td>
							</tr>
						</tbody>
						<tbody id="friendsListExt_<!--{$smarty.foreach.list.iteration}-->" class="friendsListExt" style="display:none">
							<tr>
								<td colspan="4">
									<span class="alphaFontFamily">E-mail：<!--{$item.email}--></span>
									<span class="alphaFontFamily">QQ：<!--{$item.qq}--></span>
									<span class="alphaFontFamily">MSN：<!--{$item.msn}--></span>
								</td>
							</tr>
							<tr>
								<td colspan="4">
									<span class="alphaFontFamily">服务区域：<!--{$item.cityarea_name}--></span>
									<span class="alphaFontFamily">门店地址：<!--{$item.outlet_addr}--></span>
								</td>
							</tr>
							<tr>
								<td colspan="4" class="tdContRight"><img src="<!--{$cfg.path.images}-->deleteIcon.gif" align="absmiddle" /><a href="snsFriends.php?action=delete&id=<!--{$item.id}-->" onclick="if(confirm('确定要删除该好友吗？')){return true;}else{return false;}">删除好友</a></td>
							</tr>
						</tbody>
					</table>
					<!--{/foreach}-->
					<!--{$pagePanel}-->
				</div>
			</div>
		</div>
	</div>
	
</div>
</body>
</html>