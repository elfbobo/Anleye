<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="X-UA-Compatible" content="IE=EmulateIE7" />
<meta http-equiv="Content-Type" content="text/html; charset=gb2312" />
<title><!--{$cfg.page.title}--> - 用户中心</title>
<!--{$cssFiles}-->
<!--{$jsFiles}-->
</head>
<body>
<!--{include file="inc/memberHeader.tpl"}-->
<div class="main">
	<div class="page">
<!--{include file="inc/member_left.tpl"}-->
		<div class="memberBox">
         <!--{if $memberInfo.status ==1}-->
         <div class="bigNote" id="bigNote">
				<div class="noteTxt" style="font-size:14px;">您的帐号尚未开通！请联系 <!--{$cfg.page.rexian}--> 进行开通!</div>
			</div><br />

            <!--{/if}-->
			<!--{if $memberInfo.mobile ==""}-->
			<div class="bigNote" id="bigNote">
				<div class="noteTxt" style="font-size:14px;">您还未<a href="brokerProfile.php">完善经纪人资料</a>！</div>
			</div>
			<!--{/if}-->
			<div class="infoTipBox">
				<div class="loginTip"><!--{$nowTime}-->好，<img src="<!--{if $memberInfo.avatar}--><!--{$cfg.url}-->upfile/<!--{$memberInfo.avatar}--><!--{else}--><!--{$cfg.path.images}-->demoPhoto_52_52.gif<!--{/if}-->" width="20" height="20" align="absmiddle" /><!--{$username}-->&nbsp;<span><a class="underline" href="brokerPhoto.php">修改我的头像</a></span></div>
            
				<div class="universityTip"><a class="underline" href="<!--{$cfg.url_shop}--><!--{$memberInfo.id}-->" target="_blank">进入我的网上店铺</a><span class="shopTip familyAlpha">&nbsp;(<!--{$cfg.url_shop}--><!--{$memberInfo.id}-->)</span></div>
<br />
<br />


                 <h4>帐号信息</h4>
                <hr>
 <div class="memberProfile">
				<ul>
                <li>用户姓名：<span class="familyAlpha color999"><!--{$username}--></span></li>
                
                <li>用户类型：
                <!--{if $memberInfo.vip==1}-->
                <span class="familyAlpha color999">体验套餐</span> 到期时间：<span class="familyAlpha color999"><!--{$vip_totime|date_format:"%Y-%m-%d"}--> <a class="underline" href="#" onclick="showBoxType();return false;">升级为标准套餐</a></span>
                <!--{elseif $memberInfo.vip==2}-->
                <span class="familyAlpha color999">标准套餐</span> 到期时间：<span class="familyAlpha color999"><!--{$vip_totime|date_format:"%Y-%m-%d"}--></span>
                <!--{else}-->
                <span class="familyAlpha color999">普通用户</span> <a class="underline" href="#" onclick="showBoxType();return false;">升级</a>                 <!--{/if}-->
               </li>
                <li>账户状态：<span class="familyAlpha color999"> <!--{if $memberInfo.status ==1}-->未开通 <!--{else}-->正常 <!--{/if}--></span></li>
                     <li>账户余额：<span class="familyAlpha color999"><!--{$memberInfo.money}-->元</span>&nbsp;<span><a class="underline" href="pay.html" target="_blank">我要充值</a></span></li>
                     
                   	<li>服务特长：<span class="familyAlpha color999"><!--{if $memberInfo.specialty}--><!--{$memberInfo.specialty}--><!--{else}-->还没有设置服务特长 <a class="underline" href="brokerProfile.php" >马上设置</a><!--{/if}--></span></li>
                    
					<li>你的等级：<img src="<!--{$cfg.path.images}-->rank/<!--{$memberInfo.brokerRank}-->.png" align="absmiddle" />&nbsp;<span class="familyAlpha color999">(<!--{$memberInfo.scores}-->积分)</span></li>
					<li>你的认证：<!--{if $memberInfo.idcard ==""}--><span>
                    <a class="underline" href="brokerIdentity.php" ><img src="<!--{$cfg.path.images}-->no_brokerIdentity.png" align="absmiddle" alt="未经过 <!--{$cfg.page.titlec}-->实名认证" /></a>
                    </span>
                    <!--{else}-->
               <img src="<!--{$cfg.path.images}-->brokerIdentity.png" align="absmiddle" alt="已通过 <!--{$cfg.page.titlec}-->实名认证" /><!--{/if}-->
               &nbsp;
               <!--{if $memberInfo.aptitude ==""}-->
               <span><a class="underline" href="brokerAptitude.php" ><img src="<!--{$cfg.path.images}-->no_brokerAptitude.png" align="absmiddle" alt="未经过 <!--{$cfg.page.titlec}-->执业认证" /></a></span>
               <!--{else}-->
               <img src="<!--{$cfg.path.images}-->brokerAptitude.png" align="absmiddle" alt="已通过 <!--{$cfg.page.titlec}-->资质认证" /><!--{/if}-->&nbsp;</li>
					<li>活跃指数：<!--{foreach from=$memberInfo.active_str item=item}--><!--{if $item}--><img src="<!--{$cfg.path.images}-->activityYes.gif" title="当天来过" /><!--{else}--><img src="<!--{$cfg.path.images}-->activityNo.gif" title="当天没来过" /><!--{/if}--><!--{/foreach}-->&nbsp;&nbsp;上次登录：<span class="familyAlpha color999"><!--{$memberInfo.last_login|date_format:"%Y-%m-%d %T"}--></span>&nbsp;&nbsp;注册时间：<span class="familyAlpha color999"><!--{$memberInfo.add_time|date_format:"%Y-%m-%d %T"}--></span></li>
				</ul>
			</div><br />


                 <h4>常用事物管理</h4>
                <hr>
                <ul class="infoTip">
					<li><img src="<!--{$cfg.path.images}-->innerMsg.gif" />&nbsp;邮件：<span class="familyAlpha weightBold"><!--{$messageCount}-->&nbsp;</span>封<a class="underline" href="msgInbox.php">未读邮件</a></li>
					
					<li><img src="<!--{$cfg.path.images}-->memberMsnIcon.gif" />&nbsp;好友：<span class="familyAlpha weightBold"><!--{$firendInviteCount}-->&nbsp;</span>个<a class="underline" href="snsInviteIn.php">好友邀请</a></li>
				</ul>
                <br />

                <h4>房源管理</h4>
                <hr>
				<ul class="houseTip">
					<li><a class="underline familyAlpha size14px weightBold" href="manageSale.php">出售中(<!--{$saleCount}-->)</a></li>
					<li><a class="underline familyAlpha size14px weightBold" href="manageRent.php">出租中(<!--{$rentCount}-->)</a></li>
					<li><a class="underline familyAlpha size14px weightBold" href="manageSaleRecycle.php">出售下架(<!--{$saleRecycleCount}-->)</a></li>
					<li><a class="underline familyAlpha size14px weightBold" href="manageRentRecycle.php">出租下架(<!--{$rentRecycleCount}-->)</a></li>    
				</ul>
                  <br />
 <ul class="houseTip">
					<li><a class="underline familyAlpha size14px weightBold" href="manageSaleTop.php">出售置顶(<!--{$saleTopCount}-->)</a></li>
					<li><a class="underline familyAlpha size14px weightBold" href="manageRentTop.php">出租置顶(<!--{$rentTopCount}-->)</a></li>
				</ul>
  
			</div>
			
			
			
			
		</div>
	</div>
	
</div>
<script language="javascript">

function showBoxType(){
	TB_show('服务升级','vipShop.php?height=170&width=610&modal=true&rnd='+Math.random(),false);
}
</script>
<script src="<!--{$cfg.path.js}-->My97DatePicker/WdatePicker.js" language="javascript"></script>
</body>
</html>