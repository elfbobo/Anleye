<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="X-UA-Compatible" content="IE=EmulateIE7" />
<meta http-equiv="Content-Type" content="text/html; charset=gb2312" />
<title><!--{$cfg.page.title}--> - 成交房源管理</title>
<!--{$cssFiles}-->
<!--{$jsFiles}-->
</head>
<body>
<!--{include file="inc/memberHeader.tpl"}-->
<div class="main">
	<div class="page">
<!--{include file="inc/member_left.tpl"}-->
		<div class="memberBox">
			<div class="memberBoxTab">
				<ul>
				  <li><a href="houseSale.php"><span>发布出售</span></a></li>
				  <li><a href="houseRent.php"><span>发布出租</span></a></li>
				  <li><a href="manageSale.php"><span>出售管理</span></a></li>
				  <li><a href="#" class="linkOn"><span>出租管理</span></a></li>
				</ul>
			</div>
			<div class="manageSub">
				<ul class="manageSubNav">
					<li><a href="manageRent.php"><span>出租中</span></a></li>
                    <li><a href="manageRentTop.php"><span>置顶中</span></a></li>
					<li><a href="manageRentRecycle.php"><span>回收站</span></a></li>
					<li class="linkOn"><a href="#"><span>成交榜</span></a></li>
				</ul>
			</div>
			<div class="manageBox">
				<div class="houseSearch">
					<form name="searchForm" action="" method="GET">
						成交时间：<input class="input" id="from_date" name="from_date" type="text" size="10" value="<!--{$smarty.get.from_date}-->" onclick="WdatePicker({maxDate:'#F{$dp.$D(\'to_date\')}',skin:'whyGreen'})" /> - <input class="input" id="to_date"  name="to_date" type="text" size="10" value="<!--{$smarty.get.to_date}-->" onClick="WdatePicker({minDate:'#F{$dp.$D(\'from_date\')}',skin:'whyGreen'})" />&nbsp;
						交易来源：<select class="select" name="">
						  <option value="">不限</option>
						  <!--{html_options options=$bargain_from_option selected=$smarty.get.bargain_from}-->
						</select>&nbsp;
						<input class="input" name="q" type="text" size="36" value="<!--{if $q}--><!--{$q}--><!--{else}-->输入小区名，或租客/房东信息，或备注信息<!--{/if}-->" onblur="if(this.value ==''||this.value == '输入小区名，或租客/房东信息，或备注信息'){this.value = '输入小区名，或租客/房东信息，或备注信息';}" onfocus="if(this.value == '输入小区名，或租客/房东信息，或备注信息'){this.value = '';}" />
						<input type="button" value="查询" onclick="javascript:document.searchForm.submit();" />
					</form>
				</div>
				<div class="houseList">
					<table border="0" cellpadding="0" cellspacing="1">
						<thead class="tableTitle">
							<tr>
								<td colspan="3">房源基本信息</td>
								<td colspan="7">成交信息</td>
								<td>操作</td>
							</tr>
						</thead>
						<thead class="tableSubTitle">
							<tr>
								<td width="17%">小区名称</td>
								<td width="8%">面积</td>
								<td width="6%">租金</td>
								<td width="11%">交易来源</td>
								<td width="7%">租客</td>
								<td width="10%">租客电话</td>
								<td width="7%">房东</td>
								<td width="10%">房东电话</td>
								<td width="7%">成交租金</td>
								<td width="9%">成交时间</td>
								<td width="8%">&nbsp;</td>
							</tr>
						</thead>
						<tbody>
							<!--{foreach name=dataList from=$dataList item=item key=key}-->
							<tr>
								<td><!--{if $item.house_id}--><a href="<!--{$cfg.url_rent}-->d-<!--{$item.house_id}-->.html" target="_blank"><!--{$item.borough_name}--></a><!--{else}--><!--{$item.borough_name}--><!--{/if}--></td>
								<td><!--{$item.house_totalarea}-->㎡</td>
								<td class="f90"><!--{if $item.house_price == 0}-->面议<!--{else}--><!--{$item.house_price}-->元	<!--{/if}--></td>
								<td><!--{$item.bargain_from}--></td>
								<td><!--{$item.buyer}--></td>
								<td><!--{$item.buyer_tel}--></td>
								<td><!--{$item.saler}--></td>
								<td><!--{$item.saler_tel}--></td>
								<td class="f90"><!--{$item.bargain_price}--></td>
								<td><!--{$item.bargain_time|date_format:'%Y-%m-%d'}--></td>
								<td><a href="?action=edit&id=<!--{$item.id}-->" title="编辑成交信息" onclick="showBox(<!--{$item.id}-->);return false;">编辑</a>&nbsp;<!--{if $item.remark}--><a href="?action=remark&id=<!--{$item.id}-->" title="备注" onclick="showRemark(<!--{$item.id}-->);return false;"><img src="<!--{$cfg.path.images}-->saleDoneNote.gif" /></a><!--{else}--><a href="?action=remark&id=<!--{$item.id}-->" title="添加备注" onclick="showRemark(<!--{$item.id}-->);return false;"><img src="<!--{$cfg.path.images}-->saleDoneUnnote.gif" /></a><!--{/if}--></td>
							</tr>
							<!--{/foreach}-->
							<tr>
								<td colspan="11" class="listOperation">
								<input class="listOperationBtn" type="button" value="添加交易记录" onclick="showBox(0);return false;" />
								</td>
							</tr>
						</tbody>
					</table>
					<!--{$pagePanel}-->
				</div>
			</div>
			<div class="note">
				<p><a name="notice" id="notice"></a>成交榜管理必读：</p>
				<ul>
					<li>“成交榜”提供经纪人管理日常交易业绩及客户资料，仅供本人查看；</li>
					<li>“交易查询”可对历史交易进行时间分类查询、交易来源分类查询，还可针对小区名，租客/房东姓名或电话，或备注信息进行搜索；</li>
					<li>交易来源，即交易双方是来自 <!--{$cfg.page.title}-->还是来自 <!--{$cfg.page.title}-->以外的线下渠道；</li>
					<li>点击“小区名称”链接查看历史成交房源详细信息；</li>
					<li>通过“编辑”可对交易记录错误信息进行修正；</li>
					<li><img src="<!--{$cfg.path.images}-->saleDoneUnnote.gif" />表示无备注，点击可添加备注；<img src="<!--{$cfg.path.images}-->saleDoneNote.gif" />表示有备注，点击可修改备注；</li>
					<li>通过“添加交易记录”可以直接增加个人通过其它渠道成交的交易记录；</li>
				</ul>
			</div>
		</div>
	</div>
	
</div>
<script language="javascript">
function showBox(item_id){
	TB_show('房源成交','rentBargain.php?id='+item_id+'&height=370&width=400&modal=true&rnd='+Math.random(),false);
}
function showRemark(item_id){
	TB_show('查看备注','rentBargain.php?action=remark&id='+item_id+'&height=200&width=400&modal=true&rnd='+Math.random(),false);
}
</script>
<script src="<!--{$cfg.path.js}-->My97DatePicker/WdatePicker.js" language="javascript"></script>
</body>
</html>