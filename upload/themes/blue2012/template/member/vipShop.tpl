<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="zh-CN" xml:lang="zh-CN">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=gbk2312" />
<title><!--{$cfg.page.title}--> - 服务套餐升级</title>
<link href="<!--{$cfg.path.css}-->vip.css" rel="stylesheet" type="text/css" />
<script type="text/javascript"><!--
function pay_a(){
	var cf=confirm("确定要购买吗？");
	if (cf==true)
	{
		return true;
	}else{
		return false;
	}
}
//--></script>
</head>

<body>

<div class="hbox">
	<div class="hbox_l"><div class="hbox_r">
		<p class="hbox_toptext"><strong>您当前的账户余额：<font color="#FF0000"><!--{$money}-->元</font></strong> | 您当前套餐：<font color="#FF0000"><!--{if $vip==0}-->无套餐<!--{elseif $vip==1}-->体验套餐<!--{elseif $vip==2}-->标准套餐<!--{/if}--></font></p>
        
 <!--{if $vip==0}-->
		<dl class="hbox_pay">
			<dt><span><strong><!--{$cfg.page.vip1Price}--></strong>元<label>30天</label></span></dt>
			<dd class="text"><span class="text_r"><span class="img1">售<!--{$cfg.page.vip1SaleNum}-->、租<!--{$cfg.page.vip1RentNum}-->套房源|急售<!--{$cfg.page.vip1Vexation}-->个</span></span></dd>
			<dd class="but"><a href="vipShop.php?action=save&type=1" onclick="javascript:return pay_a();"></a></dd>
		</dl>
<!--{/if}-->
		<dl class="hbox_pay">
			<dt><span><strong><!--{$cfg.page.vip2Price}--></strong>元<label>90天</label></span></dt>
			<dd class="text"><span class="text_r"><span class="img2">售<!--{$cfg.page.vip2SaleNum}-->、租<!--{$cfg.page.vip2RentNum}-->套房源|急售<!--{$cfg.page.vip2Vexation}-->个</span></span></dd>
			<dd class="but"><a href="vipShop.php?action=save&type=2" onclick="javascript:return pay_a();"></a></dd>
		</dl>
	</div></div>
</div>

</body>
</html>
