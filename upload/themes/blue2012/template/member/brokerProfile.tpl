<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="X-UA-Compatible" content="IE=EmulateIE7" />
<meta http-equiv="Content-Type" content="text/html; charset=gb2312" />
<title><!--{$cfg.page.title}--> - 店铺资料</title>
<!--{$cssFiles}-->
<!--{$jsFiles}-->
<script type="text/javascript">
function addToBoroughItem(bid,bname,b_addr){
	$("#borough_id").val(bid);
	$("#borough_name").val(bname);
	$("#borough_addr").val(b_addr);
	$("#borough_addr_tr").css("display","");
}
$().ready(function() {
	$("#borough_name").autocomplete("ajax.php?action=getBoroughList", {
		minChars: 2,
		width: 260,
		delay:0,
		mustMatch:true,
		matchContains: false,
		scrollHeight: 220,
		selectFirst:true,
		scroll: true,
		formatItem: function(data, i, total) {
			if(data[1]=="addBorough"){
				return '<strong>'+data[0]+'</strong>';
			}
			return data[0];
		}
	});
	
	$("#borough_name").result(function(event, data, formatted) {
		if(data[1]=="addBorough"){
			//TB_show('增加小区','#TB_inline?height=155&width=400&inlineId=modalWindow',false);
			TB_show('增加小区','addBorough.php?height=170&width=400&modal=true&rnd='+Math.random(),false);
			$(this).val('');
		}else{
			$("#borough_id").val(data[1]);
			$("#borough_addr").val(data[2]);
			$("#borough_addr_tr").css("display",""); 
		}
		
		/*if (data)
			$(this).parent().next().find("input").val(data[1]);*/
	});
});
</script>

</head>
<body>
<!--{include file="inc/memberHeader.tpl"}-->
<div class="main">
	<div class="page">
<!--{include file="inc/member_left.tpl"}-->
		<div class="memberBox">
			<div class="memberBoxTab">
				<ul>
				  <li><a href="brokerProfile.php" class="linkOn"><span>修改资料</span></a></li>
				  <li><a href="brokerIdentity.php"><span>实名认证</span></a></li>
				  <li><a href="brokerPhoto.php"><span>修改头像</span></a></li>
				  <li><a href="pwdEdit.php"><span>修改密码</span></a></li>
				  <li><a href="brokerAptitude.php"><span>执业认证</span></a></li>
				</ul>
			</div>
			<!--{if $dataInfo.mobile==''}-->
			<div class="bigNote" id="bigNote">
				<div class="noteTxt">在完善<a href="brokerProfile.php">从业资料</a>前，您所发布的房源暂时无法对外展示！
				</div>
				<div class="closeNote">
					<a href="javascript:;" onclick="document.getElementById('bigNote').style.display='none'" title="隐藏提示"><img src="<!--{$cfg.path.images}-->closeNote.gif" title="隐藏提示" /></a>
				</div>
			</div>
			<!--{/if}-->
			<form name="dataInfo" method="POST" action="?action=save">
			<input type="hidden" name="id" value="<!--{$dataInfo.id}-->">
			<table class="memberBoxTable" cellpadding="0" cellspacing="5" border="0">
				<thead>
					<tr>
						<td colspan="4"><span class="concentTitle">从业资料 - 让客户信赖你！</span></td>
					</tr>
				</thead>
				<tbody>
					<tr>
						<td class="row1">用 户 名：</td>
						<td colspan="3"><input id="username" class="input" name="username" type="text" size="25" value="<!--{$dataInfo.username}-->" disabled="disabled" /></td>
					</tr>
       
                    <tr>
						<td class="row1"><span class="must">*</span>真实姓名：</td>
						<td colspan="3">
                     
 <input id="realname" class="input" name="realname" type="text" size="25" value="<!--{$dataInfo.realname}-->" disabled="disabled" />
 
                        
                        </td>
					</tr>
                    
					<tr>
						<td class="row1"><span class="must">*</span>移动电话：</td>
						<td colspan="3"><input id="mobile" class="input" name="mobile" type="text" size="25" valid="required|isTelephone" errmsg="请输入移动电话!|请输入正确的移动电话号码" value="<!--{$dataInfo.mobile}-->" />&nbsp;<span class="tip">显示为看房电话，请留手机或小灵通号码，方便客户随时联系你</span></td>
					</tr>
					<tr>
						<td class="row1"><span class="must">*</span>Email：</td>
						<td colspan="3"><input id="email" class="input" name="email" type="text" size="25" valid="required|isEmail" errmsg="请输入Email地址!|Email地址格式不对!" value="<!--{$dataInfo.email}-->" />&nbsp;<span class="tip">请输入有效且常用的Email邮箱地址，这是找回密码的唯一方式</span></td>
					</tr>
					
				</tbody>
				<tbody id="jobFile" style="display: <!--{if $dataInfo.broker_type == 2}-->none<!--{else}-->''<!--{/if}-->">
					<tr>
						<td class="row1"><span class="must">*</span>服务区域：</td>
						<td colspan="3"><select class="select" name="cityarea_id" id="addBoroughCityarea" valid="required" errmsg="请选择服务区域!">
						  <option>请选择</option>
						  <!--{html_options options=$cityarea_option selected=$dataInfo.cityarea_id}-->
						</select>

                        &nbsp;<span class="tip">选择服务区域，让你的客户更有针对性；</span></td>
					</tr>
                    <tr>
						<td class="row1"><span class="must">*</span>所属门店：</td>
						<td colspan="3">
                        <!--{if $dataInfo.company_id}-->
                        <!--{$dataInfo.company_name}-->
                          <!--{else}-->
                        <input id="outlet_first" class="input" name="outlet_first" type="text" size="8" valid="required" errmsg="请填写所属公司!" value="<!--{$dataInfo.outlet_first}-->" />
                          <!--{/if}-->
                        &nbsp;-<input id="outlet_last" class="input" name="outlet_last" valid="required" errmsg="请填写所属门店!" type="text" size="7" value="<!--{$dataInfo.outlet_last}-->" />&nbsp;<span class="tip">公司简称-门店，例：言微房产-红旗店，或 言微房产-总部</span></td>
					</tr>
                    <tr>
						<td class="row1">服务特长：</td>
						<td colspan="3">
                        
                        <!--{foreach name=specialty from=$specialty_option item=item key=key}-->
							<!--{if $key == $dataInfo.specialty}-->
								<label for="specialty_<!--{$key}-->"><input  type="radio" name="specialty" id="specialty_<!--{$key}-->" value="<!--{$key}-->"  checked /><!--{$item}--> </label>	
							<!--{else}-->
								<label for="specialty_<!--{$key}-->"><input  type="radio" name="specialty" id="specialty_<!--{$key}-->" value="<!--{$key}-->"  /><!--{$item}--> </label>
							<!--{/if}-->
						<!--{/foreach}-->
                        
                        </td>
					</tr>
                    
					<tr>
						<td class="row1">公司全称：</td>
						<td colspan="3"><input id="company" class="input" name="company" type="text" size="25" value="<!--{$dataInfo.company}-->" /></td>
					</tr>
					
					<tr>
						<td class="row1">门店地址：</td>
						<td colspan="3"><input id="outlet_addr" class="input" name="outlet_addr" type="text" size="25" value="<!--{$dataInfo.outlet_addr}-->" /></td>
					</tr>
                    <tr>
						<td class="row1">在线QQ：</td>
						<td colspan="3"><input id="qq" class="input" name="qq" type="text" size="25" value="<!--{$dataInfo.qq}-->" />&nbsp;
                        
                        <span class="tip">此QQ会表现在你的网店和房源等信息上</span></td>
					</tr>
					<tr>
						<td class="row1">固定电话：</td>
						<td colspan="3"><input id="com_tell" class="input" name="com_tell" type="text" size="25" value="<!--{$dataInfo.com_tell}-->" />&nbsp;<span class="tip">格式如：<!--{$cfg.page.phone}--></span></td>
					</tr>
                   
					<tr>
						<td class="row1">传&nbsp;&nbsp;&nbsp;&nbsp;真：</td>
						<td colspan="3"><input id="com_fax" class="input" name="com_fax" type="text" size="25" value="<!--{$dataInfo.com_fax}-->" />&nbsp;<span class="tip">格式如：<!--{$cfg.page.phone}--></span></td>
					</tr>
					<tr><td colspan="4" class="br"></td></tr>
				</tbody>
			</table>
			<table class="memberBoxTable" cellpadding="0" cellspacing="5" border="0">
				<thead>
					<tr>
						<td colspan="4"><span class="concentTitle">个人资料 - 让好友了解你！</span></td>
					</tr>
				</thead>
				<tbody>
					<tr>
						<td class="row1">性别：</td>
						<td colspan="3"><input id="gender" name="gender" type="radio" <!--{if $dataInfo.gender == 0 }-->checked<!--{/if}--> />帅哥&nbsp;<input id="gender" name="gender" type="radio" <!--{if $dataInfo.gender == 1 }-->checked<!--{/if}--> />美女</td>
					</tr>
					<tr>
						<td class="row1">出生日期：</td>
						<td colspan="3"><input id="birthday" class="input" name="birthday" type="text" size="25" value="<!--{$dataInfo.birthday}-->" onClick="WdatePicker()" /></td>
					</tr>
					<tr>
						<td class="row1">现住小区：</td>
						<td colspan="3"><input type="hidden" id="borough_id" class="input" name="borough_id" size="25" value="<!--{$dataInfo.borough_id}-->" /><input id="borough_name" class="input" name="borough_name" type="text" size="25" value="<!--{$dataInfo.borough_name}-->" />&nbsp;<span class="tip">例：输入“大名城”或拼音首字母“dmc”，从下拉列表中选择</span></td>
					</tr>
					<tr id="borough_addr_tr" style="display:none;">
						<td class="row1">小区地址：</td>
						<td colspan="3">
							<input id="borough_addr" type="text" class="input" name="borough_addr"  size="30" disabled />
						</td>
					</tr>
					<tr>
						<td class="row1">MSN：</td>
						<td colspan="3"><input id="msn" class="input" name="msn" type="text" size="25" value="<!--{$dataInfo.msn}-->" /></td>
					</tr>
					<tr>
						<td class="row1">个性签名：</td>
						<td colspan="3"><input id="signed" class="input" name="signed" type="text" size="40" value="<!--{$dataInfo.signed}-->" />&nbsp;<span class="tip">如你的座右铭、人生格言等，不超过30个汉字</span></td>
					</tr>
					<tr>
						<td class="row1">自我介绍：</td>
						<td colspan="3"><span class="tip">&nbsp;不超过200个汉字<br><textarea class="textarea" name="introduce" cols="70" rows="8"><!--{$dataInfo.introduce}--></textarea></td>
					</tr>
					<tr><td colspan="4" class="br"></td></tr>
				</tbody>
			</table>
			</form>
			<div class="submitBtn"><input type="button" value="确认修改" onclick="javascript:if(validator(document.dataInfo)){document.dataInfo.submit();}" /></div>
		</div>
	</div>
	
</div>
</body>
</html>