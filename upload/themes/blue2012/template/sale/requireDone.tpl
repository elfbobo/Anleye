<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<!--{include file="inc/head.tpl"}-->
<script src="<!--{$cfg.path.js}-->jquery-1.4.2.min.js" type="text/javascript"></script>
<script src="<!--{$cfg.path.js}-->select.js" type="text/javascript"></script>
<script src="<!--{$cfg.path.js}-->js.js" type="text/javascript"></script>
<script language="JavaScript" type="text/javascript" src="<!--{$cfg.path.js}-->FormValid.js"></script>
<link href="<!--{$cfg.path.css}-->add.css" rel="stylesheet" type="text/css" />
<link href="<!--{$cfg.path.css}-->menuSearch.css" rel="stylesheet" type="text/css" />
</head>
 
<body>
 
<!--{include file="inc/top.tpl"}-->
 
<div id="content">
<div id="requireMain">
		<div class="requireTitle">
			发布求购&nbsp;<img src="<!--{$cfg.path.images}-->requireFree.gif" align="absmiddle" />
		</div>
		<div class="sysConfirm" id="sysConfirm">
			<div class="confirmTxt">发布成功！您的求购信息编号为：<a class="familyAlpha colorF90 aUnderline" href="requireDetail.php?id=<!--{$dataInfo.id}-->" target="_parent"><!--{$dataInfo.house_no}--></a>
			<p>您发布的信息只有经过 <!--{$cfg.page.title}-->审核通过后才能对外显示</p>
			<p>请常回来看看回复，以免错过符合您要求的房源</p>
			<p><a class="colorF90 aUnderline" href="<!--{$cfg.url_sale}-->index.php">任务完成，再去淘一淘房源吧！</a></p>
			</div>
		</div>
	</div>
</div>
<!--{include file="inc/foot.tpl"}-->
</body>
</html>
