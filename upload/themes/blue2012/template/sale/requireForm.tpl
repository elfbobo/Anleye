<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<!--{include file="inc/head.tpl"}-->
<script src="<!--{$cfg.path.js}-->jquery-1.4.2.min.js" type="text/javascript"></script>
<script src="<!--{$cfg.path.js}-->select.js" type="text/javascript"></script>
<script src="<!--{$cfg.path.js}-->js.js" type="text/javascript"></script>
<script language="JavaScript" type="text/javascript" src="<!--{$cfg.path.js}-->FormValid.js"></script>
<link href="<!--{$cfg.path.css}-->add.css" rel="stylesheet" type="text/css" />
<link href="<!--{$cfg.path.css}-->menuSearch.css" rel="stylesheet" type="text/css" />

</head>
 
<body>
 
<!--{include file="inc/top.tpl"}-->
 
<div id="content">
<div id="requireMain">
		<div class="requireTitle">
			发布求购&nbsp;<img align="absmiddle" src="<!--{$cfg.path.images}-->requireFree.gif">
		</div>
		
		<!-- 登记表单 -->
		<div class="requireForm">
			<form name="dataInfo" method="POST" action="?action=save">
				<input name="id" type="hidden" value="<!--{$dataInfo.id}-->">
				<table cellpadding="0" cellspacing="5" border="0">
					<tr>
						<td width="12%"><span class="must">*</span>联 系 人：</td>
						<td width="88%"><input type="text" size="30" class="inp" name="linkman" value="<!--{$dataInfo.linkman}-->" valid="required" errmsg="请输入联系人!" /></td>
					</tr>
					<tr>
						<td><span class="must">*</span>联系电话：</td>
						<td><input type="text" size="30" class="inp" name="link_tell"  value="<!--{$dataInfo.link_tell}-->" valid="required|isTelephone" errmsg="请输入联系电话!|请输入正确的电话号码" />&nbsp;&nbsp;
							</td>
					</tr>
					<tr>
						<td width="10%"><span class="must">*</span>房源要求：</td>
						<td width="90%">
							<textarea name="requirement" valid="required" errmsg="请输入房源要求!"><!--{$dataInfo.requirement}--></textarea>
							<p>范例：我想在市中心买一套2室1厅1卫1阳的房子，最好是3-4楼，精装修，单价在6000元-8000元/平方，总价不要超过70万，最好是划片重点小学的。</p>
						</td>
					</tr>
					<tr>
						<td>&nbsp;</td>
						<td>
							<input type="button" value="立即发布" class="btn" onclick="javascript:if(validator(document.dataInfo)){document.dataInfo.action ='?action=save&consign=0';document.dataInfo.submit();}"  />
						</td>
					</tr>
					
					<tr>
						<td>&nbsp;</td>
						<td>请不要发布虚假信息，所有信息都需经过 <!--{$cfg.page.title}-->审核通过后才能显示！</td>
					</tr>
				</table>
			</form>
		</div>
		<!-- 登记表单 结束 -->
	</div>

</div>
<!--{include file="inc/foot.tpl"}-->
</body>
</html>
