<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>城市切换 - <!--{$cfg.page.titlec}--></title>
<meta name="keywords" content="">
<meta name="description" content="">
<link rel="stylesheet" rev="stylesheet" href="<!--{$cfg.path.css}-->city/Home_Index.css" type="text/css">
</head>

<body>
<div id="container">

    <div id="content" style="z-index:1;position:relative;">
        <div class="index_home_top w_970">
        
    <div class="index_logo"></div>
   
    
    <div class="index_top_seach">
        <div class="f16">
        
        搜索 <!--{if $cityUrl}--><strong class="c_red"><!--{$area}--></strong><!--{else}--><strong class="c_red"><!--{$cfg.page.city}--></strong><!--{/if}--> 的百万套房源
        
        </div>
        <div class="home_seach_box">
            <div class="index_home_bg seach_bg_box">
            <!--{if $cityUrl}-->
            <form action="<!--{$cityUrl}-->/sale/index.php" method="GET">
            <!--{else}-->
            <form action="<!--{$cfg.url_sale}-->index.php" method="GET">
             <!--{/if}-->
               <input name="q" type="text" maxlength="30" onblur="if(this.value ==''||this.value == '可输入小区名、路名或房源特征'){this.value = '可输入小区名、路名或房源特征';}" onfocus="if(this.value == '可输入小区名、路名或房源特征'){this.value = '';}" value="<!--{if $smarty.get.q==""}-->可输入小区名、路名或房源特征<!--{else}--><!--{$smarty.get.q}--><!--{/if}-->" />
                 <input name="b" value="utf8" type="hidden">
                 </div>
                 <input type="submit" value=" " class="index_home_bg seach_button">
                 
                 </form>
                
            
            <div class="clear"></div>
        </div>
    </div>
    <div class="clear"></div>
    <div class="index_top_hot">
        <span class="f14">
        <!--{if $cityUrl}-->
        猜您可能在<!--{$area}-->，<a href="<!--{$cityUrl}-->" title="点击进入<!--{$area}-->站" class="city_a_button index_home_bg">点击进入<!--{$area}-->站&gt;&gt;</a>
        <!--{else}-->
        您可能在<!--{$area}-->，但您的城市目前没有开通 <a href="hezuo" class="city_a_button index_home_bg" target="_blank">我要加盟</a>
        <!--{/if}-->
        </span>
        <span class="hot_city_title">热门城市：</span>
        <span class="hot_city_key">
          
          <!--{foreach from=$hdataList item=item key=key}-->
            <a href="<!--{$item.url}-->" title="<!--{$item.name}-->"><!--{$item.name}--></a>
   <!--{/foreach}-->
        </span>
    </div>
</div>

      <div class="w_970 plate_index_home">
        <h2 class="h2_title">已开通城市：</h2>
    <div class="contet">
        <ul class="index_home_hot_img_ul clear_box">
        
         <!--{foreach from=$dataList item=item key=key}-->
            <li>
                <a href="<!--{$item.url}-->" title="<!--{$item.name}-->">
                    <img src="<!--{$cfg.url_upfile}--><!--{$item.logo}-->" alt="<!--{$item.name}-->" width="86" height="86">
                    <!--{$item.name}-->
                </a>
            </li>
   <!--{/foreach}-->
            
        </ul>
    </div>
</div>


   <div class="w_970 plate_index_home">
    <div class="line"></div>
        <h2 class="h2_title">即将开通：</h2>
    <div class="contet">
        <ul class="index_home_hot_img_ul clear_box">
        
         <!--{foreach from=$udataList item=item key=key}-->
            <li>
                <a href="<!--{$item.url}-->" title="<!--{$item.name}-->">
                    <img src="<!--{$cfg.url_upfile}--><!--{$item.logo}-->" alt="<!--{$item.name}-->" width="86" height="86">
                    <!--{$item.name}-->
                </a>
            </li>
   <!--{/foreach}-->
            
        </ul>
    </div>
</div>


<div class="blank40"></div>
<div id="footer" class="w_970 txt_c link_gray_6"><div class="foot_box foot_top_link">
      
        <a target="_blank" title="关于我们" <!--{if $cityUrl}--> href="<!--{$cityUrl}-->/about/about.php"<!--{else}--> href="<!--{$cfg.url}-->about/about.php"<!--{/if}--> >关于我们</a>
         <a target="_blank" title="人才招聘" <!--{if $cityUrl}--> href="<!--{$cityUrl}-->/about/talented.php"<!--{else}--> href="<!--{$cfg.url}-->about/talented.php"<!--{/if}--> >人才招聘</a>
          <a target="_blank" title="用户协议" <!--{if $cityUrl}--> href="<!--{$cityUrl}-->/about/agreement.php"<!--{else}--> href="<!--{$cfg.url}-->about/agreement.php"<!--{/if}--> >用户协议</a>
           <a target="_blank" title="版权声明" <!--{if $cityUrl}--> href="<!--{$cityUrl}-->/about/copyright.php"<!--{else}--> href="<!--{$cfg.url}-->about/copyright.php"<!--{/if}--> >版权声明</a>
            <a target="_blank" title="联系我们" <!--{if $cityUrl}--> href="<!--{$cityUrl}-->/about/contact.php"<!--{else}--> href="<!--{$cfg.url}-->about/contact.php"<!--{/if}--> >联系我们</a>

        </div>


    <div class="copyright c_gray9">
      客服热线：<!--{$cfg.page.rexian}--><b>客服QQ：<!--{$cfg.page.qq}--></b><br>
    <span>备案：<a href="http://www.miibeian.gov.cn" target="_blank"><!--{$cfg.page.beian}--></a></span>版权所有：<!--{$cfg.page.gongsi}--></div>
</div>

    </div>
</div>

</body></html>