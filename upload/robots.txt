#
# robots.txt for anleye v4.0
#
User-agent: * 
Disallow: /member
Disallow: /admin
Disallow: /upload.php
Disallow: /uc_client
Disallow: /tmp
Disallow: /install