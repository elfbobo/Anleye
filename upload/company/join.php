<?php
require('path.inc.php');
$page->name = 'join'; //页面名字,和文件名相同	
$company = new Company($query);
$companyId = $_GET['company_id'];
$dataInfo = $company->getInfo($companyId,'*');
$page->tpl->assign('dataInfo',$dataInfo);
if($page->action=='join'){
//判断经纪人是否登录
if($realname){
	$memberId = $member->getAuthInfo('id');
	$memberCompanyId = $member->getAuthInfo('company_id');
	//经纪人已经登录
	
    if($companyId==$memberCompanyId){
		$page->urlto('index.php','您已经是该公司经纪人');
	}
		
	try{
		//更新经纪人所属公司
     	$member->update($memberId,'company_id',$companyId);
		$page->urlto('../cshop/'.$companyId,'加入成功');
	}catch (Exception $e){
		$page->back($e->getMessage());
	}
	
	}else{
		//经纪人未登录
		$page->urlto('../login/login.php','您还未登录，无法加入');
		}	
}

$page->show();
?>