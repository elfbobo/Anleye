<?php
require('path.inc.php');
$page->name = 'index'; //页面名字,和文件名相同	
$page->title = $page->city.'公司 - '.$page->titlec;   //网站名称

//成交使用的thickBox加载
$page->addcss("thickbox.css");
$page->addjs("thickbox.js");

//区域字典
$cityarea_option = Dd::getArray('cityarea');
$page->tpl->assign('cityarea_option', $cityarea_option);

$where ='';
//cityarea
$cityarea = intval($_GET['cityarea']);
if($cityarea){
	$where .= ' and b.cityarea_id = '.$cityarea;
}
 $dd = new Dd($query);
$borough_section=$dd->getSonList($cityarea);
$page->tpl->assign('borough_section',$borough_section);
//cityarea2
$cityarea2 = intval($_GET['cityarea2']);
if($cityarea2){
	$where .= ' and cityarea2_id = '.$cityarea2;
	$page->title = $cityarea_option[$cityarea].",".$page->title;
}

//选择标签
$switch = $_GET['switch'];
if($switch == ""){
	$where .= " and type = 0 ";
	$page->title = "中介公司 - ".$page->title;
}
if($switch == "move"){
	$where .= " and type = 1 ";
	$page->title = "搬家公司 - ".$page->title;
}
if($switch == "decoration"){
	$where .=" and type =2";
	$page->title = "装修公司 - ".$page->title;
}




//q
$q = $_GET['q']=="可输入经纪人名、门店名，或公司名称关键词" ? "":trim($_GET['q']);
if($q){
	// 可输入经纪人名、门店名，或公司名称关键词
	$where.=" and company_name like '%".$q."%'" ;
}

/*print_rr($where);*/
//list_num
$list_num = intval($_GET['list_num']);
if(!$list_num){
	$list_num = 10;
}

$company = new Company($query);

require($cfg['path']['lib'] . 'classes/Pages.class.php');
$row_count = $company->getCount($where);
$pages = new Pages($row_count,$list_num);

//page
$pageno = $_GET['pageno']?intval($_GET['pageno']):1;
$pre_page = $pageno>1?$pageno-1:1;
$next_page = $pageno<$pages->pageCount?$pageno+1:$page_count;
$page->tpl->assign('pageno', $pageno);
$page->tpl->assign('row_count', $row_count);
$page->tpl->assign('page_count', $pages->pageCount);
$page->tpl->assign('pre_page', $pages->fileName.'pageno='.$pre_page);
$page->tpl->assign('next_page', $pages->fileName.'pageno='.$next_page);

$pageLimit = $pages->getLimit();
$dataList = $company->getList($pageLimit,'*',$where,$list_order);

$member = new Member($query);
//查询经纪人数量
foreach ($dataList as $key=> $item){
$dataList[$key]['brokerCount'] = $member->getCountBroker('company_id='.$item['id']);
}
$page->tpl->assign('dataList', $dataList);

//推荐的中介公司
$commendDataList = $company->getList($pageLimit,'*',' and status=1 and type=0',$list_order);
$page->tpl->assign('commendDataList', $commendDataList);

$page->tpl->assign('pagePanel', $pages->showCtrlPanel_g('5'));//分页条

$page->tpl->assign('menu', 'company');
$page->show();
?>