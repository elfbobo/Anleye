<?php

/**
 * 经纪人列表管理页面
 *
 * @copyright Copyright (c) 2007 - 2012 yanwee.com
 * @author 阿一 yandy@yanwee.com
 * @package package
 * @version $Id$
 * @2012-12-25
 */
require('path.inc.php');

$member = new Member($query);
$housesell = new HouseSell($query);
$houserent = new HouseRent($query);



if ($page->action == 'delete') {
    $id = $_GET['id'];
    try {
        $member->update($id, 'company_id','');
        $housesell->updateCompany($id,'company_id','');
    	$houserent->updateCompany($id,'company_id','');
        $page->urlto('brokerList.php', '踢出经纪人成功');
    } catch (Exception $e) {
        $page->back('踢出失败');
    }
    exit;
}else {
    //列表包括搜索
    $page->name = 'brokerList';

    //计算剩余多少房源
    $where = ' company_id = ' . $company_id;
	
    $memberNum = $member->getCount($where);
    $page->tpl->assign('memberNum', $memberNum); //总共多少条


    //成交使用的thickBox加载
    $page->addcss("thickbox.css");
    $page->addjs("thickbox.js");


    require($cfg['path']['lib'] . 'classes/Pages.class.php');   
    $pages = new Pages($member->getCount($where), 10, 'pages_g.tpl');
    $pageLimit = $pages->getLimit();
	   $q = $_GET['q'] == '输入房源编号或小区名称' ? "" : trim($_GET['q']);
    if ($q) {
   
        $search_bid = $member->getAllBroker('id', ' realname like \'%' . $q . '%\'');
        if ($search_bid) {
            $search_bid = implode(',', $search_bid);
            $where .= " and (realname like '%" . $q . "%')";
        } else {
            $where .= " and (realname like '%" . $q . "%')";
        }
    }
    $page->tpl->assign('q', $q);
    $dataList = $member->getListBroker($pageLimit, '*', $where, '');
   

    $page->tpl->assign('to_url', $_SERVER['REQUEST_URI']);
    $page->tpl->assign('dataList', $dataList);
    $page->tpl->assign('pagePanel', $pages->showCtrlPanel_m(5)); //分页条
}

$page->show();
?>