<?php

/**
 * @author 房产
 * @example 预约刷新
 * @version 1.0 二零一二年二月七日 19:06:52
 */

require('path.inc.php');
$member_id = $member->getAuthInfo('id');
$site = $_REQUEST['site'];
if($site=='rent'){
    $houseRent = new HouseRent($query);
}elseif($site=='sale'){
    $houseRent = new HouseSell($query);
}else{
    $page->urlto('../member/index.php','您没有权限查看此页面！');
}
$to_url = $_POST['to_url'];
if (!in_array($member->getAuthInfo('vip'), $appoMuth['appInterest'])) {
    $page->urlto('../member/index.php','您没有权限使用这个功能，请升级会员！');
}
//生成24小时
for ($i = 0; $i < 24; $i++) {
    $hour[] = sprintf('%02d', $i);
}
//生成可预约的分钟
for ($i = 0; $i < 60; $i++) {
    if ($i % $appoMuth['appTime'] == 0) {
        $minute[] = sprintf('%02d', $i);
    }
}
//生成可提前预约的天数
for ($i = 0; $i <= $appoMuth['appNum']; $i++) {
    $day = $i+1;$date[$i] = $i==0?'今天':$day.'天';
}
//获取每个时间段的次数
$sql = $query->execute("SELECT count(update_time) AS part,update_time FROM fke_appolist WHERE update_time >= '" . time() . "' GROUP BY update_time");
while ($rel = $query->fetchRecord($sql, MYSQL_ASSOC)) {
    $datePart['update_time'] = $rel['part'];
}
$page->tpl->assign('hour', $hour);
$page->tpl->assign('minute', $minute);
$page->tpl->assign('date', $date);

if ($page->action == 'appoRefresh') {
    $ids = is_array($_REQUEST['ids'])?$_REQUEST['ids']:array($_REQUEST['ids']);

    $page->name = 'index';
    $ids = array_filter($ids);
    if (!is_array($ids) && empty($ids)) {
        $page->back('没有选择需要预约刷新的房源');
    }
    array_walk($ids,'intval');
    foreach ($ids as $id) {
        if ($query->getValue("SELECT appo_list_id FROM fke_appolist WHERE house_id = '{$id}' AND appo_site='{$site}'")) {
            continue;
        }
//        if ($query->getValue("SELECT appo_list_id FROM fke_appolist WHERE house_id = '{$id}' AND appo_site='{$site}' AND FROM_UNIXTIME('%Y-%m-%d',update_time) = CURRENT_DATE")) {
//            continue;
//        }
        $house[] = $houseRent->getInfo($id, 'house_title,id,refresh');
    }

    if (empty($house) && !is_array($house)) {
        $page->back('您没有选择房源或此房源已经预约过！');
    }

    $page->tpl->assign('house', $house);
} elseif ($page->action == 'submitAppo') {
    foreach ($_POST['appo_house_id'] as $id) {
        if ($query->getValue("SELECT appo_list_id FROM fke_appolist WHERE house_id = '{$id}' AND appo_site='{$site}'")) {
            continue;
        }
        for ($i=0;$i<=$_POST['appo_date'][$id];$i++) {
            $appo_dates = date('Y-m-d', strtotime("+{$i} days"));
            list($year, $month, $day) = explode('-', $appo_dates);

            //获取小时和分钟
            $data = array();
            foreach ($_POST['appo_hours'][$id] as $key => $hour) {
                //检测是否正确时间
                if (!is_numeric($hour) && !is_numeric($_POST['appo_minute'][$id][$key])) {
                    continue;
                } else {
                    $data[$id][] = $hour . '-' . $_POST['appo_minute'][$id][$key];
                }
            }


            foreach ($data[$id] as $val) {
                list($hour, $minute) = explode('-', $val);
                $time = mktime($hour, $minute, 0, $month, $day, $year);
                //检测时间是否小于现在时间
                if ($time < time()) {
                    if (date('i', time()) < 30) {
                        $time = strtotime('+30 minute', strtotime(date('Y-m-d H:0:0', time())));
                    } else {
                        $time = strtotime('+1 hour', strtotime(date('Y-m-d H:0:0', time())));
                    }
                }


                //检测时间是否大于每个时间点可设置的次数
                while (1) {
                    if (array_key_exists($time, $datePart) && $datePart[$time] >= $appoMuth['appCountNum']) {
                        $time = strtotime('+' . $appoMuth['appTime'] . ' minutes ', $time);
                    } else {
                        break;
                    }
                }
                try {
                    $query->execute("INSERT INTO fke_appolist(user_id,house_id,house_title,update_time,appo_site)VALUES('{$member_id}','{$id}','{$_POST[appo_house_title][$id]}','{$time}','{$site}')");
                } catch (Exception $e) {
                    $page->back('设置预约刷新失败');
                }
            }
        }
    }
    if ($site == 'rent') {
        $page->urlto('../member/manageRent.php', '设置预约刷新成功！');
    } else {
        $page->urlto('../member/manageSale.php', '设置预约刷新成功！');
    }
}elseif($page->action == 'appoShowHouse'){

    //error_reporting(E_ALL);
    $page->name = 'detailHouse';
    $house_id = intval($_REQUEST['house_id']);
    $sql = $query->execute("SELECT * FROM fke_appolist WHERE user_id = '{$member_id}' AND house_id = '{$house_id}' AND appo_site='{$site}'");
    while($rel = $query->fetchRecord($sql,MYSQL_ASSOC)){
        $result['house_title'] = $rel['house_title'];
        $result['update'][date('Y.m.d',$rel['update_time'])][$rel['appo_list_id']]['day'] = date('Y年m月d日',$rel['update_time']);
        $result['update'][date('Y.m.d',$rel['update_time'])][$rel['appo_list_id']]['hour'] = date('H',$rel['update_time']);
        $result['update'][date('Y.m.d',$rel['update_time'])][$rel['appo_list_id']]['minute'] = date('i',$rel['update_time']);
        $result['update_limit'][date('Y.m.d',$rel['update_time'])] = date('Y年m月d日',$rel['update_time']);
    }



    if(!is_array($result)&&empty($result)){
        if($site=='rent'){
            $page->urlto('../member/manageRent.php','您选择房源没有预约刷新或者此房源不属于您！');
        }else{
            $page->urlto('../member/manageSale.php','您选择房源没有预约刷新或者此房源不属于您！');
        }
    }
    $result['house_id'] = $house_id;
    foreach($result['update_limit'] as $key=>$value){
        for($i = count($result['update'][$key]); $i < 5; $i++) {
            $result['update'][$key][$i.'in'] = array('day'=>$value);
        }
    }
    $page->tpl->assign('house',$result);
}elseif($page->action == 'editAppo'&&$_SERVER['REQUEST_METHOD']=='POST'){
    $to_url = $_SERVER['HTTP_REFERER'];
    $appo_hours = $_POST['appo_hours'];
    $appo_minute = $_POST['appo_minute'];
    $appo_date = $_POST['appo_date'];
    $house_id = $_POST['house_id'];
    $house_title = $_POST['house_title'];
    list($year, $month, $day) = explode('.', $appo_date);
    foreach ($appo_hours[$_POST['appo_date']] as $id => $hour) {
        if (is_numeric($hour) && is_numeric($appo_minute[$_POST['appo_date']][$id])) {
            $time = mktime($hour, $appo_minute[$_POST['appo_date']][$id], 0, $month, $day, $year);
            //检测时间是否小于现在时间
            if ($time < time()) {
                if (date('i', time()) < 30) {
                    $time = strtotime('+30 minute', strtotime(date('Y-m-d H:0:0', time())));
                } else {
                    $time = strtotime('+1 hour', strtotime(date('Y-m-d H:0:0', time())));
                }
            }
            //检测时间是否大于每个时间点可设置的次数
            while (1) {
                if (array_key_exists($time, $datePart) && $datePart[$time] >= $appoMuth['appCountNum']) {
                    $time = strtotime('+' . $appoMuth['appTime'] . ' minutes ', $time);
                } else {
                    break;
                }
            }
            try {
                if (!is_numeric($id)) {
                    $query->execute("INSERT INTO fke_appolist(user_id,house_id,house_title,update_time,appo_site)VALUES('{$member_id}','{$house_id}','{$house_title}','{$time}','{$site}')");
                } else {
                    $query->execute("UPDATE fke_appolist SET update_time = '{$time}' WHERE appo_list_id = '{$id}' AND appo_site = '{$site}'");
                }
                //$query->execute("INSERT INTO fke_appolist(user_id,house_id,house_title,update_time,appo_site)VALUES('{$member_id}','{$house_id}','{$house_title}','{$time}','{$site}')");
            } catch (Exception $e) {
                $page->back('设置预约刷新失败');
            }
        } else {
            $query->execute("DELETE FROM fke_appolist WHERE appo_list_id = '{$id}'");
        }
    }

    $page->urlto($to_url, '修改成功');
}elseif($page->action == "appoDel"){
    $house_id = intval($_REQUEST['house_id']);
    $query->execute("DELETE FROM fke_appolist WHERE house_id = '{$house_id}'");
    $page->urlto('index.php?action=appoRefresh&ids='.$house_id.'&site='.$site, '取消成功！');
}else{
    $page->urlto('../member/manageRent.php','您没有权限查看此页面！');
}
$page->tpl->assign('to_url', $to_url);
$page->tpl->assign('site',$site);
$page->show();
?>