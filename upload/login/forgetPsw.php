<?php
 /**
  * 取回密码页面
  *
  * @copyright Copyright (c) 2007 - 2008 Yanwee.net (www.anleye.com)
  * @author 阿一 yandy@yanwee.com
  * @package package
  * @version $Id$
  */
 
require('path.inc.php');

$page->name = 'forgetPsw';
$page->title =  $page->title.' - 取回密码';
$page->addJs('FormValid.js');
$page->addJs('FV_onBlur.js');

$member = new Member($query);
if ($_POST['action']=='getPass') {
	if (md5(strtolower($_POST['vaild']))!=$_COOKIE['validString']) {
		$errorMsg = '验证码错误！';
	}
	if (!$errorMsg) {
		specConvert($_POST, array('username'));
		if ($_POST['username'] && $_POST['email']) {
			$result = $member->checkMemberEmail($_POST['username'], $_POST['email']);
			if ($result ) {
				$key = authcode($_POST['username']."\t".$_POST['email']."\t".($cfg['time']+86400),'ENCODE',$cfg['auth_key']);
				$passUpdateUrl = $cfg['url'].'login/forgetPsw.php?action=updatePsw&username='.$_POST['username'].'&volidkey='.$key;
				//发送邮件
				$sendMail = new SendMail();
				$sendMail->mailer = $cfg['mail']['mailer'];
				$sendMail->SMTPAuth = true;
				$sendMail->host = $cfg['mail']['host'];
				$sendMail->from = $cfg['mail']['username']."@sina.com";
				$sendMail->fromName = $cfg['mail']['username'];
				$sendMail->sender = $cfg['mail']['username']."@sina.com";
				$sendMail->username = $cfg['mail']['username'];
				$sendMail->password = $cfg['mail']['password'];
				$sendMail->addAddress($_POST['email']);
				$sendMail->subject = "安乐业<fangke.cc>密码修改确认邮件";
				$sendMail->body = "密码修改地址：".$passUpdateUrl;
				$sendMail->SMTPDebug =false;
				if($sendMail->send()){
				 	echo "<script>alert('密码已经发送到你填写的邮箱中，请尽快收信修改密码');window.close();</script>";
				 	exit;
				}
				
			} else {
				$errorMsg = "你输入的用户名和邮箱有错";
			}
		} else {
			$errorMsg = '请输入用户名和邮箱地址！';
		}
	}
}elseif ($page->action =='updatePsw'){
	
	if(!$_GET['volidkey']){
		$page->urlto($cfg['url']."index.php",'参数错误');
	}
	$temp = authcode($_GET['volidkey'],'DECODE',$cfg['auth_key']);
	$key = explode("\t",$temp);
	if($_GET['username'] != $key[0]){
		$page->urlto($cfg['url']."index.php",'参数错误,请重新取回密码');
	}
	if($key[2] < $cfg['time'] ){
		$page->urlto($cfg['url']."index.php",'已经超过时间限制，你必须在24小时内修改密码，请重新申请取回密码');
	}
	$page->name = "updatePsw";
	$page->tpl->assign('volidkey', $_GET['volidkey']);
}elseif($page->action == 'doUpdatePws'){
	$member = new Member($query);
	$temp = authcode($_POST['volidkey'],'DECODE',$cfg['auth_key']);
	$key = explode("\t",$temp);
	if($member->updateFromGetPsw($key[0],$key[1],$_POST['passwd'])){
		$page->urlto($cfg['url']."index.php","修改密码成功，请重新登陆");
	}else{
		$page->back("修改密码失败");		
	}
}

$page->tpl->assign('errorMsg', $errorMsg);

$page->show();
?>