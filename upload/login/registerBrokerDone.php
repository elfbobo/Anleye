<?php
 /**
  * 会员注册选择页面
  *
  * @copyright Copyright (c) 2007 - 2008 Yanwee.net (www.anleye.com)
  * @author 阿一 yandy@yanwee.com
  * @package package
  * @version $Id$
  */
 
require('path.inc.php');

$page->name = 'registerBrokerDone';
$page->title =  $page->title.' - 会员注册';

$page->show();
?>