<?php
/**
 * 二手房源
 *
 * @author 阿一 yandy@yanwee.com
 * @package package
 * @version $Id$
 */

require('../common.inc.php');
$page->dir  = 'shop';//目录名
$page->addCss('shopCommon.css');

$realname = $_COOKIE['AUTH_MEMBER_REALNAME'] ? $_COOKIE['AUTH_MEMBER_REALNAME'] :$_COOKIE['AUTH_MEMBER_NAME'];
$page->tpl->assign('username',$realname);
if($_COOKIE['AUTH_MEMBER_STRING']){
	$member = new Member($query);
	$user_type = $member->getAuthInfo('user_type');
	$page->tpl->assign('user_type',$user_type);
	$broker_id = $member->getAuthInfo('id');
	$page->tpl->assign('broker_id',$broker_id);
}
$page->tpl->assign('menu','shop');

?>