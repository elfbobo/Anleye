<?php
require('path.inc.php');



$page->name = 'profile'; //页面名字,和文件名相同	

$id = intval($_GET['id']);
if(!$id){
	$page->back('参数错误');
}
//店长信息
$member = new Member($query);
$dataInfo = $member->getInfo($id,'*',true);
if($dataInfo['company_id']){
$company = new Company($query);
$dataInfo['company_name'] = $company->getInfo($dataInfo['company_id'],'company_name');
}
if(!$dataInfo){
	$page->back('经纪人不存在');
}
$integral_array = require_once($cfg['path']['conf'].'integral.cfg.php');
$dataInfo['brokerRank'] = getNumByScore($dataInfo['scores'],$integral_array,'pic');
$dataInfo['cityarea_name'] = Dd::getCaption('cityarea',$dataInfo['cityarea_id']);
$dataInfo['active_str'] = explode('|',$dataInfo['active_str']);
$dataInfo['broker_type'] = Dd::getCaption('broker_type',$dataInfo['broker_type']);
$page->tpl->assign('dataInfo', $dataInfo);
$page->title = $dataInfo['realname']."的网店 - 店长介绍 - ".$page->title;
//网店信息
$shop = new Shop($query);
$shopConf = $shop->getShopConf($id);
$page->tpl->assign('shopConf', $shopConf);
if($shopConf['shop_style']){
	$page->addCss($shopConf['shop_style']);
}else{
	$page->addCss('shopStyleDefault.css');
}

$page->show();
?>