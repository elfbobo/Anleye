<?php
/**
 * 经纪人后台管理信息
 *
 * @package user
 * @author 王岩 yandy@yanwee.com
 * @version 1.0
 */

require('path.inc.php');

$user->allow('brokerList');
$member = new Member($query);
if($page->action=='avatar'){
	$back_url = $_SERVER['HTTP_REFERER'];
	$ids = $_POST['ids'];
	if(!is_array($ids) || empty($ids)){
		$page->back('没有选择删除条目');
	}
	try{
		$fieldData = array('avatar'=>'');
		foreach($ids as $id){
			$member->updateInfo($id,$fieldData,true,1);
		}
		
		$page->urlto($back_url,'删除用户头像成功');
	}catch (Exception $e){
		$page->back($e->getMessage());
	}
}elseif($page->action=='view'){
	$page->name ='memberView';
	$id = intval($_GET['id']); 
	
	$userInfo = $member->getInfo($id,'*',true);
	$dd = new Dd($query);
	if($userInfo['cityarea_id']){
		$cityarea_arr = Dd::getArray('cityarea');
		$userInfo['cityarea_id'] = $cityarea_arr[$userInfo['cityarea_id']];
	}
	if($userInfo['broker_type']){
		$broker_type = Dd::getArray('broker_type');
		$userInfo['broker_type'] = $broker_type[$userInfo['broker_type']];
	}
	$borough = new Borough($query);
	if($userInfo['borough_id']){
		$userInfo['borough_id'] = $borough->getInfo($userInfo['borough_id'],'borough_name');
	}
	$page->tpl->assign('dataInfo',$userInfo);
	
}elseif ($page->action=='delete') {
	$back_url = $_SERVER['HTTP_REFERER'];
	$ids = $_POST['ids'];
	if(!is_array($ids) || empty($ids)){
		$page->back('没有选择删除条目');
	}
		try{
		if($page->uc==1){
		//删除uc用户
		require($cfg['path']['root'].'uc_client/client.php');
		foreach($ids as $id){
			$datainfo = $member->getInfo($id);
			$uid = uc_get_user($datainfo['username'],0);
			uc_user_delete($uid);
			}
		}
		$member->delete($ids,2);
		$page->urlto($back_url,'删除用户成功');
	}catch (Exception $e){
		$page->back($e->getMessage());
	}

	exit;
}elseif ($page->action=='isindex') {
	$back_url = $_SERVER['HTTP_REFERER'];
	$ids = $_POST['ids'];
	$status = intval($_GET['status']);
	$usertype=$member->isbroker($ids);
	foreach ($usertype as $key => $value){
		if($value['user_type'] == 2 ){
		$page->back('要推荐的信息包含业主，业主是不允许推荐的');
		}
	
	}
	if(!is_array($ids) || empty($ids)){
		$page->back('没有选择需要操作的条目');
	}
		try{
		    $member->update($ids,'is_index',$status);
	        $page->urlto($return_to,'操作成功');
	}catch (Exception $e){
		$page->back($e->getMessage());
	}
	
	exit;
	
}elseif ($page->action=='status') {
	$back_url = $_SERVER['HTTP_REFERER'];
	$ids = $_POST['ids'];
	$dostatus = intval($_GET['dostatus']);
	if(!is_array($ids) || empty($ids)){
		$page->back('没有选择需要操作的条目');
	}
	try{
		$member->changeStatus($ids,$dostatus);
		$page->urlto($back_url,'操作成功');
	}catch (Exception $e){
		$page->back($e->getMessage());
	}

	exit;
}else{
	$page->name = 'brokerList'; //页面名字,和文件名相同

	//察看受理情况使用的thickBox加载
	$page->addcss("thickbox.css");
	$page->addjs("thickbox.js");
	
	$where = '1 and user_type=1';
	if($_GET['status']){
		$where .= ' and status ='.intval($_GET['status']);
	}
	if($_GET['vip']){
		$where .= ' and vip ='.intval($_GET['vip']);
	}
	if($_GET['isindex']){
		$where .= ' and is_index ='.intval($_GET['isindex']);
	}
	if($_GET['is_open']){
		$where .= ' and is_open ='.intval($_GET['is_open']);
	}
	if($_GET['username']){
		$where .=" and username like '%".trim($_GET['username'])."%'";
	}
	
	if($_GET['realname']){
		$ids_realname = $member->searchMember('realname',trim($_GET['realname']));
	}
	if($_GET['tel']){
		$ids_tel = $member->searchMember('tel',trim($_GET['tel']));
	}
	if($_GET['email']){
		$where .=" and email like '%".trim($_GET['email'])."%'";
	}
	if($_GET['idcard']){
		$ids_idcard = $member->searchMember('idcard',trim($_GET['idcard']));
	}
	if($_GET['com']){
		$ids_com = $member->searchMember('com',trim($_GET['com']));
	}
	if($_GET['avatar']){
		$ids_avatar = $member->searchMember('avatar',$_GET['avatar']);
	}
	if($_GET['identity']){
		$ids_idcard = $member->searchMember('identity',$_GET['identity']);
	}
	
	if($_GET['realname'] || $_GET['tel'] || $_GET['email'] || $_GET['idcard'] || $_GET['com'] || $_GET['avatar'] || $_GET['identity'] ){
		$ids = array_merge((array)$ids_realname,(array)$ids_tel,(array)$ids_idcard,(array)$ids_com,(array)$ids_avatar,(array)$ids_idcard);
		$ids = array_unique($ids);
		if($ids){
			$where .=" and id in (".implode(',',$ids).")";
		}else{
			$where .=" and 0";
		}
	}
	$user_type = Dd::getArray('user_type');
	
	require($cfg['path']['lib'] . 'classes/Pages.class.php');
	$pages = new Pages($member->getCount($where));
	$pageLimit = $pages->getLimit();
	$memberList = $member->getList($pageLimit,'*',$where,' order by add_time desc ');
	foreach ($memberList as $key => $value){
	$memberList[$key]['member_info'] = $member->getMoreInfo($value['id'],1);
	}
	
	$page->tpl->assign('dataList', $memberList);
	$page->tpl->assign('pagePanel', $pages->showCtrlPanel());//分页条
}
$page->show();
?>