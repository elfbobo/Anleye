<?php
/**
 * 经纪人后台管理信息
 *
 * @package user
 * @author 王岩 yandy@yanwee.com
 * @version 1.0
 */

require('path.inc.php');

$user->allow('evaluate');
$member = new Member($query);


if ($page->action=='delete') {
	$back_url = $_SERVER['HTTP_REFERER'];
	$ids = $_POST['ids'];
	if(!is_array($ids) || empty($ids)){
		$page->back('没有选择删除条目');
	}
	try{
		$member->deleteEvaluate($ids);
		$page->urlto($back_url,'删除成功');
	}catch (Exception $e){
		$page->back($e->getMessage());
	}

	exit;
}else{
	$page->name = 'evaluate'; //页面名字,和文件名相同

	//察看受理情况使用的thickBox加载
	$page->addcss("thickbox.css");
	$page->addjs("thickbox.js");
	
	$where = '1 ';
	
	require($cfg['path']['lib'] . 'classes/Pages.class.php');
	$pages = new Pages($member->getCountEvaluate($where,'*'));
	$pageLimit = $pages->getLimit();
	
	$evaluateList = $member->getEvaluateList($pageLimit,'*',$where,' order by time desc ');
	
	foreach ($evaluateList as $key => $item){
		$evaluateList[$key]['broker_info'] = $member->getInfo($item['broker_id'],'*',true);
	}
	
	$page->tpl->assign('dataList', $evaluateList);
	$page->tpl->assign('pagePanel', $pages->showCtrlPanel());//分页条
}
$page->show();
?>