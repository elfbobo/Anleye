<?php
/**
 * 站内信信息
 *
 * @package user
 * @author 王岩 yandy@yanwee.com
 * @version 1.0
 */

require('path.inc.php');

$user->allow('innernote');
$innernote = new Innernote($query);

if ($page->action=='edit') {
	$page->action = 'add';
}
if ($page->action=='add') {
	$page->name = 'innernoteEdit'; //页面名字,和文件名相同
	$page->addJs('FormValid.js');

}elseif ($page->action=='save') {
	try{
		//发送模具
		$msg_from = $user->getAuthInfo('username');
		$model_id = $innernote->send($msg_from,$_POST['msg_to'],$_POST['msg_title'],$_POST['msg_content']);
		//根据信息
		$msg_from = "系统";
		$reciever = explode(',',$_POST['msg_to']);
		$member = new Member($query);
		if(is_array($reciever)){
			foreach ($reciever as $a_reviever){
				$a_reviever = trim($a_reviever);
				if($a_reviever == ''){
					continue;
				}
				if($a_reviever == "[所有经纪人]"){
					$a_reviever = $member->getAll(1,'id,username');
				}
				if($a_reviever == "[所有业主]"){
					$a_reviever = $member->getAll(2,'id,username');
				}
				if(is_array($a_reviever)){
					foreach($a_reviever as $a_r){
						$innernote->send($msg_from,$a_r['username'],$_POST['msg_title'],$_POST['msg_content'],0,$model_id);
					}
				}else{
					$innernote->send($msg_from,$a_reviever,$_POST['msg_title'],$_POST['msg_content'],0,$model_id);
				}
			}
		}
		$page->urlto('index.php','发送短信成功');
	}catch (Exception $e){
		$page->back($e->getMessage());
	}
	exit;
}elseif ($page->action=='delete') {
	$ids = $_POST['ids'];
	if(!is_array($ids) || empty($ids)){
		$page->back('没有选择删除条目');
	}
	
	try{
		//删除自己的条目
		$innernote->delete($ids);
		//删除附属条目
		$innernote->deleteBelongsTo($ids);
		
		$page->urlto('index.php','删除成功');
	}catch (Exception $e){
		$page->back($e->getMessage());
	}

	exit;
}elseif ($page->action=='fromDel') {
	$ids = $_POST['ids'];
	if(!is_array($ids) || empty($ids)){
		$page->back('没有选择删除条目');
	}
	try{
		$innernote->fromDel($ids);
		$page->urlto('index.php','删除成功');
	}catch (Exception $e){
		$page->back($e->getMessage());
	}

	exit;
}else{
	$page->name = 'innernoteList'; //页面名字,和文件名相同
	
	$where = ' msg_from =\''.$user->getAuthInfo('username').'\' and from_del = 0';
	
	require($cfg['path']['lib'] . 'classes/Pages.class.php');
	$pages = new Pages($innernote->getCount($where));
	$pageLimit = $pages->getLimit();
	$innernoteList = $innernote->getList($pageLimit,'*',$where,' order by add_time desc ');

	$page->tpl->assign('dataList', $innernoteList);
	$page->tpl->assign('pagePanel', $pages->showCtrlPanel());//分页条
}
$page->show();
?>