<?php
require('path.inc.php');

$page->title =  $page->title.' - 管理房源图片';

$house_id = intval($_GET['house_id']);
if(!$house_id){
	exit;
}
if($_GET['cls'] == 'sell'){
	$house = new HouseSell($query);
}elseif ($_GET['cls'] == 'rent'){
	$house = new HouseRent($query);
}else{
	exit;
}
$borough = new Borough($query);

if($page->action == 'getBoroughImg'){
	//删除房源图片 Ajax
	$houseInfo = $house->getInfo($house_id);
	if($_GET['tp'] == 'pic'){
		$boroughImgList = $borough->getImgList($houseInfo['borough_id'],false);
	}else{
		$boroughImgList = $borough->getImgList($houseInfo['borough_id'],true);
	}
	$str = '';
	foreach ($boroughImgList as $item){
		$str .= '<div style="float:left;width:80px;">
					<img src="'.$cfg['url'].'upfile/'.$item['pic_url'].'" width="80" height="60">
				</div>';
	}
	echo $str;
	exit;
}elseif($page->action == 'delImg'){
	//删除房源图片 Ajax
	$ids = explode(',',$_POST['picid']);
	if($_GET['tp'] == 'pic'){
		if(is_array($ids)){
			array_remove_empty($ids);
			foreach ($ids as $id){
				$house->delHousePic($id);
			}
			$house_img = $house->getImgList($house_id,'*');
			if($house_img){
				$house->update($house_id,'house_thumb',$house_img[0]['pic_url']);
			}else{
				$house->update($house_id,'house_thumb','');
			}
		}
	}else{
		$house->delHouseDraw($house_id);
	}
	echo 1;
	exit;
}elseif($page->action == 'moveToBorough'){
	//移动到小区图片库中 Ajax
	$ids = explode(',',$_POST['picid']);
	$houseInfo = $house->getInfo($house_id);
	if($_GET['tp'] == 'pic'){
		if(is_array($ids)){
			array_remove_empty($ids);
			foreach ($ids as $id){
				//存入小区图片
				$imgInfo = $house->getImgInfo($id);
				$fieldData = array(
					'pic_url'=>$imgInfo['pic_url'],
					'pic_thumb'=>$imgInfo['pic_thumb'],
					'pic_desc'=>$imgInfo['pic_desc'],
					'borough_id'=>$houseInfo['borough_id'],
					'creater'=>$imgInfo['creater'],
					'addtime'=>$imgInfo['addtime'],
				);
				$borough->insertPic($fieldData);
				//删除
				$house->delHousePic($id);
			}
			$house_img = $house->getImgList($house_id,'*');
			if($house_img){
				$house->update($house_id,'house_thumb',$house_img[0]['pic_url']);
			}
		}
	}else{
		$member = new Member($query);
		$houseInfo['broker_id'] = $houseInfo['broker_id']? $houseInfo['broker_id']:$houseInfo['consigner_id'];
		if($houseInfo['broker_id']){
			$creater = $member->getInfo($houseInfo['broker_id'],'username');
		}
		$fieldData = array(
			'pic_url'=>$houseInfo['house_drawing'],
			'pic_thumb'=>$houseInfo['house_drawing'],
			'pic_desc'=>$houseInfo['borough_name'],
			'borough_id'=>$houseInfo['borough_id'],
			'creater'=>$creater,
			'addtime'=>$houseInfo['created'],
		);
		$borough->insertDrawing($fieldData);
		//$house->delHouseDraw($house_id);
	}
	echo 1;
	exit;
}else{
	$page->name = 'managePic';
	$houseInfo = $house->getInfo($house_id,'*');
	if($_GET['tp'] == 'pic'){
		$houseImgList = $house->getImgList($house_id,'*');
	}else{
		$houseImgList[0]['pic_url'] = $houseInfo['house_drawing'];
	}
	$page->tpl->assign('houseImgList',$houseImgList);
	
	if(!$houseInfo['borough_id']){
		$page->back("没有选择小区");
	}
	if($_GET['tp'] == 'pic'){
		$boroughImgList = $borough->getImgList($houseInfo['borough_id'],false);
	}else{
		$boroughImgList = $borough->getImgList($houseInfo['borough_id'],true);
	}
	$page->tpl->assign('boroughImgList',$boroughImgList);
}

$page->show();
?>