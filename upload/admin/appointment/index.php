<?php
/**
 * @author 房产
 * @example 预约设置
 * @version 1.0 二零一二年二月七日 19:06:52
 */
require('path.inc.php');
$user->allow('brokerList');
$page->name ='index';
if($_SERVER['REQUEST_METHOD']=='POST'){
    $appTime = is_numeric($_POST['appTime'])?$_POST['appTime']:1;
    $appNum = is_numeric($_POST['appNum'])?$_POST['appNum']:1;
    $appCountNum = is_numeric($_POST['appCountNum'])?$_POST['appCountNum']:1;
    $appInterest = is_array($_POST['appInterest'])?serialize($_POST['appInterest']):serialize(array(0));
    $query->execute("UPDATE fke_appomuch SET appo_value='{$appTime}' WHERE appo_name='appTime'");
    $query->execute("UPDATE fke_appomuch SET appo_value='{$appNum}' WHERE appo_name='appNum'");
    $query->execute("UPDATE fke_appomuch SET appo_value='{$appCountNum}' WHERE appo_name='appCountNum'");
    $query->execute("UPDATE fke_appomuch SET appo_value='{$appInterest}' WHERE appo_name='appInterest'");
    echo '<script>alert("更新成功！")</script>';
}
$sql = $query->execute('SELECT * FROM fke_appomuch',1);
while($rel = $query->fetchRecord($sql,MYSQL_ASSOC)){
    if($rel['appo_name']=='appInterest'){
        $rel['appo_value'] = unserialize($rel['appo_value']);
    }
    $result[$rel['appo_name']] = $rel['appo_value'];
}
$page->tpl->assign('appo',$result);
$page->show();
?>
