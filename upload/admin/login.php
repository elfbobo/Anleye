<?php
require('path.inc.php');

$page->name = 'login';
$page->addCss('adminlogin.css');
$page->addJs('FormValid.js');

require_once($cfg['path']['apps'] . 'User.class.php');
$user = new User($query);
if ($_POST['action']=='login') {
	if (md5(strtolower($_POST['vaild']))!=$_COOKIE['validString']) {
		$errorMsg = '验证码错误！';
	}
	if (!$errorMsg) {
		specConvert($_POST, array('username'));
		if ($_POST['username'] && $_POST['passwd']) {
			$result = $user->login($_POST['username'], $_POST['passwd']);
			if ($result===true) {
				header('location:index.php');
			} else {
				$errorMsg = $result;
			}
		} else {
			$errorMsg = '请输入用户名或密码！';
		}
	}
} elseif ($_GET['action']=='logout') {
	$user->logout();
}
$page->tpl->assign('errorMsg', $errorMsg);
$page->show();
?>