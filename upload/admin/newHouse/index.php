<?php
/**
 * 小区详细信息
 *
 * @package user
 * @author 王岩 yandy@yanwee.com
 * @version 1.0
 */

require('path.inc.php');

$user->allow('boroughList');
$borough = new Borough($query);

if ($page->action=='edit') {
	$page->action = 'add';
}
if ($page->action=='add') {
	$page->name = 'newHouseEdit'; //页面名字,和文件名相同
	$page->addJs('FormValid.js');
	//字典
	$cityarea_option = Dd::getArray('cityarea');
	$page->tpl->assign('cityarea_option', $cityarea_option);
	$borough_section_option = Dd::getArray('borough_section');
	$page->tpl->assign('borough_section_option', $borough_section_option);
	
	$properties_option = Dd::getArray('borough_properties');
	$page->tpl->assign('properties_option', $properties_option);
	
	$borough_type_option = Dd::getArray('borough_type');
	$page->tpl->assign('borough_type_option', $borough_type_option);
	$borough_support_option = Dd::getArray('borough_support');
	$page->tpl->assign('borough_support_option', $borough_support_option);
	$borough_sight_option = Dd::getArray('borough_sight');
	$page->tpl->assign('borough_sight_option', $borough_sight_option);
	$borough_developer_option = Dd::getArray('borough_developer');
	$page->tpl->assign('borough_developer_option', $borough_developer_option);
	$picture_num = 0;
	$drawing_num = 0;
	
	if($_GET['id']){
		$id = intval($_GET['id']);
		$boroughInfo = $borough->getInfo($id,'*',1);
		$boroughInfo['boroughInfo']['borough_green'] = round($boroughInfo['boroughInfo']['borough_green'],2);
		$boroughInfo['boroughInfo']['borough_sight'] = explode(',',$boroughInfo['boroughInfo']['borough_sight']);
		$boroughInfo['boroughInfo']['borough_support'] = explode(',',$boroughInfo['boroughInfo']['borough_support']);

		$boroughInfo['borough']['botough_picture'] = $borough->getImgList($id,0);
		$picture_num = count($boroughInfo['borough']['botough_picture']);
		$boroughInfo['borough']['botough_drawing'] = $borough->getImgList($id,1);
		$drawing_num = count($boroughInfo['borough']['botough_drawing']);
		
		$page->tpl->assign('boroughInfo', $boroughInfo['boroughInfo']);
		$page->tpl->assign('borough', $boroughInfo['borough']);
	}
	$page->tpl->assign('to_url', $_SERVER['HTTP_REFERER']);
	$page->tpl->assign('picture_num', $picture_num);
	$page->tpl->assign('drawing_num', $drawing_num);
} elseif ($page->action=='save') {
	$to_url = $_POST['to_url'];
	$_POST['creater'] = $user->getAuthInfo('username');
	$_POST['borough']['isnew'] = 1;
	if($borough->save($_POST)){
		$page->urlto($to_url,'添加/编辑新盘成功');	
	}else{
		$page->back('添加/编辑新盘失败');
	}
	exit;
	
} elseif ($page->action=='intention') {
	$page->name = 'IntentionList'; //页面名字,和文件名相同
	$id = intval($_GET['id']);
	$boroughName = $borough->getInfo($id,'borough_name',0,false);
	$linktype = intval($_GET['linktype']);
	$where = " borough_id = ".$id;
	if($linktype == 1){
		$q="参加团购";
		$where .= " and link_type like '%".$q."%'";
		}
	if($linktype == 2){
		$q="邮寄楼书发我";
		$where .= " and link_type like '%".$q."%'";
		}
	if($linktype == 3){
		$q="登记现场看房";
		$where .= " and link_type like '%".$q."%'";
		}
	if($linktype == 4){
		$q="通过电子邮件联系";
		$where .= " and link_type like '%".$q."%'";
		}
	if($linktype == 5){
		$q="通过电话联系";
		$where .= " and link_type like '%".$q."%'";
		}
	$intentionList = $borough->getIntentionList($where,' order by addtime desc ');
	$page->tpl->assign('intentionList', $intentionList);
	$page->tpl->assign('id', $id);
	$page->tpl->assign('boroughName', $boroughName);
}
elseif ($page->action=='news') {
	$page->name = 'news'; //页面名字,和文件名相同
	$boroughId = $_GET['id'];
	$boroughNewsList = $borough->getNewsList($boroughId);
	$page->tpl->assign('boroughNewsList', $boroughNewsList);
} 
elseif ($page->action=='newsSave') {
	    $to_url = $_SERVER['HTTP_REFERER'];
		$borough->saveNews($_POST);
		$page->urlto( $to_url,'添加成功');
}


elseif ($page->action=='intentionDelete') {
	$ids = $_POST['ids'];
	$to_url = $_SERVER['HTTP_REFERER'];
	if(!is_array($ids) || empty($ids)){
		$page->back('没有选择删除条目');
	}
	if($borough->intentionDelete($ids)){
		$page->urlto($to_url,'删除意向成功');
	}else{
		$page->back('删除意向失败');
	}

	exit;
}elseif ($page->action=='delete') {
	$ids = $_POST['ids'];
	$to_url = $_SERVER['HTTP_REFERER'];
	if(!is_array($ids) || empty($ids)){
		$page->back('没有选择删除条目');
	}
	
	if($borough->delete($ids)){
		$page->urlto($to_url,'删除新盘成功');
	}else{
		$page->back('删除新盘失败,可能楼盘中有房源，请删除房源之后更新缓存在删除新盘');
	}

	exit;
}elseif ($page->action=='deleteNews') {
	$ids = $_POST['ids'];
	$to_url = $_SERVER['HTTP_REFERER'];
	if(!is_array($ids) || empty($ids)){
		$page->back('没有选择删除条目');
	}
	
	if($borough->deleteNews($ids)){
		$page->urlto($to_url,'删除成功');
	}else{
		$page->back('删除失败');
	}

	exit;
}elseif ($page->action=='promote') {
	$ids = $_POST['ids'];
	$to_url = $_SERVER['HTTP_REFERER'];
	if(!is_array($ids) || empty($ids)){
		$page->back('没有选择推荐条目');
	}
	if($borough->updateNewHouse($ids,'is_promote',$_GET['value'])){
		$page->urlto($to_url,'操作成功');
	}else{
		$page->back('操作失败');
	}

	exit;
}elseif ($page->action=='priceoff') {
	$ids = $_POST['ids'];
	$to_url = $_SERVER['HTTP_REFERER'];
	if(!is_array($ids) || empty($ids)){
		$page->back('没有选择促销条目');
	}
	if($borough->updateNewHouse($ids,'is_priceoff',$_GET['value'])){
		$page->urlto($to_url,'操作成功');
	}else{
		$page->back('操作失败');
	}

	exit;
}else{
	$page->name = 'newHouseList'; //页面名字,和文件名相同
    $check = intval($_GET['check']);
	$keyword = $_REQUEST['q']=='请输入小区名称,小区地址'?"":trim($_REQUEST['q']); 
	$cityarea_id = intval($_REQUEST['cityarea']);
	$where = ' and isnew =1 and isdel=0 ';
	if($cityarea_id){
		$where .= " and cityarea_id =".$cityarea_id;
	}
	if($keyword){
		$where .= " and (borough_name like '%".$keyword."%' or borough_address like '%".$keyword."%')";
	}
	
	$areaLists = Dd::getArray('cityarea');
	$page->tpl->assign('areaLists', $areaLists);
	$borough_section = Dd::getArray('borough_section');
	
	require($cfg['path']['lib'] . 'classes/Pages.class.php');
	$pages = new Pages($borough->getCount($check,$where));
	$pageLimit = $pages->getLimit();
	$dataList = $borough->getList($pageLimit,$check,$where,' order by created desc ');
	foreach ($dataList as $key => $value){
		$dataList[$key]['cityarea_id'] = $areaLists[$value['cityarea_id']];
		$dataList[$key]['borough_section'] = $borough_section[$value['borough_section']];
	}
	
	$page->tpl->assign('cityarea', $cityarea_id);
	$page->tpl->assign('q', $keyword);
	$page->tpl->assign('dataList', $dataList);
	$page->tpl->assign('pagePanel', $pages->showCtrlPanel());//分页条
}
$page->show();
?>