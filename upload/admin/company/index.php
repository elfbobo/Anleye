<?php
/**
 * 后台用户
 *
 * @package user
 * @author 王岩 yandy@yanwee.com
 * @version 1.0
 */

require('path.inc.php');
$page->title .= ' 中介公司';
$page->name = 'company'; //页面名字,和文件名相同
$company = new Company($query);

//获取公司类别
$type = $_GET['type'];
if ($page->action=='edit') {
	$page->action = 'add';
}
if ($page->action=='add') {
	$page->addJs('FormValid.js');
    $id = intval($_GET['id']);
	if($id){
		$dataInfo=$company->getInfo($id);
		$page->tpl->assign('dataInfo', $dataInfo);
	}
	
} elseif ($page->action=='save') {
  $type = $_POST['company']['type'];
  if($_POST['id'] && empty($_POST['company']['passwd'])){
	  unset($_POST['company']['passwd']);
	  }else{
		    $_POST['company']['passwd'] = md5($_POST['company']['passwd']);
		  }
	if($company->save($_POST)){
		$page->urlto('index.php?type='.$type,'保存成功');	
	}else{
		$page->back('保存失败');
	}
	
	exit;
} elseif ($page->action=='delete') {
     $back_url = $_SERVER['HTTP_REFERER'];
	$company_id = $_POST['ids'];
	if ($company_id) {
		try {
			$company->delete($company_id);
			$page->urlto($back_url,'删除成功');
		} catch (Exception $e) {
			//echo $e->getMessage();
			$page->urlto('index.php','删除失败');
		}
		
	}
	exit;
}elseif ($page->action=='status') {
	$back_url = $_SERVER['HTTP_REFERER'];
	$ids = $_POST['ids'];
	$dostatus = intval($_GET['dostatus']);
	if(!is_array($ids) || empty($ids)){
		$page->back('没有选择需要操作的条目');
	}
	try{
		$company->changeStatus($ids,$dostatus);
		$page->urlto($back_url,'操作成功');
	}catch (Exception $e){
		$page->back($e->getMessage());
	}

	exit;
}else {
	require($cfg['path']['lib'] . 'classes/Pages.class.php');
	//分页类
	$pages = new Pages($user->getCount());
	$pageLimit = $pages->getLimit();
	$companyList = $company->getList($pageLimit,'*',' and type='.$type,'');
	$page->tpl->assign('companyList', $companyList);
	$page->tpl->assign('pagePanel', $pages->showCtrlPanel());//分页条
}
$page->show();
?>