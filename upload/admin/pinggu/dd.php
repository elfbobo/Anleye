<?php
/**
 * 字典操作
 *
 * @author 王岩 yandy@yanwee.com
 * @package package
 * @version $Id$
 */


require('path.inc.php');
$page->title .= '评估调整系数管理';
$page->name = 'dd'; //页面名字,和文件名相同
$page->addJs('FormValid.js');
$dd = new PingGuDd($query);
if($page->action == 'order'){
	//order
	$order = $_POST['list_order'];
	$group = $_POST['list_group'];
	$dd_id = intval($_GET['dd_id']);
	if(empty($order)){
		exit;
	}
	try{
		$dd->order($order,$dd_id);
		$dd->group($group,$dd_id);
		$page->urlto('?action=edit&dd_id=' . $dd_id);
	}catch (Exception $e){
		$page->back($e->getMessage());
	}
	
}elseif ($page->action=='edit') {
	$_GET['dd_id'] = $_GET['dd_id'] ? $_GET['dd_id'] : $_POST['dd_id'];
	if ($_POST) {//添加编辑项
		try {
			$dd->save($_POST);
		} catch (Exception $e) {
			$page->urlto('?action=edit&dd_id=' . $_GET['dd_id'], $e->getMessage());
		}
	}
	if ($_GET['di_id']) {//取编辑项信息
		$diInfo = $dd->getDiInfo($_GET['di_id']);
		$page->tpl->assign('diInfo', $diInfo);
	}
	$page->tpl->assign('dd_id', $_GET['dd_id']);
	$page->tpl->assign('list', $dd->getItemList($_GET['dd_id']));
} else if ($page->action=='delete') {
	//  删除
	if ($_POST['dds']) {
		$dd->delete($_POST['dds']);
	}
	$page->urlto('?action=edit&dd_id=' . $_GET['dd_id']);
} else {
	$page->tpl->assign('list', $dd->getList());
}
$page->show();
?>