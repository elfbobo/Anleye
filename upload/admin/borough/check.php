<?php

require('path.inc.php');

$user->allow('boroughCheck');
$borough = new Borough($query);

if ($page->action=='combine') {
	$back_to = $_SERVER['HTTP_REFERER'];
	$ids = $_POST['ids'];
	$targetid = intval($_POST['borough_id']);
	$target_name = $_POST['borough_name'];
	if(!is_array($ids) || empty($ids)){
		$page->back('没有选择需要合并的小区');
	}else{
		array_walk($ids,'intval');
	}
	
	if(!$targetid){
		$page->back("没有选择合并到哪个小区");
	}
	
	$member = new Member($query);
	$messageRule = new MessageRule($query);
	$innernote = new Innernote($query);
	
	foreach ($ids as $afromid){
		$add_field = array('old_id'=>$afromid,'new_id'=>$targetid);
		$borough->db->insert('fke_borough_log',$add_field);
		//删除
		$update_field = array('isdel'=>1);
		$borough->db->update('fke_borough',$update_field,'id='.$afromid);
		//房源移动到新的小区
		$housesell = new HouseSell($query);
		$housesell->db->execute("update ".$housesell->tName." set borough_id = $targetid ,borough_name='$target_name'  where borough_id = $afromid" );
		$houserent = new HouseRent($query);
		$houserent->db->execute("update ".$houserent->tName." set borough_id = $targetid ,borough_name='$target_name' where borough_id = $afromid" );
		$member = new Member($query);
		$member->db->execute("update ".$member->tNameBrokerInfo." set borough_id = $targetid where borough_id = $afromid ");
		$member->db->execute("update ".$member->tNameOwnerInfo." set borough_id = $targetid where borough_id = $afromid ");
	
		//审核不成功，发送站内信
		$dataInfo = $borough->getInfo($afromid);
		//没通过，发送自动站内信
		if($dataInfo['creater']){
			$dataInfo['broker_id'] = $member->getIdByUsername($dataInfo['creater']);
			$username = $dataInfo['creater'];
			$message = $messageRule->getInfo(21,'rule_remark');
			$real_name = $member->getRealName($dataInfo['broker_id'],1);
			$message = sprintf($message,$real_name,$cfg['url_community'],$dataInfo['id'],$dataInfo['borough_name'],$cfg['url_community'],$targetid,$target_name,$target_name);
			$innernote->send('系统',$username,'系统消息',$message);
		}
	}
	
	$page->urlto($back_to,'合并小区成功');
	exit;
}elseif ($page->action=='delete') {
	$back_to = $_SERVER['HTTP_REFERER'];
	$ids = $_POST['ids'];
	if(!is_array($ids) || empty($ids)){
		$page->back('没有选择删除条目');
	}
	
	try{
		if($borough->delete($ids)){
			//审核不成功，发送站内信
		$member = new Member($query);
		$messageRule = new MessageRule($query);
		$innernote = new Innernote($query);
		
		foreach ($ids as $id){
			$dataInfo = $borough->getInfo($id);
			//没通过，发送自动站内信
			if($dataInfo['creater']){
				$dataInfo['broker_id'] = $member->getIdByUsername($dataInfo['creater']);
				$username = $dataInfo['creater'];
				$message = $messageRule->getInfo(3,'rule_remark');
				$real_name = $member->getRealName($dataInfo['broker_id'],1);
				$message = sprintf($message,$real_name,$cfg['url_community'],$dataInfo['id'],$dataInfo['borough_name']);
				$innernote->send('系统',$username,'系统消息',$message);
			}
		}
		$page->urlto($back_to,'删除小区成功');
			
			}
       else{
		   
		   $page->back('删除失败,可能所选择的小区中还有房源');
		   
		   }
	}
	catch(Exception $e){
		$page->back($e->getMessage());
		//$page->back('审核失败');
	}
	exit;
}elseif ($page->action=='check') {
	$ids = $_POST['ids'];
	if(!is_array($ids) || empty($ids)){
		$page->back('没有选择删除条目');
	}
	try{
		$borough->check($ids);
		$integral = new Integral($query);
		$member = new Member($query);
		foreach ($ids as $id){
			$creater = $borough->getInfo($id,'creater');
			if($creater && $creater !='游客'){
				//发布一条增加5分
				$member_id = $member->getIdByUsername($creater);
				$integral->add($member_id,13);
			}
		}
		$page->urlto('index.php','审核小区成功');
	}catch(Exception $e){
		$page->back($e->getMessage());
		//$page->back('审核失败');
	}
	exit;
}else{
	$page->name = 'boroughCheck'; //页面名字,和文件名相同

	$where = ' and isdel=0';
	//autocomplete
	$page->addjs($cfg['path']['js']."Autocompleter/lib/jquery.bgiframe.min.js");
	$page->addjs($cfg['path']['js']."Autocompleter/lib/ajaxQueue.js");
	$page->addcss($cfg['path']['js']."Autocompleter/jquery.autocomplete.css");
	$page->addjs($cfg['path']['js']."Autocompleter/jquery.autocomplete.js");
	
	$areaLists = Dd::getArray('cityarea');
	$page->tpl->assign('areaLists', $areaLists);
	$borough_section = Dd::getArray('borough_section');
	
	require($cfg['path']['lib'] . 'classes/Pages.class.php');
	$pages = new Pages($borough->getCount(5,$where));
	$pageLimit = $pages->getLimit();
	$boroughList = $borough->getList($pageLimit,5,$where,' order by created desc');
	foreach ($boroughList as $key => $value){
		$boroughList[$key]['cityarea_id'] = $areaLists[$value['cityarea_id']];
		$boroughList[$key]['borough_section'] = $borough_section[$value['borough_section']];
	}
	
	$page->tpl->assign('boroughList', $boroughList);
	$page->tpl->assign('pagePanel', $pages->showCtrlPanel());//分页条
}
$page->show();
?>