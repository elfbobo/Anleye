<?php
/**
 * 小区更新审核后台管理
 *
 * @package user
 * @author 王岩 yandy@yanwee.com
 * @version 1.0
 */

require('path.inc.php');

$user->allow('boroughUpdateCheck');
$boroughUpdate = new BoroughUpdate($query);
$member = new Member($query);
	$borough = new Borough($query);
	$messageRule = new MessageRule($query);
	$innernote = new Innernote($query);

if ($page->action=='delete') {

	$ids = $_POST['ids'];
	if(!is_array($ids) || empty($ids)){
		$page->back('没有选择删除条目');
	}
	
	try{
	foreach ($ids as $id){
				$dataInfo = $boroughUpdate->getInfo($id);
				$field = $boroughTable[$dataInfo['field_name']];
				$dataInfo['borough_name'] = $borough->getInfo($dataInfo['borough_id'],'borough_name');
				$field_name = $field['caption'];
				if($field['type'] == 'image'){
					//图片另外发送
					if($dataInfo['broker_id']){
						$message = $messageRule->getInfo(18,'rule_remark');
						$real_name = $member->getRealName($dataInfo['broker_id'],1);
						$username = $member->getInfo($dataInfo['broker_id'],'username');
						$updateTime = date("Y-m-d H:i:s",$dataInfo['add_time']);
						$message = sprintf($message,$real_name,$updateTime,$cfg['url_community'],$dataInfo['borough_id'],$dataInfo['borough_name'],$field_name);
						$innernote->send('系统',$username,'系统消息',$message);
					}
				}else{
					//其他包括选择，文字的更新
					if($dataInfo['broker_id']){
						$message = $messageRule->getInfo(4,'rule_remark');
						$real_name = $member->getRealName($dataInfo['broker_id'],1);
						$username = $member->getInfo($dataInfo['broker_id'],'username');
						$updateTime = date("Y-m-d H:i:s",$dataInfo['add_time']);
						$message = sprintf($message,$real_name,$updateTime,$cfg['url_community'],$dataInfo['borough_id'],$dataInfo['borough_name'],$field_name);
						$innernote->send('系统',$username,'系统消息',$message);
					}
				}
				
			}
		$boroughUpdate->delete($ids);
		$page->urlto('updateCheck.php?status=0','删除成功');
	}catch (Exception $e){
		$page->back($e->getMessage());
	}
	exit;
}elseif ($page->action=='status') {
	$back_to = $_SERVER['HTTP_REFERER'];
	$ids = $_POST['ids'];
	$dostatus = intval($_GET['dostatus']);
	if(!is_array($ids) || empty($ids)){
		$page->back('没有选择需要操作的条目');
	}
	
	//更新配制表
	$boroughTable = require($cfg['path']['conf'].'boroughTable.cfg.php');
	
	try{
		if($dostatus==1){
			//通过，更该信息
			$integral = new Integral($query);
			foreach($ids as $key => $a_id){
				//修改单条信息
				$boroughUpdate->changeStatus($a_id,$dostatus);
				//取得该条信息
				$dataInfo = $boroughUpdate->getInfo($a_id);
				$dataInfo['borough_name'] = $borough->getInfo($dataInfo['borough_id'],'borough_name');
				
				if(!$field = $boroughTable[$dataInfo['field_name']]){
					//更新非法字段
					unset($ids[$key]);
					continue;
				}
				if( $dataInfo['field_name']=="borough_alias" || $dataInfo['field_name']=="borough_name" ){
					//需要更新拼写字段
					$borough_name = $borough->getInfo($dataInfo['borough_id'],'borough_name');
					$borough_letter = GetPinyin($borough_name.$dataInfo['new_value'],1);
					$updateField = array (
						"borough_alias"=>$dataInfo['new_value'],
						"borough_letter"=>$borough_letter
					);
					$borough->db->update($field['table'],$updateField,' id ='.$dataInfo['borough_id'] );
					if($dataInfo['broker_id']){
						$integral->add($dataInfo['broker_id'],14);
					}
					$ruleInfo = $integral->getInfo(14);
					//发送站内信
					$field_name = $field['caption'];
					if($dataInfo['broker_id']){
						$message = $messageRule->getInfo(19,'rule_remark');
						$real_name = $member->getRealName($dataInfo['broker_id'],1);
						$username = $member->getInfo($dataInfo['broker_id'],'username');
						$updateTime = date("Y-m-d H:i:s",$dataInfo['add_time']);
						$message = sprintf($message,$real_name,$updateTime,$cfg['url_community'],$dataInfo['borough_id'],$dataInfo['borough_name'],$field_name ,$ruleInfo['rule_score']);
						$innernote->send('系统',$username,'系统消息',$message);
					}
					
				}elseif($field['type'] == 'image' && $field['num'] > 1){
					//图片和户型图执行插入操作
					$images = explode('|',$dataInfo['new_value']);
					$insertField = array(
						'pic_url'=>$images[1],
						'pic_thumb'=>$images[2],
						'pic_desc'=>$images[0],
						'borough_id'=>$dataInfo['borough_id'],
						'creater'=>$member->getInfo($dataInfo['broker_id'],'username',false),
						'addtime'=>time(),
					);
					$borough->db->insert($field['table'],$insertField);
					if($dataInfo['field_name']=="borough_pic"){
						//图片。如果没有缩略图 就拿这张当缩略图
						$borough_thumb = $borough->getInfo($dataInfo['borough_id'],'borough_thumb');
						if($borough_thumb == ""){
							$updateField = array (
								'borough_thumb'=>$images[2]
							);
							$borough->db->update($borough->tName,$updateField,' id ='.$dataInfo['borough_id'] );
						}
					}
					//增加积分
					if($dataInfo['broker_id']){
						$integral->add($dataInfo['broker_id'],15);
					}
					$ruleInfo = $integral->getInfo(15);
					//发送站内信
					$field_name = $field['caption'];
					if($dataInfo['broker_id']){
						$message = $messageRule->getInfo(20,'rule_remark');
						$real_name = $member->getRealName($dataInfo['broker_id'],1);
						$username = $member->getInfo($dataInfo['broker_id'],'username');
						$updateTime = date("Y-m-d H:i:s",$dataInfo['add_time']);
						$message = sprintf($message,$real_name,$updateTime,$cfg['url_community'],$dataInfo['borough_id'],$dataInfo['borough_name'],$field_name,$ruleInfo['rule_score']);
						$innernote->send('系统',$username,'系统消息',$message);
					}
					
				}else{
					$updateField = array (
						$dataInfo['field_name']=>$dataInfo['new_value']
					);
					$borough->db->update($field['table'],$updateField,' id ='.$dataInfo['borough_id'] );
					if($dataInfo['broker_id']){
						$integral->add($dataInfo['broker_id'],14);
					}
					//发送站内信
					$ruleInfo = $integral->getInfo(14);
					$field_name = $field['caption'];
					if($dataInfo['broker_id']){
						$message = $messageRule->getInfo(19,'rule_remark');
						$real_name = $member->getRealName($dataInfo['broker_id'],1);
						$username = $member->getInfo($dataInfo['broker_id'],'username');
						$updateTime = date("Y-m-d H:i:s",$dataInfo['add_time']);
						$message = sprintf($message,$real_name,$updateTime,$cfg['url_community'],$dataInfo['borough_id'],$dataInfo['borough_name'],$field_name,$ruleInfo['rule_score']);
						$innernote->send('系统',$username,'系统消息',$message);
					}
				}
			}
		}else{
			//其他更改标志即可，无需更改用户信息
			$boroughUpdate->changeStatus($ids,$dostatus);
			//没通过，发送自动站内信
			
			foreach ($ids as $id){
				$dataInfo = $boroughUpdate->getInfo($id);
				$field = $boroughTable[$dataInfo['field_name']];
				$dataInfo['borough_name'] = $borough->getInfo($dataInfo['borough_id'],'borough_name');
				$field_name = $field['caption'];
				if($field['type'] == 'image'){
					//图片另外发送
					if($dataInfo['broker_id']){
						$message = $messageRule->getInfo(18,'rule_remark');
						$real_name = $member->getRealName($dataInfo['broker_id'],1);
						$username = $member->getInfo($dataInfo['broker_id'],'username');
						$updateTime = date("Y-m-d H:i:s",$dataInfo['add_time']);
						$message = sprintf($message,$real_name,$updateTime,$cfg['url_community'],$dataInfo['borough_id'],$dataInfo['borough_name'],$field_name);
						$innernote->send('系统',$username,'系统消息',$message);
					}
				}else{
					//其他包括选择，文字的更新
					if($dataInfo['broker_id']){
						$message = $messageRule->getInfo(4,'rule_remark');
						$real_name = $member->getRealName($dataInfo['broker_id'],1);
						$username = $member->getInfo($dataInfo['broker_id'],'username');
						$updateTime = date("Y-m-d H:i:s",$dataInfo['add_time']);
						$message = sprintf($message,$real_name,$updateTime,$cfg['url_community'],$dataInfo['borough_id'],$dataInfo['borough_name'],$field_name);
						$innernote->send('系统',$username,'系统消息',$message);
					}
				}
				
			}
		}
		$page->urlto($back_to,'操作成功');
	}catch (Exception $e){
		$page->back($e->getMessage());
	}

	exit;
}else{
	$page->name = 'boroughUpdateCheck'; //页面名字,和文件名相同

	if(isset($_GET['status'])){
		$where = ' status ='.intval($_GET['status']);
	}
	
	require($cfg['path']['lib'] . 'classes/Pages.class.php');
	$pages = new Pages($boroughUpdate->getCount($where));
	$pageLimit = $pages->getLimit();
	$boroughUpdateList = $boroughUpdate->getList($pageLimit,'*',$where,' order by add_time desc ');
	$member = new Member($query);
	$borough = new Borough($query);
	$boroughTable = require($cfg['path']['conf'].'boroughTable.cfg.php');
	
	foreach ($boroughUpdateList as $key => $value){
		if(!$field = $boroughTable[$value['field_name']]){
			//更新非法字段
			unset($boroughUpdateList[$key]);
			continue;
		}
		$boroughUpdateList[$key]['field_caption'] = $field['caption'];
		switch ($field['type'])
		{
			case 'dd':
				//可能是用逗号分割的字典
				$dd_name = $field['dd_name'];
				$dd_array = Dd::getArray($dd_name);
				
				$temp = explode(',',$value['new_value']);
				if(is_array($temp)){
					foreach ($temp as $tkey => $a_temp){
						$temp[$tkey] = $dd_array[$a_temp];
					}
				}
				$boroughUpdateList[$key]['new_value'] = implode(',',$temp);
				
				$temp = explode(',',$value['old_value']);
				if(is_array($temp)){
					foreach ($temp as $tkey => $a_temp){
						$temp[$tkey] = $dd_array[$a_temp];
					}
				}
				$boroughUpdateList[$key]['old_value'] = implode(',',$temp);
				break;
			case 'image':
				$images = explode('|',$value['new_value']);
				if($images[1]){
					$boroughUpdateList[$key]['new_value'] = '<a href="#" onmouseover="showPic(\''.$images[1].'\');return false;">
						<img src="'.$cfg[url].'upfile/'.$images[1].'" width="80" height="64">
					</a>';
				}
				$img_exist = '';
				if($value['old_value']){
					$imgList = explode('|',$value['old_value']);
					foreach ($imgList as $item){
						 $img_exist.= '<a href="#" onmouseover="showPic(\''.$item.'\');return false;">
							<img src="'.$cfg[url].'upfile/'.$item.'" width="80" height="64">
						</a>';
					}
					$boroughUpdateList[$key]['old_value']  = $img_exist;
				}
				break;
			case 'timestamp':
				$boroughUpdateList[$key]['old_value'] = date('%Y-%m-$d H:i:s',$value['old_value']);
				$boroughUpdateList[$key]['new_value'] = date('%Y-%m-$d H:i:s',$value['new_value']);
				break;
			case 'custom':
				$function_name =  $field['fnc'];
				$boroughUpdateList[$key]['old_value'] = eval($function_name.'('.$value['old_value'].');');
				$boroughUpdateList[$key]['new_value'] = eval($function_name.'('.$value['new_value'].');');
				break;
			default:
				break;		
		}
		$boroughUpdateList[$key]['user'] = $member->getInfo($value['broker_id'],'*',true);
		$boroughUpdateList[$key]['borough_name'] = $borough->getInfo($value['borough_id'],'borough_name');
	}
	
	$page->tpl->assign('dataList', $boroughUpdateList);
	$page->tpl->assign('pagePanel', $pages->showCtrlPanel());//分页条
}
$page->show();
?>