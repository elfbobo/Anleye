<?php
require('path.inc.php');
$user = new User($query);
$user->auth();
$page->title .= '后台首页';
$page->name = 'main'; //页面名字,和文件名相同
$page->addCss('admin.css');

$stat = new Statistics($query);
$HouseWanted = new HouseWanted($query);
$houserent = new HouseRent($query);
$housesell = new HouseSell($query);
$xiaoqu = new BoroughUpdate($query);
$borough = new Borough($query);
$company = new Company($query);
$member = new Member($query);
//取二手房、小区、租房信息
$allNum = $stat->getAll('allNum');
$allNum=array_to_hashmap($allNum,'stat_index','stat_value');
$page->tpl->assign('statistics', $allNum);

//取得新楼盘数量
$newhouse = $borough->getCount('3','');
$page->tpl->assign('newhouse',$newhouse);

//取得中介公司数量
$companyCount = $company->getCount(' and type=0');
$page->tpl->assign('companyCount',$companyCount);

//取得未审核的小区更新数量
$xiaoqu_num = $xiaoqu->getCount('status = 0 ');
$page->tpl->assign('xiaoqu_num',$xiaoqu_num);


//取得搬家公司数量
$moveCompanyCount = $company->getCount(' and type=1');
$page->tpl->assign('moveCompanyCount',$moveCompanyCount);

//取得装修公司数量
$decorationCompanyCount = $company->getCount(' and type=2');
$page->tpl->assign('decorationCompanyCount',$decorationCompanyCount);

//取得未审核求租数量
$Unaudited_hold = $HouseWanted->getCount('wanted_type = 2 and status = 0');
$page->tpl->assign('Unaudited_hold',$Unaudited_hold);

//取得未审核求购数量
$Unaudited_buy = $HouseWanted->getCount('wanted_type = 1 and status = 0');
$page->tpl->assign('Unaudited_buy',$Unaudited_buy);

//取二手房均价
$val = $stat->getAll('val');
$val=array_to_hashmap($val,'stat_index','stat_value');
$page->tpl->assign('val', $val);

//取得出售房源置顶数量
$Unaudited_housesell = $housesell->getCount('10','');
$page->tpl->assign('Unaudited_housesell',$Unaudited_housesell);


//取得出租房源置顶数量
$Unaudited_houserent = $houserent->getCount('10','');
$page->tpl->assign('Unaudited_houserent',$Unaudited_houserent);

//未开通经纪人
$noOpen = $member->getCount('status =1');
$page->tpl->assign('noOpen',$noOpen);

//个人出售未审核数量
$guestSale = $housesell->getCount('2','');
$page->tpl->assign('guestSale',$guestSale);

//个人出租未审核数量
$guestRent = $houserent->getCount('2','');
$page->tpl->assign('guestRent',$guestRent);

//取得未审核的小区更新数量
$xiaoqu_num = $xiaoqu->getCount('status = 0 ');
$page->tpl->assign('xiaoqu_num',$xiaoqu_num);

//取得未审核的小区数量
$boroughnum = new Borough($query);
$borough_num = $boroughnum->getCount(0,' and is_checked=0 ');
$page->tpl->assign('borough_num',$borough_num);

//经纪人身份未审核数量
$brokerid= new Identity ($query);
$broker_id = $brokerid->getCount('status=0');
$page->tpl->assign('broker_id',$broker_id);

//未受理的委托房源
$consignSale= new ConsignSale ($query);
$consignCount = $consignSale->getCount('1','');
$page->tpl->assign('consignCount',$consignCount);

//未受理的加盟信息
$city= new City ($query);
$UunionCount = $city->getCountUnion('1','');
$page->tpl->assign('UunionCount',$UunionCount);

//经纪人头像未审核数量
$brokerAvatar= new Avatar ($query);
$broker_avatar = $brokerAvatar->getCount('status=0');
$page->tpl->assign('broker_avatar',$broker_avatar);

//经纪人身份未审核数量
$brokerapi = new Aptitude($query);
$broker_api = $brokerapi->getCount('status=0');
$page->tpl->assign('broker_api',$broker_api);

//获取服务器信息
$page->banben = PHP_VERSION;
$page->xitong = PHP_OS;
$page->mysqlbanben = mysql_get_server_info();
$page->size = ini_get(post_max_size);
$page->mag = ini_get(magic_quotes_gpc);


$page->show();
?>