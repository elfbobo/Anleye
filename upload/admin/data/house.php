<?php
require('path.inc.php');

//超过10000条记录不导入？
//先导入前5000条
//再排序导入后面的剩余条数
$sql ="select * from T_H_BuyHouse";
$query_mssql->open($sql);

while ($rs = $query_mssql->fetchRecord(null,MSSQL_ASSOC)) {
	$rs = c_addslashes($rs,1);
	//封页图片
	$thumb_url = $rs['HouseImage'] && $rs['HouseImage']!="test03.jpg"?'house/sell/thumb/'.$rs['HouseImage']:'';
	
	$created = $rs['AddTime']?MyDate::transform('timestamp',$rs['AddTime']):"";
	//户型图，需要把图片存到小区库中，再取ID
	$fileExt = FileSystem::fileExt($rs['HouseMapImage'],true);
	$newDrawing =time().rand(0,1000);
	$newDrawing = $newDrawing.$fileExt;
	
	$drwaing_url = trim($rs['HouseMapImage'])?'house/sell/drawing/'.$newDrawing:'';
	//人气
	$click_num = $query_mssql2->getValue('select count(*) from T_H_Visit where HouseSize =\''.$rs['HouseSize'].'\'');
	//经纪人
	$broker_id = $query_mssql2->getValue('select Brokers_ID from FK_User_Brokers where Brokers_LoginNum =\''.$rs['UserId'].'\'');
	//朝向
	$toward = array(47=>1,48=>2,49=>3,50=>4,51=>5,52=>6,53=>7,54=>8,55=>9,548=>10);
	//装修情况
	$fitmentContent = array(557=>1,558=>2,559=>3,560=>4);
	//区域 - 冗余
	$CityErea = array(111=>1,112=>3,113=>2,114=>5,115=>4,116=>4,117=>9,118=>7,119=>6,120=>8,121=>10,122=>11,123=>12,124=>13);
	$rs['EreaId'] = $CityErea[$rs['EreaId']];
	//房屋类型
	$house_type = array(42=>1,301=>1,295=>1,43=>2,45=>3,46=>4,298=>5,300=>6,544=>7,556=>8);
	
	//以下状态需要重新考虑以前的字段
	if($rs['Isdele'] ==1 ){
		$status = 5;
	}elseif($rs['Isdele'] ==2 ){
		$status = 4;
	}elseif($rs['HouseEffec']){
		$status = 1;
	}else{
		$status = 2;
	}
	
	
	$fieldArray = array(
		'id'=>$rs['ID'],
		'house_title'=>$rs['HouseTitle'],
		'cityarea_id'=>$rs['EreaId'],
		'house_type'=>$house_type[$rs['purpose']],
		'house_no'=>substr($rs['HouseSize'],2),
		'house_price'=>$rs['PriceBegin'],
		'house_totalarea'=>$rs['EreaBegin'],
		'house_room'=>$rs['Room'],
		'house_hall'=>$rs['Hall'],
		'house_toilet'=>$rs['Guard'],
		'house_veranda'=>$rs['sun'],
		'house_topfloor'=>$rs['FloorEnd'],
		'house_floor'=>$rs['FloorBegin'],
		'house_age'=>$rs['age'],
		'house_toward'=>$toward[$rs['HouseWay']],
		'house_fitment'=>$fitmentContent[$rs['FitmentContent']],
		'house_thumb'=>$thumb_url,
		'house_desc'=>$rs['HouseMemo'],
		'borough_name'=>$rs['BoroughName'],
		'broker_id'=>$broker_id,
		'house_drawing'=>$drwaing_url,
		'is_share'=>$rs['IsShare'],
		'status'=>$status,
		'is_checked'=>1,
		'tips_num'=>0,
		'click_num'=>$click_num,
		'tel_num'=>0,
		'created'=>$created,
		'updated'=>$created,
		);
	$query->insert('fke_housesell',$fieldArray);
	//拷贝缩略图
	
	if($thumb_url){
		//$sourcePath = "d:/系统/House/".$rs['HouseImage'];
		if(in_array(substr($rs['HouseImage'],0,1),array('0','1','2','3'))){
			$sourcePath = "d:/系统/03/".$rs['HouseImage'];
		}else{
			$sourcePath = "d:/系统/49/".$rs['HouseImage'];
		}
		$destinPath = "d:/AppServ/www/系统/upfile/house/sell/thumb/".$rs['HouseImage'];
		copy($sourcePath,$destinPath);
	}
	//拷贝户型图
	if($drwaing_url){
		//$sourcePath = "d:/系统/House/".$rs['HouseMapImage'];
		if(in_array(substr($rs['HouseMapImage'],0,1),array('0','1','2','3'))){
			$sourcePath = "d:/系统/03/".$rs['HouseMapImage'];
		}else{
			$sourcePath = "d:/系统/49/".$rs['HouseMapImage'];
		}
		$destinPath = "d:/AppServ/www/系统/upfile/house/sell/drawing/".$newDrawing;
		copy($sourcePath,$destinPath);
	}
}
//把最大的house_no 存入staties库
$max_house_no = $query->getValue('select max(house_no) from fke_housesell');
$max_house_no = intval($max_house_no)+1;
$query->execute('update fke_statistics set stat_value =\''.$max_house_no.'\' where stat_index=\'housesell_no\'');
//计算总数 , 放在刷新任务中计算

print_r("房源导入完成");
exit;
?>