<?php
/**
 * 加盟信息管理
 *
 * @package user
 * @author 阿一 ayi@yanwee.com
 * @version 1.0
 * 2012-09-08
 * 
 */

require('path.inc.php');

$user->allow('union');
$city = new City($query);
$return_to = $_SERVER['HTTP_REFERER'];

if ($page->action=='delete') {
	$ids = $_POST['ids'];
	if(!is_array($ids) || empty($ids)){
		$page->back('没有选择删除条目');
	}
	try{
		$city->deleteUnion($ids);
		$page->urlto($return_to,'删除加盟信息成功');
	}catch (Exception $e){
		$page->back('删除失败，',$e->getMessage());
	}
	exit;
	}
	elseif ($page->action=='dostatus') {
	$ids = $_POST['ids'];
	$status = intval($_GET['status']);
	if(!is_array($ids) || empty($ids)){
		$page->back('没有选择更改的信息');
	}
	
	try{
		$city->checkUnion($ids,$status);
		$page->urlto($return_to,'状态修改成功');
	}catch (Exception $e){
		$page->back($e->getMessage());
	}
	
	exit;
}else{
	$page->name = 'unionList'; //页面名字,和文件名相同
	
	$check = intval($_GET['check']);
	$where = " and 1 = 1";
	
	require($cfg['path']['lib'] . 'classes/Pages.class.php');
	$pages = new Pages($city->getCountUnion($check,$where));
	$pageLimit = $pages->getLimit();
	$unionList = $city->getListUnion($pageLimit,'*',$check,$where,' order by time desc ');
	
	$page->tpl->assign('dataList', $unionList);
	$page->tpl->assign('pagePanel', $pages->showCtrlPanel());//分页条
}
$page->show();
?>