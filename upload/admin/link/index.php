<?php

require('path.inc.php');

$user->allow('class');
$outlink = new outlink($query);

if ($page->action=='edit') {
	$page->action = 'add';
}
if ($page->action=='add') {
	$page->name = 'linkEdit'; //页面名字,和文件名相同
	$page->addJs('FormValid.js');
	
	$linkClass = new linkClass($query);
	$link_class = $linkClass->getAll();
	$link_class = array_to_hashmap($link_class,'id','class_name');
	$page->tpl->assign('link_class', $link_class);
	
	if($_GET['id']){
		$id = intval($_GET['id']);
		$dataInfo = $outlink->getInfo($id,'*');
		$page->tpl->assign('dataInfo', $dataInfo);
	}
	$back_url = $_SERVER['HTTP_REFERER'];
	$page->tpl->assign('back_url', $back_url);
} elseif ($page->action=='save') {
	$to_url = $_POST['back_url'];
	if(strpos($_POST['link_url'],'http://') === false){
		$_POST['link_url'] = "http://".$_POST['link_url'];
	}
	
	if($outlink->save($_POST)){
		$page->urlto($to_url,'保存成功');	
	}else{
		$page->back('保存失败');
	}
	exit;
}elseif ($page->action=='order') {
	array_walk($_POST['list_order'],'intval');
	$back_url = $_SERVER['HTTP_REFERER'];
	if($outlink->order($_POST['list_order'])){
		$page->urlto($back_url,'保存成功');
	}else{
		$page->back('保存失败');
	}
	exit;
}elseif ($page->action=='status') {
	$ids = $_POST['ids'];
	$back_url = $_SERVER['HTTP_REFERER'];
	if(!is_array($ids) || empty($ids)){
		$page->back('没有选择条目');
	}
	$status = intval($_GET['status']);
	if($outlink->status($ids,$status)){
		$page->urlto($back_url,'操作成功');
	}else{
		$page->back('操作成功');
	}
	exit;
}elseif ($page->action=='delete') {
	$ids = $_POST['ids'];
	$back_url = $_SERVER['HTTP_REFERER'];
	if(!is_array($ids) || empty($ids)){
		$page->back('没有选择删除条目');
	}
	
	if($outlink->delete($ids)){
		$page->urlto($back_url,'删除成功');
	}else{
		$page->back('删除失败');
	}
	exit;
}else{
	$page->name = 'linkList'; //页面名字,和文件名相同
	
	$where = "1";
	if($_GET['class']){
		$where .= " and link_class = ".intval($_GET['class']);
	}
	
	$linkClass = new linkClass($query);
	$link_class = $linkClass->getAll();
	$link_class = array_to_hashmap($link_class,'id','class_name');
	$page->tpl->assign('link_class', $link_class);
	
	require($cfg['path']['lib'] . 'classes/Pages.class.php');
	$pages = new Pages($outlink->getCount($where));
	$pageLimit = $pages->getLimit();
	$dataList = $outlink->getList($pageLimit,'*',$where,' order by link_class ,list_order asc ');
	
	foreach ($dataList as $key => $item){
		$dataList[$key]['link_class_str'] = $link_class[$item['link_class']];
	}
	$page->tpl->assign('dataList', $dataList);
	$page->tpl->assign('pagePanel', $pages->showCtrlPanel());//分页条
}
$page->show();
?>