<?php
	require('path.inc.php');
	header('content-Type: text/html; charset=utf-8');
	$page->googlekey = iconv('gb2312','utf-8',$page->googlekey);
	$borough_id = intval($_GET['id']);
	if(!$borough_id){
		echo '
		<script>
			parent.document.getElementById(\'mapDiv\').style.display="none";
      parent.document.getElementById(\'mapDivLink\').style.display="none";
    </script>
		';
		exit;
	}
	$borough = new Borough($query);
	$boroughInfo = $borough->getInfo($borough_id,'*',1,true);
	//charsetIconv($boroughInfo,'gbk','utf-8');
	$pos = strpos($boroughInfo['borough_name'],'(');
	if($pos!==false){		
		$boroughInfo['borough_name'] = substr($boroughInfo['borough_name'],0,$pos);
	}
	
	$boroughInfo['borough_address'] = iconv('gb2312','utf-8',$boroughInfo['borough_address']);
	$boroughInfo['borough_name'] = iconv('gb2312','utf-8',$boroughInfo['borough_name']);
	
	$pos = strpos($boroughInfo['borough_name'],'（');
	if($pos!==false){		
		$boroughInfo['borough_name'] = substr($boroughInfo['borough_name'],0,$pos);
	}
	if(strpos($boroughInfo['borough_name'],iconv('gb2312','utf-8',$page->city))===false){
		$boroughInfo['borough_name'] = iconv('gb2312','utf-8',$page->city).$boroughInfo['borough_name'];
	}
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xmlns:v="urn:schemas-microsoft-com:vml">
  <head>
    <meta http-equiv="content-type" content="text/html; charset=utf-8"/>
    <title>小区地图</title>
    <script src="<?php echo $cfg['path']['js'].'jquery.js'; ?>" type="text/javascript"></script>
 <script type="text/javascript" src="http://maps.google.cn/maps?file=api&amp;v=3&amp;key&amp;sensor=false&amp;hl=zh-CN"></script>
    <script src="http://www.google.com/uds/api?file=uds.js&amp;v=1.0" type="text/javascript"></script>
    
    <script type="text/javascript">
    var map = null;
    var geocoder = null;         
    function initialize() {
      if (GBrowserIsCompatible()) {
        map = new GMap2(document.getElementById("map_canvas"));
        //map.setCenter(new GLatLng(39.917, 116.397), 13);
        geocoder = new GClientGeocoder();
        map.addControl(new GLargeMapControl());
          //map.addControl(new GMapTypeControl());
      }
    }

    function showAddress(address) {
      if (geocoder) {
        geocoder.getLatLng(
          address,
          function(point) {
            if (!point) {
            	parent.document.getElementById('mapDiv').style.display="none";
            	parent.document.getElementById('mapDivLink').style.display="none";
            } else {
            	$.post("ajax.php", { point: point,action:"point", id: "<?php echo $boroughInfo['id'];?>" } ,function(data){
            			//alert(data);
            		});
            	
              map.setCenter(point, 13);
              var marker = new GMarker(point);
              map.addOverlay(marker);
              marker.openInfoWindowHtml(address+"<br><span style='font-size:12px; color:#999; line-height:150%;'><?php echo $boroughInfo['borough_address']; ?></span><br><a href='<?php echo $cfg['url_community'];?>sale.php?id=<?php echo $borough_id ?>' style='font-size:12px; color:#f90; line-height:150%;' target='_blank'>二手房源:<?php echo $boroughInfo['sell_num']; ?>套</a><br><a href='<?php echo $cfg['url_community'];?>rent.php?id=<?php echo $borough_id ?>' style='font-size:12px; color:#f90; line-height:150%;' target='_blank'>租房源:<?php echo $boroughInfo['rent_num']; ?>套</a>");
            }
          }
        );
      }
    }
    function setPoint(point){
			eval("var latlng = new GLatLng"+point+";");
			map.setCenter(latlng, 13);
			var marker = new GMarker(latlng);
			map.addOverlay(marker);
			marker.openInfoWindowHtml("<a href='<?php echo $cfg['url_community'];?>g-<?php echo $borough_id ?>.html' style='font-size:14px; color:#000; line-height:150%;' target='_blank'><?php echo $boroughInfo['borough_name']; ?></a><br><span style='font-size:12px; color:#999; line-height:150%;'><?php echo $boroughInfo['borough_address']; ?></span><br><a href='<?php echo $cfg['url_community'];?>sale.php?id=<?php echo $borough_id ?>' style='font-size:12px; color:#f90; line-height:150%;' target='_blank'>二手房源:<?php echo $boroughInfo['sell_num']; ?>套</a><br><a href='<?php echo $cfg['url_community'];?>rent.php?id=<?php echo $borough_id ?>' style='font-size:12px; color:#f90; line-height:150%;' target='_blank'>租房源:<?php echo $boroughInfo['rent_num']; ?>套</a>");
    }
    </script>
  </head>

  <body onunload="GUnload()">
      <div id="map_canvas" style="width: 680px; height: 300px"></div>
  </body>
  <script type="text/javascript">
  	initialize();
  	<?php if ($boroughInfo['layout_map'] ){ ?>
  	  setPoint('<?php echo $boroughInfo['layout_map']; ?>');
    <?php }else{ ?>
    	showAddress('<?php echo $boroughInfo['borough_name']; ?>');
    <?php } ?>
  </script>
</html>


