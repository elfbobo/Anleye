<?php
require('path.inc.php');
$page->name = 'index'; //页面名字,和文件名相同	
$page->title = $page->city.'二手房，'.$page->city.'租房 - '. $page->titlec.' 专业的'.$page->city.'房产网站';   //网站名称

//关键词和描述
$page->keyword = $page->index_keyword;
$page->description = $page->index_description;

$page->addJs('jquery-1.2.6.min.js');

$houseSell = new HouseSell($query);
$houseRent = new HouseRent($query);
$borough = new Borough($query);
$statistics = new Statistics($query);
$consign = new Consign($query);
$member = new Member($query);
$todaySell=$houseSell->getCount(6," and timestampdiff(day ,from_unixtime(created, '%Y-%m-%d'),now())<=1");
$todayRent=$houseRent->getCount(6," and timestampdiff(day ,from_unixtime(created, '%Y-%m-%d'),now())<=1");
$addFangNum= $todaySell+$todayRent;
$page->tpl->assign('addFangNum', $addFangNum);
//区域字典
$cityarea_option = Dd::getArray('cityarea');
$page->tpl->assign('cityarea_option', $cityarea_option);

//面积
$house_totalarea_option = array(
	'0-50'=>'50㎡以下',
	'50-70'=>'50-70㎡',
	'70-90'=>'70-90㎡',
	'90-110'=>'90-110㎡',
	'110-130'=>'110-130㎡',

);
$page->tpl->assign('house_totalarea_option', $house_totalarea_option);

//二手房价
$house_price_option = array(
	'0-40'=>'40万以下',
	'40-60'=>'40-60万',
	'60-80'=>'60-80万',
	'80-100'=>'80-100万',
	'100-120'=>'100-120万',
	'120-150'=>'120-150万',
);
$page->tpl->assign('house_price_option', $house_price_option);

//租房价格
$house_price_rent_option = array(
	'0-600'=>'600元以下',
	'600-800'=>'600-800元',
	'800-1000'=>'800-1000元',
	'1000-1200'=>'1000-1200元',
	'1200-1500'=>'1200-1500元',
	'1500-2000'=>'1500-2000元',
	'2000-3000'=>'2000-3000元',
);
$page->tpl->assign('house_price_rent_option', $house_price_rent_option);


//取得首页友情文字链接
$lianjie = new outlink($query);
$lianjie_s = $lianjie->getlists('*','link_type = 1','order by list_order asc');
$page->tpl->assign('lianjie_s', $lianjie_s);

//取得首页友情图片链接
$lianjie_a = $lianjie->getlists('*','link_type = 2','order by list_order asc');
$page->tpl->assign('lianjie_a', $lianjie_a);

$company = new Company($query);
//取得推荐中介公司
$companyList = $company->getList(array('rowFrom'=>0,'rowTo'=>5),'*',' and status=1 and type=0','');
$page->tpl->assign('companyList', $companyList);

//猜你喜欢出售
$houseSellLike = $houseSell->getList(array('rowFrom'=>0,'rowTo'=>2),'*',1,' and status =1 and is_like=1' ,' order by order_weight desc');
foreach ($houseSellLike as $key =>$item ){
	$houseSellLike[$key]['title'] = substrs($item['house_title'],14);
	$houseSellLike[$key]['cityarea_name'] = $cityarea_option[$item['cityarea_id']];
	$houseSellLike[$key]['description'] = $item['house_room'].'室'.$item['house_hall'].'厅，'.$item['house_totalarea'].'平米';
}
$page->tpl->assign('houseSellLike', $houseSellLike);

//猜你喜欢出租
$houseRentLike = $houseRent->getList(array('rowFrom'=>0,'rowTo'=>2),'*',1,' and status =1 and is_like=1' ,' order by order_weight desc');
foreach ($houseRentLike as $key =>$item ){
	$houseRentLike[$key]['title'] = substrs($item['house_title'],14);
	$houseRentLike[$key]['cityarea_name'] = $cityarea_option[$item['cityarea_id']];
	$houseRentLike[$key]['description'] = $item['house_room'].'室'.$item['house_hall'].'厅，'.$item['house_totalarea'].'平米';
}
$page->tpl->assign('houseRentLike', $houseRentLike);

//主题推荐
$houseSellThemes = $houseSell->getList(array('rowFrom'=>2,'rowTo'=>5),'*',1,' and status =1 and is_themes=1' ,' order by order_weight desc');
foreach ($houseSellThemes as $key =>$item ){
	$houseSellThemes[$key]['title'] = substrs($item['house_title'],14);
	$houseSellThemes[$key]['cityarea_name'] = $cityarea_option[$item['cityarea_id']];
	$houseSellThemes[$key]['description'] = $item['house_room'].'室'.$item['house_hall'].'厅，'.$item['house_totalarea'].'平米';
}
$page->tpl->assign('houseSellThemes', $houseSellThemes);

//主题推荐
$houseSellThemes1 = $houseSell->getList(array('rowFrom'=>0,'rowTo'=>0),'*',1,' and status =1 and is_themes=1' ,' order by order_weight desc');
foreach ($houseSellThemes1 as $key =>$item ){
	$houseSellThemes1[$key]['title'] = substrs($item['house_title'],14);

	$houseSellThemes1[$key]['cityarea_name'] = $cityarea_option[$item['cityarea_id']];
	$houseSellThemes1[$key]['description'] = $item['house_room'].'室'.$item['house_hall'].'厅，'.$item['house_totalarea'].'平米';
}
$page->tpl->assign('houseSellThemes1', $houseSellThemes1);

//经纪人列表
$company = new Company($query);
$brokerList = $member->getListBroker(array('rowFrom'=>0,'rowTo'=>4),'*',' m.status = 0 and m.user_type=1',' order by m.e_accurate desc');

foreach ($brokerList as $key =>$item ){
	if($item['company_id']){
		$brokerList[$key]['company_name'] = $company->getInfo($item['company_id'],'company_name');
		}
}
$page->tpl->assign('brokerList', $brokerList);


//优质二手房
$houseSellBest = $houseSell->getList(array('rowFrom'=>0,'rowTo'=>5),'*',1,' and status =1 and is_index=1' ,' order by order_weight desc');
foreach ($houseSellBest as $key =>$item ){
	$houseSellBest[$key]['title'] = substrs($item['house_title'],14);
	$houseSellBest[$key]['cityarea_name'] = $cityarea_option[$item['cityarea_id']];
	$houseSellBest[$key]['description'] = $item['house_room'].'室'.$item['house_hall'].'厅，'.$item['house_totalarea'].'平米';
}
$page->tpl->assign('houseSellBest', $houseSellBest);
//优质租房
$houseRentBest = $houseRent->getList(array('rowFrom'=>0,'rowTo'=>5),'*',1,' and status =1 and is_index=1' ,' order by order_weight desc');
foreach ($houseRentBest as $key =>$item ){
	$houseRentBest[$key]['title'] =substrs($item['house_title'],14);
	$houseRentBest[$key]['cityarea_name'] = substr($cityarea_option[$item['cityarea_id']],0,-2);
	$houseRentBest[$key]['description'] = $item['house_room'].'室'.$item['house_hall'].'厅，'.$item['house_totalarea'].'平米';
}
$page->tpl->assign('houseRentBest', $houseRentBest);

//最新二手房
$houseSellNew = $houseSell->getList(array('rowFrom'=>0,'rowTo'=>9),'*',1,' and status =1 ' ,' order by update_order desc');
foreach ($houseSellNew as $key =>$item ){
    $houseSellNew[$key]['borough_name'] = $borough->getInfo($item['borough_id'],'borough_name','','');
	$houseSellNew[$key]['cityarea_name'] = $cityarea_option[$item['cityarea_id']];
	$houseSellNew[$key]['description'] = $item['house_room'].'室';
}
$page->tpl->assign('houseSellNew', $houseSellNew);
//最新租房
$houseRentNew = $houseRent->getList(array('rowFrom'=>0,'rowTo'=>9),'*',1,' and status =1' ,' order by update_order desc');
foreach ($houseRentNew as $key =>$item ){
    $houseRentNew[$key]['borough_name'] = $borough->getInfo($item['borough_id'],'borough_name','','');
	$houseRentNew[$key]['cityarea_name'] = $cityarea_option[$item['cityarea_id']];
	$houseRentNew[$key]['description'] = $item['house_room'].'室';
}
$page->tpl->assign('houseRentNew', $houseRentNew);
//小区列表
$boroughList = $borough->getList(array('rowFrom'=>0,'rowTo'=>12),1,' and isdel =0' ,' order by sell_num+rent_num desc');
foreach ($boroughList as $key =>$item ){
	$boroughList[$key]['cityarea_name'] = $cityarea_option[$item['cityarea_id']];
	$boroughList[$key]['borough_name'] = substrs($item['borough_name'],8);
}
$page->tpl->assign('boroughList', $boroughList);

$boroughHot = $borough->getList(array('rowFrom'=>0,'rowTo'=>4),1,'','order by sell_num+rent_num desc');
$page->tpl->assign('boroughHot', $boroughHot);


$allNum = $statistics->getAll('allNum');
$allNum=array_to_hashmap($allNum,'stat_index','stat_value');

//取二手房均价
$val = $statistics->getAll('val');
$val=array_to_hashmap($val,'stat_index','stat_value');
$page->tpl->assign('val', $val);

$page->tpl->assign('statistics', $allNum);




//增加小区的thickBox
$page->addcss("thickbox.css");
$page->addjs("thickbox.js");

$page->addjs($cfg['path']['js']."Autocompleter/lib/jquery.bgiframe.min.js");
$page->addjs($cfg['path']['js']."Autocompleter/lib/ajaxQueue.js");
$page->addcss($cfg['path']['js']."Autocompleter/jquery.autocomplete.css");
$page->addjs($cfg['path']['js']."Autocompleter/jquery.autocomplete.js");

//字典
$house_type_option = Dd::getArray('house_type');
$page->tpl->assign('house_type_option', $house_type_option);

//特色
$house_toword_option = Dd::getArray('house_toward');
$page->tpl->assign('house_toword_option', $house_toword_option);

$page->tpl->assign('menu', 'index');
$page->show();
//小区均价与评估价更新
$dotime = new DoTime($query);

$today = mktime(0,0,0,date('m'),date('d'),date('Y'));
$toTime = $dotime->getDotime('*');
if($today>$toTime['borough_avg']){
	$dotime->boroughAvg();
}
//经纪人房源数量统计
if($today>$toTime['member_num']){
	$dotime->memberNum();
}

//经纪人活跃度统计
if($today>$toTime['broker_active_rate']){
	$dotime->brokerActiveRate();
}

//经纪人登录积分增加
if($today>$toTime['broker_integral']){
	$dotime->brokerIntegral();
}

//全站统计
if($today>$toTime['statistics']){
	$dotime->statistics();
}
//小区图片数量导入
if($today>$toTime['borough_pic_num']){
	$dotime->boroughPicNum();
}

//房源刷新执行
$dotime->dorefresh();


//房源过期处理
if ($page->expired_switch==1){
	if($today>$toTime['houseInvalid']){
	        $dotime->houseInvalid();
         }
	}


?>