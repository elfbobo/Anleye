<?php
/**
 * 用户中心
 *
 * @author 阿一 yandy@yanwee.com
 * @package package
 * @version $Id$
 */

require('../common.inc.php');
$page->dir  = 'community';//目录名
$page->addCss('common.css');
$page->addCss('community.css');
$page->addCss('header.css');
$page->addCss('Home_Home2.css');
$page->addJs('jquery-1.2.6.min.js');

$member = new Member($query);
$realname = $_COOKIE['AUTH_MEMBER_REALNAME'] ? $_COOKIE['AUTH_MEMBER_REALNAME'] :$_COOKIE['AUTH_MEMBER_NAME'];
$page->tpl->assign('username',$realname);
if($_COOKIE['AUTH_MEMBER_STRING']){
	$member = new Member($query);
	$user_type = $member->getAuthInfo('user_type');
	$page->tpl->assign('user_type',$user_type);
}

$newMsgCount = 0;
if($_COOKIE['AUTH_MEMBER_NAME']){
	$innernote = new Innernote($query);
	$newMsgCount = $innernote->getCount(' to_del=0 and is_new = 1 and msg_to = \''.$_COOKIE['AUTH_MEMBER_NAME'].'\'');
}
$page->tpl->assign('msgCount',$newMsgCount);

$page->tpl->assign('menu', 'community');
?>