<?php
require('path.inc.php');

$page->name = 'photo'; //页面名字,和文件名相同

//区域字典
$cityarea_option = Dd::getArray('cityarea');
$page->tpl->assign('cityarea_option', $cityarea_option);

//id
$id = intval($_GET['id']);
if(!$id){
	$page->urlto('index.php');
}

//小区
$borough = new Borough($query);

//小区详细信息
$boroughInfo = $borough->getInfo($id,'id,borough_name,borough_alias',0);
$page->tpl->assign('dataInfo', $boroughInfo);

//小区图片
$boroughImageList = $borough->getImgList($id,0);
$p_id=intval($_GET['p_id']);

if(!$p_id){
	$p_id=1;
}
$picInfo=$boroughImageList[$p_id-1]['pic_url'];
$page->tpl->assign('pic_url', $picInfo);	
$page->tpl->assign('boroughImageList', $boroughImageList);
$page->tpl->assign('next_img_id', $p_id+1);

$borough_img_num = count($boroughImageList);

$page->tpl->assign('borough_img_num', $borough_img_num);


//小区详细信息
$boroughInfo = $borough->getInfo($id,'*',1,true);

if(!$boroughInfo){
	$page->urlto('index.php');
}

$boroughInfo['cityarea_name'] = $cityarea_option[$boroughInfo['cityarea_id']];
$boroughInfo['borough_section'] = Dd::getCaption('borough_section',$boroughInfo['borough_section']);
$boroughInfo['borough_support'] = Dd::getCaption('borough_support',$boroughInfo['borough_support']);
$boroughInfo['borough_sight'] = Dd::getCaption('borough_sight',$boroughInfo['borough_sight']);
$boroughInfo['borough_type'] = Dd::getCaption('borough_type',$boroughInfo['borough_type']);
$boroughInfo['unsign_percent_change'] = abs($boroughInfo['percent_change']);
if(!$boroughInfo['borough_thumb']){
	if($boroughImageList){
		$boroughInfo['borough_thumb'] = $boroughImageList[0]['pic_url'];
		$borough->updateThumb($boroughInfo['id'],$boroughInfo['borough_thumb']);
	}
}
$page->tpl->assign('dataInfo', $boroughInfo);
//页面标题
$page->title = $boroughInfo['borough_name'].'小区照片，'.$boroughInfo['borough_name'].'小区实景，'.$boroughInfo['borough_name'].' - '.$page->city.$page->title;

//关键词
$page->keyword = $boroughInfo['borough_name'].','.$boroughInfo['borough_name'].'小区照片,'.$boroughInfo['borough_name'].'小区实景';
//描述
$page->description='';
$page->tpl->assign('community_menu','photo');
$page->show();
?>