<?php
require('path.inc.php');

$page->name = 'index'; //页面名字,和文件名相同	
$page->title = $page->city.'小区 - '.$page->titlec;   //网站名称

//关键词和描述
$page->keyword = $page->community_keyword;
$page->description = $page->community_description;


//区域字典
$cityarea_option = Dd::getArray('cityarea');
$page->tpl->assign('cityarea_option', $cityarea_option);
$borough_type_option = Dd::getArray('borough_type');
$page->tpl->assign('borough_type_option', $borough_type_option);

$house_letter_option = range('A','Z');
$page->tpl->assign('house_letter_option', $house_letter_option);

$where =' and isdel = 0 ';
//cityarea
$cityarea = intval($_GET['cityarea']);
if($cityarea){
	$where .= ' and cityarea_id = '.$cityarea;
	$page->title = $cityarea_option[$cityarea].",".$page->title;
}

$dd = new Dd($query);
$borough_section=$dd->getSonList($cityarea);
$page->tpl->assign('borough_section',$borough_section);
//cityarea2
$cityarea2 = intval($_GET['cityarea2']);
if($cityarea2){
	$where .= ' and cityarea2_id = '.$cityarea2;
	$page->title = $cityarea_option[$cityarea].",".$page->title;
}
//type
$type = intval($_GET['type']);
if($type){
	$where .= ' and borough_type = '.$type;
	$page->title = $borough_type_option[$type].",".$page->title;
}
//q
$q = $_GET['q']=="可输入小区名、路名或划片学校" ? "":trim($_GET['q']);
if($q){
	// 小区名、路名或划片学校
	$where.=" and (borough_name like '%".$q."%' or borough_alias like '%".$q."%' or borough_letter like '%".$q."%' or borough_address like '%".$q."%' or elementary_school like '%".$q."%' or middle_school like '%".$q."%' )" ;
	$page->title = $q.",".$page->title;
}
//letter
$letter = $_GET['letter'];
if($letter){
	$where .= " and borough_letter like '".$letter."%'";
	$page->title = "字母".$letter."开头,".$page->title;
}
//list_order 排序转换
switch ($_GET['list_order']){
	case "house_num desc":
		$list_order = " order by sell_num+rent_num desc";
		break;
	case "borough_avgprice asc":
		$list_order = " order by borough_avgprice asc";
		break;
	case "borough_avgprice desc":
		$list_order = " order by borough_avgprice desc";
		break;
	case "percent_change asc":
		$list_order = " order by percent_change asc";
		break;
	case "percent_change desc":
		$list_order = " order by percent_change desc";
		break;
	case "sell_num asc":
		$list_order = " order by sell_num desc";
		break;
	case "rent_num desc":
		$list_order = " order by rent_num desc";
		break;
	default:
		$list_order = " order by sell_num desc";
		break;
}

//list_num
$list_num = intval($_GET['list_num']);
if(!$list_num){
	$list_num = 10;
}

$borough = new Borough($query);

require($cfg['path']['lib'] . 'classes/Pages.class.php');
$row_count = $borough->getCount(1,$where);
$pages = new Pages($row_count,$list_num);

//page
$pageno = $_GET['pageno']?intval($_GET['pageno']):1;
$pre_page = $pageno>1?$pageno-1:1;
$next_page = $pageno<$pages->pageCount?$pageno+1:$page_count;
$page->tpl->assign('pageno', $pageno);
$page->tpl->assign('row_count', $row_count);
$page->tpl->assign('page_count', $pages->pageCount);
$page->tpl->assign('pre_page', $pages->fileName.'pageno='.$pre_page);
$page->tpl->assign('next_page', $pages->fileName.'pageno='.$next_page);

$pageLimit = $pages->getLimit();
$dataList = $borough->getList($pageLimit,1,$where,$list_order);
$member = new Member($query);
//积分配置文件
$integral_array = require_once($cfg['path']['conf'].'integral.cfg.php');

foreach ($dataList as $key=> $item){
	//随机取一个专家
	$expert_id = $borough->getRandomAdviser($item['id']);
	//专家信息
	if($expert_id){
		$dataList[$key]['broker_info'] = $member->getInfo($expert_id,'*',true);
		$dataList[$key]['broker_info']['outlet'] = substr($dataList[$key]['broker_info']['outlet'],0,strpos($dataList[$key]['broker_info']['outlet'],'-'));
		$dataList[$key]['broker_info']['brokerRank'] = getNumByScore($dataList[$key]['broker_info']['scores'],$integral_array,'pic');
	}
	$dataList[$key]['cityarea_name'] = $cityarea_option[$item['cityarea_id']];
}

$page->tpl->assign('dataList', $dataList);
$page->tpl->assign('pagePanel', $pages->showCtrlPanel_g('5'));//分页条

//右边的优质房源列表

$boroughList = $borough->getList(array('rowFrom'=>0,'rowTo'=>4),1,'','order by sell_num+rent_num desc');
$page->tpl->assign('boroughList', $boroughList);

$page->tpl->assign('back_to', urlencode($_SERVER['REQUEST_URI']));

$page->show();
?>