<?php
require('path.inc.php');

$page->name = 'general'; //页面名字,和文件名相同

//区域字典
$cityarea_option = Dd::getArray('cityarea');
$page->tpl->assign('cityarea_option', $cityarea_option);

//id
$id = intval($_GET['id']);
if(!$id){
	$page->urlto('index.php');
}

//小区
$borough = new Borough($query);

//小区详细信息
$boroughInfo = $borough->getInfo($id,'*',1,true);

if(!$boroughInfo){
	$page->urlto('index.php');
}

$boroughInfo['cityarea_name'] = $cityarea_option[$boroughInfo['cityarea_id']];
$boroughInfo['borough_section'] = Dd::getCaption('borough_section',$boroughInfo['borough_section']);
$boroughInfo['borough_support'] = Dd::getCaption('borough_support',$boroughInfo['borough_support']);
$boroughInfo['borough_sight'] = Dd::getCaption('borough_sight',$boroughInfo['borough_sight']);
$boroughInfo['borough_type'] = Dd::getCaption('borough_type',$boroughInfo['borough_type']);
$boroughInfo['unsign_percent_change'] = abs($boroughInfo['percent_change']);

//小区图片
$boroughImageList = $borough->getImgList($id,0,4);
$page->tpl->assign('boroughImageList', $boroughImageList);
$borough_img_num = count($boroughImageList);
if($borough_img_num%2){
	$borough_img_num = 2-$borough_img_num%2;
	$page->tpl->assign('borough_img_num', $borough_img_num);
}
if(!$boroughInfo['borough_thumb']){
	if($boroughImageList){
		$boroughInfo['borough_thumb'] = $boroughImageList[0]['pic_url'];
		$borough->updateThumb($boroughInfo['id'],$boroughInfo['borough_thumb']);
	}
}
$page->tpl->assign('dataInfo', $boroughInfo);

//小区户型图
$boroughDrawList = $borough->getImgList($id,1,4);
$page->tpl->assign('boroughDrawList', $boroughDrawList);
$borough_draw_num = count($boroughDrawList);
if($borough_draw_num%2){
	$borough_draw_num = 2-$borough_draw_num%2;
	$page->tpl->assign('borough_draw_num', $borough_draw_num);
}
//出售房源
$houseSell = new HouseSell($query);
$boroughSellList = $houseSell->getList(array('rowFrom'=>0,'rowTo'=>2),'*',1,' and status =1 and borough_id = '.$id,'order by order_weight desc');
foreach ($boroughSellList as $key=> $item){
	if($item['house_totalarea']){
		$boroughSellList[$key]['avg_price']= round($item['house_price']*10000/$item['house_totalarea']);
	}
}
$page->tpl->assign('boroughSellList', $boroughSellList);

//租房源
$houseRent = new HouseRent($query);
$boroughRentList =$houseRent->getList(array('rowFrom'=>0,'rowTo'=>2),'*',1,' and status =1 and borough_id = '.$id,'order by order_weight desc');
$fitment_arr = Dd::getArray('house_fitment');
foreach ($boroughRentList as $key=>$item){
	$boroughRentList[$key]['house_fitment'] = $fitment_arr[$item['house_fitment']];
}
$page->tpl->assign('boroughRentList', $boroughRentList);
//热们小区
/*
$boroughHotList =$borough->getList(array('rowFrom'=>0,'rowTo'=>2),1,' and isdel =0','order by click_num+sell_num desc');
$page->tpl->assign('boroughHotList', $boroughHotList);
*/
//价格相近小区 +-500
$boroughSamePriceList =$borough->getList(array('rowFrom'=>0,'rowTo'=>2),1,' and id<>'.$id.' and isdel =0 and borough_avgprice >='.($boroughInfo['borough_avgprice']-500).' and borough_avgprice<='.($boroughInfo['borough_avgprice']+500),'order by sell_num desc');
$page->tpl->assign('boroughSamePriceList', $boroughSamePriceList);

//cityarea小区
$sameCityareaBorough =$borough->getList(array('rowFrom'=>0,'rowTo'=>2),1,' and id<>'.$id.' and isdel =0 and cityarea_id = '.$boroughInfo['cityarea_id'],'order by sell_num desc');
$page->tpl->assign('sameCityareaBorough', $sameCityareaBorough);

//页面标题
$page->title = $boroughInfo['borough_name'].'小区信息，'.$boroughInfo['borough_name'].'小区房价 - '.$page->city.$page->titlec;

//关键词
$page->keyword =$boroughInfo['borough_name'].','.$boroughInfo['borough_name'].'小区信息,'.$boroughInfo['borough_name'].'小区房价,'.$boroughInfo['borough_name'].'小区房源';

//增加计数
$borough->increase($boroughInfo['id'],'click_num');
$page->tpl->assign('community_menu','general');
$page->show();
?>