<?php
require('path.inc.php');
$page->name = 'index'; //页面名字,和文件名相同	
$page->title = $page->city.'新房 - '.$page->titlec;   //网站名称


//关键词和描述
$page->keyword = $page->newhouse_keyword;
$page->description = $page->newhouse_description;


$borough = new Borough($query);

//二手房按价格统计
$sell_price_option = array(
	'0-5000'=>'5000元/㎡以下',
	'5000-6000'=>'5000-6000元/㎡以下',
	'6000-7000'=>'6000-7000元/㎡',
	'7000-8000'=>'7000-8000元/㎡',
	'8000-9000'=>'8000-9000元/㎡',
	'9000-10000'=>'9000-10000元/㎡',
	'10000-0'=>'10000元/㎡以上',
);

$page->tpl->assign('sell_price_option', $sell_price_option);

//按区域统计
$cityarea_option = Dd::getArray('cityarea');
$page->tpl->assign('cityarea_option', $cityarea_option);

$house_type_option = Dd::getArray('house_type');
$page->tpl->assign('house_type_option', $house_type_option);

$boroughCountCityarea = $borough->getCountGroupBy('cityarea_id',' and isnew =1 and isdel = 0 ',0);
foreach ($boroughCountCityarea as $key => $item){
	$boroughCountCityarea[$key]['cityarea_name'] = $cityarea_option[$item['cityarea_id']];
}
$page->tpl->assign('boroughCountCityarea', $boroughCountCityarea);

//按板块统计
$borough_section_option = Dd::getArray('borough_section');

$page->tpl->assign('boroughCountSection', $borough_section_option);

//按开发商统计
$borough_developer_option = Dd::getArray('borough_developer');
$borough_developer_option[0] = "其他";
$boroughCountDeveloper = $borough->getCountGroupBy('borough_developer_id',' and isnew =1 and isdel = 0 ',0);
foreach ($boroughCountDeveloper as $key => $item){
	$boroughCountDeveloper[$key]['borough_developer'] = $borough_developer_option[$item['borough_developer_id']];
}
$page->tpl->assign('boroughCountDeveloper', $boroughCountDeveloper);


//字母索引
$house_letter_option = range('A','Z');
$page->tpl->assign('house_letter_option', $house_letter_option);


$where =' and isnew =1 and isdel = 0 ';

$state= $_GET['state'];
switch ($state){
	case 'promote':
		$where .=" and is_promote=1 ";
		$page->title = "热推楼盘,".$page->title;
		break;
	case 'priceoff':
		$where .=" and is_priceoff=1 ";
		$page->title = "促销楼盘,".$page->title;
		break;
	case 'new':
		$where .=" and (sell_time>'".date('Y-m-d')."' or sell_time ='' )";
		$page->title = "即将开盘,".$page->title;
		break;
	default:
		//all
		break;
}
//cityarea
$cityarea = intval($_GET['cityarea']);
if($cityarea){
	$where .= ' and cityarea_id = '.$cityarea;
	$page->title = $cityarea_option[$cityarea].",".$page->title;
}
$dd = new Dd($query);
$borough_section=$dd->getSonList($cityarea);
$page->tpl->assign('borough_section',$borough_section);

//cityarea2
$cityarea2 = intval($_GET['cityarea2']);
if($cityarea2){
	$where .= ' and cityarea2_id = '.$cityarea2;
	$page->title = $cityarea_option[$cityarea].",".$page->title;
}

//section
$section = intval($_GET['section']);
if($section){
	$where .= ' and borough_section = '.$section;
	$page->title = $borough_section_option[$section].",".$page->title;
}
//type
$type = intval($_GET['type']);
if($type){
	$where .= ' and borough_type = '.$type;
	$page->title = $house_type_option[$type].",".$page->title;
}

if(isset($_GET['developer']) && $_GET['developer']!==''){
	$developer = intval($_GET['developer']);
	$where .= ' and borough_developer_id = '.$developer;
	$page->title = $borough_developer_option[$developer].",".$page->title;
}
//q
$q = $_GET['q']=="可输入小区名、路名或划片学校" ? "":trim($_GET['q']);
if($q){
	// 小区名、路名或划片学校
	$where.=" and borough_name like '%".$q."%' or borough_address like '%".$q."%' or elementary_school like '%".$q."%' or middle_school like '%".$q."%'" ;
	$page->title = $q.",".$page->title;
}
//price
$price = $_GET['price'];
if($price){
	$tmp = explode('-',$price);
	if($tmp[0]){
		$where .= ' and borough_avgprice >= '.intval($tmp[0]);
	}
	if($tmp[1]){
		$where .= ' and borough_avgprice <= '.intval($tmp[1]);
	}
	$page->title = "均价".$price.",".$page->title;
}
//letter
$letter = $_GET['letter'];
if($letter){
	$where .= " and borough_letter like '".$letter."%'";
	$page->title = "字母".$letter."开头,".$page->title;
}
//list_order 排序转换
switch ($_GET['list_order']){
	case "updated desc":
		$list_order = " order by is_promote desc,updated desc";
		break;
	case "avg_price asc":
		$list_order = " order by  is_promote desc,borough_avgprice asc";
		break;
	case "avg_price desc":
		$list_order = " order by  is_promote desc,borough_avgprice desc";
		break;
	case "sell_time desc":
		$list_order = " order by  is_promote desc,sell_time desc";
		break;
	default:
		$list_order = " order by  is_promote desc,updated desc";
		break;
}

//list_num
$list_num = intval($_GET['list_num']);
if(!$list_num){
	$list_num = 12;
}

$borough = new Borough($query);

$boroughPriceoffList = $borough->getList(array('rowFrom'=>0,'rowTo'=>5),'2','','');
$page->tpl->assign('boroughPriceoffList', $boroughPriceoffList);

require($cfg['path']['lib'] . 'classes/Pages.class.php');
$row_count = $borough->getCount(1,$where);
$pages = new Pages($row_count,$list_num);

//page
$pageno = $_GET['pageno']?intval($_GET['pageno']):1;
$pre_page = $pageno>1?$pageno-1:1;
$next_page = $pageno<$pages->pageCount?$pageno+1:$page_count;
$page->tpl->assign('pageno', $pageno);
$page->tpl->assign('row_count', $row_count);
$page->tpl->assign('page_count', $pages->pageCount);
$page->tpl->assign('pre_page', $pages->fileName.'pageno='.$pre_page);
$page->tpl->assign('next_page', $pages->fileName.'pageno='.$next_page);

$pageLimit = $pages->getLimit();
$dataList = $borough->getListDetail($pageLimit,1,$where,$list_order);
$member = new Member($query);
//积分配置文件
$integral_array = require_once($cfg['path']['conf'].'integral.cfg.php');

foreach ($dataList as $key=> $item){
	$dataList[$key]['cityarea_name'] = $cityarea_option[$item['cityarea_id']];
	$dataList[$key]['section_name'] = $borough_section_option[$item['borough_section']];
	$dataList[$key]['borough_content'] = substrs(strip_tags($item['borough_content']),80);
}

$borough_list_num = count($dataList);
if($borough_list_num%2){
	$borough_list_num = 2-$borough_list_num%2;
	$page->tpl->assign('borough_list_num', $borough_list_num);
}

$page->tpl->assign('dataList', $dataList);
$page->tpl->assign('pagePanel', $pages->showCtrlPanel_g('5'));//分页条
$page->tpl->assign('pagePanel_top', $pages->showCtrlPanel_h('5'));//分页条

$page->tpl->assign('back_to', urlencode($_SERVER['REQUEST_URI']));

$page->show();
?>