<?php
require('path.inc.php');


$page->name = 'detail'; //页面名字,和文件名相同
//小区
$borough = new Borough($query);
//区域字典
$cityarea_option = Dd::getArray('cityarea');
$page->tpl->assign('cityarea_option', $cityarea_option);

//id
$id = intval($_GET['id']);
if($page->action=='save'){
	$borough_id = $_GET['borough_id'];
	$_POST['borough_id'] = $borough_id;
	$borough->saveIntention($_POST);
    $page->urlto('d-'.$borough_id.'.html','成功提交您的意向');	
}
if(!$id){
	$page->urlto('index.php');
}

//小区详细信息
$boroughInfo = $borough->getInfo($id,'*',1,true);
$boroughInfo['cityarea_name'] = $cityarea_option[$boroughInfo['cityarea_id']];
$boroughInfo['borough_section'] = Dd::getCaption('borough_section',$boroughInfo['borough_section']);
$boroughInfo['borough_support'] = Dd::getCaption('borough_support',$boroughInfo['borough_support']);
$boroughInfo['borough_sight'] = Dd::getCaption('borough_sight',$boroughInfo['borough_sight']);
$boroughInfo['borough_type'] = Dd::getCaption('borough_type',$boroughInfo['borough_type']);
$boroughInfo['unsign_percent_change'] = abs($boroughInfo['percent_change']);

//小区图片
$boroughImageList = $borough->getImgList($id,0,4);
$page->tpl->assign('boroughImageList', $boroughImageList);
$borough_img_num = count($boroughImageList);
if($borough_img_num%2){
	$borough_img_num = 2-$borough_img_num%2;
	$page->tpl->assign('borough_img_num', $borough_img_num);
}
if(!$boroughInfo['borough_thumb']){
	if($boroughImageList){
		$boroughInfo['borough_thumb'] = $boroughImageList[0]['pic_url'];
		$borough->updateThumb($boroughInfo['id'],$boroughInfo['borough_thumb']);
	}
}
$page->tpl->assign('dataInfo', $boroughInfo);

//小区户型图
$boroughDrawList = $borough->getImgList($id,1,4);
$page->tpl->assign('boroughDrawList', $boroughDrawList);
$borough_draw_num = count($boroughDrawList);
if($borough_draw_num%2){
	$borough_draw_num = 2-$borough_draw_num%2;
	$page->tpl->assign('borough_draw_num', $borough_draw_num);
}

//新盘新闻动态
$boroughNewsList = $borough->getNewsList($boroughInfo['id']);
$page->tpl->assign('boroughNewsList', $boroughNewsList);

$intentionCount = $borough->getIntentionCount(' and borough_id='.$boroughInfo['id']);
$page->tpl->assign('intentionCount', $intentionCount);

//价格小区
$samePriceBorough =$borough->getList(array('rowFrom'=>0,'rowTo'=>5),1,' and id<>'.$id.' and isdel =0 and isnew =1 and borough_avgprice >='.($boroughInfo['borough_avgprice']-200).' and borough_avgprice<='.($boroughInfo['borough_avgprice']+200),'order by borough_avgprice desc');
$page->tpl->assign('samePriceBorough', $samePriceBorough);

//cityarea小区
$sameCityareaBorough =$borough->getList(array('rowFrom'=>0,'rowTo'=>5),1,' and id<>'.$id.' and isdel =0 and isnew =1 and cityarea_id = '.$boroughInfo['cityarea_id'],'order by updated desc');
$page->tpl->assign('sameCityareaBorough', $sameCityareaBorough);

//网友察看
$boroughClickList =$borough->getList(array('rowFrom'=>0,'rowTo'=>5),1,' and id<>'.$id.' and isdel =0 and isnew =1','order by rand() desc');
$page->tpl->assign('boroughClickList', $boroughClickList);

//页面标题
$page->title = $boroughInfo['borough_name'].'的楼盘概况 - '.$page->title;


//增加计数
$borough->increase($boroughInfo['id'],'click_num');

$page->tpl->assign('boroughAdviserList', $boroughAdviserList);
$page->show();
?>