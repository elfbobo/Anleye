<?php
//set_magic_quotes_runtime(0);
/**
 * 主包含页面
 *
 * @author 阿一 ayi@yanwee.com
 * @package 5.0
 */



/*
* 路径
*/
date_default_timezone_set('PRC');
require('config.cfg.php');
require('safe.php');
header('content-Type: text/html; charset=' . $cfg['charset']);

require($cfg['path']['lib'] . 'classes/Util.inc.php');
//$cfg['path']['doc'] = getRootPath();
// sql server 的程序magic_quotes_gpc必须为off
$magic_quotes_gpc = get_magic_quotes_gpc();
$_COOKIE = c_addslashes($_COOKIE);
$_POST 	 = c_addslashes($_POST);
$_GET 	 = c_addslashes($_GET);


if (!$cfg['debug']) {
	error_reporting(0);
	ob_start('ob_gzhandler');
} else {
	error_reporting(E_ALL ^ E_NOTICE);
}

/*
* 数据库连接
*/

require_once($cfg['path']['lib'] . 'classes/db/DataSource.class.php');
try {
	$db = new DataSource($cfg['db']['host'], $cfg['db']['user'], $cfg['db']['password'],
	$cfg['db']['name'], $cfg['db']['provider'], true );
	$query = new DbQueryForMysql($db);

	unset($cfg['db']);
} catch (DbException $e) {
	// 数据库出错处理处
	echo '服务器忙，请<a href="javascript:document.location.reload()">刷新</a>页面，或稍后再试！谢谢！!';exit;
}
require($cfg['path']['lib']  . 'smarty/libs/Smarty.class.php');


/*
* 模版引擎
*/
$tpl = new Smarty();
$tpl->template_dir 		= $cfg['path']['template'];
$tpl->compile_dir  		= $cfg['path']['root'] . 'tmp/template_c/';

$tpl->compile_check  	= $cfg['debug'];
$tpl->debugging 	    = false;
$tpl->caching 	    	= 0;
$tpl->cache_lifetime 	= 6000;

$tpl->left_delimiter 	= '<!--{';
$tpl->right_delimiter 	= '}-->';
$tpl->force_compile		= false;

/*
* 引用赋值
*/
$tpl->assign_by_ref('cfg',$cfg);


require($cfg['path']['lib'] . 'classes/Html.class.php');

function time2Units ($time)
{
$year = floor($time / 60 / 60 / 24 / 365);
$time -= $year * 60 * 60 * 24 * 365;
$month = floor($time / 60 / 60 / 24 / 30);
$time -= $month * 60 * 60 * 24 * 30;
$week = floor($time / 60 / 60 / 24 / 7);
$time -= $week * 60 * 60 * 24 * 7;
$day = floor($time / 60 / 60 / 24);
$time -= $day * 60 * 60 * 24;
$hour = floor($time / 60 / 60);
$time -= $hour * 60 * 60;
$minute = floor($time / 60);
$time -= $minute * 60;
$second = $time;
$elapse = '';

$unitArr = array('年' =>'year', '个月'=>'month', '周'=>'week', '天'=>'day',
'小时'=>'hour', '分钟'=>'minute', '秒'=>'second'
);

foreach ( $unitArr as $cn => $u )
{
if ( $$u > 0 )
{
$elapse = $$u . $cn;
break;
}
}

return $elapse;
}


/**
 * 页面信息类
 */
class Page extends Html {

	public $tpl;

	public $cache;


	function __construct() {
		$GLOBALS['cfg']['page'] = array();
	}

	public function __set($key, $value) {
		$GLOBALS['cfg']['page'][$key] = $value;
	}

	public function __get($key) {
		return $GLOBALS['cfg']['page'][$key];
	}
	
	

	/**
	 * 添加一个JS文件包含
	 * @param string $file 文件名
	 * @access public
	 * @return void
	 */
	public function addJs($file,$btm=NULL) {
		if (strpos($file,'/')==false) {
			$file = $GLOBALS['cfg']['path']['js'] . $file;
		}
		if ($btm==NULL) {
			$GLOBALS['cfg']['page']['jsfiles'][$file] = $file;
		} else {
			$GLOBALS['cfg']['page']['jsbtmfiles'][$file] = $file;
		}
	}



	/**
	 * 取生成的包含JS HTML
	 * @access public
	 * @return string
	 */
	public function getJsHtml($btm=NULL) {
		$html = '';
		if (!$GLOBALS['cfg']['page']['jsfiles']) {
			return;
		}
		$jsFile = $btm?'jsbtmfiles':'jsfiles';
		if ($GLOBALS['cfg']['page'][$jsFile]) {
			foreach ($GLOBALS['cfg']['page'][$jsFile] as $value) {
				$html .= $this->jsInclude($value,true)."\n";
			}
			return $html;
		} else {
			return ;
		}
	}

	/**
	 * 添加一个CSS文件包含
	 * @param string $file 文件名
	 * @access public
	 * @return void
	 */
	public function addCss($file) {
		if (strpos($file,'/')==false) {
			$file = $GLOBALS['cfg']['path']['css'] . $file;
		}
		$GLOBALS['cfg']['page']['cssfiles'][$file] = $file;
	}

	/**
	 * 取生成的包含CSS HTML
	 * @access public
	 * @return string
	 */
	public function getCssHtml() {
		if (!$GLOBALS['cfg']['page']['cssfiles']) {
			return;
		}
		$html = '';
		foreach ($GLOBALS['cfg']['page']['cssfiles'] as $value) {
			$html .= $this->cssInclude($value,true);
		}
		return $html;
	}

	/**
	 * 显示输出页面
	 * @access public
	 * @return string
	 */
	public function show() {
		$path = '';
		if ($this->dir) {
			$path = $this->dir . '/';
		}
		$path .= $this->name . '.tpl';

		$this->tpl->assign('jsFiles',$this->getJsHtml());
		$this->tpl->assign('jsFiles1',$this->getJsHtml(1));
		$this->tpl->assign('cssFiles',$this->getCssHtml());
		$this->tpl->display($path);
	}

	/**
	 * 转到URL,并提示信息
	 * @param string $url URL
	 * @param string $msg 提示信息
	 * @access public
	 * @return void
	 */
	public function urlto($url, $msg=NULL) {
		if ($msg) {
			$this->jsAlert($msg);
		}
		$this->js('document.location="' . $url . '";');
		$this->output(true);
		exit;
	}
	/**
	 * replace方式转到URL,并提示信息
	 * @param string $url URL
	 * @param string $msg 提示信息
	 * @access public
	 * @return void
	 */
	public function replace($url, $msg=NULL) {
		if ($msg) {
			$this->jsAlert($msg);
		}
		$this->js('location.replace("' . $url . '");');
		$this->output(true);
		exit;
	}

	/**
	 * 返回,并提示信息
	 * @param string $msg 提示信息
	 * @access public
	 * @return void
	 */
	public function back($msg){
		$this->jsAlert($msg);
		$this->js('history.back();');
		$this->output(true);
		exit;
	}

	/**
	 * 开始页面缓存
	 * @param string $file 文件名
	 * @param string $time 有效时间
	 * @param string $output 是否输出
	 * @access public
	 * @return void
	 */
	public function cache($file, $time, $output) {
		global $cfg;
		require_once($cfg['path']['lib'] . 'classes/PageCache.class.php');
		$this->cache = new PageCache($file, $time);
		$this->cache->get($output);
	}

	/**
	 * 保存页面缓存
	 * @access public
	 * @return void
	 */
	public function save() {
		$path = '';
		if ($this->dir) {
			$path = $this->dir . '/';
		}
		$path .= $this->name . '.tpl';

		$this->tpl->assign('jsFiles',$this->getJsHtml());
		$this->tpl->assign('cssFiles',$this->getCssHtml());
		$content = $this->tpl->fetch($path);
		if ($this->cache) {
			$this->cache->set($content);
		}
	}
}
/**
 * 自动载入apps,classes
 *
 * @param string $name
 * @return bool
 */
function __autoload($name) {
	global $cfg;
	if (!file_exists($cfg['path']['apps'] . $name . '.class.php')) {
		if (file_exists($cfg['path']['lib'] .'classes/' . $name . '.class.php')) {
			require_once($cfg['path']['lib'] .'classes/' . $name . '.class.php');
			return true;
		} else {
			return false;
		}
	}
	require_once($cfg['path']['apps'] . $name . '.class.php');
	return true;
}

$page = new Page();
$page->tpl = $tpl;
$page->action = $_REQUEST['action'];
$page->version = 'ANLEYE V5 20130205';
if ($_GET['request']=='ajax') {
	$_POST = charsetIconv($_POST);
}
/**
 * 预约刷新配置
 * @author 房产
 */
$sql = $query->execute('SELECT * FROM fke_appomuch');
while ($rel = $query->fetchRecord($sql, MYSQL_ASSOC)) {
    if ($rel['appo_name'] == 'appInterest') {
        $rel['appo_value'] = unserialize($rel['appo_value']);
    }
    $appoMuth[$rel['appo_name']] = $rel['appo_value'];
}
$time = time();
$updateDate = strtotime(date('Y-m-d H:i',$time));
if(date('i',$time)%2==0){
    $houseRent = new HouseRent($query);
    $houseSell = new HouseSell($query);
    $sql = $query->execute("SELECT appo_list_id,house_id,update_time,appo_site FROM fke_appolist WHERE update_time <= '{$updateDate}'");
    while($rel = $query->fetchRecord($sql,MYSQL_ASSOC)){
        if($rel['appo_site']=='rent'){
            $houseRefresh = $houseRent->getInfo($rel['house_id'],'refresh');
            if ($houseRefresh == 0) {
                continue;
            }
            $houseRent->updateRefresh($rel['house_id'],$rel['update_time']);
            $houseRent->refresh($rel['house_id'],$rel['update_time']);
        }else{
            $houseRefresh = $houseSell->getInfo($rel['house_id'],'refresh');
            if ($houseRefresh == 0) {
                continue;
            }
            $houseSell->updateRefresh($rel['house_id'],$rel['update_time']);
            $houseSell->refresh($rel['house_id'],$rel['update_time']);
        }
    }
    $query->execute("DELETE FROM fke_appolist WHERE update_time <= '{$updateDate}'");

}
require('anleye.php');
function print_rr($var){
	echo "<pre>";
	print_r($var);
	echo "</pre>";
}
?>