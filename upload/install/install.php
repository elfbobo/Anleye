<?php
/*
	 安装文件,修改自pbdigg.
*/
error_reporting(0);
session_start();
define('THINKSNS_INSTALL', TRUE);
define('THINKSNS_ROOT', str_replace('\\', '/', substr(dirname(__FILE__), 0, -7)));

$_TSVERSION = 'V5 正式版';

include 'install_function.php';
include 'install_lang.php';

$timestamp				=	time();
$ip						=	getip();
$installfile			=	'anleye.sql';
$thinksns_config_file	=	'config.cfg.php';
$thinksns_define_file	=	'define.inc.php';

//判断是否安装过
header('Content-Type: text/html; charset=GBK');
if (file_exists('install.lock'))
{
	exit($i_message['install_lock']);
}
if (!is_readable($installfile))
{
	exit($i_message['install_dbFile_error']);
}
$quit = false;
$msg = $alert = $link = $sql = $allownext = '';

$PHP_SELF = addslashes(htmlspecialchars($_SERVER['PHP_SELF'] ? $_SERVER['PHP_SELF'] : $_SERVER['SCRIPT_NAME']));
set_magic_quotes_runtime(0);
if (!get_magic_quotes_gpc())
{
	addS($_POST);
	addS($_GET);
}
@extract($_POST);
@extract($_GET);
?>
<html>
<head>
<title><?php echo $i_message['install_title']; ?></title>
<meta http-equiv=Content-Type content="text/html; charset=gbk">
<link href="images/style.css" rel="stylesheet" type="text/css" />
<body>
<div id='content'>
<div id='pageheader'>
	<div id="logo"><img src="images/anleye.gif" width="260" height="80" border="0" alt="ANLEYE" /></div>
	<div id="version" class="rightheader">Version <?php echo $_TSVERSION; ?></div>
</div>
<div id='innercontent'>
	<h1>安乐业 <?php echo $_TSVERSION, ' ', $i_message['install_wizard']; ?></h1>
<?php
if (!$v)
{
?>
<div class="botBorder">
<p><span class='red'><?php echo $i_message['install_warning'];?></span></p>
</div>
<div class="botBorder">
<?php echo $i_message['install_intro'];?>
</div>
<form method="post" action="install.php?v=1">
<p class="center"><input type="submit" class="submit" value="<?php echo $i_message['install_start'];?>" /></p>
</form>
<?php
}
elseif ($v == '1')
{
?>
<h2><?php echo $i_message['install_license_title'];?></h2>
<p>
<textarea class="textarea" readonly="readonly" cols="50">
<?php echo $i_message['install_license'];?>
</textarea>
</p>
<form action="install.php?v=2" method="post">
<p><input type="checkbox" name="agree" value="1" onClick="if(this.checked==true){this.form.next.disabled=''}else{this.form.next.disabled='true'}" checked="checked" /><?php echo $i_message['install_agree'];?></p>
<p class="center"><input type="submit" style="width:200px;" class="submit" name="next" value="<?php echo $i_message['install_next'];?>" /></p>
</form>
<?php
}
elseif ($v == '2')
{
if ($agree == 'no')
{
	echo '<script>alert('.$i_message['install_disagree_license'].');history.go(-1)</script>';
}
$dirarray = array (
	'data',
	'conf',
	'install',
	'upfile',
	'tmp',
);
$writeable = array();
foreach ($dirarray as $key => $dir)
{
	if (writable($dir))
	{
		$writeable[$key] = $dir.result(1, 0);
	}
	else
	{
		$writeable[$key] = $dir.result(0, 0);
		$quit = TRUE;
	}
}
?>
<div class="botBorder">
<p><span class='red'><?php echo $i_message['install_dirmod'];?></span></p>
</div>
<div class="shade">
<div class="settingHead"><?php echo $i_message['install_env'];?></div>
<h5><?php echo $i_message['php_os'];?></h5>
<p><?php echo PHP_OS;result(1, 1);?></p>
<h5><?php echo $i_message['php_version'];?></h5>
<p>
<?php
echo PHP_VERSION;
if (PHP_VERSION < '5.1.2')
{
	result(0, 1);
	$quite = TRUE;
}
else
{
	result(1, 1);
}
?></p>
<h5><?php echo $i_message['file_upload'];?></h5>
<p>
<?php
if (@ini_get('file_uploads'))
{
	echo $i_message['support'],'/',@ini_get('upload_max_filesize');
}
else
{
	echo '<span class="red">'.$i_message['unsupport'].'</span>';
}
result(1, 1);
?></p>
<h5><?php echo $i_message['php_extention'];?></h5>
<p>
<?php
if (extension_loaded('mysql'))
{
	echo 'mysql:'.$i_message['support'];
	result(1, 1);
}
else
{
	echo '<span class="red">'.$i_message['php_extention_unload_mysql'].'</span>';
	result(0, 1);
	$quite = TRUE;
}
?></p>
<p>
<?php
if (extension_loaded('gd'))
{
	echo 'gd:'.$i_message['support'];
	result(1, 1);
}
else
{
	echo '<span class="red">'.$i_message['php_extention_unload_gd'].'</span>';
	result(0, 1);
	$quite = TRUE;
}
?></p>
<p>
<?php
if (extension_loaded('curl'))
{
	echo 'curl:'.$i_message['support'];
	result(1, 1);
}
else
{
	echo '<span class="red">'.$i_message['php_extention_unload_curl'].'</span>';
	result(0, 1);
	$quite = TRUE;
}
?></p>
<p>
<?php
if (extension_loaded('mbstring'))
{
	echo 'mbstring:'.$i_message['support'];
	result(1, 1);
}
else
{
	echo '<span class="red">'.$i_message['php_extention_unload_mbstring'].'</span>';
	result(0, 1);
	$quite = TRUE;
}
?></p>



<h5><?php echo $i_message['mysql'];?></h5>
<p>
<?php
if (function_exists('mysql_connect'))
{
	echo $i_message['support'];
	result(1, 1);
}
else
{
	echo '<span class="red">'.$i_message['mysql_unsupport'].'</span>';
	result(0, 1);
	$quite = TRUE;
}
?></p>


</div>
<div class="shade">
<div class="settingHead"><?php echo $i_message['dirmod'];?></div>
<?php
foreach ($writeable as $value)
{
	echo '<p>'.$value.'</p>';
}

if (is_writable(THINKSNS_ROOT.$thinksns_config_file))
{
	echo '<p>'.$thinksns_config_file.result(1, 0).'</p>';
}
else
{
	echo '<p>'.$thinksns_config_file.result(0, 1).'</p>';
	$quite = TRUE;
}

if (is_writable(THINKSNS_ROOT.$thinksns_define_file))
{
	echo '<p>'.$thinksns_define_file.result(1, 0).'</p>';
}
else
{
	echo '<p>'.$thinksns_define_file.result(0, 1).'</p>';
	$quite = TRUE;
}
?>
</div>
<p class="center">
	<form method="post" action='install.php?v=3'>
	<input style="width:200px;" type="submit" class="submit" name="next" value="<?php echo $i_message['install_next'];?>" <?php if($quit) echo "disabled=\"disabled\"";?>>
	</form>
</p>
<?php
}
elseif ($v == '3')
{
?>
<h2><?php echo $i_message['install_setting'];?></h2>
<form method="post" action="install.php?v=4" id="install" onSubmit="return check(this);">
<div class="shade">
<div class="settingHead"><?php echo $i_message['dirmod'];?></div>

<h5><?php echo $i_message['install_mysql_host'];?></h5>
<p><?php echo $i_message['install_mysql_host_intro'];?></p>
<p><input type="text" name="db_host" value="localhost" size="40" class='input' /></p>

<h5><?php echo $i_message['install_mysql_username'];?></h5>
<p><input type="text" name="db_username" value="root" size="40" class='input' /></p>

<h5><?php echo $i_message['install_mysql_password'];?></h5>
<p><input type="password" name="db_password" value="" size="40" class='input' /></p>

<h5><?php echo $i_message['install_mysql_name'];?></h5>
<p><input type="text" name="db_name" value="anleye" size="40" class='input' />
</p>


<h5><?php echo $i_message['site_url'];?></h5>
<p><?php echo "请输入URL地址，WWW形式";?></p>
<p><input type="text" name="db_URL" value="http://<?php echo$_SERVER['SERVER_NAME'];?>/" size="40" class='input' /></p>

</div>

<div class="center">
<input type="button" class="submit" name="prev" value="<?php echo $i_message['install_prev'];?>" onClick="history.go(-1)">&nbsp;
	<input type="submit" class="submit" name="next" value="<?php echo $i_message['install_next'];?>">
</form>
</div>
<script type="text/javascript" language="javascript">
function check(obj)
{
	if (!obj.db_host.value)
	{
		alert('<?php echo $i_message['install_mysql_host_empty'];?>');
		obj.db_host.focus();
		return false;
	}
	else if (!obj.db_username.value)
	{
		alert('<?php echo $i_message['install_mysql_username_empty'];?>');
		obj.db_username.focus();
		return false;
	}
	else if (!obj.db_name.value)
	{
		alert('<?php echo $i_message['install_mysql_name_empty'];?>');
		obj.db_name.focus();
		return false;
	}
	else if (obj.password.value.length < 6)
	{
		alert('<?php echo $i_message['install_founder_password_length'];?>');
		obj.password.focus();
		return false;
	}
	else if (obj.password.value != obj.rpassword.value)
	{
		alert('<?php echo $i_message['install_founder_rpassword_error'];?>');
		obj.rpassword.focus();
		return false;
	}
	else if (!obj.email.value)
	{
		alert('<?php echo $i_message['install_founder_email_empty'];?>');
		obj.email.focus();
		return false;
	}
	return true;
}
</script>
<?php
}
elseif ($v == '4')
{
	
		foreach ($forbiddencharacter as $value)
		{
			if (strpos($username, $value) !== FALSE)
			{
				$msg .= '<p>'.$i_message['forbidden_character'].'</p>';
				$quit = TRUE;
				break;
			}
		}
	

	if ($quit)
	{
		$allownext = 'disabled="disabled"';
		?>
		<div class="error"><?php echo $i_message['error'];?></div>
		<?php
		echo $msg;
	}
	else
	{

		$config_file_content	=	array();
		$config_file_content['db_host']			=	$db_host;
		$config_file_content['db_name']			=	$db_name;
		$config_file_content['db_username']		=	$db_username;
		$config_file_content['db_password']		=	$db_password;
		$config_file_content['db_prefix']		=	$db_prefix;
		$config_file_content['db_URL']		=	$db_URL;
		$config_file_content['db_pconnect']		=	0;
		$config_file_content['db_charset']		=	'gbk';
		$config_file_content['dbType']			=	'MySQL';

		$default_manager_account	=	array();
		$default_manager_account['email']		=	$email;
		$default_manager_account['password']	=	md5($password);

		$_SESSION['config_file_content']		=	$config_file_content;
		$_SESSION['default_manager_account']	=	$default_manager_account;
		$_SESSION['first_user_id']				=	$first_user_id;
		$_SESSION['site_url']					=	$site_url;
	}
?>
	<div class="botBorder">
        <p>后台地址：http://<?php echo$_SERVER['SERVER_NAME'];?>/admin</p>
		<p>帐号：admin</p>
		<p>密码：admin</p>
	</div>
	<div class="botBorder">

<?php
//写配置文件
$fp = fopen(THINKSNS_ROOT.$thinksns_config_file, 'wb');
$configfilecontent = <<<EOT
<?php
/* 
 * 数据库
 */
\$cfg['db']['host'] 		= '$db_host';  // 主机地址
\$cfg['db']['user'] 		= '$db_username';	    // 用户名	
\$cfg['db']['password'] 	= '$db_password';	    // 密码 
\$cfg['db']['name'] 		= '$db_name';		// 数据库名
\$cfg['db']['provider'] 	= 'mysql';		// 数据库类型
/* 
 * 文档根路径
 */
\$cfg['path']['root'] = dirname(__FILE__) . '/';
\$cfg['version'] = '0.1';
\$cfg['time'] = time();


/* 
 * 站点信息
 */
\$cfg['charset']    	= 	'gb2312';
\$cfg['style']  		= 	'blue2012';       // 主题
\$cfg['url']   		= 	'$db_URL';   // 站点URL

\$cfg['url_sale']   	= 	 \$cfg['url'].'sale/';
\$cfg['url_pinggu']  	= 	\$cfg['url'].'pinggu/';
\$cfg['url_rent']   		= 	\$cfg['url'].'rent/';
\$cfg['url_news']   		= 	\$cfg['url'].'news/';
\$cfg['url_newHouse']   	= 	\$cfg['url'].'newHouse/';
\$cfg['url_company']   	= 	\$cfg['url'].'company/';
\$cfg['url_community']   = \$cfg['url'].'community/';
\$cfg['url_broker']   	= 	\$cfg['url'].'broker/';
\$cfg['url_university']  = 	\$cfg['url'].'university/';
\$cfg['url_shop']  		= 	\$cfg['url'].'shop/';
\$cfg['url_bbs']   		= 	\$cfg['url'].'bbs/';
\$cfg['url_upfile']   		= \$cfg['url'].'upfile/';


\$cfg['domain'] 		= 	'';   // 域名
\$cfg['debug']  		= 	0;               // 是否发布状态0为发布
\$cfg['path']['conf'] 	= \$cfg['path']['root'] . 'conf/';
\$cfg['path']['part'] 	= \$cfg['path']['root'] . 'part/';
\$cfg['path']['data'] 	= \$cfg['path']['root'] . 'data/';
\$cfg['path']['common'] 	= \$cfg['path']['root'] . 'common/';
\$cfg['path']['lib']     = \$cfg['path']['common'] . 'lib/';
\$cfg['path']['apps'] 	= \$cfg['path']['common'] . 'apps/';
\$cfg['path']['themes'] 	= \$cfg['path']['root'] . 'themes/' . \$cfg['style'] . '/';
\$cfg['path']['template'] = \$cfg['path']['themes'] . 'template/';

\$cfg['site']['themes'] 	= \$cfg['url'] . 'themes/' .\$cfg['style'] . '/';
\$cfg['path']['images']	= \$cfg['site']['themes'] . 'images/';
\$cfg['path']['css']		= \$cfg['site']['themes'] . 'css/';
\$cfg['path']['js']		=  \$cfg['site']['themes'] . 'js/';
\$cfg['path']['url_lib']	= \$cfg['url'] . 'common/lib/';

\$cfg['auth_key'] = 'fangke_cc_on_board';// cookie验证hash值

\$cfg['cityName'] = "哈尔滨";   //此处要与anleye.php里配置的名称要一致
\$cfg['cityCode'] = 451;       //此处为程序代码可以  也可以随便写3个数字

//在间隔时间内去数据库验证下Cookie ,设置为 0 即为每次都是在数据库中验证用户
\$auth_time = 3600;

//发送邮件的方法("mail", "sendmail", "qmail", or "smtp").
\$cfg['mail']['mailer'] = 'smtp';  //服务器类型 例：smtp
\$cfg['mail']['host'] = '';  //发信服务器地址：例：smtp.qq.com
\$cfg['mail']['username'] = '';   //发信用户名
\$cfg['mail']['password'] = '';   //发信密码


define('SYSTEM_ADMIN_ID', 1);
define('TABLEPREFIX','fke_');//表前缀

//Ucenter数据库配置文件


?>
EOT;
chmod(THINKSNS_ROOT.$thinksns_config_file, 0777);
$result_1	=	fwrite($fp, trim($configfilecontent));
@fclose($fp);

if($result_1 && file_exists(THINKSNS_ROOT.$thinksns_config_file)){
?>
	<p><?php echo $i_message['config_log_success']; ?></p>
<?php
}else{
?>
	<p><?php echo $i_message['config_read_failed']; $quit = TRUE;?></p>

<?php
}


$fp = fopen(THINKSNS_ROOT.$thinksns_define_file, 'wb');

$site_path	=	THINKSNS_ROOT;
$definefilecontent = <<<EOT
<?php
//由于多应用情况下，目录由应用自己设置，核心系统就需要定义主程序的路径
define("SITE_PATH"	,	'$site_path');

//应用跳转到核心需要的路径
define('SITE_URL'	,	'$site_url');

//兼容旧系统
define('ROOT_PATH'	,	SITE_URL);

//ThinkPHP框架目录
define('THINK_PATH'	,	SITE_PATH.'/thinkphp');
define('THINK_MODE'	,	'ThinkSNS');

//公共目录和风格目录
define('PUBLIC_PATH',	SITE_PATH."/public");
define('PUBLIC_URL'	,	SITE_URL."/public");
define('__PUBLIC__'	,	SITE_URL."/public");

//附件上传目录
define('UPLOAD_PATH',	SITE_PATH."/data/uploads/");	// 结尾有 /
define('UPLOAD_URL'	,	SITE_URL."/data/uploads/");		// 结尾有 /
?>
EOT;
chmod(THINKSNS_ROOT.$thinksns_define_file, 0777);
$result_2	=	fwrite($fp, trim($definefilecontent));
fclose($fp);

if($result_2 && file_exists(THINKSNS_ROOT.$thinksns_define_file)){
?>
	<p><?php echo $i_message['define_log_success']; ?></p>
<?php
}else{
?>
	<p><?php echo $i_message['define_read_failed']; $quit = TRUE;?></p>

<?php
}
?>
	</div>
	<div class="center">
		<form method="post" action="install.php?v=5">
		<input type="button" class="submit" name="prev" value="<?php echo $i_message['install_prev'];?>" onClick="history.go(-1)">&nbsp;
		<input type="submit" class="submit" name="next" value="<?php echo $i_message['install_next'];?>" <?=$allownext?>>
		</form>
	</div>
<?php
}
elseif ($v == '5')
{
	$db_config	=	$_SESSION['config_file_content'];

	if (!$db_config['db_host'] && !$db_config['db_name'])
	{
		$msg .= '<p>'.$i_message['configure_read_failed'].'</p>';
		$quit = TRUE;
	}
	else
	{
		mysql_connect($db_config['db_host'], $db_config['db_username'], $db_config['db_password']);
		$sqlv = mysql_get_server_info();

			$db_charset	=	$db_config['db_charset'];
			$db_charset = (strpos($db_charset, '-') === FALSE) ? $db_charset : str_replace('-', '', $db_charset);

			mysql_query(" CREATE DATABASE IF NOT EXISTS `{$db_config['db_name']}` DEFAULT CHARACTER SET $db_charset ");

			if (mysql_errno())
			{
				$errormsg = $i_message['database_errno_'.mysql_errno()];
				$msg .= '<p>'.($errormsg ? $errormsg : $i_message['database_errno']).'</p>';
				$quit = TRUE;
			}
			else
			{
				mysql_select_db($db_config['db_name']);
			}

			//判断是否有用同样的数据库前缀安装过
			$re		=	mysql_query("SELECT COUNT(1) FROM {$db_config['db_prefix']}user");
			$link	=	@mysql_fetch_row($re);

			if( intval($link[0]) > 0 )
			{
				$thinksns_rebuild	=	true;
				$msg .= '<p>'.$i_message['thinksns_rebuild'].'</p>';
				$alert = ' onclick="return confirm(\''.$i_message['thinksns_rebuild'].'\');"';
			}
		
	}

if ($quit)
{
		$allownext = 'disabled="disabled"';
?>
<div class="error"><?php echo $i_message['error'];?></div>
<?php
	echo $msg;
}
else
{
?>
<div class="botBorder">
<?php
if($thinksns_rebuild){
?>
<p style="color:red;font-size:16px;"><?php echo $i_message['thinksns_rebuild'];?></p>
<?php
}
?>
<p><?php echo $i_message['mysql_import_data'];?></p>
</div>
<?php
}
?>
<div class="center">
	<form method="post" action="install.php?v=6">
	<input type="button" class="submit" name="prev" value="<?php echo $i_message['install_prev'];?>" onClick="history.go(-1)">&nbsp;
	<input type="submit" class="submit" name="next" value="<?php echo $i_message['install_next'];?>" <?php echo $allownext,$alert?>>
	</form>
</div>
<?php
}
elseif ($v == '6')
{
	$db_config	=	$_SESSION['config_file_content'];

	mysql_connect($db_config['db_host'], $db_config['db_username'], $db_config['db_password']);
	if (mysql_get_server_info() > '5.0')
	{
		mysql_query("SET sql_mode = ''");
	}
	$db_config['db_charset'] = (strpos($db_config['db_charset'], '-') === FALSE) ? $db_config['db_charset'] : str_replace('-', '', $db_config['db_charset']);
	mysql_query("SET character_set_connection={$db_config['db_charset']}, character_set_results={$db_config['db_charset']}, character_set_client=binary");
	mysql_select_db($db_config['db_name']);
	$tablenum = 0;

	$fp = fopen($installfile, 'rb');
	$sql = fread($fp, filesize($installfile));
	fclose($fp);


?>
<div class="botBorder">
<h4><?php echo $i_message['import_processing'];?></h4>
<div style="overflow-y:scroll;height:100px;width:715px;padding:5px;border:1px solid #ccc;">
<?php
	$db_charset	=	$db_config['db_charset'];
	$db_prefix	=	$db_config['db_prefix'];
	$sql = str_replace("\r", "\n", str_replace('`'.'ts_', '`'.$db_prefix, $sql));
	$ret = array();
	$num = 0;
	foreach(explode(";\n", trim($sql)) as $query)
	{
		$queries = explode("\n", trim($query));
		$sq = "";
		foreach($queries as $query)
		{
			if(substr($query,0,2)<>"--"){
				$sq .= $query;
			}
		}
		$ret[$num] = $sq;
		$num ++;
	}
	unset($sql);
	foreach($ret as $query)
	{
		$query = trim($query);
		if($query) {
			if(substr($query, 0, 12) == 'CREATE TABLE')
			{
				$name = preg_replace("/CREATE TABLE `([a-z0-9_]+)` .*/is", "\\1", $query);
				echo '<p>'.$i_message['create_table'].' '.$name.' ... <span class="blue">OK</span></p>';
				@mysql_query(createtable($query, $db_charset));
				$tablenum ++;
			}
			else
			{
				@mysql_query($query);
			}
		}
	}
?>

</div>
</div>
<div class="botBorder">
<h4>祝您体验愉快！</h4>
<?php
	if(!$quit){
		//锁定安装
		fopen('install.lock', 'w');
		@unlink('../index.html');
	}else{
		echo '请重新安装';
	}
?>
</div>
<div class="botBorder">
<h4><?php echo $i_message['install_success'];?></h4>
<?php echo $i_message['install_success_intro'];?>
</div>
<iframe src="<?php echo $_SESSION['site_url'];?>/cleancache.php" height=0 width=0></iframe>
<?php
}
?>
</div>
<div class='copyright'>ANLEYE <?php echo $_TSVERSION; ?> &#169; copyright 2009-2012 anleye.yanwee.com All Rights Reserved</div>
</div>
<div style="display:none;">
<iframe src="http://anleyetj.yanwee.com/" scrolling="auto"></iframe>
</div>
</body>
</html>