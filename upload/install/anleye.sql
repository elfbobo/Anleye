-- phpMyAdmin SQL Dump
-- version 3.5.1
-- http://www.phpmyadmin.net
--
-- 主机: localhost
-- 生成日期: 2013 年 01 月 31 日 15:56
-- 服务器版本: 5.5.24-log
-- PHP 版本: 5.3.13

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";

--
-- 数据库: `anleye`
--

-- --------------------------------------------------------

--
-- 表的结构 `fke_about`
--

DROP TABLE IF EXISTS `fke_about`;
CREATE TABLE `fke_about` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `content` mediumtext,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=gbk AUTO_INCREMENT=6 ;

--
-- 转存表中的数据 `fke_about`
--

INSERT INTO `fke_about` (`id`, `content`) VALUES
(1, '关于我们请到后台关于我们菜单中修改！'),
(2, '人才招聘请到后台关于我们菜单中修改！'),
(3, '用户协议请到后台关于我们菜单中修改！'),
(4, '版权声明请到后台关于我们菜单中修改！'),
(5, '联系我们请到后台关于我们菜单中修改！');

-- --------------------------------------------------------

--
-- 表的结构 `fke_ads`
--

DROP TABLE IF EXISTS `fke_ads`;
CREATE TABLE `fke_ads` (
  `id` mediumint(8) unsigned NOT NULL AUTO_INCREMENT,
  `ads_name` varchar(40) NOT NULL,
  `introduce` varchar(255) NOT NULL,
  `ad_type` int(11) NOT NULL,
  `link_url` varchar(100) NOT NULL,
  `image_url` varchar(100) NOT NULL,
  `image_thumb` varchar(100) NOT NULL,
  `alt` varchar(20) NOT NULL,
  `flash_url` varchar(100) NOT NULL,
  `wmode` varchar(20) NOT NULL,
  `text` text NOT NULL,
  `code` text NOT NULL,
  `creater` varchar(20) NOT NULL,
  `add_time` int(10) unsigned NOT NULL DEFAULT '0',
  `status` tinyint(1) unsigned NOT NULL DEFAULT '1',
  `height` int(11) DEFAULT NULL,
  `width` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `placeid` (`creater`),
  KEY `username` (`creater`)
) ENGINE=MyISAM  DEFAULT CHARSET=gbk AUTO_INCREMENT=10 ;

--
-- 转存表中的数据 `fke_ads`
--

INSERT INTO `fke_ads` (`id`, `ads_name`, `introduce`, `ad_type`, `link_url`, `image_url`, `image_thumb`, `alt`, `flash_url`, `wmode`, `text`, `code`, `creater`, `add_time`, `status`, `height`, `width`) VALUES
(1, '右侧广告', '', 1, 'http://anleye.yanwee.com', 'extend/pic/20120406114331.jpg', '', '', '', '', '', '', 'admin', 0, 0, 76, 300),
(2, '右侧广告二', '', 1, 'http://anleye.yanwee.com', 'extend/pic/20120406114426.jpg', '', '', '', '', '', '', 'admin', 0, 0, 76, 300),
(3, '底部广告', '', 1, 'http://anleye.yanwee.com', 'extend/pic/20120406114109.gif', '', '', '', '', '', '', 'admin', 0, 0, 76, 676),
(4, 'flash广告', '', 1, 'http://anleye.yanwee.com', 'extend/pic/20120406114151.gif', '', '', '', '', '', '', 'admin', 0, 0, 76, 680),
(5, '出租页面', '', 1, 'http://anleye.yanwee.com', 'extend/pic/20120406122541.jpg', '', '', '', '', '', '', 'admin', 0, 0, 76, 740),
(6, '出售页面', '', 1, 'http://anleye.yanwee.com', 'extend/pic/20120406122556.jpg', '', '', '', '', '', '', 'admin', 0, 0, 76, 740),
(7, '地图找房', '', 1, 'm/map', 'extend/pic/20120406114238.jpg', '', '', '', '', '', '', 'admin', 0, 0, NULL, NULL),
(8, '二手房频道右侧', '', 1, '#', 'extend/pic/20120831165125.jpg', '', '', '', '', '', '<script type="text/javascript">alimama_bm_revision = "20120313";alimama_bm_bid = "31727914";alimama_bm_width = 993;alimama_bm_height = 182;alimama_bm_xmlsrc = "http://img.uu1001.cn/x2/2012-08-01/16-04/2012-08-01_342607bcaccb9b88e9505fcf038d95bd_0.xml";alimama_bm_link = "http://";alimama_bm_ds = "";alimama_bm_as = "default"</script><script type="text/javascript" src="http://img.uu1001.cn/bmv3.js?v=20120313"></script>', 'admin', 0, 0, 82, 238),
(9, '二手房右侧广告2', '564564564', 1, '#', 'extend/pic/20120831165248.jpg', '', '阿萨德发', '', '', '', '', 'admin', 0, 0, 82, 238);

-- --------------------------------------------------------

--
-- 表的结构 `fke_appolist`
--

DROP TABLE IF EXISTS `fke_appolist`;
CREATE TABLE `fke_appolist` (
  `appo_list_id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `house_id` int(11) NOT NULL,
  `house_title` varchar(255) NOT NULL,
  `update_time` varchar(255) NOT NULL,
  `appo_site` char(10) NOT NULL,
  PRIMARY KEY (`appo_list_id`),
  KEY `user_id` (`user_id`,`house_id`)
) ENGINE=MyISAM DEFAULT CHARSET=gbk AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- 表的结构 `fke_appomuch`
--

DROP TABLE IF EXISTS `fke_appomuch`;
CREATE TABLE `fke_appomuch` (
  `appo_id` int(8) NOT NULL AUTO_INCREMENT,
  `appo_name` varchar(255) NOT NULL,
  `appo_value` varchar(255) NOT NULL,
  PRIMARY KEY (`appo_id`),
  UNIQUE KEY `appo_id` (`appo_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=gbk AUTO_INCREMENT=5 ;

--
-- 转存表中的数据 `fke_appomuch`
--

INSERT INTO `fke_appomuch` (`appo_id`, `appo_name`, `appo_value`) VALUES
(1, 'appTime', '6'),
(2, 'appNum', '3'),
(3, 'appCountNum', '1'),
(4, 'appInterest', 'a:2:{i:0;s:1:"1";i:1;s:1:"2";}');

-- --------------------------------------------------------

--
-- 表的结构 `fke_borough`
--

DROP TABLE IF EXISTS `fke_borough`;
CREATE TABLE `fke_borough` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `borough_name` varchar(100) NOT NULL,
  `borough_letter` varchar(10) DEFAULT NULL,
  `borough_alias` varchar(100) DEFAULT NULL,
  `cityarea_id` tinyint(4) DEFAULT '0',
  `cityarea2_id` tinyint(4) DEFAULT '0',
  `borough_section` tinyint(4) DEFAULT '0',
  `borough_address` varchar(250) DEFAULT NULL,
  `borough_type` tinyint(4) DEFAULT '0',
  `elementary_school` varchar(40) DEFAULT NULL,
  `middle_school` varchar(40) DEFAULT NULL,
  `borough_thumb` varchar(100) DEFAULT NULL,
  `sell_num` int(11) DEFAULT '0',
  `rent_num` int(11) DEFAULT '0',
  `sell_time` varchar(10) DEFAULT NULL,
  `borough_avgprice` int(11) DEFAULT '0',
  `borough_evaluate` int(10) unsigned DEFAULT NULL,
  `evaluate_time` int(11) NOT NULL,
  `percent_change` float DEFAULT '0',
  `layout_drawing` int(11) DEFAULT '0',
  `layout_picture` int(11) DEFAULT '0',
  `layout_map` varchar(50) DEFAULT NULL,
  `click_num` int(11) DEFAULT '0',
  `is_checked` tinyint(4) NOT NULL DEFAULT '0',
  `creater` varchar(20) DEFAULT NULL,
  `borough_developer_id` int(11) DEFAULT '0',
  `sell_price` int(11) DEFAULT '0',
  `room_type` varchar(200) DEFAULT NULL,
  `sell_phone` varchar(250) DEFAULT NULL,
  `isnew` tinyint(4) DEFAULT NULL,
  `is_promote` tinyint(4) DEFAULT NULL,
  `is_priceoff` tinyint(4) DEFAULT NULL,
  `isdel` tinyint(4) DEFAULT '0',
  `created` int(11) NOT NULL,
  `updated` int(11) NOT NULL DEFAULT '0',
  `video` varchar(600) CHARACTER SET gb2312 DEFAULT NULL,
  `borough_nid` varchar(600) DEFAULT NULL,
  `lat` double NOT NULL,
  `lng` double NOT NULL,
  `borough_properties` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `borough_name` (`borough_name`),
  KEY `cityarea_id` (`cityarea_id`)
) ENGINE=MyISAM DEFAULT CHARSET=gbk AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- 表的结构 `fke_borough_adviser`
--

DROP TABLE IF EXISTS `fke_borough_adviser`;
CREATE TABLE `fke_borough_adviser` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `borough_id` int(11) NOT NULL,
  `member_id` int(11) NOT NULL,
  `status` tinyint(4) DEFAULT '0',
  `remark` varchar(250) DEFAULT NULL,
  `add_time` int(11) DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `borough_id` (`borough_id`)
) ENGINE=MyISAM DEFAULT CHARSET=gbk AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- 表的结构 `fke_borough_avgprice`
--

DROP TABLE IF EXISTS `fke_borough_avgprice`;
CREATE TABLE `fke_borough_avgprice` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `borough_id` int(11) NOT NULL,
  `borough_avgprice` int(11) DEFAULT '0',
  `percent_change` float DEFAULT '0',
  `add_time` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `borough_id` (`borough_id`)
) ENGINE=MyISAM DEFAULT CHARSET=gbk AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- 表的结构 `fke_borough_draw`
--

DROP TABLE IF EXISTS `fke_borough_draw`;
CREATE TABLE `fke_borough_draw` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `pic_url` varchar(200) NOT NULL,
  `pic_thumb` varchar(200) DEFAULT NULL,
  `pic_desc` varchar(200) DEFAULT NULL,
  `borough_id` int(11) NOT NULL,
  `creater` varchar(20) DEFAULT NULL,
  `addtime` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `borough_id` (`borough_id`)
) ENGINE=MyISAM DEFAULT CHARSET=gbk AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- 表的结构 `fke_borough_evaluate`
--

DROP TABLE IF EXISTS `fke_borough_evaluate`;
CREATE TABLE `fke_borough_evaluate` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `borough_id` int(11) NOT NULL,
  `borough_evaluate` int(11) DEFAULT '0',
  `creater` varchar(20) DEFAULT NULL,
  `add_time` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=gbk AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- 表的结构 `fke_borough_info`
--

DROP TABLE IF EXISTS `fke_borough_info`;
CREATE TABLE `fke_borough_info` (
  `id` int(11) NOT NULL,
  `borough_developer` varchar(100) DEFAULT NULL,
  `borough_company` varchar(100) DEFAULT NULL,
  `borough_costs` varchar(20) DEFAULT NULL,
  `borough_totalarea` decimal(10,0) DEFAULT '0',
  `borough_area` decimal(10,0) DEFAULT '0',
  `borough_green` float DEFAULT '0',
  `borough_volume` float DEFAULT '0',
  `borough_parking` int(11) DEFAULT '0',
  `borough_number` int(11) DEFAULT '0',
  `borough_completion` date DEFAULT NULL,
  `borough_support` varchar(100) DEFAULT NULL,
  `borough_bus` varchar(100) DEFAULT NULL,
  `borough_shop` varchar(100) DEFAULT NULL,
  `borough_hospital` varchar(100) DEFAULT NULL,
  `borough_bank` varchar(100) DEFAULT NULL,
  `borough_dining` varchar(100) DEFAULT NULL,
  `borough_sight` varchar(100) DEFAULT NULL,
  `borough_content` mediumtext,
  `project_site` varchar(200) DEFAULT NULL,
  `company_site` varchar(200) DEFAULT NULL,
  `sale_office` varchar(200) DEFAULT NULL,
  `sale_licence` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=gbk;

--
-- 转存表中的数据 `fke_borough_info`
--

INSERT INTO `fke_borough_info` (`id`, `borough_developer`, `borough_company`, `borough_costs`, `borough_totalarea`, `borough_area`, `borough_green`, `borough_volume`, `borough_parking`, `borough_number`, `borough_completion`, `borough_support`, `borough_bus`, `borough_shop`, `borough_hospital`, `borough_bank`, `borough_dining`, `borough_sight`, `borough_content`, `project_site`, `company_site`, `sale_office`, `sale_licence`) VALUES
(1, NULL, NULL, NULL, '0', '0', 0, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- 表的结构 `fke_borough_intention`
--

DROP TABLE IF EXISTS `fke_borough_intention`;
CREATE TABLE `fke_borough_intention` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `borough_id` int(11) DEFAULT NULL,
  `link_type` varchar(250) DEFAULT NULL,
  `content` varchar(500) DEFAULT NULL,
  `nickname` varchar(100) DEFAULT NULL,
  `gender` int(11) DEFAULT NULL,
  `mobile` varchar(100) DEFAULT NULL,
  `age` int(11) DEFAULT NULL,
  `qq` varchar(100) DEFAULT NULL,
  `tel` varchar(100) DEFAULT NULL,
  `email` varchar(100) DEFAULT NULL,
  `postcode` varchar(100) DEFAULT NULL,
  `address` varchar(600) DEFAULT NULL,
  `addtime` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=gbk AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- 表的结构 `fke_borough_log`
--

DROP TABLE IF EXISTS `fke_borough_log`;
CREATE TABLE `fke_borough_log` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `old_id` int(11) NOT NULL,
  `new_id` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=gbk AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- 表的结构 `fke_borough_news`
--

DROP TABLE IF EXISTS `fke_borough_news`;
CREATE TABLE `fke_borough_news` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(600) DEFAULT NULL,
  `type` varchar(100) DEFAULT NULL,
  `time` int(11) DEFAULT NULL,
  `borough_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=gbk AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- 表的结构 `fke_borough_pic`
--

DROP TABLE IF EXISTS `fke_borough_pic`;
CREATE TABLE `fke_borough_pic` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `pic_url` varchar(200) NOT NULL,
  `pic_thumb` varchar(200) DEFAULT NULL,
  `pic_desc` varchar(200) DEFAULT NULL,
  `borough_id` int(11) NOT NULL,
  `creater` varchar(20) DEFAULT NULL,
  `addtime` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `borough_id` (`borough_id`)
) ENGINE=MyISAM DEFAULT CHARSET=gbk AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- 表的结构 `fke_borough_update`
--

DROP TABLE IF EXISTS `fke_borough_update`;
CREATE TABLE `fke_borough_update` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `borough_id` int(11) NOT NULL,
  `field_name` varchar(20) NOT NULL,
  `old_value` text,
  `new_value` text,
  `broker_id` int(11) NOT NULL,
  `status` int(11) DEFAULT '0',
  `add_time` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=gbk AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- 表的结构 `fke_broker_aptitude`
--

DROP TABLE IF EXISTS `fke_broker_aptitude`;
CREATE TABLE `fke_broker_aptitude` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `broker_id` int(11) NOT NULL,
  `aptitude_pic` varchar(100) NOT NULL,
  `status` tinyint(4) DEFAULT '0',
  `add_time` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=gbk AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- 表的结构 `fke_broker_avatar`
--

DROP TABLE IF EXISTS `fke_broker_avatar`;
CREATE TABLE `fke_broker_avatar` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `broker_id` int(11) NOT NULL,
  `avatar_pic` varchar(100) NOT NULL,
  `status` tinyint(4) DEFAULT '0',
  `add_time` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=gbk AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- 表的结构 `fke_broker_evaluate`
--

DROP TABLE IF EXISTS `fke_broker_evaluate`;
CREATE TABLE `fke_broker_evaluate` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `broker_id` int(11) DEFAULT NULL,
  `client_ip` varchar(50) DEFAULT NULL,
  `accurate` int(11) DEFAULT NULL,
  `satisfaction` int(11) DEFAULT NULL,
  `specialty` int(11) DEFAULT NULL,
  `content` mediumtext,
  `time` int(11) DEFAULT NULL,
  `reply` mediumtext,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=gbk AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- 表的结构 `fke_broker_friends`
--

DROP TABLE IF EXISTS `fke_broker_friends`;
CREATE TABLE `fke_broker_friends` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `broker_id` int(11) NOT NULL,
  `friend_id` int(11) NOT NULL,
  `add_time` int(11) DEFAULT '0',
  `status` int(11) DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `broker_id` (`broker_id`)
) ENGINE=MyISAM DEFAULT CHARSET=gbk AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- 表的结构 `fke_broker_identity`
--

DROP TABLE IF EXISTS `fke_broker_identity`;
CREATE TABLE `fke_broker_identity` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `realname` varchar(20) NOT NULL,
  `idcard` varchar(18) NOT NULL,
  `idcard_pic` varchar(100) NOT NULL,
  `broker_id` int(11) NOT NULL,
  `status` tinyint(4) NOT NULL,
  `add_time` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=gbk AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- 表的结构 `fke_broker_info`
--

DROP TABLE IF EXISTS `fke_broker_info`;
CREATE TABLE `fke_broker_info` (
  `id` int(11) NOT NULL,
  `realname` varchar(20) DEFAULT NULL,
  `cityarea_id` int(11) DEFAULT NULL,
  `cityarea2_id` int(11) DEFAULT '0',
  `mobile` varchar(20) DEFAULT NULL,
  `avatar` varchar(100) DEFAULT NULL,
  `idcard_pic` varchar(100) DEFAULT NULL,
  `idcard` varchar(20) DEFAULT NULL,
  `aptitude` varchar(100) DEFAULT NULL,
  `signed` varchar(200) DEFAULT NULL,
  `company` varchar(100) DEFAULT NULL,
  `outlet` varchar(100) DEFAULT NULL,
  `outlet_addr` varchar(200) DEFAULT NULL,
  `post_code` varchar(10) DEFAULT NULL,
  `com_tell` varchar(20) DEFAULT NULL,
  `com_fax` varchar(20) DEFAULT NULL,
  `gender` tinyint(1) DEFAULT '0',
  `birthday` date DEFAULT NULL,
  `borough_id` int(11) DEFAULT NULL,
  `qq` varchar(15) DEFAULT NULL,
  `msn` varchar(50) DEFAULT NULL,
  `introduce` mediumtext,
  `broker_type` tinyint(4) DEFAULT '1',
  `outlet_first` varchar(100) DEFAULT NULL,
  `outlet_last` varchar(100) DEFAULT NULL,
  `specialty` int(11) DEFAULT NULL,
  `banner` varchar(100) DEFAULT NULL,
  `background` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=gbk;

-- --------------------------------------------------------

--
-- 表的结构 `fke_city`
--

DROP TABLE IF EXISTS `fke_city`;
CREATE TABLE `fke_city` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(20) DEFAULT NULL,
  `url` varchar(100) DEFAULT NULL,
  `is_proxy` tinyint(4) DEFAULT '0',
  `status` tinyint(4) DEFAULT '0',
  `logo` varchar(200) DEFAULT NULL,
  `add_time` int(11) DEFAULT NULL,
  `pid` int(11) DEFAULT '0',
  `is_hot` tinyint(4) DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=gbk AUTO_INCREMENT=12 ;

--
-- 转存表中的数据 `fke_city`
--

INSERT INTO `fke_city` (`id`, `name`, `url`, `is_proxy`, `status`, `logo`, `add_time`, `pid`, `is_hot`) VALUES
(3, '上海', '#', 0, 1, 'city/logo/20120911130728.jpg', 1347368853, 2, 1),
(2, '北京', 'http://qq.com', 0, 0, 'city/logo/20120906122712.jpg', 1346934434, 1, 1),
(4, '广州', '#', 0, 1, 'city/logo/20120911130810.jpg', 1347368893, 4, 1),
(5, '深圳', '#', 0, 1, 'city/logo/20120911130843.jpg', 1347368924, 5, 0),
(6, '杭州', '#', 0, 0, 'city/logo/20120911130901.jpg', 1347368942, 6, 0),
(7, '苏州', '#', 0, 1, 'city/logo/20120911130934.jpg', 1347368976, 6, 0),
(8, '天津', '#', 0, 0, 'city/logo/20120911131005.jpg', 1347369007, 7, 0),
(9, '大连', '#', 0, 0, 'city/logo/20120911131031.jpg', 1347369031, 8, 0),
(10, '哈尔滨', '#', 0, 0, 'city/logo/20120911131127.jpg', 1347369088, 9, 0),
(11, '长春', '#', 0, 0, 'city/logo/20120911131141.jpg', 1347369102, 10, 0);

-- --------------------------------------------------------

--
-- 表的结构 `fke_company`
--

DROP TABLE IF EXISTS `fke_company`;
CREATE TABLE `fke_company` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `company_name` varchar(400) DEFAULT NULL,
  `company_about` mediumtext,
  `company_phone` varchar(600) DEFAULT NULL,
  `company_logo` varchar(600) DEFAULT NULL,
  `company_address` varchar(600) DEFAULT NULL,
  `company_banner` varchar(600) DEFAULT NULL,
  `company_clerk` varchar(600) DEFAULT NULL,
  `company_description` varchar(600) DEFAULT NULL,
  `status` tinyint(4) NOT NULL DEFAULT '0',
  `email` varchar(600) DEFAULT NULL,
  `qq` varchar(600) DEFAULT NULL,
  `company_style` varchar(600) DEFAULT NULL,
  `type` tinyint(4) DEFAULT NULL,
  `username` varchar(32) DEFAULT NULL,
  `passwd` varchar(32) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=gbk AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- 表的结构 `fke_consign_sale`
--

DROP TABLE IF EXISTS `fke_consign_sale`;
CREATE TABLE `fke_consign_sale` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `borough_name` varchar(100) DEFAULT NULL,
  `house_floor` int(11) DEFAULT NULL,
  `house_topfloor` int(11) DEFAULT NULL,
  `house_room` int(11) DEFAULT NULL,
  `house_hall` int(11) DEFAULT NULL,
  `house_price` float DEFAULT NULL,
  `house_desc` varchar(600) DEFAULT NULL,
  `owner_name` varchar(20) DEFAULT NULL,
  `owner_phone` varchar(30) DEFAULT NULL,
  `time` int(11) DEFAULT NULL,
  `status` tinyint(4) DEFAULT '0',
  `house_area` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=gbk AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- 表的结构 `fke_dd`
--

DROP TABLE IF EXISTS `fke_dd`;
CREATE TABLE `fke_dd` (
  `dd_id` int(11) NOT NULL AUTO_INCREMENT,
  `dd_name` varchar(50) DEFAULT NULL,
  `dd_caption` varchar(50) NOT NULL,
  PRIMARY KEY (`dd_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=gbk AUTO_INCREMENT=26 ;

--
-- 转存表中的数据 `fke_dd`
--

INSERT INTO `fke_dd` (`dd_id`, `dd_name`, `dd_caption`) VALUES
(5, 'borough_sight', '公园景观'),
(4, 'borough_support', '小区配套'),
(3, 'borough_type', '物业类型'),
(2, 'borough_section', '小区版块'),
(1, 'cityarea', '小区区域'),
(6, 'house_toward', '房源朝向'),
(7, 'house_fitment', '装修情况'),
(8, 'house_feature', '房源特色'),
(9, 'house_installation', '出租配套'),
(10, 'house_type', '房源类型'),
(11, 'rent_feature', '出租特色'),
(12, 'rent_deposittype', '出租押金'),
(13, 'user_type', '用户类型'),
(15, 'bargain_from', '交易来源'),
(16, 'borough_developer', '开发商'),
(20, 'integral_ruleclass', '积分规则分类'),
(22, 'message_ruleclass', '站内信规则分类'),
(23, 'belong', '房屋产权'),
(24, 'borough_properties', '楼盘属性'),
(25, 'specialty', '经纪人特长');

-- --------------------------------------------------------

--
-- 表的结构 `fke_dd_item`
--

DROP TABLE IF EXISTS `fke_dd_item`;
CREATE TABLE `fke_dd_item` (
  `di_id` int(11) NOT NULL AUTO_INCREMENT,
  `dd_id` int(11) NOT NULL,
  `p_id` int(11) DEFAULT '0',
  `di_value` varchar(10) NOT NULL,
  `di_caption` varchar(50) NOT NULL,
  `list_group` tinyint(4) DEFAULT '0',
  `list_order` int(11) DEFAULT '0',
  PRIMARY KEY (`di_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=gbk AUTO_INCREMENT=303 ;

--
-- 转存表中的数据 `fke_dd_item`
--

INSERT INTO `fke_dd_item` (`di_id`, `dd_id`, `p_id`, `di_value`, `di_caption`, `list_group`, `list_order`) VALUES
(76, 2, 0, '6', '江滨', 0, 8),
(75, 2, 0, '5', '西二环', 0, 5),
(74, 2, 0, '4', '五四北', 0, 4),
(73, 2, 0, '3', '五一广场', 0, 2),
(72, 2, 0, '2', '五四温泉', 0, 3),
(71, 2, 0, '1', '市中心', 0, 1),
(280, 1, 0, '1', '南岗区', 0, 0),
(281, 1, 0, '3', '道外区', 0, 0),
(77, 2, 0, '7', '东区', 0, 6),
(78, 2, 0, '8', '西区', 0, 7),
(79, 2, 0, '9', '金山', 0, 10),
(80, 2, 0, '10', '乌西', 0, 11),
(81, 3, 0, '1', '住宅楼', 0, 1),
(82, 3, 0, '2', '别墅', 0, 2),
(167, 10, 0, '2', '别墅', 0, 2),
(84, 3, 0, '3', '写字楼', 0, 3),
(166, 10, 0, '1', '住宅楼', 0, 1),
(86, 5, 0, '1', '西湖公园 ', 0, 1),
(87, 5, 0, '2', '左海公园 ', 0, 5),
(88, 5, 0, '3', '温泉公园 ', 0, 4),
(89, 5, 0, '4', '南江滨 ', 0, 13),
(90, 5, 0, '5', '北江滨 ', 0, 12),
(91, 5, 0, '6', '于山 ', 0, 11),
(92, 5, 0, '7', '乌山 ', 0, 10),
(93, 5, 0, '8', '鼓山 ', 0, 9),
(94, 5, 0, '9', '金牛山 ', 0, 8),
(95, 5, 0, '10', '旗山 ', 0, 7),
(96, 5, 0, '11', '森林公园 ', 0, 3),
(97, 5, 0, '12', '动物园', 0, 2),
(98, 4, 0, '1', '门卫安防 ', 0, 1),
(99, 4, 0, '2', '停车场 ', 0, 2),
(100, 4, 0, '3', '便利店 ', 0, 3),
(101, 4, 0, '4', '幼儿园 ', 0, 4),
(102, 4, 0, '5', '游泳池 ', 0, 5),
(103, 4, 0, '6', '高尚会所 ', 0, 6),
(104, 4, 0, '7', '中央花园 ', 0, 7),
(105, 4, 0, '8', '网球场 ', 0, 8),
(106, 4, 0, '9', '篮球场 ', 0, 9),
(107, 4, 0, '10', '羽毛球场 ', 0, 10),
(108, 4, 0, '11', '健身房 ', 0, 11),
(109, 4, 0, '12', '台球室 ', 0, 12),
(110, 4, 0, '13', '乒乓球室 ', 0, 13),
(111, 4, 0, '14', '阅览室 ', 0, 14),
(112, 4, 0, '15', '棋牌室 ', 0, 15),
(113, 4, 0, '16', '咖啡厅 ', 0, 16),
(114, 4, 0, '17', '餐吧 ', 0, 17),
(115, 4, 0, '18', '洗衣店 ', 0, 18),
(116, 4, 0, '19', '美容美发 ', 0, 19),
(117, 4, 0, '20', '歌舞厅 ', 0, 20),
(118, 4, 0, '21', '儿童游乐场 ', 0, 21),
(119, 4, 0, '22', '高尔夫练习场 ', 0, 22),
(120, 4, 0, '23', '高尔夫球场 ', 0, 23),
(121, 6, 0, '1', '东', 0, 1),
(122, 6, 0, '2', '西', 0, 2),
(123, 6, 0, '3', '南', 0, 3),
(124, 6, 0, '4', '北', 0, 4),
(125, 6, 0, '5', '东南', 0, 5),
(126, 6, 0, '6', '西南', 0, 6),
(127, 6, 0, '7', '东北', 0, 7),
(128, 6, 0, '8', '西北', 0, 8),
(129, 6, 0, '9', '南北', 0, 9),
(130, 6, 0, '10', '东西', 0, 10),
(131, 7, 0, '1', '毛坯', 0, 1),
(132, 7, 0, '2', '普通装修', 0, 2),
(133, 7, 0, '3', '精装修', 0, 3),
(134, 7, 0, '4', '豪华装修', 0, 4),
(135, 8, 0, '1', '划片名校', 2, 6),
(136, 8, 0, '2', '投资潜力', 2, 10),
(137, 8, 0, '3', '黄金商圈', 2, 7),
(138, 8, 0, '4', '山水景观', 2, 8),
(139, 8, 0, '5', '交通便利 ', 2, 9),
(140, 8, 0, '6', '国际社区', 1, 1),
(141, 8, 0, '7', '泳池球场', 1, 2),
(142, 8, 0, '8', '私家车库', 1, 3),
(143, 8, 0, '9', '复式美居', 1, 4),
(144, 8, 0, '10', '豪华精装', 1, 5),
(145, 8, 0, '11', '附送家具', 3, 11),
(146, 8, 0, '12', '房东急售', 3, 12),
(147, 9, 0, '1', '空调', 0, 1),
(148, 9, 0, '2', '热水器', 0, 2),
(149, 9, 0, '3', '电视机', 0, 3),
(150, 9, 0, '4', '管道煤气', 0, 4),
(151, 9, 0, '5', '防盗门', 0, 5),
(152, 9, 0, '6', '电梯', 0, 6),
(153, 9, 0, '7', '车库', 0, 7),
(154, 9, 0, '8', '床铺', 0, 8),
(155, 9, 0, '9', '沙发', 0, 9),
(156, 9, 0, '10', '冰箱', 0, 10),
(157, 9, 0, '11', '温泉', 0, 11),
(158, 9, 0, '12', '宽带', 0, 12),
(159, 9, 0, '13', '闭路', 0, 13),
(160, 9, 0, '14', '液化器', 0, 14),
(161, 9, 0, '15', '壁柜', 0, 15),
(162, 9, 0, '16', '洗衣机', 0, 16),
(163, 9, 0, '17', '防盗网', 0, 17),
(164, 9, 0, '18', '厨具', 0, 18),
(165, 9, 0, '19', '电话', 0, 19),
(168, 10, 0, '3', '沿街店面', 0, 4),
(169, 10, 0, '4', '厂房/仓库', 0, 7),
(170, 10, 0, '5', '写字楼', 0, 3),
(171, 10, 0, '6', '车库', 0, 8),
(172, 10, 0, '7', '商铺', 0, 5),
(206, 5, 0, '13', '屏山', 0, 6),
(174, 11, 0, '1', '高尚社区', 1, 1),
(175, 11, 0, '2', '交通便利', 2, 7),
(176, 11, 0, '3', '繁华商圈', 2, 8),
(177, 11, 0, '4', '院校周边 ', 2, 9),
(178, 11, 0, '5', '家具齐全', 1, 3),
(179, 11, 0, '6', '家电齐全 ', 1, 2),
(180, 11, 0, '7', '独立厨卫 ', 1, 4),
(181, 11, 0, '8', '舒适装修 ', 1, 5),
(182, 11, 0, '9', '有匙即看 ', 3, 10),
(183, 11, 0, '10', '商住两用', 1, 6),
(184, 11, 0, '11', '可供短租 ', 3, 11),
(185, 11, 0, '12', '免中介费', 3, 12),
(186, 12, 0, '1', '付一押一', 0, 1),
(187, 12, 0, '2', '付一押二', 0, 2),
(188, 12, 0, '3', '付二押一', 0, 3),
(189, 12, 0, '4', '付二押二', 0, 4),
(190, 12, 0, '5', '付三押一', 0, 5),
(191, 12, 0, '6', '付三押二', 0, 6),
(192, 12, 0, '7', '其它', 0, 7),
(193, 13, 0, '1', '经纪人', 0, 1),
(195, 14, 0, '2', '自由经纪人', 0, 2),
(196, 14, 0, '1', '在职经纪人', 0, 1),
(205, 10, 0, '8', '自建房', 0, 6),
(200, 3, 0, '4', '商业', 0, 4),
(204, 2, 0, '11', '城南', 0, 9),
(207, 5, 0, '14', '乌龙江', 0, 14),
(208, 2, 0, '12', '马尾', 0, 12),
(209, 15, 0, '1', '买方-安乐业', 0, 0),
(210, 15, 0, '2', '卖方-安乐业', 0, 0),
(211, 15, 0, '3', '双方-安乐业', 0, 0),
(212, 15, 0, '4', '双方-线下', 0, 0),
(213, 16, 0, '1', '三木', 0, 1),
(214, 16, 0, '2', '融侨', 0, 2),
(215, 16, 0, '3', '金辉', 0, 3),
(216, 16, 0, '4', '尚锦', 0, 4),
(217, 16, 0, '5', '三盛', 0, 5),
(218, 16, 0, '6', '阳光', 0, 6),
(219, 16, 0, '7', '仁文', 0, 7),
(220, 16, 0, '8', '万科', 0, 8),
(221, 16, 0, '9', '中庚', 0, 9),
(222, 16, 0, '10', '福晟', 0, 10),
(223, 16, 0, '11', '正祥', 0, 11),
(224, 16, 0, '12', '世欧', 0, 12),
(225, 16, 0, '13', '城乡建总', 0, 13),
(226, 16, 0, '14', '建发', 0, 14),
(227, 16, 0, '15', '群升', 0, 15),
(228, 16, 0, '16', '华润', 0, 16),
(229, 16, 0, '17', '泰禾', 0, 17),
(230, 16, 0, '18', '融信', 0, 18),
(231, 16, 0, '19', '世茂', 0, 19),
(232, 16, 0, '20', '禹洲', 0, 20),
(233, 16, 0, '21', '三迪', 0, 21),
(234, 16, 0, '22', '华辰', 0, 22),
(235, 16, 0, '23', '中联', 0, 23),
(236, 16, 0, '24', '融汇', 0, 24),
(237, 16, 0, '25', '永兴', 0, 25),
(238, 16, 0, '26', '心家泊', 0, 26),
(239, 16, 0, '27', '景城', 0, 27),
(240, 16, 0, '28', '名城', 0, 28),
(241, 16, 0, '29', '汇诚', 0, 29),
(242, 16, 0, '30', '缔景', 0, 30),
(243, 16, 0, '31', '万事达', 0, 31),
(244, 16, 0, '32', '东源', 0, 32),
(245, 16, 0, '33', '闽长置业', 0, 33),
(246, 16, 0, '34', '旭源', 0, 34),
(247, 20, 0, '1', 'A．注册、登录', 0, 0),
(248, 20, 0, '2', 'B．实名、执业认证', 0, 0),
(249, 20, 0, '3', 'C．房源发布及质量', 0, 0),
(250, 20, 0, '4', 'D．小区信息贡献', 0, 0),
(251, 20, 0, '5', 'E．成交业绩和客户评价', 0, 0),
(252, 20, 0, '6', 'F．论坛贡献', 0, 0),
(253, 20, 0, '7', 'G．其它奖励', 0, 0),
(254, 20, 0, '8', 'H．积分处罚', 0, 0),
(255, 2, 0, '13', '东二环', 0, 13),
(256, 2, 0, '14', '八县', 0, 14),
(269, 21, 0, '1', '鼓楼区', 0, 1),
(270, 21, 0, '2', '晋安区', 0, 2),
(271, 21, 0, '3', '台江区', 0, 3),
(272, 21, 0, '4', '仓山区', 0, 4),
(273, 21, 0, '5', '马尾区', 0, 5),
(274, 22, 0, '1', '房源审核', 0, 1),
(277, 22, 0, '3', '求租求购', 0, 3),
(278, 22, 0, '2', '小区审核', 0, 2),
(279, 22, 0, '4', '会员认证', 0, 4),
(282, 23, 0, '1', '私产', 0, 0),
(283, 23, 0, '2', '公产', 0, 0),
(284, 23, 0, '3', '商品房', 0, 0),
(285, 23, 0, '4', '期房', 0, 0),
(286, 23, 0, '5', '经济适用房', 0, 0),
(287, 23, 0, '6', '使用权房', 0, 0),
(288, 23, 0, '7', '房改房', 0, 0),
(289, 1, 280, '2', '测试', 0, 0),
(290, 1, 281, '4', '在测试', 0, 0),
(291, 24, 0, '1', '期房预售', 0, 0),
(292, 24, 0, '2', '新盘热销', 0, 0),
(293, 24, 0, '3', '现房销售', 0, 0),
(294, 24, 0, '4', '售完', 0, 0),
(295, 1, 280, '5', '123', 0, 0),
(296, 25, 0, '1', '风水', 0, 0),
(297, 25, 0, '2', '商业社区', 0, 0),
(298, 25, 0, '3', '投资房', 0, 0),
(299, 25, 0, '4', '婚房', 0, 0),
(300, 1, 0, '6', '香坊区', 0, 0),
(301, 1, 0, '7', '道里区', 0, 0),
(302, 1, 0, '8', '平房区', 0, 0);

-- --------------------------------------------------------

--
-- 表的结构 `fke_dotime`
--

DROP TABLE IF EXISTS `fke_dotime`;
CREATE TABLE `fke_dotime` (
  `time` int(11) NOT NULL,
  `sell_time` int(11) DEFAULT '1',
  `borough_avg` int(11) DEFAULT NULL,
  `member_num` int(11) DEFAULT NULL,
  `broker_integral` int(11) DEFAULT NULL,
  `broker_active_rate` int(11) DEFAULT NULL,
  `statistics` int(11) DEFAULT NULL,
  `borough_pic_num` int(11) DEFAULT NULL,
  `houseInvalid` int(11) DEFAULT NULL,
  PRIMARY KEY (`time`)
) ENGINE=MyISAM DEFAULT CHARSET=gbk;

--
-- 转存表中的数据 `fke_dotime`
--

INSERT INTO `fke_dotime` (`time`, `sell_time`, `borough_avg`, `member_num`, `broker_integral`, `broker_active_rate`, `statistics`, `borough_pic_num`, `houseInvalid`) VALUES
(1359561600, 1332979200, 1360339200, 1359561600, 1359648000, 1359648000, 1359561600, 1359993600, 0);

-- --------------------------------------------------------

--
-- 表的结构 `fke_group`
--

DROP TABLE IF EXISTS `fke_group`;
CREATE TABLE `fke_group` (
  `group_id` int(11) NOT NULL AUTO_INCREMENT,
  `group_name` varchar(50) NOT NULL,
  PRIMARY KEY (`group_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=gbk AUTO_INCREMENT=6 ;

--
-- 转存表中的数据 `fke_group`
--

INSERT INTO `fke_group` (`group_id`, `group_name`) VALUES
(1, '房源管理员'),
(2, '楼盘管理员'),
(3, '会员管理员'),
(4, '小区管理员'),
(5, '总管理员');

-- --------------------------------------------------------

--
-- 表的结构 `fke_houserent`
--

DROP TABLE IF EXISTS `fke_houserent`;
CREATE TABLE `fke_houserent` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `house_title` varchar(40) NOT NULL,
  `cityarea_id` tinyint(4) DEFAULT '0',
  `cityarea2_id` tinyint(4) DEFAULT '0',
  `house_type` tinyint(4) NOT NULL,
  `house_no` varchar(20) NOT NULL,
  `house_price` float NOT NULL,
  `house_deposit` tinyint(4) DEFAULT '1',
  `house_totalarea` float DEFAULT '0',
  `house_room` tinyint(4) DEFAULT '0',
  `house_hall` tinyint(4) DEFAULT '0',
  `house_toilet` tinyint(4) DEFAULT '0',
  `house_veranda` tinyint(4) DEFAULT '0',
  `house_thumb` varchar(100) DEFAULT NULL,
  `house_topfloor` tinyint(4) DEFAULT '0',
  `house_floor` tinyint(4) DEFAULT '0',
  `house_age` varchar(4) DEFAULT NULL,
  `house_toward` tinyint(4) DEFAULT '0',
  `house_fitment` tinyint(4) DEFAULT '0',
  `house_support` varchar(100) DEFAULT NULL,
  `house_feature` varchar(250) DEFAULT NULL,
  `house_desc` mediumtext,
  `borough_id` int(11) DEFAULT '0',
  `borough_name` varchar(100) DEFAULT NULL,
  `broker_id` int(11) DEFAULT '0',
  `consigner_id` int(11) DEFAULT '0',
  `drawing_id` int(11) DEFAULT '0',
  `house_drawing` varchar(100) DEFAULT NULL,
  `is_share` tinyint(4) DEFAULT '0',
  `is_promote` tinyint(4) DEFAULT '2',
  `status` tinyint(4) DEFAULT '1',
  `house_downtime` int(11) DEFAULT '0',
  `is_checked` tinyint(4) DEFAULT '1',
  `order_weight` float DEFAULT '0',
  `update_order` int(11) DEFAULT '0',
  `tips_num` tinyint(4) DEFAULT '0',
  `click_num` int(11) DEFAULT '0',
  `tel_num` int(11) DEFAULT '0',
  `created` int(11) DEFAULT '0',
  `updated` int(11) DEFAULT '0',
  `is_index` tinyint(4) DEFAULT '0',
  `video` varchar(600) CHARACTER SET gb2312 DEFAULT NULL,
  `is_top` int(11) DEFAULT '0',
  `refresh` int(11) DEFAULT '2',
  `owner_name` varchar(100) DEFAULT NULL,
  `owner_phone` varchar(20) DEFAULT NULL,
  `owner_notes` mediumtext,
  `is_more_pic` tinyint(4) DEFAULT NULL,
  `company_id` tinyint(4) DEFAULT NULL,
  `is_like` tinyint(4) DEFAULT '0',
  `is_themes` tinyint(4) DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `house_price` (`house_price`),
  KEY `house_totalarea` (`house_totalarea`),
  KEY `broker_id` (`broker_id`)
) ENGINE=MyISAM DEFAULT CHARSET=gbk AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- 表的结构 `fke_houserent_bargain`
--

DROP TABLE IF EXISTS `fke_houserent_bargain`;
CREATE TABLE `fke_houserent_bargain` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `house_id` int(11) NOT NULL,
  `broker_id` int(11) NOT NULL,
  `borough_name` varchar(100) DEFAULT NULL,
  `house_totalarea` float DEFAULT '0',
  `house_price` float DEFAULT '0',
  `bargain_from` tinyint(4) NOT NULL DEFAULT '1',
  `buyer` varchar(20) DEFAULT NULL,
  `buyer_tel` varchar(20) DEFAULT NULL,
  `saler` varchar(20) DEFAULT NULL,
  `saler_tel` varchar(20) DEFAULT NULL,
  `bargain_price` float DEFAULT '0',
  `bargain_time` int(11) DEFAULT NULL,
  `remark` text,
  `evalute_score` tinyint(4) DEFAULT '0',
  `add_time` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=gbk AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- 表的结构 `fke_houserent_pic`
--

DROP TABLE IF EXISTS `fke_houserent_pic`;
CREATE TABLE `fke_houserent_pic` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `pic_url` varchar(200) NOT NULL,
  `pic_thumb` varchar(200) DEFAULT NULL,
  `pic_desc` varchar(200) DEFAULT NULL,
  `houserent_id` int(11) NOT NULL,
  `creater` varchar(20) DEFAULT NULL,
  `addtime` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `houserent_id` (`houserent_id`)
) ENGINE=MyISAM DEFAULT CHARSET=gbk AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- 表的结构 `fke_houserent_stat`
--

DROP TABLE IF EXISTS `fke_houserent_stat`;
CREATE TABLE `fke_houserent_stat` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `house_id` int(11) NOT NULL,
  `click_num` int(11) NOT NULL,
  `stat_date` date NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=gbk AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- 表的结构 `fke_houserent_top`
--

DROP TABLE IF EXISTS `fke_houserent_top`;
CREATE TABLE `fke_houserent_top` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `member_id` int(11) DEFAULT NULL,
  `houserent_id` int(11) DEFAULT NULL,
  `add_time` int(11) DEFAULT NULL,
  `to_time` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=gbk AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- 表的结构 `fke_housesell`
--

DROP TABLE IF EXISTS `fke_housesell`;
CREATE TABLE `fke_housesell` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `house_title` varchar(40) NOT NULL,
  `cityarea_id` tinyint(4) DEFAULT '0',
  `cityarea2_id` tinyint(4) DEFAULT '0',
  `house_type` tinyint(4) NOT NULL,
  `house_no` varchar(20) NOT NULL,
  `house_price` float NOT NULL,
  `house_totalarea` float DEFAULT '0',
  `house_room` tinyint(4) DEFAULT '0',
  `house_hall` tinyint(4) DEFAULT '0',
  `house_toilet` tinyint(4) DEFAULT '0',
  `house_veranda` tinyint(4) DEFAULT '0',
  `house_thumb` varchar(100) DEFAULT NULL,
  `house_topfloor` tinyint(4) DEFAULT '0',
  `house_floor` tinyint(4) DEFAULT '0',
  `house_age` varchar(4) DEFAULT NULL,
  `house_toward` tinyint(4) DEFAULT '0',
  `house_fitment` tinyint(4) DEFAULT '0',
  `house_feature` varchar(250) DEFAULT NULL,
  `house_desc` mediumtext,
  `borough_id` int(11) DEFAULT '0',
  `borough_name` varchar(100) DEFAULT NULL,
  `broker_id` int(11) DEFAULT '0',
  `consigner_id` int(11) DEFAULT '0',
  `drawing_id` int(11) DEFAULT '0',
  `house_drawing` varchar(100) DEFAULT NULL,
  `is_share` tinyint(4) DEFAULT '0',
  `is_promote` tinyint(4) DEFAULT '2',
  `status` tinyint(4) DEFAULT '1',
  `house_downtime` int(11) DEFAULT '0',
  `is_checked` tinyint(4) DEFAULT '1',
  `order_weight` float DEFAULT '0',
  `update_order` int(11) DEFAULT '0',
  `tips_num` tinyint(4) DEFAULT '0',
  `click_num` int(11) DEFAULT '0',
  `tel_num` int(11) DEFAULT '0',
  `created` int(11) DEFAULT '0',
  `updated` int(11) DEFAULT '0',
  `is_index` tinyint(4) DEFAULT '0',
  `video` varchar(600) CHARACTER SET gb2312 DEFAULT NULL,
  `is_top` int(11) DEFAULT '0',
  `belong` int(11) DEFAULT NULL,
  `owner_name` varchar(100) DEFAULT NULL,
  `owner_phone` varchar(20) DEFAULT NULL,
  `owner_notes` mediumtext,
  `refresh` int(11) DEFAULT '2',
  `is_more_pic` tinyint(4) DEFAULT '0',
  `is_vexation` tinyint(4) DEFAULT '0',
  `company_id` tinyint(4) DEFAULT NULL,
  `is_like` tinyint(4) DEFAULT '0',
  `is_themes` tinyint(4) DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `house_price` (`house_price`),
  KEY `broker_id` (`broker_id`),
  KEY `house_totalarea` (`house_totalarea`)
) ENGINE=MyISAM DEFAULT CHARSET=gbk AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- 表的结构 `fke_housesell_bargain`
--

DROP TABLE IF EXISTS `fke_housesell_bargain`;
CREATE TABLE `fke_housesell_bargain` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `house_id` int(11) NOT NULL,
  `broker_id` int(11) NOT NULL,
  `borough_name` varchar(100) DEFAULT NULL,
  `house_totalarea` float DEFAULT '0',
  `house_price` float DEFAULT '0',
  `bargain_from` tinyint(4) NOT NULL DEFAULT '1',
  `buyer` varchar(20) DEFAULT NULL,
  `buyer_tel` varchar(20) DEFAULT NULL,
  `saler` varchar(20) DEFAULT NULL,
  `saler_tel` varchar(20) DEFAULT NULL,
  `bargain_price` float DEFAULT '0',
  `bargain_time` int(11) DEFAULT NULL,
  `remark` text,
  `evalute_score` tinyint(4) DEFAULT '0',
  `add_time` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=gbk AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- 表的结构 `fke_housesell_pic`
--

DROP TABLE IF EXISTS `fke_housesell_pic`;
CREATE TABLE `fke_housesell_pic` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `pic_url` varchar(200) NOT NULL,
  `pic_thumb` varchar(200) DEFAULT NULL,
  `pic_desc` varchar(200) DEFAULT NULL,
  `housesell_id` int(11) NOT NULL,
  `creater` varchar(20) DEFAULT NULL,
  `addtime` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `housesell_id` (`housesell_id`)
) ENGINE=MyISAM DEFAULT CHARSET=gbk AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- 表的结构 `fke_housesell_stat`
--

DROP TABLE IF EXISTS `fke_housesell_stat`;
CREATE TABLE `fke_housesell_stat` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `house_id` int(11) NOT NULL,
  `click_num` int(11) NOT NULL,
  `stat_date` date NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=gbk AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- 表的结构 `fke_housesell_top`
--

DROP TABLE IF EXISTS `fke_housesell_top`;
CREATE TABLE `fke_housesell_top` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `member_id` int(11) DEFAULT NULL,
  `housesell_id` int(11) DEFAULT NULL,
  `add_time` int(11) DEFAULT NULL,
  `to_time` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=gbk AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- 表的结构 `fke_housesell_trend`
--

DROP TABLE IF EXISTS `fke_housesell_trend`;
CREATE TABLE `fke_housesell_trend` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `price` float(11,0) DEFAULT NULL,
  `area` float(11,0) DEFAULT NULL,
  `number` int(11) DEFAULT NULL,
  `time` varchar(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=gbk AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- 表的结构 `fke_house_wanted`
--

DROP TABLE IF EXISTS `fke_house_wanted`;
CREATE TABLE `fke_house_wanted` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `wanted_type` tinyint(4) DEFAULT '1',
  `house_no` varchar(20) DEFAULT NULL,
  `linkman` varchar(20) DEFAULT NULL,
  `link_tell` varchar(50) DEFAULT NULL,
  `open_tell` tinyint(4) DEFAULT '0',
  `requirement` varchar(250) DEFAULT NULL,
  `expert_id` varchar(250) DEFAULT NULL,
  `add_time` int(11) DEFAULT '0',
  `invalid_time` int(11) DEFAULT '0',
  `status` tinyint(4) DEFAULT '0',
  `wanted_hits` int(11) DEFAULT '0',
  `is_solve` tinyint(4) DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=gbk AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- 表的结构 `fke_house_wantedreply`
--

DROP TABLE IF EXISTS `fke_house_wantedreply`;
CREATE TABLE `fke_house_wantedreply` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `wanted_id` int(11) NOT NULL,
  `linkman` varchar(20) DEFAULT NULL,
  `content` text,
  `add_time` int(11) DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=gbk AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- 表的结构 `fke_innernote`
--

DROP TABLE IF EXISTS `fke_innernote`;
CREATE TABLE `fke_innernote` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `msg_from` varchar(20) NOT NULL,
  `msg_to` varchar(20) NOT NULL,
  `msg_title` varchar(100) NOT NULL,
  `msg_content` text,
  `is_new` tinyint(1) DEFAULT '1',
  `from_del` tinyint(4) NOT NULL,
  `to_del` tinyint(4) NOT NULL,
  `replay_to` int(11) NOT NULL,
  `belongs_to` int(11) DEFAULT '0',
  `add_time` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=gbk AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- 表的结构 `fke_integral_log`
--

DROP TABLE IF EXISTS `fke_integral_log`;
CREATE TABLE `fke_integral_log` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `member_id` int(11) NOT NULL,
  `rule_id` int(11) NOT NULL,
  `rule_class` tinyint(4) NOT NULL,
  `rule_score` int(11) DEFAULT '0',
  `add_time` int(11) DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=gbk AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- 表的结构 `fke_integral_rule`
--

DROP TABLE IF EXISTS `fke_integral_rule`;
CREATE TABLE `fke_integral_rule` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `rule_name` varchar(250) NOT NULL,
  `rule_class` tinyint(4) DEFAULT NULL,
  `rule_score` int(11) DEFAULT '0',
  `rule_remark` text,
  `rule_status` tinyint(4) DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=gbk AUTO_INCREMENT=30 ;

--
-- 转存表中的数据 `fke_integral_rule`
--

INSERT INTO `fke_integral_rule` (`id`, `rule_name`, `rule_class`, `rule_score`, `rule_remark`, `rule_status`) VALUES
(1, '经纪人登录', 1, 5, '经纪人登录，每日只记一次', 0),
(2, '注册使用满1年', 1, 1000, '', 0),
(3, '注册使用满2年', 1, 2000, '', 0),
(4, '注册使用满3年以上', 1, 3000, '', 0),
(7, '每发布一套出售房源', 3, 5, '', 0),
(5, '通过实名认证', 2, 2000, '上传身份证扫描件，并通过审核', 0),
(6, '通过执业认证', 2, 3000, '上传房产经纪人执业资格证书扫描，并通过审核', 0),
(10, '每发布一套出租房源', 3, 10, '', 0),
(11, '发布的房源有5张（含）以上室内照片', 3, 50, '', 0),
(12, '发布的房源具有有效户型图', 3, 50, '', 0),
(13, '新增一条小区信息', 4, 10, '新增一条房客网未收录的小区信息，并被审核通过', 0),
(14, '小区信息纠错或完善', 4, 5, '对已有的小区信息进行纠错或完善，并被审核通过', 0),
(15, '每上传一张小区图片（小区照片或户型图）', 4, 5, '上传真实有效的小区照片或户型图，并被采纳', 0),
(23, '每发一条新帖', 6, 10, '发布合法帖子，无违反互联网信息发布的有关规定', 0),
(24, '每回复一条', 6, 5, '回复帖子，无违反互联网信息发布的有关规定', 0),
(25, '每成功举报网站虚假或违法信息一次', 7, 10, '通过网站举报功能或拨打举报电话（88822114）', 0),
(26, '为网站提出建设性的意见或建议', 7, 100, '在“社区”的“投诉与建议”版块提交，并被采纳', 0),
(27, '每发布一条虚假房源', 8, -1000, '指价格严重失实、编造房源、重复发布同一房源、未及时下架的已租、已售房源等', 0),
(28, '发布虚假图片或重复发布相同图片', 8, -100, '房源室内照片、户型图或小区照片、户型图', 0),
(29, '发布违法信息', 8, -1000, '故意群发反动、色情信息等与当前法律、法规相抵触的行为，情节严重者，积分清零、账号封存。', 0);

-- --------------------------------------------------------

--
-- 表的结构 `fke_linkclass`
--

DROP TABLE IF EXISTS `fke_linkclass`;
CREATE TABLE `fke_linkclass` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `class_name` varchar(100) NOT NULL,
  `link_type` tinyint(4) NOT NULL,
  `list_order` tinyint(4) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=gbk AUTO_INCREMENT=5 ;

--
-- 转存表中的数据 `fke_linkclass`
--

INSERT INTO `fke_linkclass` (`id`, `class_name`, `link_type`, `list_order`) VALUES
(1, '房地产类', 0, 1),
(3, '传媒类', 0, 3),
(4, '图片链接', 1, 4);

-- --------------------------------------------------------

--
-- 表的结构 `fke_map`
--

DROP TABLE IF EXISTS `fke_map`;
CREATE TABLE `fke_map` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `cityarea_name` varchar(100) CHARACTER SET gb2312 DEFAULT NULL,
  `lat` varchar(100) DEFAULT NULL,
  `lnt` varchar(100) DEFAULT NULL,
  `sort` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=gbk AUTO_INCREMENT=10 ;

--
-- 转存表中的数据 `fke_map`
--

INSERT INTO `fke_map` (`id`, `cityarea_name`, `lat`, `lnt`, `sort`) VALUES
(9, '道外区', '45.859412', '126.55426', 2),
(8, '南岗区', '45.783806', '126.66687', 1);

-- --------------------------------------------------------

--
-- 表的结构 `fke_member`
--

DROP TABLE IF EXISTS `fke_member`;
CREATE TABLE `fke_member` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(32) DEFAULT NULL,
  `passwd` varchar(32) DEFAULT NULL,
  `email` varchar(50) DEFAULT NULL,
  `user_type` tinyint(4) NOT NULL DEFAULT '1',
  `logins` int(11) DEFAULT '0',
  `scores` int(11) DEFAULT '0',
  `sell_num` int(11) DEFAULT '0',
  `rent_num` int(11) DEFAULT '0',
  `last_login` int(11) DEFAULT '0',
  `active_str` varchar(14) DEFAULT NULL,
  `active_rate` int(11) DEFAULT '0',
  `active_total` int(11) DEFAULT '0',
  `order_weight` float DEFAULT '0',
  `top_day` tinyint(4) DEFAULT '0',
  `add_time` int(11) DEFAULT NULL,
  `status` int(11) DEFAULT '0',
  `is_index` tinyint(4) DEFAULT '0',
  `money` varchar(600) DEFAULT '0',
  `addsale` int(11) DEFAULT '0',
  `addrent` int(11) DEFAULT '0',
  `vip` tinyint(4) DEFAULT '0',
  `vexation` int(11) DEFAULT '0',
  `company_id` int(11) DEFAULT NULL,
  `e_accurate` int(11) DEFAULT '0',
  `e_satisfaction` int(11) DEFAULT '0',
  `e_specialty` int(11) DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `username` (`username`),
  KEY `active_rate` (`active_rate`)
) ENGINE=MyISAM DEFAULT CHARSET=gbk AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- 表的结构 `fke_member_loginlog`
--

DROP TABLE IF EXISTS `fke_member_loginlog`;
CREATE TABLE `fke_member_loginlog` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(20) NOT NULL,
  `login_times` int(11) DEFAULT '1',
  `add_time` int(11) DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `username` (`username`)
) ENGINE=MyISAM DEFAULT CHARSET=gbk AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- 表的结构 `fke_member_vip`
--

DROP TABLE IF EXISTS `fke_member_vip`;
CREATE TABLE `fke_member_vip` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `member_id` int(11) DEFAULT NULL,
  `add_time` int(11) DEFAULT NULL,
  `to_time` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=gbk AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- 表的结构 `fke_message_rule`
--

DROP TABLE IF EXISTS `fke_message_rule`;
CREATE TABLE `fke_message_rule` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `rule_name` varchar(250) NOT NULL,
  `rule_class` tinyint(4) DEFAULT NULL,
  `rule_remark` text,
  `rule_status` tinyint(4) DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=gbk AUTO_INCREMENT=22 ;

--
-- 转存表中的数据 `fke_message_rule`
--

INSERT INTO `fke_message_rule` (`id`, `rule_name`, `rule_class`, `rule_remark`, `rule_status`) VALUES
(1, '出售房源审核不通过', 1, '%s，您好：<br>您发布的出售房源【<a href="%sd-%s.html" target="_blank">%s</a>】未能通过审核，请完善房源信息后重新发布。\r\n', 0),
(2, '出租房源审核不通过', 1, '%s，您好：<br>您发布的出租房源【<a href="%sd-%s.html"  target="_blank">%s</a>】未能通过审核，请完善房源信息后重新发布。\r\n\r\n', 0),
(3, '小区添加审核不通过', 2, '%s，您好：<br>您添加的小区【<a href="%sg-%d.html" target="_blank">%s</a>】经审核，该小区不存在或已经有同名小区，请确认小区名称拼写是否有误。', 0),
(4, '小区文字信息不通过', 2, '%s，您好：<br>您于%s对小区【<a href="%sg-%d.html" target="_blank">%s</a>】%s进行的修改未能通过审核，已被删除。', 0),
(9, '身份认证不通过', 4, '%s，您好：<br>您本次未通过实名认证审核，请上传真实、清晰的身份证照片。', 0),
(10, '身份认证已通过', 4, '%s，您好：<br>恭喜您已通过实名认证，并获得%d积分。<br>\r\n您的网店已经开通，现在就去张罗网店，开张吧！\r\n', 0),
(11, '资质认证不通过', 4, '%s，您好：<br>您本次未通过资质认证，请上传真实、清晰的经纪人资格证书照片。', 0),
(12, '资质认证已通过', 4, '%s，恭喜您已通过资质认证，并获得%d积分。', 0),
(13, '头像认证不通过', 4, '%s，您好：<br>您本次上传的头像经审核不符合经纪人头像要求，请按要求重新上传。\r\n', 0),
(14, '头像认证通过', 4, '%s，您好：<br>您的头像已更新。<a href="%s%d">进入网店查看</a>', 0),
(16, '出售房源删除', 1, '%s，您好：<br>您发布的出售房源【<a href="%sd-%s.html" target="_blank">%s</a>】已被删除。', 0),
(17, '出租房源删除', 1, '%s，您好：<br>您发布的出租房源【<a href="%sd-%s.html">%s</a>】已被删除。', 0),
(18, '小区照片审核不通过', 2, '%s，您好：<br>您于%s为【<a href="%sg-%d.html">%s</a>】上传的%s未能通过审核，已被删除。', 0),
(19, '小区文字信息通过', 2, '%s，您好：<br>您于%s对小区【<a href="%sg-%d.html">%s</a>】%s进行的修改已通过审核，获得%d积分。', 0),
(20, '小区照片审核通过', 2, '%s，您好：<br>您于%s为【<a href="%sg-%d.html">%s</a>】上传的%s已通过审核，获得%d积分。', 0),
(21, '新添加的小区合并', 2, '%s，您好：<br>您添加的小区【<a href="%sg-%d.html" target="_blank">%s</a>】经审核，网上已经有同名小区【<a href="%sg-%d.html" target="_blank">%s</a>】，请使用小区名【%s】发布房源。', 0);

-- --------------------------------------------------------

--
-- 表的结构 `fke_money_log`
--

DROP TABLE IF EXISTS `fke_money_log`;
CREATE TABLE `fke_money_log` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `member_id` int(11) DEFAULT NULL,
  `time` int(11) DEFAULT NULL,
  `money` varchar(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=gbk AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- 表的结构 `fke_outlink`
--

DROP TABLE IF EXISTS `fke_outlink`;
CREATE TABLE `fke_outlink` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `link_name` varchar(100) NOT NULL,
  `link_class` tinyint(4) NOT NULL,
  `link_type` tinyint(4) NOT NULL,
  `link_logo` varchar(250) NOT NULL,
  `link_text` varchar(250) NOT NULL,
  `link_url` varchar(250) NOT NULL,
  `list_order` tinyint(4) NOT NULL DEFAULT '0',
  `click_num` int(11) NOT NULL DEFAULT '0',
  `status` tinyint(4) NOT NULL DEFAULT '0',
  `add_time` int(11) NOT NULL,
  `shouye` tinyint(4) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=gbk AUTO_INCREMENT=18 ;

--
-- 转存表中的数据 `fke_outlink`
--

INSERT INTO `fke_outlink` (`id`, `link_name`, `link_class`, `link_type`, `link_logo`, `link_text`, `link_url`, `list_order`, `click_num`, `status`, `add_time`, `shouye`) VALUES
(17, '图片', 4, 2, 'outlink/20120406122847.gif', '', 'http://anleye.yanwee.com', 0, 0, 0, 1268580723, 1),
(16, '安乐业', 3, 1, '', '言微论坛', 'http://anleye.yanwee.com', 2, 0, 0, 1257487408, 1),
(15, '言微在线', 3, 1, '', '言微网络', 'http://www.yanwee.com', 1, 0, 0, 1257487373, 1);

-- --------------------------------------------------------

--
-- 表的结构 `fke_pinggu`
--

DROP TABLE IF EXISTS `fke_pinggu`;
CREATE TABLE `fke_pinggu` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `house_type` tinyint(4) NOT NULL,
  `borough_id` int(11) NOT NULL,
  `borough_name` varchar(250) NOT NULL,
  `home_no` tinyint(4) NOT NULL,
  `room_no` tinyint(4) NOT NULL,
  `house_totalarea` float NOT NULL,
  `house_room` tinyint(4) NOT NULL,
  `house_hall` tinyint(4) NOT NULL,
  `house_toilet` tinyint(4) NOT NULL,
  `house_topfloor` tinyint(4) NOT NULL,
  `house_floor` tinyint(4) NOT NULL,
  `house_toward` tinyint(4) NOT NULL,
  `has_lift` tinyint(4) NOT NULL,
  `has_empty` tinyint(4) NOT NULL,
  `house_fitment` tinyint(4) NOT NULL,
  `fitment_price` float NOT NULL,
  `fitment_year` float NOT NULL,
  `house_place` tinyint(4) NOT NULL,
  `house_view` tinyint(4) NOT NULL,
  `house_light` tinyint(4) NOT NULL,
  `house_noise` tinyint(4) NOT NULL,
  `house_quality` tinyint(4) NOT NULL,
  `is_detail` tinyint(4) NOT NULL,
  `click_num` int(11) NOT NULL,
  `house_totalprice` float NOT NULL,
  `house_avgprice` float NOT NULL,
  `house_pgprice` float NOT NULL,
  `house_avgpgprice` float NOT NULL,
  `creater` varchar(50) NOT NULL,
  `add_time` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=gbk AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- 表的结构 `fke_pinggu_dd`
--

DROP TABLE IF EXISTS `fke_pinggu_dd`;
CREATE TABLE `fke_pinggu_dd` (
  `dd_id` int(11) NOT NULL AUTO_INCREMENT,
  `dd_name` varchar(50) DEFAULT NULL,
  `dd_caption` varchar(50) NOT NULL,
  PRIMARY KEY (`dd_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=gbk AUTO_INCREMENT=13 ;

--
-- 转存表中的数据 `fke_pinggu_dd`
--

INSERT INTO `fke_pinggu_dd` (`dd_id`, `dd_name`, `dd_caption`) VALUES
(1, 'house_type', '房屋类型'),
(2, 'house_toward', '房屋朝向'),
(3, 'house_place', '房产位置'),
(4, 'house_light', '通风、采光'),
(5, 'house_totalarea', '房屋面积'),
(6, 'house_fitment', '装修情况'),
(7, 'house_view', '房产景观'),
(8, 'house_noise', '噪音情况'),
(9, 'house_quality', '建筑质量'),
(10, 'house_floornoliftnoempty', '所在楼层(无电梯,无架空)'),
(11, 'house_floornoliftempty', '所在楼层(无电梯,有架空)'),
(12, 'house_floorlift', '所在楼层(有电梯)');

-- --------------------------------------------------------

--
-- 表的结构 `fke_pinggu_dd_item`
--

DROP TABLE IF EXISTS `fke_pinggu_dd_item`;
CREATE TABLE `fke_pinggu_dd_item` (
  `di_id` int(11) NOT NULL AUTO_INCREMENT,
  `dd_id` int(11) NOT NULL,
  `di_value` varchar(10) NOT NULL,
  `di_caption` varchar(50) NOT NULL,
  `di_quotiety` float NOT NULL,
  `list_group` tinyint(4) DEFAULT '0',
  `list_order` int(11) DEFAULT '0',
  PRIMARY KEY (`di_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=gbk AUTO_INCREMENT=62 ;

--
-- 转存表中的数据 `fke_pinggu_dd_item`
--

INSERT INTO `fke_pinggu_dd_item` (`di_id`, `dd_id`, `di_value`, `di_caption`, `di_quotiety`, `list_group`, `list_order`) VALUES
(1, 5, '1', '0-40', 0.15, 0, 0),
(2, 5, '2', '40-60', 0.1, 0, 0),
(3, 5, '3', '60-75', 0.06, 0, 0),
(4, 5, '4', '75-90', 0.04, 0, 0),
(5, 5, '5', '90-110', 0, 0, 0),
(6, 5, '6', '110-130', -0.02, 0, 0),
(7, 5, '7', '130-160', -0.04, 0, 0),
(8, 5, '8', '160-0', -0.08, 0, 0),
(9, 2, '1', '南北', 0, 0, 0),
(10, 2, '2', '东南', 0.03, 0, 0),
(11, 2, '3', '西南', 0.02, 0, 0),
(12, 2, '4', '东北', -0.02, 0, 0),
(13, 2, '5', '西北', -0.04, 0, 0),
(14, 2, '6', '南', -0.02, 0, 0),
(15, 2, '7', '北', -0.04, 0, 0),
(16, 1, '1', '多层', 0, 0, 0),
(17, 1, '2', '小高层', 0, 0, 0),
(18, 1, '3', '高层', 0, 0, 0),
(19, 1, '4', '独立', 0.5, 0, 0),
(20, 1, '5', '双拼', 0.325, 0, 0),
(21, 1, '6', '联排', 0.3, 0, 0),
(22, 1, '7', '叠加', 0.25, 0, 0),
(23, 6, '1', '毛坯', 0, 0, 0),
(24, 6, '2', '普通装修', 450, 0, 0),
(25, 6, '3', '精装修', 700, 0, 0),
(26, 6, '4', '豪华装修', 1000, 0, 0),
(27, 4, '1', '好', 0.02, 0, 1),
(28, 4, '2', '一般', 0, 0, 2),
(29, 4, '3', '差', -0.03, 0, 3),
(30, 7, '1', '景观好', 0.03, 0, 0),
(31, 7, '2', '一般', 0, 0, 0),
(32, 3, '1', '离小区出口近', 0.02, 0, 0),
(33, 3, '2', '离小区出口远', -0.02, 0, 0),
(34, 3, '3', '位置适中', 0, 0, 0),
(35, 8, '1', '无噪音', 0, 0, 0),
(36, 8, '2', '噪音较小', -0.03, 0, 0),
(37, 8, '3', '噪音较大', -0.06, 0, 0),
(38, 9, '1', '有沉降现象', -0.1, 0, 0),
(39, 9, '2', '有墙面开裂现象', -0.1, 0, 0),
(40, 9, '3', '有渗水现象', -0.1, 0, 0),
(41, 10, '1', '1', -0.1, 0, 0),
(42, 10, '2', '2', -0.06, 0, 0),
(43, 10, '3', '3', -0.02, 0, 0),
(44, 10, '4', '4', 0, 0, 0),
(45, 10, '5', '5', -0.02, 0, 0),
(46, 10, '6', '6', -0.04, 0, 0),
(47, 10, '7', '7', -0.08, 0, 0),
(48, 10, '8', '8', -0.12, 0, 0),
(49, 11, '1', '1', -0.08, 0, 0),
(50, 11, '2', '2', -0.03, 0, 0),
(51, 11, '3', '3', 0, 0, 0),
(52, 11, '4', '4', 0, 0, 0),
(53, 11, '5', '5', -0.02, 0, 0),
(54, 11, '6', '6', -0.06, 0, 0),
(55, 11, '7', '7', -0.1, 0, 0),
(56, 11, '8', '8', -0.15, 0, 0),
(57, 12, '1', '1', -0.1, 0, 0),
(58, 12, '2', '2', -0.04, 0, 0),
(59, 12, '3', '3', -0.02, 0, 0),
(60, 12, '4', '4', 0, 0, 0),
(61, 12, '5', '5', 0.02, 0, 0);

-- --------------------------------------------------------

--
-- 表的结构 `fke_report`
--

DROP TABLE IF EXISTS `fke_report`;
CREATE TABLE `fke_report` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `house_type` char(4) NOT NULL,
  `house_id` int(11) NOT NULL,
  `report_target` int(11) NOT NULL,
  `reason` text,
  `reporter` int(11) NOT NULL,
  `status` tinyint(11) DEFAULT '0',
  `addtime` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=gbk AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- 表的结构 `fke_shop_conf`
--

DROP TABLE IF EXISTS `fke_shop_conf`;
CREATE TABLE `fke_shop_conf` (
  `broker_id` int(11) NOT NULL,
  `shop_name` varchar(100) DEFAULT NULL,
  `shop_url` varchar(150) DEFAULT NULL,
  `shop_notice` text,
  `shop_style` varchar(20) DEFAULT 'default',
  `click_num` int(11) DEFAULT '0',
  `created` int(11) NOT NULL,
  PRIMARY KEY (`broker_id`)
) ENGINE=MyISAM DEFAULT CHARSET=gbk;

-- --------------------------------------------------------

--
-- 表的结构 `fke_shop_viewlog`
--

DROP TABLE IF EXISTS `fke_shop_viewlog`;
CREATE TABLE `fke_shop_viewlog` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `broker_id` int(11) NOT NULL,
  `friend_id` int(11) NOT NULL,
  `click_num` int(11) DEFAULT NULL,
  `add_time` int(11) DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=gbk AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- 表的结构 `fke_statistics`
--

DROP TABLE IF EXISTS `fke_statistics`;
CREATE TABLE `fke_statistics` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `stat_name` varchar(50) NOT NULL,
  `stat_index` varchar(20) NOT NULL,
  `stat_value` int(11) NOT NULL DEFAULT '0',
  `stat_unit` varchar(6) DEFAULT NULL,
  `stat_class` varchar(20) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `stat_index` (`stat_index`)
) ENGINE=MyISAM  DEFAULT CHARSET=gbk AUTO_INCREMENT=18 ;

--
-- 转存表中的数据 `fke_statistics`
--

INSERT INTO `fke_statistics` (`id`, `stat_name`, `stat_index`, `stat_value`, `stat_unit`, `stat_class`) VALUES
(1, '二手房发布', 'sellNum', 0, '套', 'allNum'),
(2, '租房发布', 'rentNum', 0, '套', 'allNum'),
(3, '房产经纪人', 'brokerNum', 0, '位', 'allNum'),
(4, '业主', 'ownerNum', 0, '位', 'allNum'),
(5, '小区信息收录', 'boroughNum', 0, '个', 'allNum'),
(6, '出售房源编号', 'housesell_no', 10000, '条', 'maxNumber'),
(7, '出租房源编号', 'houserent_no', 10000, '条', 'maxNumber'),
(8, '求租求购房源编号', 'houseWanted_no', 5910000, '条', 'maxNumber'),
(17, '二手房均价', 'avgprice', 10000, '元/平方', 'val');

-- --------------------------------------------------------

--
-- 表的结构 `fke_union`
--

DROP TABLE IF EXISTS `fke_union`;
CREATE TABLE `fke_union` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `cname` varchar(50) DEFAULT NULL,
  `cityid` varchar(20) DEFAULT NULL,
  `address` varchar(200) DEFAULT NULL,
  `con` varchar(20) DEFAULT NULL,
  `mo` varchar(30) DEFAULT NULL,
  `tel` varchar(30) DEFAULT NULL,
  `company` mediumtext,
  `good` mediumtext,
  `status` tinyint(4) DEFAULT '0',
  `time` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=gbk AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- 表的结构 `fke_users`
--

DROP TABLE IF EXISTS `fke_users`;
CREATE TABLE `fke_users` (
  `user_id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(32) NOT NULL,
  `passwd` varchar(32) NOT NULL,
  `email` varchar(150) DEFAULT NULL,
  `group_id` smallint(6) DEFAULT NULL,
  `isdel` tinyint(1) DEFAULT '0',
  PRIMARY KEY (`user_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=gbk AUTO_INCREMENT=2 ;

--
-- 转存表中的数据 `fke_users`
--

INSERT INTO `fke_users` (`user_id`, `username`, `passwd`, `email`, `group_id`, `isdel`) VALUES
(1, 'admin', '21232f297a57a5a743894a0e4a801fc3', 'admin@admin.com', 5, 0);

-- --------------------------------------------------------

--
-- 表的结构 `fke_web_config`
--

DROP TABLE IF EXISTS `fke_web_config`;
CREATE TABLE `fke_web_config` (
  `id` int(11) NOT NULL,
  `base_title` varchar(20) DEFAULT NULL,
  `base_titlec` varchar(20) DEFAULT NULL,
  `base_city` varchar(20) DEFAULT NULL,
  `base_mapcity` varchar(20) DEFAULT NULL,
  `base_is_shop` tinyint(4) DEFAULT NULL,
  `base_is_guest` tinyint(4) DEFAULT NULL,
  `base_member_open` tinyint(4) DEFAULT NULL,
  `base_beian` varchar(20) DEFAULT NULL,
  `contact_gongsi` varchar(40) DEFAULT NULL,
  `contact_dizhi` varchar(40) DEFAULT NULL,
  `contact_youbian` varchar(20) DEFAULT NULL,
  `contact_chuanzhen` varchar(20) DEFAULT NULL,
  `contact_dianhua` varchar(20) DEFAULT NULL,
  `contact_rexian` varchar(20) DEFAULT NULL,
  `contact_qq` varchar(20) DEFAULT NULL,
  `vip_vip1_price` varchar(100) DEFAULT NULL,
  `vip_vip1_sale_num` int(11) DEFAULT NULL,
  `vip_vip1_rent_num` int(11) DEFAULT NULL,
  `vip_vip1_vexation` int(11) DEFAULT NULL,
  `vip_vip2_price` varchar(100) DEFAULT NULL,
  `vip_vip2_sale_num` int(11) DEFAULT NULL,
  `vip_vip2_rent_num` int(11) DEFAULT NULL,
  `vip_vip2_vexation` int(11) DEFAULT NULL,
  `alipay_name` varchar(100) DEFAULT NULL,
  `alipay_partner` varchar(200) DEFAULT NULL,
  `alipay_security_code` varchar(300) DEFAULT NULL,
  `alipay_sell_price` varchar(100) DEFAULT NULL,
  `alipay_rent_price` varchar(100) DEFAULT NULL,
  `alipay_sell_num` varchar(100) DEFAULT NULL,
  `alipay_rent_num` varchar(100) DEFAULT NULL,
  `base_googlekey` varchar(300) DEFAULT NULL,
  `base_tongji` varchar(300) DEFAULT NULL,
  `base_uc` tinyint(4) DEFAULT NULL,
  `lat` varchar(100) DEFAULT NULL,
  `lnt` varchar(100) DEFAULT NULL,
  `sinaapp` varchar(100) DEFAULT NULL,
  `newsOpen` tinyint(4) DEFAULT NULL,
  `bbsOpen` tinyint(4) DEFAULT NULL,
  `switch_url` varchar(300) DEFAULT NULL,
  `index_keyword` varchar(200) DEFAULT NULL,
  `index_description` varchar(300) DEFAULT NULL,
  `sale_keyword` varchar(200) DEFAULT NULL,
  `sale_description` varchar(300) DEFAULT NULL,
  `rent_keyword` varchar(200) DEFAULT NULL,
  `rent_description` varchar(300) DEFAULT NULL,
  `broker_keyword` varchar(200) DEFAULT NULL,
  `broker_description` varchar(300) DEFAULT NULL,
  `community_keyword` varchar(200) DEFAULT NULL,
  `community_description` varchar(300) DEFAULT NULL,
  `newhouse_keyword` varchar(200) DEFAULT NULL,
  `newhouse_description` varchar(300) DEFAULT NULL,
  `newhouseOpen` int(11) DEFAULT '2',
  `citySwitch_site` varchar(100) DEFAULT NULL,
  `borough_avg_time` int(11) DEFAULT '30',
  `member_num_time` int(11) DEFAULT NULL,
  `broker_integral_time` int(11) DEFAULT NULL,
  `broker_active_Rate_time` int(11) DEFAULT NULL,
  `statistics_time` int(11) DEFAULT NULL,
  `borough_pic_num_time` int(11) DEFAULT NULL,
  `expired_switch` int(11) DEFAULT NULL,
  `house_invalid_time` int(11) DEFAULT NULL,
  `vip_vip1_refresh` varchar(10) DEFAULT NULL,
  `vip_vip2_refresh` varchar(10) DEFAULT NULL,
  `themesDescription` varchar(300) DEFAULT NULL,
  `themesMessage` varchar(300) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=gbk;

--
-- 转存表中的数据 `fke_web_config`
--

INSERT INTO `fke_web_config` (`id`, `base_title`, `base_titlec`, `base_city`, `base_mapcity`, `base_is_shop`, `base_is_guest`, `base_member_open`, `base_beian`, `contact_gongsi`, `contact_dizhi`, `contact_youbian`, `contact_chuanzhen`, `contact_dianhua`, `contact_rexian`, `contact_qq`, `vip_vip1_price`, `vip_vip1_sale_num`, `vip_vip1_rent_num`, `vip_vip1_vexation`, `vip_vip2_price`, `vip_vip2_sale_num`, `vip_vip2_rent_num`, `vip_vip2_vexation`, `alipay_name`, `alipay_partner`, `alipay_security_code`, `alipay_sell_price`, `alipay_rent_price`, `alipay_sell_num`, `alipay_rent_num`, `base_googlekey`, `base_tongji`, `base_uc`, `lat`, `lnt`, `sinaapp`, `newsOpen`, `bbsOpen`, `switch_url`, `index_keyword`, `index_description`, `sale_keyword`, `sale_description`, `rent_keyword`, `rent_description`, `broker_keyword`, `broker_description`, `community_keyword`, `community_description`, `newhouse_keyword`, `newhouse_description`, `newhouseOpen`, `citySwitch_site`, `borough_avg_time`, `member_num_time`, `broker_integral_time`, `broker_active_Rate_time`, `statistics_time`, `borough_pic_num_time`, `expired_switch`, `house_invalid_time`, `vip_vip1_refresh`, `vip_vip2_refresh`, `themesDescription`, `themesMessage`) VALUES
(1, '安乐业', '安乐业', '哈尔滨', '哈尔滨市', 2, 1, 1, '黑10000001', '哈尔滨言微网络技术开发有限公司', '红旗大街997号', '150001', '0451-88888888', '0451-88888888', '4006660694', '442990999', '101', 100, 100, 5, '300', 100, 100, 20, 'pay@yanwee.com', '2088102268240957', '6fynnedx89uc3jp5k5uagkq0osb49wkw', '2.5', '2', '3', '1', '', '', 2, '45.828799', '126.539154', '515128800', 2, 2, '', '阿萨德发射点发', '阿萨德发', '爱的色放', 'ad发射点发', '', '', '', '', '', '', '', '', 2, '', 1296000, 86400, 86400, 86400, 86400, 432000, 2, 86400, '10', '20', '<span>的说法撒旦法</span>', '阿萨德发送到');
