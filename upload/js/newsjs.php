<?php
require('path.inc.php');
$borough = new Borough($query);
$member = new Member($query);
$houseSell = new HouseSell($query);
$houseRent = new HouseRent($query);

$id = $_GET['id'];
//推广楼盘调用
if($id == 0){
	$page->name = 'commendborough'; //页面名字,和文件名相同	
	$commendborough = $borough->getList(array('rowFrom'=>0,'rowTo'=>4),1,' and isdel =0 and isnew =1 and is_promote=1' ,' order by updated desc');
     $page->tpl->assign('commendborough', $commendborough);
	}

//即将开盘调用
if($id == 1){
	$page->name = 'newborough'; //页面名字,和文件名相同	
	$newborough = $borough->getList(array('rowFrom'=>0,'rowTo'=>6),1," and isdel =0 and isnew =1 and (sell_time>'".date('Y-m-d')."' or sell_time ='')" ,' order by updated desc');
    $page->tpl->assign('newborough', $newborough);
	}
	
//促销楼盘调用
if($id == 2){
	$page->name = 'priceoffborough'; //页面名字,和文件名相同	
	$priceoffborough = $borough->getList(array('rowFrom'=>0,'rowTo'=>6),1,' and isdel =0 and isnew =1 and is_priceoff=1' ,' order by updated desc');
    $page->tpl->assign('priceoffborough', $priceoffborough);
	}
//区域调用
if($id == 3){
	$page->name = 'cityarea'; //页面名字,和文件名相同	
	$cityarea_option = Dd::getArray('cityarea');
    $page->tpl->assign('cityarea_option', $cityarea_option);
	}
//调用热点新房
if($id == 4){
	$page->name = 'clickborough'; //页面名字,和文件名相同	
	$clickborough = $borough->getList(array('rowFrom'=>0,'rowTo'=>9),1,' and isdel =0 and isnew =1' ,' order by click_num desc');
    $page->tpl->assign('clickborough', $clickborough);
	}
//调用推荐经纪人
if($id == 5){
	$page->name = 'commendbroker'; //页面名字,和文件名相同	
	$commendbroker = $member->getListBroker(array('rowFrom'=>0,'rowTo'=>2),'*',' m.status = 0 and m.user_type=1',' order by m.order_weight desc');
    $page->tpl->assign('commendbroker', $commendbroker);
	}
//调用最新出售房源
if($id == 6){
	$page->name = 'houseSellBest'; //页面名字,和文件名相同	
    $houseSellBest = $houseSell->getList(array('rowFrom'=>0,'rowTo'=>7),'*',1,' and status =1 and broker_id <>0' ,' order by updated desc');
   foreach ($houseSellBest as $key =>$item ){
	$houseSellBest[$key]['house_title'] = substrs($item['house_title'],20);
	$houseSellBest[$key]['cityarea_name'] = substr($cityarea_option[$item['cityarea_id']],0,-2);
	$houseSellBest[$key]['borough_name'] = substrs($item['borough_name'],12);
	}
	$page->tpl->assign('houseSellBest', $houseSellBest);
}
//调用最新个人出售房源
if($id == 7){
	$page->name = 'perHouseSellBest'; //页面名字,和文件名相同	
    $perHouseSellBest = $houseSell->getList(array('rowFrom'=>0,'rowTo'=>7),'*',1,' and status =1 and broker_id = 0' ,' order by updated desc');
   foreach ($perHouseSellBest as $key =>$item ){
	$perHouseSellBest[$key]['house_title'] = substrs($item['house_title'],20);
	$perHouseSellBest[$key]['cityarea_name'] = substr($cityarea_option[$item['cityarea_id']],0,-2);
	$perHouseSellBest[$key]['borough_name'] = substrs($item['borough_name'],12);
	}
	$page->tpl->assign('perHouseSellBest', $perHouseSellBest);
}
//调用最新出租房源
if($id == 8){
	$page->name = 'houseRentBest'; //页面名字,和文件名相同	
    $houseRentBest = $houseRent->getList(array('rowFrom'=>0,'rowTo'=>7),'*',1,' and status =1 and broker_id <> 0' ,' order by updated desc');
   foreach ($houseRentBest as $key =>$item ){
	$houseRentBest[$key]['house_title'] = substrs($item['house_title'],20);
	$houseRentBest[$key]['cityarea_name'] = substr($cityarea_option[$item['cityarea_id']],0,-2);
	$houseRentBest[$key]['borough_name'] = substrs($item['borough_name'],12);
	}
	$page->tpl->assign('houseRentBest', $houseRentBest);
}
//调用最新个人出租房源
if($id == 9){
	$page->name = 'perHouseRentBest'; //页面名字,和文件名相同	
    $perhouseRentBest = $houseRent->getList(array('rowFrom'=>0,'rowTo'=>7),'*',1,' and status =1 and broker_id = 0' ,' order by updated desc');
   foreach ($perhouseRentBest as $key =>$item ){
	$perhouseRentBest[$key]['house_title'] = substrs($item['house_title'],20);
	$perhouseRentBest[$key]['cityarea_name'] = substr($cityarea_option[$item['cityarea_id']],0,-2);
	$perhouseRentBest[$key]['borough_name'] = substrs($item['borough_name'],12);
	}
	$page->tpl->assign('perHouseRentBest', $perhouseRentBest);
}
//检查会员是否登录
if($id == 10){
	$page->name = 'memberLogin'; //页面名字,和文件名相同	
}

$page->show();
?>