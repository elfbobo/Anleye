<?php
require('path.inc.php');

//id
$id = intval($_GET['id']);
if(!$id){
	$page->urlto('requireList.php');
}
$houseWanted = new HouseWanted($query);

if($page->action =="reply"){
	//回复
	$_POST['wanted_id'] = $id;
	if (md5(strtolower($_POST['vaild']))!=$_COOKIE['validString']) {
		$page->back("验证码错误");
	}
	if(!$_POST['wanted_id']){
		$page->urlto("requireList.php");
	}
	if(!$_POST['linkman']){
		$page->back("请填写联系人");
	}
	if(!$_POST['content']){
		$page->back("请填写内容");
	}
	try{
		$houseWanted->saveReply($_POST);
		$page->urlto('requireDetail.php?id='.$id);
	}catch (Exception $e){
		$page->back('出错了');
	}
	exit;
}else{
	$page->name = 'requireDetail'; //页面名字,和文件名相同
	
	$page->addJs('FormValid.js');
	
	//区域字典
	$cityarea_option = Dd::getArray('cityarea');
	$page->tpl->assign('cityarea_option', $cityarea_option);
	
	$house_price_option = array(
		'0-40'=>'40万以下',
		'40-60'=>'40-60万',
		'60-80'=>'60-80万',
		'80-100'=>'80-100万',
		'100-120'=>'100-120万',
		'120-150'=>'100-120万',
		'150-200'=>'150-200万',
		'200-250'=>'200-250万',
		'250-300'=>'250-300万',
		'300-500'=>'300-500万',
		'500-0'=>'500万以上'
	);
	$page->tpl->assign('house_price_option', $house_price_option);
	//详细信息
	$dataInfo = $houseWanted->getInfo($id,'*');
	
	if(!$dataInfo){
		$page->urlto('requireList.php','信息不存在或已删除');
	}
	$dataInfo['requirement_short'] = substrs($dataInfo['requirement'],40);
	$page->tpl->assign('dataInfo', $dataInfo);
	//回复列表
	$replyList = $houseWanted->getReplyList($id);
	$page->tpl->assign('replyList', $replyList);
	
	$page->tpl->assign('reply_num', count($replyList));
	
	$page->show();
}
?>