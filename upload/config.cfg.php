<?php
/* 
 * 数据库
 */
$cfg['db']['host'] 		= 'localhost';  // 主机地址
$cfg['db']['user'] 		= 'root';	    // 用户名	
$cfg['db']['password'] 	= '';	    // 密码 
$cfg['db']['name'] 		= 'anleye';		// 数据库名
$cfg['db']['provider'] 	= 'mysql';		// 数据库类型
/* 
 * 文档根路径
 */
$cfg['path']['root'] = dirname(__FILE__) . '/';
$cfg['version'] = '0.1';
$cfg['time'] = time();


/* 
 * 站点信息
 */
$cfg['charset']    	= 	'gb2312';
$cfg['style']  		= 	'blue2012';       // 主题
$cfg['url']   		= 	'http://127.0.0.1/';   // 站点URL

$cfg['url_sale']   	= 	 $cfg['url'].'sale/';
$cfg['url_pinggu']  	= 	$cfg['url'].'pinggu/';
$cfg['url_rent']   		= 	$cfg['url'].'rent/';
$cfg['url_news']   		= 	$cfg['url'].'news/';
$cfg['url_newHouse']   	= 	$cfg['url'].'newHouse/';
$cfg['url_company']   	= 	$cfg['url'].'company/';
$cfg['url_community']   = $cfg['url'].'community/';
$cfg['url_broker']   	= 	$cfg['url'].'broker/';
$cfg['url_university']  = 	$cfg['url'].'university/';
$cfg['url_shop']  		= 	$cfg['url'].'shop/';
$cfg['url_bbs']   		= 	$cfg['url'].'bbs/';
$cfg['url_upfile']   		= $cfg['url'].'upfile/';


$cfg['domain'] 		= 	'';   // 域名
$cfg['debug']  		= 	1;               // 是否发布状态0为发布
$cfg['path']['conf'] 	= $cfg['path']['root'] . 'conf/';
$cfg['path']['part'] 	= $cfg['path']['root'] . 'part/';
$cfg['path']['data'] 	= $cfg['path']['root'] . 'data/';
$cfg['path']['common'] 	= $cfg['path']['root'] . 'common/';
$cfg['path']['lib']     = $cfg['path']['common'] . 'lib/';
$cfg['path']['apps'] 	= $cfg['path']['common'] . 'apps/';
$cfg['path']['themes'] 	= $cfg['path']['root'] . 'themes/' . $cfg['style'] . '/';
$cfg['path']['template'] = $cfg['path']['themes'] . 'template/';

$cfg['site']['themes'] 	= $cfg['url'] . 'themes/' .$cfg['style'] . '/';
$cfg['path']['images']	= $cfg['site']['themes'] . 'images/';
$cfg['path']['css']		= $cfg['site']['themes'] . 'css/';
$cfg['path']['js']		=  $cfg['site']['themes'] . 'js/';
$cfg['path']['url_lib']	= $cfg['url'] . 'common/lib/';

$cfg['auth_key'] = 'fangke_cc_on_board';// cookie验证hash值

$cfg['cityName'] = "哈尔滨";   //此处要与anleye.php里配置的名称要一致
$cfg['cityCode'] = 451;       //此处为程序代码可以  也可以随便写3个数字

//在间隔时间内去数据库验证下Cookie ,设置为 0 即为每次都是在数据库中验证用户
$auth_time = 3600;

//发送邮件的方法("mail", "sendmail", "qmail", or "smtp").
$cfg['mail']['mailer'] = 'smtp';  //服务器类型 例：smtp
$cfg['mail']['host'] = '';  //发信服务器地址：例：smtp.qq.com
$cfg['mail']['username'] = '';   //发信用户名
$cfg['mail']['password'] = '';   //发信密码


define('SYSTEM_ADMIN_ID', 1);
define('TABLEPREFIX','fke_');//表前缀

//Ucenter数据库配置文件


?>