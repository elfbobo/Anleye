<?php
/**
 * 计划任务管理
 *
 * @author 阿一 ayi@yanwee.com
 * @package 4.0
 * 2012-09-13
 */

/**
 * 计划时间类
 * @package Apps
 */
class DoTime {

	/**
	 * @var Object $db 数据库查询对象
	 * @access private
	 */
	var $db = NULL;
	
	/**
	 * 主用户表
	 *
	 * @var string
	 */
	var $tName = "fke_dotime";
	
	/**
	 * 构造函数
	 *
	 * @param source $db
	 */
	function __construct($db) {
		$this->db = $db;
	}
	
    /**
	 * 取执行时间
	 */
	function getDotime($field = '*') {
		return $this->db->getValue('select ' . $field . ' from '.$this->tName);
	}
	
	 /**
	 * 小区房源数量统计
	 */
	function boroughHouseNum() {
		global $query;
		$dataList = $this->db->select('select id from fke_borough');
		$houseSell= new HouseSell($query);
		$houseRent= new HouseRent($query);
		foreach ($dataList as $key=> $item){
			$housesell_num = $houseSell->getCount(0,' and status =1 and borough_id='.$item['id']);
			$houserent_num = $houseRent->getCount(0,' and status =1 and borough_id='.$item['id']);
			$this->db->execute('update fke_borough set sell_num = '.$housesell_num.', rent_num = '.$houserent_num .' where id ='.$item['id']);
		}
		$totime = mktime(0,0,0,date('m'),date('d'),date('Y'))+864000;
		return $this->db->execute('update '.$this->tName.' set borough_house_num = '.$totime);
	}
	
		
	/**
	 * 房源刷新统计脚本执行
	 */
	function dorefresh() {
	   global $cfg,$query,$page;
		$today = mktime(0,0,0,date('m'),date('d'),date('Y'));
		$data = $this->getDotime('time');
		if($today != $data) {
			 $this->db->execute('update fke_houserent set refresh = 2');
			 $this->db->execute('update fke_housesell set refresh = 2');
			 $member = new Member($query);
			 $vip1MemberId = $member->getListFileld('id','vip =1');
			 $vip2MemberId = $member->getListFileld('id','vip =2');
             
			 $array1Len = sizeof($vip1MemberId);
		     for($i = 0; $i<$array1Len ;$i++ )
		  		{
				 $this->db->execute('update fke_housesell set refresh = '.$page->vip1refresh.' where broker_id ='.$vip1MemberId[$i]);
				 $this->db->execute('update fke_houserent set refresh = '.$page->vip1refresh.' where broker_id ='.$vip1MemberId[$i]);
				}
			 
			 
			 $array2Len = sizeof($vip2MemberId);
		     for($i = 0; $i<$array2Len ;$i++ )
		  		{
				 $this->db->execute('update fke_housesell set refresh = '.$page->vip2refresh.' where broker_id ='.$vip2MemberId[$i]);
				 $this->db->execute('update fke_houserent set refresh = '.$page->vip2refresh.' where broker_id ='.$vip2MemberId[$i]);
				}
				
			 $this->db->execute('update '.$this->tName.' set time = '.$today);
			}
	}
	
	
	 /**
	 * 小区均价统计  每月统计一次
	 */
	function boroughAvg() {
		global $cfg,$query,$page;
		  $this->db->open("select * from fke_borough where isdel = 0");
		  $threeMonthBefore = mktime(0,0,0,date('m')-3,date('d'),date('Y'));
		  
		  while ($rs =  $this->db->next()) {
			  $percent_change = 0;
			  $avg_price = 0;
			  $sum_array = $this->db->getValue('select sum( t.house_price/t.house_totalarea ) as sum_p,count(*) as sum_c from fke_housesell as t where borough_id='.$rs['id'].' and created >='.$threeMonthBefore.' and house_type in (1,2)');
			  
			  if($sum_array['sum_c']<4){
				  continue;
			  }
			  if($sum_array){
				  $avg_price = intval($sum_array['sum_p']*10000/$sum_array['sum_c']);
				  if($rs['borough_avgprice']){
					  $percent_change = round(($avg_price-$rs['borough_avgprice'])/$rs['borough_avgprice']*100,2);
				  }
				  if($percent_change){
					  $this->db->execute('update fke_borough set borough_avgprice='.$avg_price.',percent_change ='.$percent_change.',borough_evaluate ='.$avg_price.' where id = '.$rs['id']);
				  }else{
					 $this->db->execute('update fke_borough set borough_avgprice='.$avg_price.',borough_evaluate ='.$avg_price.' where id = '.$rs['id']);
				  }
			  }
			  //echo $rs['id']."<br/>";
			  
			  //记录LOG
			  //now
			  $now_date = mktime(0,0,0,date('m',$cfg['time']),date('d',$cfg['time']),date('Y',$cfg['time']));
			  if($this->db->select("select id from fke_borough_avgprice where borough_id =".$rs['id']." and add_time = ".$now_date)){
				  $this->db->execute("update fke_borough_avgprice set borough_avgprice=".$avg_price.",percent_change=".$percent_change." where borough_id =".$rs['id']." and add_time = ".$now_date);
			  }else{
				 $this->db->execute("insert into fke_borough_avgprice (borough_id, borough_avgprice,percent_change,add_time) values ('".$rs['id']."','".$avg_price."','".$percent_change."','".$now_date."')");
			  }  
		  }
		  $totime = mktime(0,0,0,date('m'),date('d'),date('Y'))+$page->borough_avg_time;
		  return $this->db->execute('update '.$this->tName.' set borough_avg = '.$totime);
	}
	
	
	 /**
	 * 小区房源数量统计
	 */
	function memberNum() {
		global $cfg,$query,$page;
		$this->db->open("select * from fke_member where user_type=1 ");

		while ($rs = $this->db->next()) {
			$housesell_num = $this->db->getValue("select count(*) as num from fke_housesell where status =1 and broker_id = ".$rs['id']);
		
			$houserent_num = $this->db->getValue("select count(*) as num from fke_houserent where status =1 and broker_id = ".$rs['id']);
			
			$this->db->execute('update fke_member set sell_num='.$housesell_num.',rent_num='.$houserent_num.' where id='.$rs['id']);
             }
			 
		  $totime = mktime(0,0,0,date('m'),date('d'),date('Y'))+$page->member_num_time;
		  return $this->db->execute('update '.$this->tName.' set member_num = '.$totime);
		
		}
		
     /**
	 * 经纪人活跃度统计
	 */
	function brokerActiveRate() {
		global $cfg,$query,$page;
		$this->db->open("select * from fke_member where user_type=1");
		while ($rs = $this->db->next()) {
			//取得前六天的登陆情况
			$dateNow = mktime(0,0,0,date('m'),date('d'),date('Y'));
			
			//$dateNow = MyDate::transform('timestamp','2009-04-30');
			$dateBefore =  $dateNow -518400;
			$loginlog = $this->db->select("select add_time from fke_member_loginlog where username = '".$rs['username']."' and add_time>=".$dateBefore." order by add_time asc");
			$active_arr = array_fill(0, 7, 0);
			$activeRate = 0;
			foreach($loginlog as $item){
				for($i=0;$i<=6;$i++){
					if($dateBefore+$i*86400 == $item ){
						//print_rr('$dateBefore='.($dateBefore+$i*86400));
						//print_rr('$item='.$item);
						$activeRate += 1000+pow(2, $i);
						$active_arr[$i]=1;
						break;
					}
				}
			}
			//print_rr($active_arr);
			//print_rr($activeRate);
			if($active_arr){
				$active_str = implode('|',$active_arr);
			}else{
				$active_str = '';
			}
			$total_loginday = $this->db->getValue("select count(*) from fke_member_loginlog where username = '".$rs['username']."'");
			$registe_day = (time() - $rs['add_time'])/86400;
			$total_rate = $total_loginday/$registe_day;
			//print_rr("update fke_member set active_str ='".$active_str."' , active_rate='".$activeRate."' , active_total='".$total_rate."' where id = ".$rs['id']);
			$this->db->execute("update fke_member set active_str ='".$active_str."' , active_rate='".$activeRate."' , active_total='".$total_rate."' where id = ".$rs['id']);
		       }
		 $totime = mktime(0,0,0,date('m'),date('d'),date('Y'))+$page->broker_active_Rate_time;
		  return $this->db->execute('update '.$this->tName.' set broker_active_Rate = '.$totime);
			}
	
	    /**
		 * 经纪人积分增加统计
		 */
		function brokerIntegral() {
			global $cfg,$query,$page;
			$this->db->open("select * from fke_member where user_type=1");
	
			$integral = new Integral($query);
			while ($rs = $this->db->next()) {
				//取得昨天的登陆情况
				$dateNow = mktime(0,0,0,date('m'),date('d'),date('Y'));
				$dateNow = $dateNow- 86400;
				//$dateNow = MyDate::transform('timestamp','2009-03-11');
				//昨天登陆了就加5分
				$haslogin = $this->db->getValue("select add_time from fke_member_loginlog where username = '".$rs['username']."' and add_time = ".$dateNow);
				if($haslogin){
					$integral->add($rs['id'],1);
				}
				
				
				/*也不行，满两年后部登陆，就会重复加
				//满3年
				
				$last3year = mktime(0,0,0,date('m'),date('d'),date('Y')-3);
				$last3year_login = $query_mysql->getValue("select count(*) from fke_member_loginlog where username = '".$rs['username']."' and add_time <= ".$last3year);
				if($last3year_login==1){
					$integral->add($rs['id'],4);
					break;
				}
				//满2年
				$last2year = mktime(0,0,0,date('m'),date('d'),date('Y')-2);
				$last2year_login = $query_mysql->getValue("select count(*) from fke_member_loginlog where username = '".$rs['username']."' and add_time <= ".$last2year);
				if($last2year_login==1){
					$integral->add($rs['id'],3);
					break;
				}
				//满1年
				$last1year = mktime(0,0,0,date('m'),date('d'),date('Y')-1);
				$last1year_login = $query_mysql->getValue("select count(*) from fke_member_loginlog where username = '".$rs['username']."' and add_time <= ".$last1year);
				if($last1year_login==1){
					$integral->add($rs['id'],2);
					break;
				}*/
				
				/*	
				不行，当天没登陆就不行
				$last3year = mktime(0,0,0,date('m'),date('d'),date('Y')-3);
				$last2year = mktime(0,0,0,date('m'),date('d'),date('Y')-2);
				$last1year = mktime(0,0,0,date('m'),date('d'),date('Y')-1);
				if($rs['add_time']<$last3year-86400 && $rs['add_time']>$last3year){
					//3年以上
					$integral->add($rs['id'],4);
					break;
				}
				if($rs['add_time']<$last2year-86400 && $rs['add_time']>$last2year){
					//3年以上
					$integral->add($rs['id'],3);
					break;
				}
				if($rs['add_time']<$last1year-86400 && $rs['add_time']>$last1year){
					//3年以上
					$integral->add($rs['id'],2);
					break;
				}
				*/
				
				$last3year = mktime(0,0,0,date('m'),date('d'),date('Y')-3);
				$last2year = mktime(0,0,0,date('m'),date('d'),date('Y')-2);
				$last1year = mktime(0,0,0,date('m'),date('d'),date('Y')-1);
				
				if($rs['add_time']<$last3year-86400){
					//3年以上
					if(!$integral->getLogByRuleId($rs['id'],4)){
						$integral->add($rs['id'],4);
					}
				}elseif($rs['add_time']<$last2year-86400){
					//2年以上
					if(!$integral->getLogByRuleId($rs['id'],3)){
						$integral->add($rs['id'],3);
					}
				}elseif($rs['add_time']<$last1year-86400){
					//1年以上
					if(!$integral->getLogByRuleId($rs['id'],2)){
						$integral->add($rs['id'],2);
					}
				}
			}
		  $totime = mktime(0,0,0,date('m'),date('d'),date('Y'))+$page->broker_integral_time;
		  return $this->db->execute('update '.$this->tName.' set broker_integral = '.$totime);
		}
		
		 /**
		 * 全站数据统计
		 */
		function statistics() {
			global $cfg,$query,$page;
			
			$borough = new Borough($query);
			$borough_num = $borough->getCount(0,' and isdel = 0 ');
			$this->db->execute("update fke_statistics set stat_value = ".$borough_num." where stat_index ='boroughNum'");
			
			$member = new Member($query);
			
			$broker_num = $member->getCount(' status =0 and user_type = 1 ');
			$this->db->execute("update fke_statistics set stat_value = ".$broker_num." where stat_index ='brokerNum'");
			
			
			$sumPrice = $this->db->getValue("select sum(house_price/house_totalarea) from fke_housesell where status =1 or status =2 ");
			/*
			$query->open("select house_price,house_totalarea sumavg from fke_housesell where where status =1 or status =2 order by house_price/house_totalarea desc limit 0,100");
			while ($rs = $query->next()) {
				$sum1 += $rs['house_price']/$rs['house_totalarea'];
			}
			$query->open("select house_price,house_totalarea sumavg from fke_housesell where where status =1 or status =2 order by house_price/house_totalarea asc limit 0,100");
			while ($rs = $query->next()) {
				$sum2 += $rs['sumavg'];
			}
			$allprice = $allprice-$sum1-$sum2;
			$avgPrice =$allprice/($housesell_num-200);
			*/
			
			
			$houseSell= new HouseSell($query);
             $housesell_num = $houseSell->getCount(0,' and (status =1 or status =2 ) ');
			 if($housesell_num){
				 $avgPrice = $sumPrice*10000/$housesell_num;
			     $this->db->execute("update fke_statistics set stat_value = ".$avgPrice." where stat_index ='avgprice'");
				 }
			 $totime = mktime(0,0,0,date('m'),date('d'),date('Y'))+$page->statistics;
		     return $this->db->execute('update '.$this->tName.' set statistics = '.$totime);
			
			
			}
			
			
		 /**
		 * 小区图片数量导入
		 */
		function boroughPicNum() {
			global $cfg,$query,$page;
			$this->db->open("select * from fke_borough");
			while ($rs = $this->db->next()) {
				$borough_pic_num = $this->db->getValue("select count(*) as num from fke_borough_pic where borough_id = ".$rs['id']);
			
				$borough_draw_num = $this->db->getValue("select count(*) as num from fke_borough_draw where borough_id = ".$rs['id']);
				
				$this->db->execute('update fke_borough set layout_picture='.$borough_pic_num.',layout_drawing='.$borough_draw_num.' where id='.$rs['id']);
			}
			 $totime = mktime(0,0,0,date('m'),date('d'),date('Y'))+$page->borough_pic_num_time;
		     return $this->db->execute('update '.$this->tName.' set borough_pic_num = '.$totime);
			
			
			}
			
		 /**
		 * 房源过期处理
		 */
		function houseInvalid() {
			global $cfg,$query,$page;
			$housesell = new HouseSell($query);
			$houserent = new HouseRent($query);
			$sellId = $this->db->select('select id from fke_housesell where updated<'.($cfg['time']-604800).' and status = 1 and broker_id<>0');
			$rentId = $this->db->select('select id from fke_houserent where updated<'.($cfg['time']-604800).' and status = 1 and broker_id<>0');
			if($sellId){//如果有超过7天未刷新房源 进行下架处理
			    $housesell->update($sellId, 'house_downtime', $cfg['time']);
				$housesell->changeStatus($sellId,'2');
				}
			if($rentId){//如果有超过7天未刷新房源 进行下架处理
			    $houserent->update($rentId, 'house_downtime', $cfg['time']);
				$houserent->changeStatus($rentId,'2');
				}
			
			//删除发布90天以上的房源包含个人发布
			$deleteSellId = $this->db->select('select id from fke_housesell where created<'.($cfg['time']-7776000));
			echo $deleteSellId;
			$deleteRentId = $this->db->select('select id from fke_houserent where created<'.($cfg['time']-7776000));
			if($deleteSellId){//如果有90天以上的房源 进行删除处理
				$housesell->delete($deleteSellId);
				}
			if($deleteRentId){//如果有90天以上的房源 进行删除处理
				$houserent->delete($deleteRentId);
				}
			
			 $totime = mktime(0,0,0,date('m'),date('d'),date('Y'))+$page->house_invalid_time;
		     return $this->db->execute('update '.$this->tName.' set houseInvalid = '.$totime);
			
			
			}
	
}
?>