<?php
/**
 * 网站基本配置管理
 *
 * @author 阿一 yandy@yanwee.com
 * @package 1.6
 * @version $Id$
 */

/**
 * 网站基本配置管理类
 * @package Apps
 */

class Config {

	/**
	 * @var Object $db 数据库查询对象
	 * @access private
	 */
	var $db = NULL;
	
	/**
	 * 主用户表
	 *
	 * @var string
	 */
	var $tName = "fke_web_config";
	
	/**
	 * 构造函数
	 *
	 * @param source $db
	 */
	function __construct($db) {
		$this->db = $db;
	}
	
	/**
	 * 取得详细信息
	 * @access public
	 * @param int $id
	 * @return array 
	 */
	function getInfo($id,$field = '*'){
		return $this->db->getValue('select '.$field.' from '.$this->tName.' where id =' .$id);
	}
	
	/**
	 * 保存网站信息
	 * @access public
	 * @param int $id
	 * @return array 
	 */
	function save($fileddata){
		$this->db->update($this->tName,$fileddata['webConfig']);
		$this->cache();
	}
	
	/**
	 * 缓存网站基本信息
	 * @access public
	 * @return void
	 */
	public function cache() {
		$array = $this->db->getValue('select * from fke_web_config where id = 1', 'hashmap');
		$fp = fopen($GLOBALS['cfg']['path']['conf'] . 'dd/config.php', 'w');
		fputs($fp, '<?php return '.var_export($array, true) . '; ?>');
		fclose($fp);
	}
}
?>