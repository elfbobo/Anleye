<?php
/**
 * 中介公司类  阿一 ayi@yanwee.com
 * @package Apps
 * 2012-12-24
 */
class Company {

	/**
	 * @var Object $db 数据库查询对象
	 * @access private
	 */
	var $db = NULL;

	/**
	 * 中介公司表
	 *
	 * @var string
	 */
	var $tName = 'fke_company';
	
	
	function Company($db) {
		$this->db = $db;
	}
	
	/**
	 * 用户登录
	 * @param string $username 用户名
	 * @param string $passwd 密码
	 * @access public
	 * @return mixed
	 */
	function login($username, $passwd,$wap=0) {
		global $cfg;
		$username = strtolower($username);
		if (isset($_POST['valid'])) {
			if (md5(strtolower($_POST['valid']))!=$_COOKIE['validString']) {
				$result = '验证码错误！';
				return $result;
			}
		}
		if ($username && $passwd) {
			$info = $this->db->getValue('select * from '.$this->tName.' where username=\''. $username . '\'');
			if (!$info) {
				$result = '用户不存在！';
			} elseif (strtolower($info['passwd'])!=md5($passwd)) {
				$result = '密码错误！';
			} else {
				if (isset($_POST['notForget']) && intval($_POST['notForget'])==1) {
					setcookie('AUTH_COMPANY_FORGET',$username,time()+60*60*24*365,'/',$cfg['domain']);
				}
				$auth_code = authcode($info['id']. "\t" . md5($passwd). "\t" .$info['type'], 'ENCODE', $cfg['auth_key']);
				setcookie('AUTH_COMPANY_STRING',$auth_code,0,'/',$cfg['domain']);
				$_COOKIE['AUTH_COMPANY_STRING'] = $auth_code;
				setcookie('AUTH_COMPANY_NAME',$username,0,'/',$cfg['domain']);
				$_COOKIE['AUTH_COMPANY_NAME'] = $username;

				$result = true;
			}
		} else {
			if (trim($username)==''){
				$result = '用户名必须填写！';
			}else{
				$result = '用户密码必须填写！';
			}
			
		}
		return $result;
	}
	
	/**
	 * 检查密码
	 * @access public
	 * 
	 * @param string $pwd
	 * @param int $memberId
	 * @return bool
	 **/
	function checkPwd($pwd,$companyId){
		if (!empty($pwd)){
			return $this->db->getValue('select * from '.$this->tName.' where passwd=\''.md5($pwd).'\' and id='.$companyId);
		} else {
			return false;
		}
	}
	
	
	/**
	 * 修改密码
	 * @param int $id
	 * @param array $fieldArray
	 */
	function updateInfo($id , $fieldArray){
			$this->db->update($this->tName,$fieldArray,' id ='.$id);
	}
	
	
	
	
	
	/**
	 * 检查认证信息
	 * @access public
	 * @return NULL
	 */
	function auth() {
		global $cfg,$page;
		$authSuc = false;
		$back_to = $_SERVER['REQUEST_URI'];
		if ($_COOKIE['AUTH_COMPANY_STRING']) {
			if (!$_COOKIE['AUTH_CHECKTIME']) {
				// 间隔AUTH_CHECKTIME时间检查一次cookie信息是否和数据库一至
				$authInfo = $this->getAuthInfo();
				$user = $this->db->getValue('select * from '.$this->tName.' where id=\''.$authInfo['id'].'\' and  passwd=\''.$authInfo['passwd'].'\'');
				
				if ($user['id']) {
					$authSuc = true;
					setcookie('AUTH_CHECKTIME',1, time()+$GLOBALS['auth_time']);
					//重新设置cookie使用户的Cookie在2个小时内不再失效 ， 如果是cookie设置关闭即失效就没有必要
					/*
					setcookie('AUTH_MEMBER_STRING',authcode($user_id. "\t" . md5($postInfo['passwd']. "\t" .$user_type), 'ENCODE', $cfg['auth_key']),$cfg['time']+7200,'/',$cfg['domain']);
					setcookie('AUTH_MEMBER_NAME',$postInfo['username'],$cfg['time']+7200,'/',$cfg['domain']);
					$_COOKIE['AUTH_MEMBER_NAME'] = $postInfo['username'];
					*/
				}
			} else {
				$authSuc = true;
			}
			$user_type = $this->getAuthInfo('type');
			if($user_type == 1 ){
				if(strpos($back_to,'owner') != ""){
					$back_to = str_replace('owner','member',$back_to);
					header("location:".$back_to);
				}
			}else{
				if(strpos($back_to,'member') != 0){
					$back_to = str_replace('member','owner',$back_to);
				
					header("location:".$back_to);
				}	
			}
		}
		if ($authSuc===false) {
			$page->urlto($cfg['url'].'company/login/login.php?back_to='.$back_to);
		}
	}
	
	/**
	 * 公司登出
	 * @access public
	 * @return mixed
	 */
	function logout($wap=0) {
		global $cfg;
		setcookie('AUTH_COMPANY_STRING', 0, time()-1,'/',$cfg['domain']);
		setcookie('AUTH_COMPANY_NAME',0,time()-1,'/',$cfg['domain']);
	}
	
	
	
	/**
	 * 取当前用户信息
	 * @access public
	 * @return array
	 */ 
	function getAuthInfo($field=NULL) {
		global $cfg;
		$authInfo = authcode($_COOKIE['AUTH_COMPANY_STRING'], 'DECODE', $cfg['auth_key']);
		$authInfo = explode("\t",$authInfo);
		$result['id'] = $authInfo[0];
		$result['passwd'] = $authInfo[1];
		$result['type'] = $authInfo[2];
		if ($_COOKIE['AUTH_COMPANY_NAME']) {
			$result['username'] = $_COOKIE['AUTH_COMPANY_NAME'];
		}
		
		if ($field) {
			if ($result[$field]) {
				return $result[$field];
			} else {
				$info = $this->getInfo($result['id'],'*');
				return $info[$field];
			}
		}
		return $result;
	}
	
	
	
	/**
	 * 存储中介公司
	 * @param array $fileddata
	 * @access public
	 * @return bool
	 */
	 function save($fileddata){
	 if($fileddata['id']){
		 //编辑
		  $this->db->update($this->tName,$fileddata['company'],'id = '.$fileddata['id']);
		 } else{
			 //增加
			   $this->db->insert($this->tName,$fileddata['company']);
			 }
	   return true;
	 }
	 /**
	 * 取得中介公司信息列表
	 * @access public
	 * 
	 * @param array $pageLimit
	 * @return array
	 **/
	function getList($pageLimit,$fileld='*' ,$where_clouse = '',$order='') {
		$where =' where 1 = 1' ;
		if($where_clouse){
			$where .= $where_clouse;
		}
		$this->db->open('select '.$fileld.' from '.$this->tName.$where.' '.$order , $pageLimit['rowFrom'],$pageLimit['rowTo']);
		$result = array();
		while ($rs = $this->db->next()) {
			$result[] = $rs;
		}
		return $result;
	}
	/**
	 * 取中介公司总数
	 * @access public
	 * @return NULL
	 */
	function getCount($where_clouse = '') {
		$where =" where 1 = 1";
		
		if($where_clouse){
			$where.= $where_clouse;
		}
		
		return $this->db->getValue('select count(*) from '.$this->tName. $where );
	}
	
	/**
	 * 取中介公司详细信息
	 * @param string $id 中介公司ID
	 * @param string $field 主表字段
	 * @access public
	 * @return array
	 */
	function getInfo($id, $field = '*') {
		return $this->db->getValue('select ' . $field . ' from '.$this->tName.'  where id=' . $id);
	}
	/**
	 * 删除
	 * @param mixed $ids 选择的ID
	 * @access public
	 * @return bool
	 */
	function delete($ids) {
		if (is_array($ids)) {
			$ids = implode(',',$ids);
			$where = ' where id in (' . $ids . ')';
		} else {
			$where = ' where id=' . intval($ids);
		}
		$this->db->execute('delete from '.$this->tName. $where);
		return true;
		
	}
	
	/**
	 * 操作中介公司状态
	 * @param mixed $ids 中介公司ID
	 * @access public
	 * @return bool
	 */
	function changeStatus($ids,$status) {
		if (is_array($ids)) {
			$ids = implode(',',$ids);
			$where = ' id in (' . $ids . ')';
		} else {
			$where = ' id=' . intval($ids);
		}
		return $this->db->execute('update '.$this->tName.' set status = '.$status.' where ' . $where);
	}
	
}
?>