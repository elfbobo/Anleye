<?php
/**
 * 出售房源委托信息管理
 * @package Apps
 * 2012-08-26
 */ 
 
 class ConsignSale {
	/**
	 * @var Object $db 数据库查询对象
	 * @access private
	 */
	var $db = NULL;
	
	/**
	 * 出售房源基本信息表
	 *
	 * @var string
	 */
	var $tName = 'fke_consign_sale';
	
	
	function ConsignSale($db) {
		$this->db = $db;
	}
 
 
    /**
	 * 保存房源委托信息
	 */
	function save($fileddata) {
		$field_array  = array(
			'borough_name'=>$fileddata['borough_name'],
			'house_floor'=>$fileddata['house_floor'],
			'house_topfloor'=>$fileddata['house_topfloor'],
			'house_room'=>$fileddata['house_room'],
			'house_hall'=>$fileddata['house_hall'],
			'house_price'=>$fileddata['house_price'],
			'house_desc'=>$fileddata['house_desc'],
			'owner_name'=>$fileddata['owner_name'],
			'owner_phone'=>$fileddata['owner_phone'],
			'house_area'=>$fileddata['house_area'],
			'house_desc'=>$fileddata['house_desc'],
			'time'=>time(),
			
		);
		return $this->db->insert($this->tName,$field_array);
		}
		
		
	/**
	 * 取委托房源数
	 * @access public
	 * @return NULL
	 */
	function getCount($flag = 0,$where_clouse = '') {
		$where =" where 1 = 1";
		if($flag == 1){
			$where .= " and status = 0 ";
		}
		if($flag == 2){
			$where .= " and status = 1 ";
		}
		if($flag == 3){
			$where .= " and status = 2 ";
		}
		
		if($where_clouse){
			$where.= $where_clouse;
		}
		
		return $this->db->getValue('select count(*) from '.$this->tName. $where );
	}


 
    /**
	 * 取委托房源列表
	 * @param array limit 
	 * @param Enum $flag 0：全部 ， 1：已审核 ，2：未审核 3:未审核和已通过的 4：未通过的
	 * @access public
	 * @return array
	 */
	function getList($pageLimit,$fileld='*' , $flag = 0,$where_clouse = '',$order='') {
		$where =' where 1 = 1' ;
		if($where_clouse){
			$where .= $where_clouse;
		}
		if($flag == 1){
			$where .= " and status = 0 ";
		}
		if($flag == 2){
			$where .= " and status = 1 ";
		}
		if($flag == 3){
			$where .= " and status = 2 ";
		}
		$this->db->open('select '.$fileld.' from '.$this->tName.$where.' '.$order , $pageLimit['rowFrom'],$pageLimit['rowTo']);
		$result = array();
		while ($rs = $this->db->next()) {
			$result[] = $rs;
		}
		return $result;
	}
    
	
	/**
	 * 修改委托房源状态
	 * @param mixed $ids 选择的ID
	 * @access public
	 * @return bool
	 */
	function check($ids,$flag) {
		$where = ' where 1=1 ';
		if (is_array($ids)) {
			$ids = implode(',',$ids);
			$where .= ' and id in (' . $ids . ')';
		} else {
			$where .= ' and id=' . intval($ids);
		}
		
		return $this->db->execute('update '.$this->tName.' set status = '.$flag.$where);
	}
	
	
	/**
	 * 删除委托房源记录
	 * @param mixed $ids 选择的ID
	 * @access public
	 * @return bool
	 */
	function delete($ids) {
		if (is_array($ids)) {
			$ids = implode(',',$ids);
			$where = ' where id in (' . $ids . ')';
		} else {
			$where = ' where id=' . intval($ids);
		}
		$this->db->execute('delete from '.$this->tName. $where);
		return true;
		
	}
	
	
 
 }
 ?>